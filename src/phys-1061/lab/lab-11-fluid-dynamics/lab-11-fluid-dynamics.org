#+TITLE: Lab 11 Fluid Dynamics
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Introduction

The goal of this lab is to gain a better understanding of fluid dynamics through the application of Bernoulli's principle and the continuity equation.

* Procedure

** Apparatus

Capstone software

** Simplify Bernoulli's equation

We are told Bernoulli's Equation is \[P_1 + \frac{1}{2} \rho {v_1}^2 + \rho g h_1 = P_2 + \frac{1}{2} \rho {v_2}^2 + \rho g h_2\]
and we are given the following values for this case
\begin{align*}
v_1 &= 0 \\
h_2 &= 0 \\
P_1 &= P_2 = P_{\text{atm}}
\end{align*}

Substituting the given values, we can simplify as such:
\begin{align*}
(P_{\text{atm}}) + \frac{1}{2} \rho (0)^2 + \rho g h_1 &= (P_{\text{atm}}) + \frac{1}{2} \rho {v_2}^2 + \rho g (0) \\
g h_1 &= \frac{1}{2} {v_2}^2 \\
h_1 &= \frac{1}{2g} {v_2}^2 \\
{v_2}^2 &= 2g h_1
\end{align*}

** Solve for \(t\)

\begin{align*}
\Delta y &= v_{0_y} t - \frac{1}{2} gt^2 \\
t &= \sqrt{\frac{2 \Delta y}{g}}
\end{align*}

** Capstone

1. Import the provided video into Capstone and calibrate the distance using the calibration tool and the height of the cup (10 cm) as a reference.

2. Set the origin such that the hole is at \(x = 0\) and the bottom of the cup is at \(y = 0\).

3. In the video display, find the gears icon to bring up the video properties.
   Under the Overlay section, set the Frame Increment value to 50.

4. Create two tracked objects:

   a. The top of the water.

   b. The middle of the flow stream at zero height.

5. Track the height of the water by successively clicking on it within the video.
   Repeat this for the middle of the flow stream at zero height.

6. For each tracked object, display its corresponding data table.
   Add two digits of precision to each column.

7. Use the Ruler to measure the vertical height of the hole from the bottom of the cup.

** Excel

8. Use the free fall time and the \(x\) data to calculate \(v_2\) and its square \({v_2}^2\) into separate columns.

9. Plot \({v_2}^2\) vs. \(h\) and make a linear fit of the data.

* Precautions

- Make sure that the equations are set up properly.

- Make sure the data is reasonable.

* Error

The main source of error in this lab is from inputting the data points into Capstone.
For this, I primarily relied on my dexterity in carefully setting the data points.
Then this process inherently produces some amount of error.

* Data

** Table

| Time (s) | Time (water) (s) | Height (m) | Range (m) | \(v_2\) (m/s) | \({v_2}^2\) (m/s)^2 |
|----------+------------------+------------+-----------+---------------+---------------------|
|        0 |      0.063887656 |    -0.0612 |    0.0861 |  -0.957931522 |           0.9176328 |
|    1.666 |      0.063887656 |    -0.0604 |    0.0828 |  -0.945409541 |           0.8937992 |
|    3.333 |      0.063887656 |     -0.058 |    0.0787 |  -0.907843599 |             0.82418 |
|        5 |      0.063887656 |    -0.0544 |     0.074 |  -0.851494686 |           0.7250432 |
|    6.666 |      0.063887656 |    -0.0536 |    0.0694 |  -0.838972705 |           0.7038752 |
|    8.333 |      0.063887656 |    -0.0504 |    0.0652 |  -0.788884782 |           0.6223392 |
|       10 |      0.063887656 |    -0.0496 |     0.062 |  -0.776362802 |           0.6027392 |
|   11.666 |      0.063887656 |     -0.046 |    0.0583 |  -0.720013889 |             0.51842 |
|   13.333 |      0.063887656 |    -0.0424 |    0.0537 |  -0.663664976 |           0.4404512 |
|       15 |      0.063887656 |    -0.0392 |    0.0495 |  -0.613577053 |           0.3764768 |
|   16.666 |      0.063887656 |    -0.0356 |    0.0467 |   -0.55722814 |           0.3105032 |
|   18.333 |      0.063887656 |    -0.0324 |    0.0426 |  -0.507140217 |           0.2571912 |
|       20 |      0.063887656 |     -0.028 |    0.0389 |  -0.438269324 |             0.19208 |

** Height of the hole

I measured the vertical height of the hole to be approximately 0.02 m.

** Free fall time

From our procedure, we set up a kinematic equation and solved for \(t\).
Using the height of the hole and the acceleration due to gravity, we can substitute and evaluate to determine the free fall time of the flow stream \(t\).

\begin{align*}
t = \sqrt{\frac{2 (0.02 \text{ m})}{9.8 \text{ m/s}^2}} \approx 0.06389 \text{ s}
\end{align*}

** Excel plot

[[./lab-11-excel.jpg]]

As shown from the equation of the trendline, our experimental slope is 15.546.

** Calculated error

\begin{align*}
\left|\frac{15.546 - 19.6}{19.6}\right| = 20\% \text{ Error}
\end{align*}

* Questions

** Question 1

According to [[Simplify Bernoulli's equation][my equation]] formed in the procedure section, the height of water \(h_1\) is linearly proportional to the square of the speed of the flow \(v_2\) at Point 2.

** Question 2

The theoretical value of the slope of the graph is \(2g \approx 19.6\) from the equation derived from Bernoulli's principle.

** Question 3

The slope should be unaffected regardless of the value of \(h\) due to a linear relationship between \({v_2}^2\) and \(h\) as shown in the simplified Bernoulli's equation \({v_2}^2 = 2gh_1\).

** Question 4

According to the continuity equation, the speed of the flow at the hole at the bottom should be faster than the speed of the flow at the hole on the top of the cup since the cross-sectional area of the hole is inversely proportional to the speed of the flow through the hole.
The smaller area in this case is at the bottom hole, so the bottom hole should have a faster flow speed.

* Conclusion

In this lab, I simplified Bernoulli's equation to analyze the speed of the flow stream against the height of the water.
Then, I used the continuity equation to make additional observations of the properties of the water at different points.

The data did not match my expectations as well as I wanted.
While there may have been inherent error in the data collection process, I may have also made a calculation error somewhere considering the error is fairly large.
Specifically, I might have made a mistake in calculating the free fall time of the flow stream or the speed of the flow stream.

The results of the lab should be easy to reproduce with some minor deviations so long as the procedures are followed accordingly and the equations are set up properly.
