(TeX-add-style-hook
 "lab-12-simple-harmonic-motion"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("geometry" "margin=1in")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "geometry")
   (LaTeX-add-labels
    "sec:orga81f864"
    "sec:org8a8bb72"
    "sec:org5aeb265"
    "sec:org4d69961"
    "sec:org62a3ffa"
    "sec:org17ecd38"
    "sec:org77e9dcc"
    "sec:org40f758b"
    "sec:orgdae6cb6"
    "sec:org1f576f9"
    "sec:orgdd89d3c"
    "sec:org1de2a50"
    "sec:orgfb575ef"
    "sec:orgdb75415"
    "sec:org739b283"
    "sec:org81a7f48"
    "sec:org985f318"
    "sec:orga19c0af"
    "sec:org4fe6677"
    "sec:org03564fc"
    "sec:org9748ce1"
    "sec:org32fede6"
    "sec:org409991e"
    "sec:orga09d41b"
    "sec:orga2b9db6"
    "sec:org1545d00"
    "sec:org4a92dda"))
 :latex)

