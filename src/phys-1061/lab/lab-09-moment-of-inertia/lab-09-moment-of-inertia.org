#+TITLE: Lab Report 9 Moment of Inertia
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Introduction

The goal of this lab is to understand how the moment of inertia of an object affects our ability to rotate it and how its distribution of mass affects its moment of inertia.
We will practice using the rotation form of Newton's second law \(\tau = I \alpha\) to help us in understanding these concepts.

* Procedure

** Apparatus

Four identical butter knives, rubber bands

** Part I: Sensation of Moment of Inertia

1. For the first two knives, use the tape to attach them together with roughly one inch or so overlapping and pointing towards each other.
   Do the same for the second set of knives, except with the knifes pointing away from each other.

2. Use each hand and hold each set of knives in from the center and slowly and simultaneously rotate them back and forth.

3. Record three observations in the data section of the lab report.
   Use physical concepts such as force, torque, speed, time, rate, etc. to support the observations.

4. Determine the center of mass of an individual knife by balancing it on a single finger towards the heavier end of the knife.

5. Using the measurement of the center of mass of an individual knife, determine the moment of inertia of each set in terms of \(m\) where \(m\) is the mass of the set by taking the sum of the moment of inertia for each knife.
   Analyze how the moment of inertia between each set compare to one another and find a relation between them.
   Include this analysis in the analysis section of the lab report.

** Part II: Newton's 2nd Law for Rotation

1. Open the provided Capstone file.
   Note which quantities are measured in the graph and how they change with respect to each other.

2. For one of the runs, use the highlighter tool to highlight the data points taken during the initial period of almost constant angular acceleration.
   Use the fit tool and make a linear fit to the highlighted data and record the slope in the provided Excel sheet.
   Repeat this step for the other run.

3. Using the provided values in the Excel sheet, calculate the theoretical and experimental angular acceleration of each run.
   For the theoretical angular acceleration, use \(I_{\text{point}} = mr^2\) to find the moment of inertia of the bearings and \(I_{\text{disk}} = \frac{1}{2} mr^2\) to find the moment of inertia of the disk.
   Then, find the total moment of inertia by taking the sum of each moment of inertia.
   Use Equation 1 with the total moment of inertia and applied torque to determine the theoretical angular acceleration.

4. For each run, calculate the percent difference between the theoretical angular acceleration as calculated in the previous step and the experimental angular acceleration from the fit of the data.

* Precautions

- Make sure that all knives are the same.

- Make sure to rotate each set of knives simultaneously to compare them easily.

- Make sure to highlight only the data during (approximately) constant angular acceleration.

* Error

The main source of error in this lab would be the approximation of constant angular acceleration of the disk in Part II.
Since the angular acceleration of the disk is realistically not perfectly constant, our approximation using a linear fit inherently produces some amount of error.

* Data

** Part I: Observations

Let Set 1 refer to the set of knives pointing toward each other and Set 2 refer to the set of knives pointing away from each other.

1. Set 1 appears to rotate at a much slower speed than Set 2 during the same time interval, i.e. Set 1 requires larger torque to match the angular speed of Set 2.

2. Set 1 is more "stable" than Set 2 in the sense that Set 2 rotates large angles for small rotations in the wrist whereas Set 1 tends to rotate in a much narrower range for similar rotations.

3. Set 1 tends go back to its original rotation faster than Set 2.
   In other words, it does not "wobble" as much as Set 2.

** Part I: Analysis

The knife is 21 cm long and its center of mass is 8.5 cm from the handle and thus 12.5 cm from the tip.
Then, we may find \(I_{\text{Set 1}} = 2 \cdot m \cdot (12.5 \text{ cm})^2 = 312.5 m \text{ kg} \cdot \text{cm}^2\) and \(I_{\text{Set 2}} = 2 \cdot m \cdot (8.5 \text{ cm})^2 = 144.5m \text{ kg} \cdot \text{cm}^2\).
Thus, we get \[I_{\text{Set 1}} = \frac{312.5 m}{144.5 m} \times I_{\text{Set 2}} \approx 2.16 \times I_{\text{Set 2}}.\]
This makes sense due to the fact that the handles of my knives are not very thick and weigh only slightly more than the blade.

** Part II: Graph

[[./lab-09-part-02-graph.png]]

** Part II: Calculation of angular acceleration

\begin{align*}
I_{\text{bb}} &= m_{\text{bb}} {r_{\text{bb}}}^2 \\
I_{\text{disk}} &= \frac{1}{2} m_{\text{disk}} {r_{\text{disk}}}^2 \\
\tau &= rF = rm_{\text{hanging}} g \\
\alpha_{\text{theoretical}} &= \frac{\tau}{4I_{\text{bb}} + I_{\text{disk}}} = \frac{rm_{\text{hanging}} g}{4 m_{\text{bb}} {r_{\text{bb}}}^2 + \frac{1}{2} m_{\text{disk}} {r_{\text{disk}}}^2}
\end{align*}

** Part II (Table): Disk without bearings

| Radius (m) | Mass (kg) |
|------------+-----------|
|      0.055 |     0.034 |

** Part II (Table): Ball bearings

| Outer position radius (m) | Inner position radius (m) | Mass of one bearing (kg) |
|---------------------------+---------------------------+--------------------------|
|                      0.04 |                     0.015 |                    0.028 |

** Part II (Table): Applied Torque

| Radius (m) | Mass (kg) | Torque (N m) |
|------------+-----------+--------------|
|     0.0135 |     0.005 |       0.0007 |

** Part II (Table): Moment of inertia

|       | \(I_{\text{bb}}\) (kg m^2) | \(I_{\text{disk}}\) (kg m^2) | \(I_{\text{total}\) (kg m^2) |
|-------+----------------------------+------------------------------+------------------------------|
| Inner |                  0.0000252 |                  0.000051425 |                  0.000076625 |
| Outer |                  0.0001792 |                  0.000051425 |                  0.000230625 |

** Part II (Table): Angular acceleration

|       | \(\alpha_{\text{theoretical}}\) (rad/s^2) | \(\alpha_{\text{experimental}}\) (rad/s^2) |
|-------+-------------------------------------------+--------------------------------------------|
| Inner |                                      8.63 |                                       6.27 |
| Outer |                                      2.87 |                                       2.80 |

** Part II: Error for inner bearings

\[\text{\% Error}_{\text{inner}} = \frac{8.63 - 6.27}{8.63} \approx 27\%\]

** Part II: Error for outer bearings

\[\text{\% Error}_{\text{outer}} = \frac{2.87 - 2.80}{2.87} \approx 2.4\%\]

* Questions

** Question 1

Set 1 is difficult to move quickly due to its larger moment of inertia i.e. the rotational equivalent of mass or the tendency of an object to resist rotation.
Recall how the moment of inertia of an object is calculated by the sum of the distance of each particle of the object from its axis of rotation with respect to its mass.
Then by intuition, it makes sense that Set 1 would have more moment of inertia due to a larger set of particles further from the axis of rotation than that of Set 2 where there is a larger concentration of particles near the axis of rotation.
Hence, it is reasonable for Set 1 to be difficult to move quickly.

** Question 2

In the rotational form of Newton's 2^{nd} Law, \(F_{\text{Net}} = ma\), the moment of inertia \(I\) takes place of the mass in the equation \(\tau_{\text{Net}} = I \alpha\).
This implies that moment of inertia is the rotational equivalent of mass.

** Question 3

I disagree, adding 100 g extra mass to the center where the hand is placed would indeed have an effect on the ability of the person to rotate the set back and forth.
Recall that the moment of inertia of an object is directly proportional to its mass.
By this simple relation, we can conlude that an increased mass of an object directly increases its moment of inertia, thus affecting the object's resistance to rotation.
In this case, it would increase the moment of inertia, making it more difficult to rotate.

** Question 4

The arrangement with the higher moment of inertia would be when the ball bearings are placed in the outer position.
We know this given that the moment of inertia of an object is proportional to the distance of each particle of the object from the axis of rotation.

** Question 5

The slope of a graph of angular velocity vs. time represents the physical quantity of angular acceleration.
That is, the change in angular velocity of an object with respect to time \(\frac{d}{dt} \omega\) is equivalent to the object's angular acceleration \(\alpha\).

** Question 6

\begin{equation}
\tau = mgr = I \alpha = (I_{\text{disk}} + I_{\text{bb}}) \alpha
\end{equation}

By Equation 1, we can infer that the run that should have a steeper slope among the two should be that with the bearings in the inner position.
First, observe how the moment of inertia is inversely proportional to the angular acceleration, i.e. the slope of a run, if we rearrange the equation to be \(\alpha = \frac{\tau}{I}\).
Then, we find the moment of inertia of the ball bearings to also be inversely proportional to the angular acceleration of the disk.
Further, the positions of the bearings from the axis of rotation are directly proportional to the moment of inertia of the bearings.
We can deduce that the positions of the bearings from the axis of rotation are then inversely proportional to the angular acceleration of the disk.
That is, for shorter distances from the axis of rotation of each bearing, we have larger angular acceleration of the disk.
Hence the run with the bearings in the inner position should have a steeper slope than the run with the bearings in the outer position.

* Conclusion

In this lab, I used hands-on demonstrations as well as lab produced data to gain a better understanding of the moment of inertia of objects, how they are calculated, and how they affect the object's ability to rotate.
I observed that objects with more mass distributed further away from the axis of rotation had more moment of inertia than objects who had more mass distributed closer to the axis of rotation.

The data matched my expectations well, with one exception.
I expected that for objects whose mass is distributed further away from the axis of rotation then it would be harder to rotate those objects and my findings verify this.
However, in Part II of the lab, I ended up with 27% error for the run with the inner bearings.
While the theoretical and experimental values of the angular acceleration for the run with the inner bearings both end up being significantly larger than those for the run with the outer bearings as expected, the experimental value has a surprising amount of error.
I suppose this might either be due to some miscalculation on my part, or I may have highlighted bad data points.

The results of this lab are generally easy to reproduce since the instructions are straightforward.
The main difference between each lab would be the set of knives used and their different masses and lengths.
Otherwise, there are not many different things between each lab.
