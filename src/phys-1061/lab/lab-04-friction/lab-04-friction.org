#+TITLE: Lab Report 4 Friction
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Introduction

The goal of this lab is to understand the difference between static and kinetic friction and to get practice with applying Newton's Laws to solve problems involving friction.

* Procedure

** Apparatus

iOLab unit, iOLab software, accessory eye screw

** Part 1: Kinetic Friction

1. Screw the eye screw into the iOLab's force sensor.

2. Connect the iOLab and check the force to be graphed.

3. Click the Rezero Sensor button.

4. Hang the iOLab by the eye screw and record its weight in kilograms.

5. Reset the force graph.

6. Place the iOLab on a rough surface wheels-up and click Record.

7. Drag the iOLab gently across the surfaces and then stop recording.

8. Highlight the graph between the interval where force was applied and record the average force applied, \(\mu\).

9. Draw a free body diagram of the iOLab including all of the forces applied and include this diagram in the lab report.

10. Calculate the coefficient of kinetic friction using Newton's Laws and include the results in the lab report.

** Part 2: Static Friction

1. Draw a free body diagram of the iOLab on an inclined plane.

   - Orient the coordinate system so that the \(x\)-direction is parallel to the plane and the \(y\)-direction is perpendicular to the plane.

   - Draw the diagram from the side-view.

   - Label all three forces acting on the box.

   - Include this diagram in the lab report.

2. Use the free body diagram to show that the force of gravity \(F_g\) can be broken up into \(x\) and \(y\) components where \(F_{g,x} = mg \sin{\theta}\) and \(F_{g,y} = mg \cos{\theta}\).

3. If we consider the case where the iOLab just starts to slide we get \({F_s}^{\text{max}} = \mu_s F_N\).
   Using the free body diagram and components of the force of gravity, we get
   \begin{align*}
   \sum F_x &= F_g \sin{\theta} - \mu_s F_N \cos{\theta} = 0 \\
   \sum F_y &= -F_g \cos{\theta} + F_N \cos{\theta} = 0
   \end{align*}

4. Show how combining these two equations results in the final expression \[\mu_s = \tan{\theta}\]

5. Use a large cardboard container as the inclined plane.

6. Click the Gear button and follow the instructions to calibrate the accelerometer.

7. Select the accelerometer graph and deselect the Ax data as we only need the Ay and Az data.

8. Starting with the plane horizontal, place the iOLab on top of the plane wheels-side-up and click the Record button.

9. Increase the incline of the plane until the iOLab just begins to slide then immediately set the plane back so that the maximum angle tilted is the angle at which the iOLab started sliding then stop recording and export the data as a =.csv= file.

10. Open the =.csv= file in Excel and add a new column, Ay/Ax, and another column calculating the inverse tangent of the previous column in degrees (i.e. DEGREES(ATAN(D2))).

11. Find the maximum angle from the column at which the iOLab was tilted and record both \(\mu_s\) and the max angle \(\theta\) for the lab report.
    Include the material the plane was made of.

* Precautions

- Rezero the force sensor.

- Calibrate the accelerometer.

- Ensure that surface is both rough and flat enough to provide better results.

* Error

The main source of error in this lab is the calculation of the static coefficient, \(\mu_k\), for Part I.
With the data provided by the iOLab, it seems as though there isn't enough data to determine the frictional force without performing a separate experiment (e.g. inclined plane experiment).
So, the frictional force is assumed to be equal to the pulling force however this is probably invalid as the iOLab is moving and not stationary.

* Data

** Part I: Kinetic Friction

*** Weight of the iOLab

The weight of the iOLab is \approx 0.204 kg.
This is calculated using Newton's Second Law and solving for mass,
\[m &= \frac{F_g}{g} \text{, where } F_g = -2 \text{ N} \text{ and } g = -9.8 \text{ m/s}^2.\]

*** Force graph

#+CAPTION: Graph of force applied on the iOLab as it is dragged across a carpet floor
[[./lab-04-part-01-graph.png]]

*** Free body diagram

#+CAPTION: Free body diagram of the iOLab in Part 1
#+ATTR_LaTeX: scale=0.75
#+LABEL: fig:label
\begin{figure}
\centering
\input{lab-04-part-01-fbd.pdf_tex}
\end{figure}

*** Calculation of the coefficient \(\mu_k\)

\begin{equation}
\mu_k = \frac{F_f}{F_N} = \frac{-1.2 \text{ N}}{(-9.8 \text{ m/s}^2)(0.204 \text{ kg})} = 0.6
\end{equation}

** Part II: Static Friction

*** Free body diagram

#+CAPTION: Free body diagram of the iOLab in Part 2
#+ATTR_LaTeX: scale=0.75
#+LABEL: fig:label
\begin{figure}
\centering
\input{lab-04-part-02-fbd.pdf_tex}
\end{figure}

*** Derivation of \(\mu_s\)

\begin{align*}
\mu_s F_N \cos{\theta} &= F_g \sin{\theta} \\
\mu_s mg \cos{\theta} &= mg \sin{\theta} \\
\mu_s &= \frac{mg \sin{\theta}}{mg \cos{\theta}} = \tan{\theta}
\end{align*}

*** Acceleration graph

[[./lab-04-part-02-graph.png]]

#+INCLUDE: ./lab-04-part-02-graph-table.org :lines "-3"
#+INCLUDE: ./lab-04-part-02-graph-table.org :lines "16445-16465"

* Questions

** Question 1

Moving at constant speed, the magnitude and direction of the friction force is less than and opposite to the force I apply to the iOLab.

** Question 2

Newton's 3^{rd} law describes how forces come in /equal/ and opposite pairs.
Because the only force acting on the iOLab other than the normal force is the force of gravity (i.e. the weight of the iOLab) and since it is stationary, it must be the case that the normal force be equal to the weight of the iOLab as it would be the equal and opposite force of the force of gravity.

** Question 3

Yes, the value for the \(\mu_s \approx 0.38\) I obtained is reasonable as it falls on the higher end of the range of coefficients of friction for plastic on plastic, 0.3--0.4, since in this case it is plastic on cardboard, it makes sense that it would be on the high end.

** Question 4

The angle to get the iOLab to start sliding is larger than the angle to get the iOLab to stop sliding because as the angle increases, the normal force, \(F_N = mg \cos{\theta}\), decreases proportionally with the frictional force \(F_f = \mu_s F_N\).
As the frictional force decreases, it eventually is surpassed by the gravitational force in the \(x\)-direction and as a result begins to slide with the larger incline.

** Question 5

[[./lab-04-question-05.png]]

** Question 6

When trying to stop a car on icy pavement, it is better to apply the brakes more gently and roll to a stop rather than slam on the brakes to a skid since sliding on the road involves kinetic friction whereas rolling on the road involves static friction and as we know, static friction is greater than kinetic friction hence it will slow down faster.

* Conclusion

In this lab, I studied the force of friction and its two types, static and kinetic friction.
Using the iOLab to carry out my experiments, I observed how friction is a force that goes against the direction of the object's motion and its magnitude is dependent on both the weight of the object (technically, the normal force of the object) as well as the type of surface on which the object is moving across.

As I went through the lab, the data generally matched my expectations.
However, for Part I of the lab, I had trouble finding the magnitude of the force of friction on the iOLab, so I assumed that it was equal to the pulling force.

The lab results can be reproduced as long as the surfaces and objects used remain the same.
The main differences that may be found through different lab results would be the velocity at which the iOLab is pulled across the rough surface for Part I.
