#+TITLE: Lab Report 6 Circular Motion
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Introduction

The goal of this lab is to understand how force, period, and radius are related in circular motion using the iOLab to get the relevant data.

*Response:* The net force acting on an object moving in a circle is radially inward to the center of the circle.

* Procedure

** Apparatus

iOLab, string

** Theory

\begin{align}
F_c &= mv^2 / r \\
F_c &= \frac{4mr\pi^2}{T^2} \\
T &= \sqrt{\frac{4mr\pi^2}{F_c}}
\end{align}

** Setup

1. Screw the eye screw into the iOLab and find a string to tie to it.
   Measure and record the length of the string \(L\) including the eye screw.

2. Find an open area with enough room to rotate the iOLab around with the string.

3. Find a circular object that the iOLab will rotate around and measure its radius \(r\).

4. Turn on the iOLab and in the app, show a force graph and zero the force sensor.
   Use the force sensor to weigh the iOLab.

5. Prepare a stopwatch to measure the period \(T\).

** Data collection

1. Click the Record button in the iOLab app and begin to rotate the iOLab in a circle over the circular object.
   When the rotation is steady, start the stopwatch and record the time for 10 periods of rotation.
   Stop recording both the stopwatch and the force sensor.

2. Highlight the region where the stopwatch was recording and record the average tension force \(F_T\).
   Also include a screenshot of the graph in the lab report.

3. Record all values in a table and calculate the average period for one rotation \(T\).
   Use the values \(r\), \(L\), and \(F_t\) from the table to calculate \(F_c\).

4. Use the value of \(F_c\) to calculate the expected period.

5. Compare the expected value of \(T\) to the actual value of \(T\).

6. Repeat this procedure using a much shorter \(L\).

* Precautions

- Make sure iOLab is approximately in uniform circular motion before starting the stopwatch.

- Make sure to rezero the force sensor.

- Make sure the iOLab does not collide with any objects.

* Error

The sources of error found in this lab are mostly on data collection practices and setting up proper equations.
The length, radius, and time are all measured manually, so they inherently are prone to error.
Correctly timing and spinning the iOLab correctly requires carefulness.
Other than that, given the numerous equations needed to be figured out in this lab, there may have been mistakes in some of them since they can be quite tricky to figure out.

* Data

** Data collection

#+CAPTION: Trial 1 (\(0.6 \text{ m}\) string)
[[./lab-06-trial-1.png]]

#+CAPTION: Trial 2 (\(0.1 \text{ m}\) string)
[[./lab-06-trial-2.png]]

| Trial # | Length (m) | Radius (m) | Time (s) | \(F_T\) (N) |
|---------+------------+------------+----------+-------------|
|       1 |        0.6 |       0.12 |       14 |      -2.040 |
|       2 |        0.1 |       0.12 |        6 |      -2.507 |

** Calculated centripetal force \(F_c\)

\[F_c = F_T \sin{\left(\tan^{-1}{\left(\frac{r}{L}\right)}\right)}\]

*** Trial 1

\[F_{c_1} = (2.040 \text{ N}) \sin{\left(\tan^{-1}{\left(\frac{0.12 \text{ m}}{0.6 \text{ m}}\right)}\right)} \approx 0.4 \text{ N}\]

*** Trial 2

\[F_{c_2} = (2.507 \text{ N}) \sin{\left(\tan^{-1}{\left(\frac{0.12 \text{ m}}{0.1 \text{ m}}\right)}\right)} \approx 1.9 \text{ N}\]

** Expected period

\[T = \sqrt{\frac{4mr\pi^2}{F_c}}\]

*** Trial 1

\[T_1 = \sqrt{\frac{4 \left(\frac{-2 \text{ N}}{-9.8 \text{ m/s}^2}\right) (0.12 \text{ m}) \pi^2}{F_{c_1}}} \approx 1.6 \text{ s}\]

The period measured from the stopwatch for trial 1 is \(\frac{14 \text{ s}}{10 \text{ rev}} = 1.4 \text{ s}\).
Comparing the expected value with the actual value, we get \[\frac{1.6 - 1.4}{1.6} \times 100\% \approx 9.94\% \text{ Error.}\]

*** Trial 2

\[T_2 = \sqrt{\frac{4 \left(\frac{-2 \text{ N}}{-9.8 \text{ m/s}^2}\right) (0.12 \text{ m}) \pi^2}{F_{c_2}}} \approx 0.709 \text{ s}\]

The period measured from the stopwatch for trial 2 is \(\frac{6 \text{ s}}{10 \text{ rev}} = 0.6 \text{ s}\).
Comparing the expected value with the actual value, we get \[\frac{0.709 - 0.6}{0.709} \times 100\% \approx 15.3\% \text{ Error.}\]

* Questions

** Question 1

The force that provides the centripetal force causing the iOLab's circular path is the tension force from the string.

** Question 2

The tension in the string increased when I made the string shorter because by doing so I increased the centripetal force; and by the equilibrium condition centripetal force and tension force are positively correlated.

** Question 3

The period is decreased by increasing the centripetal force because there will be more revolutions per unit of time.

** Question 4

I think the iOLab would take path B (straight line) if the string were to break since \(F_c\) would no longer act on it, so there would no longer be any centripetal acceleration to change the direction of the iOLab.

* Conclusion

In this lab, I studied uniform circular motion and how the different forces involved can affect the motion of an object in uniform circular motion.
I also learned how to set up equations and solve for different variables involved in uniform circular motion.
For example, I was able to rearrange the centripetal force equation to solve for the period of any circular motion involving a centripetal force.

The data matches my expectations.
I expected that the shorter the length of the string would be, the faster it would go and hence there would be more force required to maintain circular motion but less time required to complete each revolution.
The lab results confirm this.

The lab results are easy to reproduce as long as the precautions are followed.
The main differences if the lab to be redone would generally be the technique used for spinning the iOLab which would affect the centripetal force and period.
