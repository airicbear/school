#+TITLE: PHYS 1061 Workshop

* Workshop 12-2a

** Description

Block A in the figure below is heavier than block B and is sliding down the incline.
All surfaces have friction.
The rope is massless, and the pulley turns on frictionless bearings, /but the pulley is mass C and is a solid sphere/.
The rope and the pulley are among the interacting objects, but you'll have to decide if they're part of the system.

** Tasks

a) Draw the interaction diagram.

b) Identify the /system/ on your interaction diagram.

c) Draw a free-body diagram for each object in the system.
   Use dashed lines to connect members of an action/reaction pair.

d) Use energy to find the speed of mass A when it has moved 2m downhill.

** Interpret
** Represent
** Develop
** Evaluate
** Assess

* Workshop Example 12.36

A rod with mass M & length L can pivot freely about hinge attached to its end & wall.
It is released when horizontal.

- Find \(\omega_f\) in terms of M & L when rod is vertical

- Find \(v_{\text{t, end}}\) at end of rod in terms of M & L when rod is vertical

- Find \(v_{\text{t, cm}}\) at center of mass of rod in terms of M & L when rod is vertical
