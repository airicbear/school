#+TITLE: Final Project Outline
#+AUTHOR: Eric Nguyen

* Stack Abstract Data Type

| Methods             | Behavior                                                             |
|---------------------+----------------------------------------------------------------------|
| =boolean isEmpty()= | Returns =true= if the stack is empty; otherwise, returns =false=     |
| =E peek()=          | Returns the object at the top of the stack without removing it       |
| =E pop()=           | Returns the object at the top of the stack and removes it            |
| =E push(E obj)=     | Pushes an item onto the top of the stack and returns the item pushed |

#+begin_src java
public interface StackInt<E> {
    E push(E obj);
    E peek();
    E pop();
    boolean isEmpty();
}
#+end_src

* Stack Application

The =java.util.Stack= class is part of the original Java API but is not recommended for new applications.
Instead, the Java designers recommend that we use the =java.util.Deque= interface and =java.util.ArrayDeque= class.

** Case Study: Finding Palindromes

*** Problem

A palindrome is a string that reads the same in either direction: left to right or right to left.
We want a program to determine whether a given string is a palindrome or not.

*** Analysis

There are many ways to solve the problem, but we can demonstrate this using Stacks even though it isn't the most efficient way:

1. Scan the input string from left to right pushing each character onto a stack

2. Pop all of the characters stored in the stack, combining the resulting sequence of characters into a string

3. Compare the resulting string to the original string and check whether or not they match

*** Design

Define a class =PalindromeFinder= with three static methods: =fillStack=, =buildReverse=, and =isPalindrome=.

| Methods                                                         |
|-----------------------------------------------------------------|
| =private static Deque<Character> fillStack(String inputString)= |
| =private String buildReverse(String inputString)=               |
| =public boolean isPalindrome(String inputString)=               |

- =fillStack= pushes all characters from the input string onto a stack

- =buildReverse= uses =fillStack= on the input string, popping the characters and joining them

- =isPalindrome= compares the input string and the reversed string to check whether they are palindromes

*** Implementation

#+begin_src java
import java.util.*;

public class PalindromeFinder {

    private static Deque<Character> fillStack(String inputString) {
        Deque<Character> charStack = new ArrayDeque<>();
        for (int i = 0; i < inputString.length(); i++) {
            charStack.push(inputString.charAt(i));
        }
        return charStack;
    }

    private static String buildReverse(String inputString) {
        Deque<Character> charStack = fillStack(inputString);
        StringBuilder result = new StringBuilder();
        while (!charStack.isEmpty()) {
            result.append(charStack.pop());
        }
        return result.toString();
    }

    public static boolean isPalindrome(String inputString) {
        return inputString.equalsIgnoreCase(buildReverse(inputString));
    }

}
#+end_src

*** Testing

Try several different strings such as

- A single character (always a palindrome)

- Multiple characters in one word

- Multiple words

- Different cases

- Even-length strings

- Odd-length strings

- An empty string (considered a palindrome)


#+begin_src java
public class PalindromeFinderTest {
    public PalindromeFinderTest() {
    }

    @Test
    public void singleCharacterIsAlwaysAPalindrome() {
        assertTrue(PalindromeFinder.isPalindrome("x"));
    }

    @Test
    public void aSingleWordPalindrome() {
        assertTrue(PalindromeFinder.isPalindrome("kayak"));
    }

    @Test
    public void aSinglWordNonPalindrome() {
        assertFalse(PalindromeFinder.isPalindrome("foobar"));
    }

    @Test
    public void multipleWordsSameCase() {
        assertTrue(PalindromeFinder.isPalindrome("I saw I was I"));
    }

    @Test
    public void multipleWordsDifferentCase() {
        assertTrue(PalindromeFinder.isPalindrome("Able was I ere I saw Elba"));
    }

    @Test
    public void anEmptyStringIsAPalindrome() {
        assertTrue(PalindromeFinder.isPalindrome(""));
    }

    @Test
    public void anEvenLengthStringPalindrome() {
        assertTrue(PalindromeFinder.isPalindrome("foooof"));
    }
}
#+end_src

* Implementing a Stack

Now we are familiar with the stack interface, so how do we implement its methods?
Here, we will demonstrate how an =ArrayList= or a singly-linked list of nodes can be used to implement a stack.

** Implementing a Stack with an =ArrayList= Component

Notice how the stack is very similar to an =ArrayList=.
Indeed, the class =Stack= extends class =Vector=, a historical predecessor of =ArrayList=.
Just as they suggest using class =Deque= instead of the class =Stack= in new applications, the Java designers recommend using class =ArrayList= instead of class =Vector=.

Then, we can write an /adapter class/ =ListStack= where =ListStack= simply adapts the methods available from the =List= interface.
That is, the =ListStack= class essentially performs the same operations as the =List= but uses different names for its methods (e.g., =push= instead of =add=)---this is known as /method delegation/.

#+begin_src java
import java.util.*;

public class ListStack<E> implements StackInt<E> {
    private List<E> theData;

    public ListStack() {
        theData = new ArrayList<>();
    }

    @Override
    public E push(E obj) {
        theData.add(obj);
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return theData.get(theData.size() - 1);
    }

    @Override
    public E pop() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return theData.remove(theData.size() - 1);
    }

    @Override
    public boolean isEmpty() {
        return theData.isEmpty();
    }
}
#+end_src

** Implementing a Stack as a Linked Data Structure

Alternatively, we can implement the stack using a singly-linked list of nodes.

#+begin_src java
import java.util.NoSuchElementException;

public class LinkedStack<E> implements StackInt<E> {
    private static class Node<E> {
        private E data;
        private Node<E> next;

        private Node(E dataItem) {
            data = dataItem;
            next = null;
        }

        private Node(E dataItem, Node<E> nodeRef) {
            data = dataItem;
            next = nodeRef;
        }
    }

    private Node<E> topOfStackRef = null;

    @Override
    public E push(E obj) {
        topOfStackRef = new Node<>(obj, topOfStackRef);
        return obj;
    }

    @Override
    public E pop() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            E result = topOfStackRef.data;
            topOfStackRef = topOfStackRef.next;
            return result;
        }
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return topOfStackRef.data;
        }
    }

    @Override
    public boolean isEmpty() {
        return topOfStackRef == null;
    }
}
#+end_src

** Comparison of Stack Implementations

The =ArrayList= implementation is the simplest, however the linked data structure has the advantage of using as much storage as is needed for the stack, but you need to allocate storage for the links.
Either implementation will give O(1) operations since the operations will only be performed on the ends of the lists.

* Additional Stack Applications

** Case Study: Evaluating Postfix Expressions

*** Problem

Write a class that evaluates a postfix expression.
The postfix expression will be a string containing digit characters and operator characters from the set +, -, *, /.
The space character will be used as a delimiter between tokens (integers and operators).

*** Analysis

In a postfix expression, the operands precede the operators.
A stack is the perfect place to save the operands until the operator is scanned.
When the operator is scanned, its operands can be popped off the stack and the result is pushed back onto the stack.

*** Design

We will write class =PostfixEvaluator= to evaluate postfix expressions.

| Method                                                            |
|-------------------------------------------------------------------|
| =public static int eval(String expression)=                       |
| =private static int evalOp(char op, Deque<Integer> operandStack)= |
| =private static boolean isOperator(char ch)=                      |

The algorithm can be written as such:

1. Create an empty stack of integers.

2. =while= there are more tokens

   a. Get the next token.

   b. =if= the first character of the token is a digit.

      a. Push the integer onto the stack.

   c. =else if= the token is an operator

      a. Pop the right operand off the stack.

      b. Pop the left operand off the stack.

      c. Evaluate the operation.

      d. Push the result onto the stack.

3. Pop the stack and return the result.

*** Implementation

#+begin_src java
import java.util.*;

public class PostfixEvaluator {

    public static class SyntaxErrorException extends Exception {
        SyntaxErrorException(Strin message) {
            super(message);
        }
    }

    private static int evalOp(char op, Deque<Integer> operandStack) {
        int rhs = operandStack.pop();
        int lhs = operandStack.pop();
        int result = 0;

        switch (op) {
        case '+':
            result = lhs + rhs;
            break;
        case '-':
            result = lhs - rhs;
            break;
        case '/':
            result = lhs / rhs;
            break;
        case '*':
            result = lhs * rhs;
            break;
        }

        return result;
    }

    private static boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != -1;
    }

    public static int eval(Strin expression) throws SyntaxErrorException {
        Deque<Integer> operandStack = new ArrayDeque<>();

        String[] tokens expression.split("\\s+");
        try {
            for (String nextToken : tokens) {
                char firstChar = nextToken.charAt(0);

                if (Character.isDigit(firstChar)) {
                    int value = Integer.parseInt(nextToken);
                    operandStack.push(value);
                } else if (isOperator(firstChar)) {
                    int result = evalOp(firstChar, operandStack);
                    operandStack.push(result);
                } else {
                    throw new SyntaxErrorException("Invalid character encountered: " + firstChar);
                }
            }

            int answer = operandStack.pop();
            if (operandStack.isEmpty()) {
                return answer;
            } else {
                throw new SyntaxErrorException("Syntax Error: Stack should be empty");
            }
        } catch (NoSuchElementException ex) {
            throw new SyntaxErrorException("Syntax Error: Stack is empty");
        }
    }
}
#+end_src

*** Testing

- Compare evaluation results with expected results of a given expression.

- Try several different syntax errors.

** Case Study: Converting from Infix to Postfix

We normally write expressions in infix notation.
Therefore, one approach to evaluating expressions in infix notation is first to convert it to postfix an then apply the evaluation technique just discussed.

*** Problem

Convert an infix expression to postfix.

*** Analysis

- Operators are evaluated according to their /precedence/ or rank.
  Higher precedence operators are evaluated before lower precedence operators.

- Operators with the same precedence are evaluated in left-to-right order.

*** Design

We will name our class =InfixToPostfix=.

| Data Field                               |
|------------------------------------------|
| =private static final String OPERATORS=  |
| =private static final int[] PRECEDENCE=  |
| =private Deque<Character> operatorStack= |
| =private StringJoiner postfix=           |

| Method                                       |
|----------------------------------------------|
| =public static String convert(String infix)= |
| =public void convertToPostfix(String infix)= |
| =private void processOperator(char op)=      |
| =private String getPostfix()=                |
| =private static int precedence(char op)=     |
| =private static boolean isOperator(char ch)= |

The algorithm:

1. Initialize =postfix= to an empty =StringJoiner=

2. Initialize the operator stack to an empty stack.

3. =while= there are more tokens in the infix string.

   a. Get the next token.

   b. =if= the next token is an operand.

      a. Append it to =postfix=.

   c. =else if= the text token is an operator.

      a. Call =processOperator= to process the operator.

   d. =else=

      a. Indicate a syntax error.

4. Pop remaining operators off the operator stack and append them to =postfix=.

*** Implementation

#+begin_src java
import java.util.*;

public class InfixToPostfix {
    public static class SyntaxErrorException extends Exception {
        SyntaxErrorException(String message) {
            super(message);
        }
    }

    private final Deque<Character> operatorStack = new ArrayDeque<>();
    private static final String OPERATORS = "+-*/";
    private static final int[] PRECEDENCE = {1, 1, 2, 2};
    private final StringJoiner postfix = new StringJoiner<>(" ");

    public static String convert(String infix) {
        InfixToPostfix infixToPostfix = new InfixToPostfix();
        infixToPostfix.convertToPostfix(infix);
        return infixToPostfix.getPostfix();
    }

    private String getPostfix() {
        return postfix.toString();
    }

    private void convertToPostfix(String infix) throws SyntaxErrorException {
        String[] tokens = infix.split("\\s+");
        try {
            for (String nextToken : tokens) {
                char firstChar = nextToken.charAt(0);
                if (Character.isJavaIdentifierStart(firstChar) || Character.isDigit(firstChar)) {
                    postfix.add(nextToken);
                } else if (isOperator(firstChar)) {
                    processOperator(firstChar);
                } else {
                    throw new SyntaxErrorException("Unexpected Character Encountered: " + firstChar);
                }
            }

            while (!operatorStack.isEmpty()) {
                char op = operatorStack.pop();
                postfix.add(new Character(op).toString());
            }
        } catch (NoSuchElementException ex) {
            throw new SyntaxErrorException("Syntax Error: The stack is empty");
        }
    }

    private void processOperator(char op) {
        if (operatorStack.isEmpty()) {
            operatorStack.push(op);
        } else {
            char topOp = operatorStack.peek();
            if (precedence(op) > precedence(topOp)) {
                operatorStack.push(op);
            } else {
                while (!operatorStack.isEmpty() && precedence(op) <= precedence(topOp)) {
                    operatorStack.pop();
                    postfix.add(new Character(topOp).toString());
                    if (!operatorStack.isEmpty()) {
                        topOp = operatorStack.peek();
                    }
                }

                operatorStack.push(op);
            }
        }
    }

    private static boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != -1;
    }

    private static int precedence(char op) {
        return PRECEDENCE[OPERATORS.indexOf(op)];
    }
}
#+end_src

** Case Study: Converting Expressions with Parenthesis

*** Problem

Convert infix expressions with parentheses into postfix expressions.

*** Analysis

We can think of an opening parenthesis on an operator stack as a boundary or fence between operators.
A closing parenthesis is the terminator symbol for a subexpression.

*** Design

We want to push each opening parenthesis onto the stack as soon as it is scanned.
When a closing parenthesis is scanned, we want to pop all operators up to and including the matching opening parenthesis, inserting all operators popped (except for the opening parenthesis) in the postfix string.

*** Implementation

We modify the class written from the previous example and name our class =InfixToPostfixParens=.

#+begin_src java
import java.util.*;

public class InfixToPostfixParens {
    public static class SyntaxErrorException extends Exception {
        SyntaxErrorException(String message) {
            super(message);
        }
    }

    private static final String OPERATORS = "-+*/()";
    private static final int[] PRECEDENCE = {1, 1, 2, 2, -1, -1};
    private static final String PATTERN =
        "\\d+\\.\\d*|\\d+|" + "\\p{L}[\\p{L}\\p{N}]*" + "|[" + OPERATORS + "]";
    private final Deque<Character> operatorStack = new ArrayDeque<>();
    private final StringJoiner postfix = new StringJoiner(" ");

    public static String convert(String infix) throws SyntaxErrorException {
        InfixToPostfixParens infixToPostfixParens = new InfixToPostfixParens();
        infixToPostfixParens.convertToPostfix(infix);
        return infixToPostfixParens.getPostfix();
    }

    private String getPostfix() {
        return postfix.toString();
    }

    private void convertToPostfix(String infix) throws SyntaxErrorException {
        try {
            String nextToken;
            Scanner scan = new Scanner(infix);
            while ((nextToken = scan.findInLine(PATTERN)) != null) {
                char firstChar = nextToken.charAt(0);

                if (Character.isLetter(firstChar) || Character.isDigit(firstChar)) {
                    postfix.add(nextToken);
                } else if (isOperator(firstChar)) {
                    processOperator(firstChar);
                } else {
                    throw new SyntaxErrorException("Unexpected Character Encountered: " + firstChar);
                }
            }

            while (!operatorStack.isEmpty()) {
                char op = operatorStack.pop();
                if (op == '()') throw new SyntaxErrorException("Unmatched opening parenthesis");
                postfix.add(new Character(op).toString());
            }

            return postfix.toString();
        } catch (NoSuchElementException ex) {
            throw new SyntaxErrorException("Syntax Error: The stack is empty");
        }
    }

    private void processOperator(char op) {
        if (operatorStack.isEmpty() || op == '(') {
            operatorStack.push(op);
        } else {
            char topOp = operatorStack.peek();
        }

        if (precedence(op) > precedence(topOp)) {
            operatorStack.push(op);
        } else {
            while (!operatorStack.isEmpty() && precedence(op) <= precedence(topOp)) {
                operatorStack.pop();
                if (topOp == '(') {
                    break;
                }
                postfix.add(new Character(topOp).toString());
                if (!operatorStack.isEmpty()) {
                    topOp = operatorStack.peek();
                }

                if (op != ')') {
                    operatorStack.push(op);
                }
            }
        }
    }

    private static boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != -1;
    }

    private static int precedence(char op) {
        return PRECEDENCE[OPERATORS.indexOf(op)];
    }
}
#+end_src

* Queue Abstract Data Type

* Queue Applications

** Case Study: Maintaining a Queue

* Implementing the Queue Interface

* The Deque Interface
