package circularlinkedlist;

import java.util.Scanner;
import java.util.logging.Logger;

import static circularlinkedlist.SolitaireCodec.decryptedString;
import static circularlinkedlist.SolitaireCodec.encryptedString;
import static circularlinkedlist.SolitaireInput.deckFromFile;
import static circularlinkedlist.SolitaireInput.messageFromFile;

/**
 * Main entry point of the program.
 * <br/>
 * <br/>
 * Usage for the command-line:
 * <pre>
 *      ./circularlinkedlist [deck] [message]
 * </pre>
 *
 * @author Eric Nguyen
 */
public class Solitaire {

    private static final String PROMPT_ACTION = "Enter (0) to encrypt (1) to decrypt: ";
    private static final String PROMPT_DECK = "Enter your deck: ";
    private static final String PROMPT_MESSAGE = "Enter your message: ";
    private static final String WARNING_INVALID_ACTION_INPUT = "You must enter (0) to encrypt or (1) to decrypt: ";
    private static final String WARNING_INVALID_DECK_INPUT = "Deck input must be numeric.";
    private static final String WARNING_DUPLICATE_DECK_NUMBER = "Duplicate deck number skipped.";
    private static final String WARNING_INVALID_DECK_NUMBER = "Invalid deck number skipped. (Valid numbers from 1-28).";
    private static final String TERMINATE_INVALID_INPUT = "Invalid input. Quitting program.";

    private static final int DECK_SIZE = 28;

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        CircularLinkedList<Integer> deck;
        String message;

        if (args.length > 1) { // INPUT: Command-line arguments - filenames
            deck = deckFromFile(args[0]);
            message = messageFromFile(args[1]);
        } else { // INPUT: Command-line input - keyboard
            deck = promptDeck();
            message = promptMessage();
        }

        if (deck.size < 1 || message.length() < 1) {
            System.err.println(TERMINATE_INVALID_INPUT);
            return;
        }

        int response = promptAction();
        System.out.println(convertedMessage(response, message, deck));
        scanner.close();
    }

    /**
     * Ask user for the deck
     *
     * @return the deck
     */
    private static CircularLinkedList<Integer> promptDeck() {
        CircularLinkedList<Integer> deck = new CircularLinkedList<>();

        System.out.print(PROMPT_DECK);
        for (int i = 0; i < DECK_SIZE; i++) {
            int nextInt = 0;
            while (!scanner.hasNextInt()) {
                System.err.println(WARNING_INVALID_DECK_INPUT);
                scanner.next();
            }
            nextInt = scanner.nextInt();
            if (nextInt < 1 || nextInt > DECK_SIZE) {
                System.err.println(WARNING_INVALID_DECK_NUMBER);
            } else if (deck.contains(nextInt)) {
                System.err.println(WARNING_DUPLICATE_DECK_NUMBER);
            } else {
                deck.add(nextInt);
            }
        }
        scanner.nextLine();

        return deck;
    }

    /**
     * Ask user for the message
     *
     * @return the message
     */
    private static String promptMessage() {
        System.out.print(PROMPT_MESSAGE);
        String message = scanner.nextLine();
        while (message.isBlank()) {
            System.out.print(PROMPT_MESSAGE);
            message = scanner.nextLine();
        }
        return message;
    }

    /**
     * Ask user whether they want to encrypt or decrypt their message
     *
     * @return the users response
     */
    private static int promptAction() {
        System.out.print(PROMPT_ACTION);
        while (!scanner.hasNextInt()) {
            System.err.print(WARNING_INVALID_ACTION_INPUT);
            scanner.next();
        }
        int response = scanner.nextInt();
        if (response != 0 && response != 1) {
            return promptAction();
        }
        return response;
    }

    /**
     * Encrypt or decrypt the message based on their user's response
     *
     * @param response the user's response
     * @param message  the message to be encrypted or decrypted
     * @param deck     the deck to be used in the encryption/decryption process
     * @return the encrypted/decrypted message
     */
    private static String convertedMessage(int response, String message, CircularLinkedList<Integer> deck) {
        String output;
        switch (response) {
            case 0:
                output = encryptedString(message, deck.copy());
                break;
            case 1:
                output = decryptedString(message, deck.copy());
                break;
            default:
                output = message;
        }
        return output;
    }

}
