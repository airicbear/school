package circularlinkedlist;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Input for deck and message
 *
 * @author Eric Nguyen
 */
public class SolitaireInput {

    private static final String ERROR_FILE_CANNOT_BE_READ =  "ERROR: Could not read file ";

    /**
     * Create a scanner to scan a file.
     *
     * @param filename filename of the file to be scanned
     * @return the scanner
     */
    private static Scanner fileScanner(String filename) {
        File file = new File(filename);
        if (!file.isFile()) {
            System.err.println(ERROR_FILE_CANNOT_BE_READ + '"' + filename + '"');
            return null;
        }

        try {
            return new Scanner(file);
        } catch (FileNotFoundException e) {
            System.err.println(ERROR_FILE_CANNOT_BE_READ + '"' + filename + '"');
            return null;
        }
    }

    /**
     * Create a deck from a file.
     *
     * @param filename filename of the file to be read
     * @return the deck
     */
    protected static CircularLinkedList<Integer> deckFromFile(String filename) {
        CircularLinkedList<Integer> deck = new CircularLinkedList<>();
        Scanner s = fileScanner(filename);
        if (s == null) {
            return deck;
        }
        while (s.hasNextInt()) {
            deck.add(s.nextInt());
        }
        s.close();
        return deck;
    }

    /**
     * Create a message from a file.
     *
     * @param filename filename of the file to be read
     * @return the message
     */
    protected static String messageFromFile(String filename) {
        StringBuilder messageBuilder = new StringBuilder();
        Scanner s = fileScanner(filename);
        if (s == null) {
            return "";
        }
        while (s.hasNextLine()) {
            messageBuilder.append(s.nextLine());
        }
        s.close();
        return messageBuilder.toString();
    }

}
