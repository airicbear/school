package circularlinkedlist;

import java.util.Iterator;

public class CircularLinkedList<E> implements Iterable<E> {

    Node<E> head;
    Node<E> tail;
    int size; // BE SURE TO KEEP TRACK OF THE SIZE

    // implement this constructor
    public CircularLinkedList() {
        size = 0;
    }

    /**
     * Generate a keystream based on the Solitaire algorithm.
     *
     * @param deck input of the keystream
     * @param size size of the keystream
     * @return a keystream following the Solitaire algorithm
     */
    protected static CircularLinkedList<Integer> generateKeystream(CircularLinkedList<Integer> deck, int size) {
        CircularLinkedList<Integer> keystream = new CircularLinkedList<>();

        int i = 0;
        while (i < size) {
            int indexJokerA = stepOne(deck);
            int indexJokerB = stepTwo(deck);
            stepThree(deck, indexJokerA, indexJokerB);
            stepFour(deck);
            int value = stepFive(deck);
            if (value > -1) {
                keystream.add(value);
                i++;
            }
        }

        return keystream;
    }

    /**
     * Find the "A" Joker (27) and swap it with the previous card in the deck.
     *
     * @param deck the deck to be modified
     * @return index of Joker A
     */
    private static int stepOne(CircularLinkedList<Integer> deck) {
        Node<Integer> prevNode = deck.head;
        int i;
        for (i = 0; i < deck.size; i++) {
            if (prevNode.item != 27) {
                prevNode = prevNode.next;
            } else {
                break;
            }
        }

        Node<Integer> jokerA = prevNode.next;
        int temp = jokerA.item;
        jokerA.item = prevNode.item;
        prevNode.item = temp;

        return i + 1;
    }

    /**
     * Find the "B" Joker (28) and swap with the card after it twice.
     *
     * @param deck the deck to be modified
     * @return index of Joker B
     */
    private static int stepTwo(CircularLinkedList<Integer> deck) {
        Node<Integer> jokerB = deck.head;
        int i = 0;
        while (jokerB.item != 28) {
            jokerB = jokerB.next;
            i++;
        }

        deck.add(i + 3, jokerB.item);
        deck.remove(i);

        return i + 2;
    }

    /**
     * Compare the positions of Joker A with Joker B.
     * Swap the cards before the first Joker with the ones after the second Joker.
     *
     * @param deck        the deck to be modified
     * @param indexJokerA position of Joker A
     * @param indexJokerB position of Joker B
     */
    private static void stepThree(CircularLinkedList<Integer> deck, int indexJokerA, int indexJokerB) {
        Node<Integer> jokerA = deck.getNode(indexJokerA);
        Node<Integer> jokerB = deck.getNode(indexJokerB);

        if (indexJokerA > indexJokerB) {
            Node<Integer> temp = deck.head;
            deck.head = jokerA.next;
            deck.tail.next = jokerB;
            jokerA.next = temp;
            deck.tail = deck.getNode(deck.size - 1);
            deck.tail.next = deck.head;
        } else {
            deck.head = jokerB.next;
        }
    }

    /**
     * Remove the last card.
     * With that card's value, move that many cards from
     * the front to the end.
     * Replace the removed card.
     *
     * @param deck the deck to be modified
     */
    private static void stepFour(CircularLinkedList<Integer> deck) {
        int n = deck.remove(deck.size - 1);
        for (int i = 0; i < n; i++) {
            deck.add(deck.remove(0));
        }
        deck.add(n);
    }

    /**
     * Get the next value of the keystream.
     *
     * @param deck the deck to be modified
     * @return using the head's value, go to that index and return the next index's item value if it isn't a Joker, otherwise return -1
     */
    private static int stepFive(CircularLinkedList<Integer> deck) {
        int value = deck.getNode(deck.head.item).item;
        if (value != 27 && value != 28) {
            return value;
        } else {
            return -1;
        }
    }

    /**
     * Test the generateKeystream() method.
     *
     * @param deck
     * @return the modified deck after finding the first value of the keystream
     */
    private static CircularLinkedList<Integer> test(CircularLinkedList<Integer> deck) {
        int[] list = {1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 3, 6, 9, 12, 15, 18, 21, 24, 27, 2, 5, 8, 11, 14, 17, 20, 23, 26};
        for (int i = 0; i < list.length; i++) {
            deck.add(list[i]);
        }
        System.out.println("Step one test:");
        System.out.println(deck);
        int indexJokerA = stepOne(deck);
        System.out.println(deck);

        System.out.println();
        System.out.println("Step two test:");
        System.out.println(deck);
        int indexJokerB = stepTwo(deck);
        System.out.println(deck);

        System.out.println();
        System.out.println("Step three test:");
        System.out.println(deck);
        stepThree(deck, indexJokerA, indexJokerB);
        System.out.println(deck);

        System.out.println();
        System.out.println("Step four test:");
        System.out.println(deck);
        stepFour(deck);
        System.out.println(deck);

        System.out.println();
        System.out.println("Step five test");
        System.out.println(deck);
        int value = stepFive(deck);
        System.out.println(value);
        System.out.println();

        return deck;
    }

    public static void main(String[] args) {
        CircularLinkedList<Integer> deck = new CircularLinkedList<>();
        CircularLinkedList<Integer> run1 = test(deck);
        CircularLinkedList<Integer> run2 = test(run1);
        CircularLinkedList<Integer> run3 = test(run2);
        CircularLinkedList<Integer> run4 = test(run3);
    }

    public boolean contains(E item) {
        if (this.size < 1) {
            return false;
        }

        Node<E> node = this.head;
        int i = 0;
        if (node.item.equals(item)) {
            return true;
        }
        while (!node.item.equals(item) && i < this.size) {
            node = node.next;
            i++;
            if (node.item.equals(item)) {
                return true;
            }
        }

        return false;
    }

    // I highly recommend using this helper method
    // Return Node<E> found at the specified index
    // be sure to handle out of bounds cases
    private Node<E> getNode(int index) {

        // inefficient way of handling out of bounds cases
        // could use modulo instead but dont know how
        while (index < 0) {
            index += size;
        }
        while (index >= size) {
            index -= size;
        }

        Node<E> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    // attach a node to the end of the list
    public boolean add(E item) {
        this.add(size, item);
        return false;
    }

    // Cases to handle
    // out of bounds
    // adding to empty list
    // adding to front
    // adding to "end"
    // adding anywhere else
    // REMEMBER TO INCREMENT THE SIZE
    public void add(int index, E item) {
        while (index < 0) {
            index += size;
        }
        while (index > size) {
            index -= size;
        }

        if (size == 0) {
            head = new Node<>(item);
            head.next = head;
            tail = head;
        } else {
            Node<E> node = new Node<>(item);
            if (index == size) {
                node.next = head;
                tail.next = node;
                tail = node;
            } else if (index == 0) {
                node.next = head;
                head = node;
                tail.next = head;
            } else {
                node.next = getNode(index);
                getNode(index - 1).next = node;
            }
        }
        size++;
    }

    // remove must handle the following cases
    // out of bounds
    // removing the only thing in the list
    // removing the first thing in the list (need to adjust the last thing in the list to point to the beginning)
    // removing the last thing
    // removing any other node
    // REMEMBER TO DECREMENT THE SIZE
    public E remove(int index) {
        Node<E> node = getNode(index);
        while (index < 0) {
            index += size;
        }
        while (index > size) {
            index -= size;
        }
        if (size == 1) {
            head = null;
            tail = null;
        } else {
            if (index == 0) {
                head = head.next;
                tail.next = head;
            } else if (index == size - 1) {
                tail = getNode(index - 1);
                tail.next = head;
            } else {
                getNode(index - 1).next = getNode(index + 1);
            }
        }
        size--;
        return node.item;
    }

    // Turns your list into a string
    // Useful for debugging
    public String toString() {
        Node<E> current = head;
        StringBuilder result = new StringBuilder();

        if (size == 0) {
            return "";
        }

        if (size == 1) {
            return head.item.toString();
        } else {
            do {
                result.append(current.item);
                result.append(" ==> ");
                current = current.next;
            } while (current != head);
        }

        return result.toString();
    }

    public Iterator<E> iterator() {
        return new ListIterator<E>();
    }

    public CircularLinkedList<E> copy() {
        CircularLinkedList<E> copy = new CircularLinkedList<>();
        Node<E> pointer = this.head;
        for (int i = 0; i < this.size; i++) {
            copy.add(pointer.item);
            pointer = pointer.next;
        }
        return copy;
    }

    // It's easiest if you keep it a singly linked list
    // SO DON'T CHANGE IT UNLESS YOU WANT TO MAKE IT HARDER
    private static class Node<E> {
        E item;
        Node<E> next;

        public Node(E item) {
            this.item = item;
        }
    }

    // provided code for different assignment
    // you should not have to change this
    // change at your own risk!
    // this class is not static because it needs the class it's inside of to survive!
    private class ListIterator<E> implements Iterator<E> {

        Node<E> nextItem;
        Node<E> prev;
        int index;

        @SuppressWarnings("unchecked")
        // Creates a new iterator that starts at the head of the list
        public ListIterator() {
            nextItem = (Node<E>) head;
            index = 0;
        }

        // returns true if there is a next node
        // this is always should return true if the list has something in it
        public boolean hasNext() {
            // TODO Auto-generated method stub
            return size != 0;
        }

        // advances the iterator to the next item
        // handles wrapping around back to the head automatically for you
        public E next() {
            // TODO Auto-generated method stub
            prev = nextItem;
            nextItem = nextItem.next;
            index = (index + 1) % size;
            return prev.item;

        }

        // removed the last node was visted by the .next() call
        // for example if we had just created a iterator
        // the following calls would remove the item at index 1 (the second person in the ring)
        // next() next() remove()
        public void remove() {
            int target;
            if (nextItem == head) {
                target = size - 1;
            } else {
                target = index - 1;
                index--;
            }
            CircularLinkedList.this.remove(target); //calls the above class
        }

    }

}
