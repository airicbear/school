package circularlinkedlist;

import static circularlinkedlist.CircularLinkedList.generateKeystream;

/**
 * Encryption and decryption
 *
 * @author Eric Nguyen
 */
public class SolitaireCodec {

    /**
     * Encrypt a message using a keystream generated with the deck.
     *
     * @param message message to be encrypted
     * @param deck    deck to be used for the keystream generation process
     * @return the encrypted message
     */
    public static String encryptedString(String message, CircularLinkedList<Integer> deck) {
        return intToString(encrypt(message, deck));
    }

    /**
     * Decrypt a message using a keystream generated with the deck.
     *
     * @param message message to be decrypted
     * @param deck    deck to be used for the keystream generation process
     * @return the decrypted message
     */
    public static String decryptedString(String message, CircularLinkedList<Integer> deck) {
        return intToString(decrypt(message, deck));
    }

    /**
     * Sum the message and the generated keystream to produce the encoded encrypted message.
     *
     * @param message message to be encrypted
     * @param deck    deck to be used in the keystream generation process
     * @return the encoded encrypted message
     */
    private static CircularLinkedList<Integer> encrypt(String message, CircularLinkedList<Integer> deck) {
        CircularLinkedList<Integer> encodedMessage = convertMessage(message);
        CircularLinkedList<Integer> keystream = generateKeystream(deck, encodedMessage.size);
        CircularLinkedList<Integer> sum = new CircularLinkedList<>();
        int size = encodedMessage.size;
        for (int i = 0; i < size; i++) {
            int itemSum = encodedMessage.remove(0) + keystream.remove(0);
            if (itemSum > 26) {
                itemSum -= 26;
            }
            sum.add(itemSum);
        }
        return sum;
    }

    /**
     * Subtract the generated keystream from the message to produce the encoded decrypted message.
     *
     * @param message encrypted message to be decrypted
     * @param deck    deck to be used in the keystream generation process
     * @return the encoded decrypted message
     */
    private static CircularLinkedList<Integer> decrypt(String message, CircularLinkedList<Integer> deck) {
        CircularLinkedList<Integer> encodedMessage = convertMessage(message);
        CircularLinkedList<Integer> keystream = generateKeystream(deck, encodedMessage.size);
        CircularLinkedList<Integer> diff = new CircularLinkedList<>();
        int size = encodedMessage.size;
        for (int i = 0; i < size; i++) {
            int itemDiff = encodedMessage.remove(0) - keystream.remove(0);
            if (itemDiff < 1) {
                itemDiff += 26;
            }
            diff.add(itemDiff);
        }
        return diff;
    }

    /**
     * Converts a message to a list of integers, i.e. encodes the message.
     *
     * @param message the message to be encoded
     * @return the encoded message
     */
    private static CircularLinkedList<Integer> convertMessage(String message) {
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        for (int i = 0; i < message.length(); i++) {
            if (Character.isAlphabetic(message.charAt(i))) {
                list.add((int) Character.toUpperCase(message.charAt(i)) - 64);
            }
        }
        if (list.size % 5 != 0) {
            for (int i = 0; i < list.size % 5; i++) {
                list.add(24);
            }
        }
        return list;
    }

    /**
     * Convert a list of integers to a string.
     *
     * @param list the list of integers
     * @return the list as a string
     */
    private static String intToString(CircularLinkedList<Integer> list) {
        StringBuilder result = new StringBuilder();
        int size = list.size;
        for (int i = 0; i < size; i++) {
            result.append((char) (list.remove(0) + 64));
        }
        return result.toString();
    }

}
