package boards;

/**
 * A 2D board of integers
 *
 * @author Eric Nguyen
 */
public class Board {
    protected int[][] grid;
    protected int numRows;
    protected int numCols;
    protected char zeroAlt;
    protected boolean showZeros;

    public Board(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.showZeros = false;
        this.zeroAlt = ' ';
        this.grid = newGrid(numRows, numCols);
    }

    public Board(int[][] grid) {
        this(grid.length, grid[0].length);
        this.grid = grid;
    }

    public int getElement(int row, int col) {
        return this.grid[row][col];
    }

    public void setElement(int row, int col, int value) {
        this.grid[row][col] = value;
    }

    public int[] getColumn(int c) {
        int[] column = new int[this.numRows];
        for (int i = 0; i < this.numRows; i++) {
            column[i] = this.grid[i][c];
        }
        return column;
    }

    public int getNumRows() {
        return this.numRows;
    }

    public int getNumCols() {
        return this.numCols;
    }

    /**
     * Create a new 2D array with each field initially set to zero.
     * @param numRows Number of rows
     * @param numCols Number of columns
     * @return The new board
     */
    private int[][] newGrid(int numRows, int numCols) {
        int[][] newBoard = new int[numRows][numCols];
        for (int r = 0; r < numRows; r++) {
            for (int c = 0; c < numCols; c++) {
                newBoard[r][c] = 0;
            }
        }

        return newBoard;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numCols; c++) {
                builder.append(this.grid[r][c]);
            }
            builder.append('\n');
        }
        return builder.toString();
    }
}