package boards;

/**
 * Board formatted for Sudoku.
 *
 * @author Eric Nguyen
 */
public class SudokuBoard extends Board {
    public static int EMPTY = 0;
    private int size;

    public SudokuBoard(int[][] grid) {
        super(grid);
        this.size = (int) Math.sqrt(grid.length);

    }

    public int getSize() {
        return this.size;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numCols; c++) {
                builder.append(toStringRow(r, c));
            }
            if ((r + 1) % this.size == 0 && r < this.numRows - 1) {
                builder.append(toStringRowSeparator());
            }
            builder.append('\n');
        }

        return builder.toString();
    }

    private String toStringRow(int r, int c) {
        StringBuilder builder = new StringBuilder();

        builder.append(' ');
        if (!this.showZeros && this.grid[r][c] == 0) {
            builder.append(' ');
        } else {
            if (this.grid[r][c] > 9) {
                builder.append((char) (this.grid[r][c] + 55));
            } else {
                builder.append(this.grid[r][c]);
            }
        }
        builder.append(' ');
        if ((c + 1) % this.size == 0 && c < this.numCols - 1) {
            builder.append('|');
        }

        return builder.toString();
    }

    private String toStringRowSeparator() {
        StringBuilder builder = new StringBuilder();

        builder.append('\n');
        for (int c = 0; c < this.numCols; c++) {
            for (int i = 0; i < 3; i++) {
                builder.append('-');
            }
            if ((c + 1) % this.size == 0 && c < this.numCols - 1) {
                builder.append('+');
            }
        }

        return builder.toString();
    }
}
