package itsgametime;

import boards.SudokuBoard;

/**
 * Solve a Sudoku puzzle.
 *
 * The biggest challenge for me in this
 * problem was writing the solve() method.
 *
 * @author Eric Nguyen
 */
public class Sudoku {
    private final SudokuBoard board;

    public Sudoku(SudokuBoard board) {
        this.board = board;
    }

    public SudokuBoard getBoard() {
        return this.board;
    }

    public SudokuBoard solve() {
        this.solve(0, 1);
        return this.board;
    }

    /**
     * Using backtracking, solve the Sudoku puzzle.
     */
    public boolean solve(int row, int num) {
        // Solution
        if (num > this.board.getSize() * this.board.getSize()) {
            return true;
        }

        // Solve next number
        if (row == this.board.getNumRows()) {
            return solve(0, num + 1);
        }

        // Check each column
        for (int col = 0; col < this.board.getNumCols(); col++) {
            if (canPlace(num, row, col)) {
                placeNumber(num, row, col);
                if (solve(row + 1, num)) return true;
                removeNumber(row, col);
            } else if (this.board.getElement(row, col) == num) {
                return solve(row + 1, num);
            }
        }

        return false;
    }

    /**
     * Check whether or not the given number can be placed at the given position.
     * @param num The number to be placed
     * @param row The row coordinate
     * @param col The column coordinate
     * @return If the number can be placed at the (row, col) coordinate
     */
    private boolean canPlace(int num, int row, int col) {
        if (this.board.getElement(row, col) != SudokuBoard.EMPTY) return false;

        // Does num exist in this row or column?
        for (int i = 0; i < this.board.getNumCols(); i++) {
            if (this.board.getElement(row, i) == num || this.board.getElement(i, col) == num) {
                return false;
            }
        }

        // Does num exist in this box?
        for (int r = row - (row % this.board.getSize()); r < row - (row % this.board.getSize()) + this.board.getSize(); r++) {
            for (int c = col - (col % this.board.getSize()); c < col - (col % this.board.getSize()) + this.board.getSize(); c++) {
                if (this.board.getElement(r, c) == num) {
                    return false;
                }
            }
        }

        return true;
    }

    private void placeNumber(int num, int row, int col) {
        this.board.setElement(row, col, num);
    }

    private void removeNumber(int row, int col) {
        this.board.setElement(row, col, SudokuBoard.EMPTY);
    }

    public static void main(String[] args) {
        Sudoku expertSudoku = new Sudoku(new SudokuBoard(
                new int[][] {
                        {9,0,0,0,0,0,0,0,7},
                        {0,0,4,0,0,0,0,1,0},
                        {0,0,0,7,0,0,0,9,2},
                        {7,5,0,0,0,8,0,0,0},
                        {2,0,0,0,0,0,0,0,0},
                        {0,0,0,1,7,0,0,2,0},
                        {5,0,0,0,2,0,0,0,0},
                        {6,0,0,0,0,0,0,7,0},
                        {0,0,0,0,0,4,6,3,1},
                }
        ));

        Sudoku hardSudoku = new Sudoku(new SudokuBoard(
                new int[][] {
                        {2,6,0,0,1,5,0,7,0},
                        {0,8,0,0,6,0,0,0,0},
                        {0,0,0,0,0,8,5,6,0},
                        {6,9,0,0,0,7,0,0,2},
                        {8,1,0,0,0,0,0,0,0},
                        {0,0,7,0,8,4,0,0,0},
                        {0,0,0,0,0,0,0,5,0},
                        {0,0,5,0,0,2,9,0,4},
                        {7,0,0,0,0,0,0,0,0},
                }
        ));

        Sudoku mediumSudoku = new Sudoku(new SudokuBoard(
                new int[][] {
                        {0,8,0,0,3,1,7,6,0},
                        {7,5,2,0,0,9,3,0,0},
                        {0,1,6,0,2,0,0,0,0},
                        {0,0,0,0,9,0,0,1,0},
                        {0,0,0,0,1,0,0,0,6},
                        {0,9,1,0,0,0,0,2,4},
                        {8,0,3,0,6,2,0,0,0},
                        {0,6,0,0,0,4,0,0,0},
                        {0,0,4,0,0,0,0,7,3},
                }
        ));

        Sudoku easySudoku = new Sudoku(new SudokuBoard(
                new int[][] {
                        {3,4,0,0,0,7,0,2,6},
                        {0,2,0,0,0,4,8,0,1},
                        {0,0,1,2,0,5,0,0,0},
                        {2,5,0,6,0,0,0,0,0},
                        {6,8,0,9,0,0,3,4,0},
                        {0,1,0,4,0,2,0,0,8},
                        {1,6,0,0,8,9,0,3,0},
                        {4,0,9,0,2,0,6,0,0},
                        {0,0,8,7,4,6,2,0,0},
                }
        ));

        Sudoku smallSudoku = new Sudoku(new SudokuBoard(
                new int[][] {
                        {1,0,0,4},
                        {0,0,0,0},
                        {2,3,0,0},
                        {0,0,0,3},
                }
        ));

        int A = 10;
        int B = 11;
        int C = 12;
        int D = 13;
        int E = 14;
        int F = 15;
        int G = 16;
        Sudoku giantSudoku = new Sudoku(new SudokuBoard(
                new int[][] {
                        {E,0,2,0,0,0,0,0,0,A,0,4,0,0,6,G},
                        {0,0,0,0,0,C,2,0,6,0,0,0,F,3,7,0},
                        {B,0,3,5,0,0,0,6,E,F,0,0,0,0,2,0},
                        {0,F,0,0,0,0,8,G,0,0,0,0,B,0,0,0},
                        {0,2,8,0,0,9,4,0,0,3,D,0,0,0,E,B},
                        {1,0,0,0,0,E,3,F,0,0,0,0,9,8,0,0},
                        {0,A,9,0,0,0,0,0,C,0,0,0,7,0,0,5},
                        {0,C,0,0,0,7,0,0,0,B,4,0,0,F,1,0},
                        {0,8,0,2,0,4,6,0,F,0,0,5,0,E,0,0},
                        {0,4,5,0,0,A,0,0,8,9,C,0,0,0,D,6},
                        {0,0,0,0,0,8,G,0,4,0,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0,5,0,0,A,D,2,7,0,0},
                        {C,9,E,0,0,0,5,1,0,0,0,0,D,0,0,0},
                        {D,B,A,3,6,0,9,0,0,E,0,0,0,C,0,0},
                        {0,G,0,0,0,0,0,E,7,0,6,9,0,5,F,0},
                        {0,0,4,0,0,0,D,7,B,0,0,2,0,0,0,0},
                }
        ));

        System.out.println("EXPERT BOARD");
        System.out.println("============");
        System.out.println(expertSudoku.getBoard());

        System.out.println("EXPERT BOARD SOLUTION");
        System.out.println("=====================");
        System.out.println(expertSudoku.solve());

        System.out.println("HARD BOARD");
        System.out.println("==========");
        System.out.println(hardSudoku.getBoard());

        System.out.println("HARD BOARD SOLUTION");
        System.out.println("===================");
        System.out.println(hardSudoku.solve());

        System.out.println("MEDIUM BOARD");
        System.out.println("============");
        System.out.println(mediumSudoku.getBoard());

        System.out.println("MEDIUM BOARD SOLUTION");
        System.out.println("=====================");
        System.out.println(mediumSudoku.solve());

        System.out.println("EASY BOARD");
        System.out.println("==========");
        System.out.println(easySudoku.getBoard());

        System.out.println("EASY BOARD SOLUTION");
        System.out.println("===================");
        System.out.println(easySudoku.solve());

        System.out.println("SMALL BOARD");
        System.out.println("===========");
        System.out.println(smallSudoku.getBoard());

        System.out.println("SMALL BOARD SOLUTION");
        System.out.println("====================");
        System.out.println(smallSudoku.solve());

        System.out.println("GIANT BOARD");
        System.out.println("===========");
        System.out.println(giantSudoku.getBoard());

        System.out.println("GIANT BOARD SOLUTION");
        System.out.println("====================");
        System.out.println("Took too long.");
        // My algorithm is too inefficient to solve a large sudoku puzzle
        // System.out.println(giantSudoku.solve());
    }
}
