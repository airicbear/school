package itsgametime;

import boards.Board;
import boards.ChessBoard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Solve the N-Queens problems using recursive backtracking.
 * <br/>
 * <br/>
 * <b>References:</b>
 * <ul>
 *   <li> [0] https://sites.fas.harvard.edu/~cscie119/lectures/recursion.pdf <br/>
 *     This PDF goes over approaches for the N-Queens problem and
 *     provides a template for recursive backtracking algorithms.
 *   </li>
 *   <li> [1] https://youtu.be/EBOKXvI8a-g <br/>
 *     This recorded lecture by Professor Rosen covers the
 *     N-Queens problem using recursive backtracking.
 *   </li>
 * </ul>
 *
 * The biggest challenge for me in this problem was remembering to free
 * the previously occupied slots when calling removeQueen().
 *
 * @author Eric Nguyen
 */
public class NQueens {
    private final List<ChessBoard> solutions;
    private final ChessBoard board;
    private static final int EMPTY = 0;
    private static final int QUEEN = 1;

    // From [0]:
    // True where the column/diagonal is safe
    // False where the column/diagonal is unsafe
    boolean[] columnSafe;
    boolean[] upDiagSafe;
    boolean[] downDiagSafe;

    public NQueens(int numRows, int numCols) {
        this.solutions = new ArrayList<>();
        this.board = new ChessBoard(numRows, numCols);
        this.columnSafe = ones(numCols);
        this.upDiagSafe = ones(numRows + numCols - 1);
        this.downDiagSafe = ones(numRows + numCols - 1);
        this.solve(0);
    }

    /**
     * Using a recursive backtracking algorithm,
     * find all solutions to the N-Queens problem
     * and add them to the set of solutions.
     * <br />
     * <br />
     * If we only wanted to find one solution, we
     * could have this method return a boolean.
     * @param row Row where the Queen will be placed
     */
    private void solve(int row) {
        // Hit the last row, we have found ourselves a solution
        // That is, each row on the board has a queen on it
        if (row == this.board.getNumRows()) {
            this.solutions.add(this.board.copy());
            return;
        }

        for (int col = 0; col < this.board.getNumCols(); col++) {
            if (isSafe(row, col)) {
                // The queen is safe at this column on this row
                placeQueen(row, col);

                // Place the queen for the next row
                solve(row + 1);

                // BACKTRACK: The queen is not safe on this row, remove it
                removeQueen(row, col);
            }
        }
    }

    /**
     * Determine whether or not a given position is safe to place a queen.
     * There are three rules for this condition:
     * <ul>
     *     <li>There are no queens in the column.</li>
     *     <li>There are no queens in the up diagonal.</li>
     *     <li>There are no queens in the down diagonal.</li>
     * </ul>
     * @param row The row coordinate
     * @param col The column coordinate
     * @return If the given position is safe
     */
    private boolean isSafe(int row, int col) {
        return (this.columnSafe[col]
                && this.upDiagSafe[row + col]
                && this.downDiagSafe[(this.board.getNumCols() - 1) + row - col]);
    }

    /**
     * Place a queen at the given position and determine and
     * establish the columns and diagonals that are no longer safe.
     * @param row The Queen's row coordinate
     * @param col The Queen's column coordinate
     */
    private void placeQueen(int row, int col) {
        this.board.setElement(row, col, QUEEN);

        // Occupy these slots
        this.columnSafe[col] = false;
        this.upDiagSafe[row + col] = false;
        this.downDiagSafe[(this.board.getNumCols() - 1) + row - col] = false;
    }

    /**
     * Reverse the placeQueen() method by removing
     * the queen and freeing the columns and diagonals.
     * @param row The Queen's row coordinate
     * @param col The Queen's column coordinate
     */
    private void removeQueen(int row, int col) {
        this.board.setElement(row, col, EMPTY);

        // Free these slots
        this.columnSafe[col] = true;
        this.upDiagSafe[row + col] = true;
        this.downDiagSafe[(this.board.getNumCols() - 1) + row - col] = true;
    }

    /**
     * Create an array of true values used to keep
     * track of each of the columns and diagonals.
     * @param size Size of the array
     * @return An array with all true values
     */
    private boolean[] ones(int size) {
        boolean[] array = new boolean[size];
        for (int i = 0; i < size; i++) {
            array[i] = true;
        }
        return array;
    }

    /**
     * Get the solution as a set of coordinates.
     * @param board A solution
     * @return The solution as a set of coordinates
     */
    public static String[] coordinates(Board board) {
        String[] positions = new String[board.getNumRows()];
        for (int r = 0; r < board.getNumRows(); r++) {
            StringBuilder builder = new StringBuilder();
            for (int c = 0; c < board.getNumCols(); c++) {
                if (board.getElement(r, c) == QUEEN) {
                    builder.append((char) (c + 97));
                    break;
                }
            }
            builder.append(board.getNumRows() - r);
            positions[r] = builder.toString();
        }
        return positions;
    }

    public List<ChessBoard> getSolutions() {
        return this.solutions;
    }

    /**
     * Output all possible solutions for the N-Queens problem.
     */
    public static void main(String[] args) {
        NQueens nqueens = new NQueens(8, 8);

        for (int i = 0; i < nqueens.getSolutions().size(); i++) {
            Board solution = nqueens.getSolutions().get(i);
            System.out.println("Coordinates: " + Arrays.toString(NQueens.coordinates(solution)));
            System.out.println(solution);
            System.out.println();
        }
    }
}
