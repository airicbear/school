package itsgametime;

import boards.ChessBoard;

//
// Testing
//
public class KnightsTour {
    private static final int EMPTY = 0;
    private static final int VISITED = 1;

    private final ChessBoard board;
    private int[] knight;
    private static final int[][] moves = {
            {-2,1},
            {-1,2},
            {1,2},
            {2,1},
            {2,-1},
            {1,-2},
            {-1,-2},
            {-2,-1},
    };

    public KnightsTour(int numRows, int numCols) {
        this.board = new ChessBoard(numRows, numCols);
        this.knight = new int[]{4, 4};
        this.board.setElement(knight[0], knight[1], VISITED);
    }

    public boolean tour(int count, int numSteps) {
        if (count == numSteps) {
            System.out.println(this.board);
            System.out.println();
            return true;
        }

        for (int move = 0; move < moves.length; move++) {
            if (canPlace(moves[move])) {
                placeKnight(knight[0], knight[1], move);
                if (tour(count + 1, numSteps)) return true;
                removeKnight(knight[0], knight[1], move);
            }
        }

        return false;
    }

    private boolean canPlace(int[] move) {
        return (knight[0] + move[0] >= 0 && knight[0] + move[0] < this.board.getNumRows())
                && (knight[1] + move[1] >= 0 && knight[1] + move[1] < this.board.getNumCols())
                && this.board.getElement(knight[0] + move[0], knight[1] + move[1]) != VISITED;
    }

    private void placeKnight(int row, int col, int move) {
        this.board.setElement(row, col, VISITED);
        this.knight[0] = row + moves[move][0];
        this.knight[1] = col + moves[move][1];
    }

    private void removeKnight(int row, int col, int move) {
        this.board.setElement(row, col, EMPTY);
        this.knight[0] = row - moves[move][0];
        this.knight[1] = col - moves[move][1];
    }

    public static void main(String[] args) {
        KnightsTour knightsTour = new KnightsTour(8, 8);
        knightsTour.tour(0, 10);
    }
}