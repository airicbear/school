package ui;

import hangman.CheatingHangman;
import hangman.GameOver;
import hangman.Guess;
import hangman.Hangman;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * CLI version of my Hangman game.
 *
 * @author Eric Nguyen
 */
public class HangmanCLI extends HangmanUI {
    private static final String PROMPT = "Make a guess: ";
    private static final String PROMPT_MAX_GUESSES = "How many guesses do you want? ";
    private static final String INVALID_MAX_GUESSES = "You can't do that. Please enter a number from 1 to 26.";
    private static final String INPUT_MISMATCH_INTEGER = "That is not an integer.";
    private static final String GAME_OVER = "Game over! The correct word is ";
    private static final String GOODBYE = "Goodbye!";
    private final Hangman game;
    private final Scanner scanner;
    private final String dictionaryFilePath;
    private boolean debugMode;

    public HangmanCLI(String dictionaryFilePath, boolean debugMode) throws FileNotFoundException {
        this.debugMode = false;
        this.scanner = new Scanner(System.in);
        this.dictionaryFilePath = dictionaryFilePath;
        this.debugMode = debugMode;
        this.game = new CheatingHangman(dictionaryFilePath, promptMaxGuesses(), debugMode);
    }

    private int promptMaxGuesses() {
        System.out.print(PROMPT_MAX_GUESSES);
        Scanner s = new Scanner(System.in);
        int maxGuesses;
        try {
            maxGuesses = s.nextInt();
            if (maxGuesses > 0 && maxGuesses <= 26) {
                return maxGuesses;
            }
        } catch (InputMismatchException ignored) {
            System.out.println(INPUT_MISMATCH_INTEGER);
        }
        System.out.println(INVALID_MAX_GUESSES);
        return promptMaxGuesses();
    }

    public boolean play() throws FileNotFoundException {
        System.out.println("You have " + this.game.getNumGuessesLeft() + (this.game.getNumGuessesLeft() == 1 ? " guess " : " guesses ") + "remaining.");

        if (this.game.isGameOver() == GameOver.NOT_GAME_OVER) {
            System.out.println(this.game);
            promptUserForGuess();
            return play();
        }

        System.out.println(this.game.isGameOver().getMessage());
        System.out.println(GAME_OVER + this.game.getCorrectWord() + ".");
        return playAgain();
    }

    public boolean playAgain() throws FileNotFoundException {
        System.out.print("Do you want to play again (y/n)? ");
        String response = this.scanner.nextLine();

        if (response.equals("y")) {
            HangmanCLI newGame = new HangmanCLI(this.dictionaryFilePath, this.debugMode);
            return newGame.play();
        } else if (response.equals("n")) {
            System.out.println(GOODBYE);
            return false;
        } else {
            return playAgain();
        }
    }

    protected void promptUserForGuess() {
        System.out.print(PROMPT);

        String input = this.scanner.nextLine();
        Guess guess;

        if (input.length() == 1) {
            guess = this.game.guess(input.charAt(0));
        } else {
            guess = this.game.guess(input);
        }

        System.out.println(guess.getMessage());
    }
}
