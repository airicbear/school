import hangman.GameMode;
import ui.HangmanCLI;
import ui.HangmanUI;

import java.io.FileNotFoundException;

/**
 * The main entry point of the program.
 *
 * @author Eric
 */
public class Main {
    public static final boolean DEBUG_MODE = true;
    private static final String DICTIONARY = "dictionary.txt";

    public static void main(String[] args) throws FileNotFoundException {
        HangmanUI hangman = new HangmanCLI(DICTIONARY, DEBUG_MODE);
        hangman.play();
    }
}
