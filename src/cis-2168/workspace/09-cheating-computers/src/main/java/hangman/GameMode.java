package hangman;

/**
 * Game modes for Hangman.
 *
 * @author Eric Nguyen
 */
public enum GameMode {
    NORMAL,
    CHEATING
}
