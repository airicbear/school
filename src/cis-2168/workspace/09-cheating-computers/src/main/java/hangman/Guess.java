package hangman;

/**
 * Different results for guesses.
 *
 * @author Eric Nguyen
 */
public enum Guess {
    ALREADY_GUESSED("You already guessed that. Try again."),
    INVALID_GUESS("Invalid guess. Please enter a letter or word."),
    CORRECT_CHARACTER_GUESS("Correct. Guess another letter or word."),
    CORRECT_WORD_GUESS("Correct. You win."),
    INCORRECT_GUESS("Incorrect.");

    private final String message;

    Guess(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
