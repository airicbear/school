package hangman;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * The Hangman game.
 *
 * @author Eric Nguyen
 */
public class Hangman {
    protected final Map<Integer, Character> blanks; // e.g. _a__n -> (1=a,4=n)
    protected final Set<String> guessed;
    protected final int maxGuesses;
    protected final Random random;
    protected boolean debugMode;
    protected Set<String> dictionary;
    protected Set<String> possibleWords;
    protected String correctWord;
    protected GameOver gameOver;

    public Hangman(String dictionaryFilePath, int maxGuesses, boolean debugMode) throws FileNotFoundException {
        this.debugMode = debugMode;
        this.gameOver = GameOver.NOT_GAME_OVER;
        this.random = new Random();
        this.guessed = new HashSet<>();
        this.blanks = new HashMap<>();
        this.dictionary = readDictionary(dictionaryFilePath);
        this.correctWord = getRandomWord(this.dictionary);
        this.maxGuesses = maxGuesses;
        this.possibleWords = this.filterSet(
                this.dictionary,
                word -> word.length() == this.correctWord.length()
        );
    }


    /**
     * Read in a dictionary file where each word is separated by line breaks.
     *
     * @param dictionaryFilePath File path of the dictionary file
     * @return The dictionary as a set of strings
     * @throws FileNotFoundException Could not find anything at file path
     */
    protected Set<String> readDictionary(String dictionaryFilePath) throws FileNotFoundException {
        Set<String> newDictionary = new HashSet<>();
        Scanner s = new Scanner(new File(dictionaryFilePath));
        while (s.hasNextLine()) {
            String nextWord = s.nextLine();
            newDictionary.add(nextWord.toLowerCase());
        }
        s.close();
        return newDictionary;
    }

    /**
     * Using an iterative method, choose a random word from a given set of words.
     *
     * @param wordSet Set of words to choose from
     * @return A random word from the word set
     */
    protected String getRandomWord(Set<String> wordSet) {
        int randomIndex = this.random.nextInt(wordSet.size());

        Iterator<String> iter = wordSet.iterator();
        for (int i = 0; i < randomIndex; i++) {
            iter.next();
        }

        return iter.next();
    }

    /**
     * For convenience of filtering sets.
     *
     * @param set       The set
     * @param predicate The filter
     * @return The filtered set
     */
    protected Set<String> filterSet(Set<String> set, Predicate<String> predicate) {
        return set.stream().filter(predicate).collect(Collectors.toSet());
    }

    /**
     * Calculate the number of wrong guesses by filtering the guessed set
     * such that the guess is not included in the correct word and getting
     * the size of the resulting set.
     *
     * @return The number of wrong guesses the player made
     */
    protected int getNumWrongGuesses() {
        return this.filterSet(this.guessed, guess -> !this.correctWord.contains(guess)).size();
    }

    public int getNumGuessesLeft() {
        return this.maxGuesses - this.getNumWrongGuesses();
    }

    public Guess guess(Character c) {
        if (!Character.isAlphabetic(c)) return Guess.INVALID_GUESS;

        if (this.guessed.add(c.toString())) {
            boolean correctGuess = false;

            for (int i = 0; i < this.correctWord.length(); i++) {
                if (c.equals(this.correctWord.charAt(i))) {
                    this.blanks.put(i, c);

                    // Filter words with corresponding letter positions
                    int finalI = i;
                    this.possibleWords = this.filterSet(
                            this.possibleWords,
                            word -> c.equals(word.charAt(finalI))
                    );

                    correctGuess = true;
                } else {
                    // Filter words who have the letter at the wrong spot
                    int finalI = i;
                    this.possibleWords = this.filterSet(
                            this.possibleWords,
                            word -> !c.equals(word.charAt(finalI))
                    );
                }
            }

            this.possibleWords = this.filterSet(
                    this.possibleWords,
                    correctGuess                           // If the character is in the correct answer
                            ? word -> word.indexOf(c) > -1 // Words that contain the character
                            : word -> word.indexOf(c) < 0  // Words that don't contain the character
            );

            return correctGuess ? Guess.CORRECT_CHARACTER_GUESS : Guess.INCORRECT_GUESS;
        }

        return Guess.ALREADY_GUESSED;
    }

    public Guess guess(String word) {
        if (word.isEmpty()) return Guess.INVALID_GUESS;

        for (int i = 0; i < word.length(); i++) {
            if (!Character.isAlphabetic(word.charAt(i))) {
                return Guess.INVALID_GUESS;
            }
        }

        if (this.guessed.add(word)) {
            this.gameOver = this.correctWord.equals(word) ? GameOver.WIN_CORRECT_WORD : GameOver.NOT_GAME_OVER;
            this.possibleWords.remove(word);
            return this.gameOver.equals(GameOver.WIN_CORRECT_WORD) ? Guess.CORRECT_WORD_GUESS : Guess.INCORRECT_GUESS;
        }

        return Guess.ALREADY_GUESSED;
    }

    public GameOver isGameOver() {
        if (this.getNumWrongGuesses() >= this.maxGuesses) {
            return GameOver.LOSE_OUT_OF_GUESSES;
        }

        if (this.blanks.size() == this.correctWord.length()) {
            return GameOver.WIN_CORRECT_CHARACTERS;
        }

        return this.gameOver;
    }

    public String getCorrectWord() {
        return this.correctWord;
    }

    protected void setCorrectWord(String word) {
        if (this.possibleWords.contains(word)) {
            this.correctWord = word;
        }
    }

    public String getBlanks() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < this.correctWord.length(); i++) {
            if (this.blanks.get(i) != null) {
                sb.append(this.blanks.get(i));
            } else {
                sb.append('_');
            }
        }

        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (debugMode) {
            sb
                    .append("Possible:\t").append(this.possibleWords).append('\n')
                    .append("Guessed:\t").append(this.guessed).append('\n')
                    .append("Correct:\t").append(this.correctWord).append('\n')
                    .append("Blanks: \t").append(this.blanks).append('\n')
                    .append("# guesses:\t").append(this.getNumGuessesLeft()).append('\n');
        }

        sb.append(this.getBlanks());

        return sb.toString();
    }
}
