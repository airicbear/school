package hangman;

public enum GameOver {
    NOT_GAME_OVER("The game is not over."),
    WIN_CORRECT_CHARACTERS("You guessed all of the letters correctly!"),
    WIN_CORRECT_WORD("You guessed the right word!"),
    LOSE_OUT_OF_GUESSES("You ran out of guesses.");

    private final String message;

    GameOver(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
