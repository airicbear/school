package hangman;

import java.io.FileNotFoundException;

/**
 * Change the correct word for each guess.
 *
 * @author Eric Nguyen
 */
public class CheatingHangman extends Hangman {
    public CheatingHangman(String dictionaryFilePath, int maxGuesses, boolean debugMode) throws FileNotFoundException {
        super(dictionaryFilePath, maxGuesses, debugMode);
    }

    private void changeCorrectWord() {
        if (!this.isGameOver().equals(GameOver.NOT_GAME_OVER)) return;
        this.setCorrectWord(this.getRandomWord(this.possibleWords));
    }

    @Override
    public Guess guess(Character c) {
        Guess g = super.guess(c);
        changeCorrectWord();
        return g;
    }

    @Override
    public Guess guess(String word) {
        Guess g = super.guess(word);
        changeCorrectWord();
        return g;
    }
}
