package indexingtext;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.function.BiConsumer;

/**
 * The majority of the code in this lab is adapted from the textbook.
 *
 * @author Eric Nguyen
 */
public class IndexTree {
	private IndexNode root;
	private static final String PATTERN = "[\\p{L}\\p{N}']+";

	private IndexTree() { }

	public void add(String word, int lineNumber) {
		this.root = add(this.root, word, lineNumber);
	}

	/**
	 * Recursive add method.
	 * If the IndexNode already exists in the tree,
	 * then add the lineNumber to that IndexNode.
	 * Otherwise, create a new IndexNode.
	 */
	private IndexNode add(IndexNode root, String word, int lineNumber) {
		if (root == null) {
			return new IndexNode(word, lineNumber);
		}

		IndexNode node = find(word);
		if (node != null) {
			node.occurrences++;
			node.list.add(lineNumber);
		} else if (word.compareTo(root.word) == 0) {
			root.occurrences++;
			root.list.add(lineNumber);
		} else if (word.compareTo(root.word) < 0) {
			root.left = add(root.left, word, lineNumber);
		} else {
			root.right = add(root.right, word, lineNumber);
		}
		return root;
	}

	public IndexNode find(String word) {
		return find(this.root, word);
	}

	private IndexNode find(IndexNode root, String word) {
		if (root == null) {
			return null;
		}

		if (word.compareTo(root.word) == 0) {
			return root;
		} else if (word.compareTo(root.word) < 0) {
			return find(root.left, word);
		} else {
			return find(root.right, word);
		}
	}
	
	// call your recursive method
	// use book as guide
	public void delete(String word) {
		this.root = delete(root, word);
	}
	
	// your recursive case
	// remove the word and all the entries for the word
	// This should be no different than the regular technique.
	private IndexNode delete(IndexNode root, String word) {
		if (root == null) {
			return null;
		}

		if (word.compareTo(root.word) < 0) {
			root.left = delete(root.left, word);
			return root;
		} else if (word.compareTo(root.word) > 0) {
			root.right = delete(root.right, word);
			return root;
		} else {
			if (root.left == null) {
				return root.right;
			} else if (root.right == null) {
				return root.left;
			} else {
				if (root.left.right == null) {
					root.word = root.left.word;
					root.left = root.left.left;
				} else {
					root.word = findLargestChild(root.left);
				}
				return root;
			}
		}
	}

	private String findLargestChild(IndexNode parent) {
		if (parent.right.right == null) {
			String returnValue = parent.right.word;
			parent.right = parent.right.left;
			return returnValue;
		} else {
			return findLargestChild(parent.right);
		}
	}

	// prints all the words in the index in inorder order
	// To successfully print it out
	// this should print out each word followed by the number of occurrences and the list of all occurrences
	// each word and its data gets its own line
	public void printIndex() {
		inOrderTraverse((e, d) -> {
			if (e != null) {
				System.out.println(e);
			}
		});
	}

	public void inOrderTraverse(BiConsumer<IndexNode, Integer> consumer) {
		inOrderTraverse(this.root, 1, consumer);
	}

	public void inOrderTraverse(IndexNode node, int depth, BiConsumer<IndexNode, Integer> consumer) {
		if (node == null) {
			consumer.accept(null, depth);
		} else {
			inOrderTraverse(node.left, depth + 1, consumer);
			consumer.accept(node, depth);
			inOrderTraverse(node.right, depth + 1, consumer);
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		IndexTree index = new IndexTree();
		
		// add all the words to the tree
		Scanner s = new Scanner(new File("shakespeare.txt"));
		int lineNumber = 0;
		while (s.hasNextLine()) {
			lineNumber++;
			String token;
			while ((token = s.findInLine(PATTERN)) != null) {
				token = token.toLowerCase();
				index.add(token, lineNumber);
			}
			if (s.hasNextLine()) {
				s.nextLine();
			}
		}
		s.close();
		
		// print out the index
		index.printIndex();
		
		// test removing a word from the index
		index.delete("zone");
		index.printIndex();

		// Allow the user to search for a word
		s = new Scanner(System.in);
		System.out.print("Search for a word ('q' to quit): ");
		while (s.hasNext()) {
			String word = s.nextLine().toLowerCase();
			if (word.equals("q")) break;
			IndexNode result = index.find(word);
			System.out.println(result == null ? "Failed to find \"" + word + "\"." : result);
			System.out.print("Search for a word ('q' to quit): ");
		}
	}
}
