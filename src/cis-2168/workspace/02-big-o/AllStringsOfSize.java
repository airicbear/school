import java.util.List;
import java.util.ArrayList;

class AllStringsOfSize {
    public static List<String> allStringsOfSize(List<String> list, int length) {
        List<String> newList = new ArrayList<>();
        for (String s : list) {
            if (s.length() == length) {
                newList.add(s);
            }
        }
        return newList;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("I");
        list.add("like");
        list.add("to");
        list.add("eat");
        list.add("eat");
        list.add("eat");
        list.add("apples");
        list.add("and");
        list.add("bananas");
        System.out.println(allStringsOfSize(list, 3));
    }
}