import java.util.List;
import java.util.ArrayList;

/**
 * Extra credit attempted: sanitize strings.
 */
class StringTokenization {

    public static List<String> tokenize(String text) {
        List<String> tokens = new ArrayList<>();
        int tokenPointer = 0;
        boolean foundValidCharacter = false;

        // Look for the first valid character
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) >= 'A' && text.charAt(i) <= 'z') {
                tokenPointer = i;
                foundValidCharacter = true;
                break;
            }
        }

        if (!foundValidCharacter) {
            return tokens;
        }

        for (int i = tokenPointer; i < text.length(); i++) {

            // Search for the next invalid character
            if (text.charAt(i) < 'A' || text.charAt(i) > 'z') {

                boolean foundNextValidCharacter = false;

                // Add token
                tokens.add(text.substring(tokenPointer, i));

                // Search for next valid character
                for (int j = i; j < text.length(); j++) {

                    if (text.charAt(j) >= 'A' && text.charAt(j) <= 'z') {
                        i = j;
                        tokenPointer = j;
                        foundNextValidCharacter = true;
                        break;
                    }

                }

                // On failure to find another valid character
                // after an invalid character, stop the loop
                if (!foundNextValidCharacter) {
                    break;
                }

            }

        }

        return tokens;
    }

    public static void main(String[] args) {
        System.out.println(tokenize("Hello, world!"));
    }

}