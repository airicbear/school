import java.util.List;
import java.util.ArrayList;

class AllMultiples {
    public static List<Integer> allMultiples(List<Integer> list, int n) {
        List<Integer> newList = new ArrayList<>();
        for (Integer item : list) {
            if (item % n == 0) {
                newList.add(item);
            }
        }
        return newList;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(25);
        list.add(2);
        list.add(5);
        list.add(30);
        list.add(19);
        list.add(57);
        list.add(2);
        list.add(25);
        System.out.println(allMultiples(list, 5));
    }
}