import java.util.List;
import java.util.ArrayList;

class RemoveAll {

    public static <E> List removeAll(List<E> list, E item) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(item)) {
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(5);
        list.add(5);
        list.add(2);
        System.out.println(removeAll(list, 5));
    }

}