#+TITLE: CSI: Evidence Analysis Report
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Summary

My partner in this case is Nathan Adiam.
My analysis of my partner's algorithms is summarized in the following table:

| Name  | Duration         | Comparisons      | Exchanges    | Conclusion     |
|-------+------------------+------------------+--------------+----------------|
| Sort1 | \(O(n \log{n})\) | \(O(n \log{n})\) | \(O(n)\)     | Quicksort      |
| Sort2 | \(O(n \log{n})\) | \(O(n)\)         | No evidence. | Merge Sort     |
| Sort3 | \(O(n^2)\)       | \(O(n^2)\)       | \(O(n^2)\)   | Insertion Sort |

These results were obtained through a brief examination of the graphs in the Data section.

From the data, it is apparent that Sort1 and Sort2 are much more efficient than Sort3, where Sort1 and Sort2 are \(O(n \log{n})\) sorting algorithms and Sort3 is a quadratic sorting algorithm.
Then, given \(O(n^2)\) for both duration, comparisons, and exchanges for Sort3, we can determine that it is most likely the quadratic sorting algorithm Insertion Sort (387).

Sort1 and Sort2 are a bit more tricky to determine, since they perform quite similarly.
However, there is one important distinction between the two and that is the number of comparisons required from each.
Sort2 only has \(O(n)\) comparisons whereas Sort1 has \(O(n \log{n})\) comparisons.
It seems that Sort2 resembles Merge Sort since we know from our manual that "the effort to do each merge is \(O(n)\)" (395) so this is very likely Merge Sort or some variant of it.

Finally, for Sort1, it must either be Quicksort or Heapsort as it cannot be a variant of Merge Sort yet it has \(O(n \log{n})\) performance.
Observing the number of exchanges, we have \(O(n)\) however Heapsort has \(O(\log{n})\) exchanges (407), hence by process of elimination Sort1 must be Quicksort.

* Issues

There were several issues that I had with this analysis that I would like to address.
First, the duration for the initial sorting seems absurd.
For some reason, it is always longer than the next trial.
Secondly, there was a lack of evidence for the number of exchanges in Sort2 which made it slightly more difficult as there was less evidence to work with.

* Data

** Sort1

|  Size | Duration (ns) | Comparisons | Exchanges |
|-------+---------------+-------------+-----------|
|     2 |       1118959 |           4 |         1 |
|     4 |         32717 |          17 |         3 |
|     8 |         50716 |          46 |         5 |
|    16 |        134583 |         105 |        16 |
|    32 |        326744 |         316 |        42 |
|    64 |        542455 |         692 |       103 |
|   128 |        475505 |        1710 |       215 |
|   256 |        721431 |        3912 |       503 |
|   512 |       1932525 |        9384 |      1111 |
|  1024 |       2381528 |       19983 |      2440 |
|  2048 |       3399520 |       45557 |      5408 |
|  4096 |       8669244 |      110168 |     11548 |
|  8192 |      18661288 |      226006 |     24989 |
| 16384 |      29706434 |      465923 |     54764 |
| 32768 |      47473769 |      981009 |    117834 |
| 65536 |      65110200 |     2325022 |    246037 |

** Sort2

|  Size | Duration (ns) | Comparisons | Exchanges |
|-------+---------------+-------------+-----------|
|     2 |       1200615 |           5 |         0 |
|     4 |         26808 |          10 |         0 |
|     8 |         39234 |          19 |         0 |
|    16 |         65780 |          37 |         0 |
|    32 |        116667 |          73 |         0 |
|    64 |        192787 |         160 |         0 |
|   128 |        348582 |         313 |         0 |
|   256 |        670133 |         639 |         0 |
|   512 |        916158 |        1277 |         0 |
|  1024 |       1351279 |        2551 |         0 |
|  2048 |       4496662 |        5115 |         0 |
|  4096 |       6821093 |       10233 |         0 |
|  8192 |      15067351 |       20474 |         0 |
| 16384 |      32394102 |       40957 |         0 |
| 32768 |      62637677 |       81919 |         0 |
| 65536 |      95210923 |      163839 |         0 |

** Sort3

|  Size | Duration (ns) | Comparisons |  Exchanges |
|-------+---------------+-------------+------------|
|     2 |       1423353 |           1 |          0 |
|     4 |         15571 |           5 |          1 |
|     8 |         10327 |          25 |          9 |
|    16 |         19190 |         121 |         53 |
|    32 |         77089 |         517 |        243 |
|    64 |        323683 |        1971 |        954 |
|   128 |        714221 |        7763 |       3818 |
|   256 |       3596846 |       33365 |      16555 |
|   512 |      10352652 |      128149 |      63819 |
|  1024 |      24830397 |      535491 |     267234 |
|  2048 |     220168890 |     2039229 |    1018591 |
|  4096 |     197651257 |     8664735 |    4330320 |
|  8192 |     596624722 |    33653821 |   16822815 |
| 16384 |    2284849303 |   132272657 |   66128137 |
| 32768 |    8235445415 |   536621739 |  268294486 |
| 65536 |   34435584777 |  2134307781 | 1067121123 |

** Graph comparisons

[[./analysis_duration.png]]
[[./analysis_comparisons.png]]
[[./analysis_exchanges.png]]
