using Plots
using LaTeXStrings
using CSV
using DataFrames
using Statistics

sort1 = get_dataframe("Sort1.csv")
sort2 = get_dataframe("Sort2.csv")
sort3 = get_dataframe("Sort3.csv")

# Convert nanoseconds to seconds
sort1[:duration] = sort1[:duration]/1e9
sort2[:duration] = sort2[:duration]/1e9
sort3[:duration] = sort3[:duration]/1e9

function main()
    xmax = maximum(sort1[:size])*1.5
    x = range(1, xmax, length=100)

    xlab = "Input size"
    ylab = "Duration (s)"
    title = "Input size vs. Duration"
    plot(dpi=300, title=title, xlabel=xlab, ylabel=ylab, ylim=[0,maximum(sort2[:duration])])
    plot_feature_comparison(x, :duration, 1e-9)

    xlab = "Input size"
    ylab = "Comparisons"
    title = "Input size vs. Comparisons"
    plot(dpi=300, title=title, xlabel=xlab, ylabel=ylab, ylim=[0,maximum(sort2[:comparisons])])
    plot_feature_comparison(x, :comparisons, 1)

    xlab = "Input size"
    ylab = "Exchanges"
    title = "Input size vs. Exchanges"
    plot(dpi=300, title=title, xlabel=xlab, ylabel=ylab, ylim=[0,maximum(sort1[:exchanges])])
    plot_feature_comparison(x, :exchanges, 1)
end

function plot_feature_comparison(x, feature, multiplier)
    plot!(sort1[:size], sort1[feature], label="Sort1", linewidth=3, markershape=:circle)
    plot!(sort2[:size], sort2[feature], label="Sort2", linewidth=3, markershape=:utriangle)
    plot!(sort3[:size], sort3[feature], label="Sort3", linewidth=3, markershape=:square)
    plot!(x, x -> log(2,x)*multiplier, label=L"\log(x)")
    plot!(x, x -> x*multiplier, label=L"x")
    plot!(x, x -> x * log(2,x)*multiplier, label=L"x \log(x)")
    plot!(x, x -> x^2*multiplier, label=L"x^2")
    savefig("analysis_$feature.png")
end

function get_dataframe(data_file::String)::DataFrame
    csv_file = CSV.File(data_file, header=1)
    df = DataFrame(csv_file)
    return df
end