package evidence;

import java.util.HashMap;
import java.util.Map;

public class Sort1 {


    public static <T extends Comparable<T>> Map<String, Integer> sort(T[] table) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Comparisons", 0);
        map.put("Exchanges", 0);
        // Sort the whole table.
        quickSort(map, table, 0, table.length - 1);
        return map;
    }

    private static <T extends Comparable<T>> void quickSort(Map<String, Integer> map, T[] table, int first, int last) {
        long startTime = System.nanoTime();
        if (first < last) { // There is data to be sorted.
            map.put("Comparisons", map.get("Comparisons") + 1);
            // Partition the table.
            int pivIndex = partition(map, table, first, last);
            // Sort the left half.
            quickSort(map, table, first, pivIndex - 1);
            // Sort the right half.
            quickSort(map, table, pivIndex + 1, last);
            map.put("Time", (int) (System.nanoTime() - startTime));
        }
    }
    /** Sort a part of the table using the quicksort algorithm.
     * @post The part of table from first through last is sorted. @param table The array to be sorted
     * @param first The index of the low bound
     * @param last The index of the high bound
    */
    private static <T extends Comparable<T>> int partition(Map<String, Integer> map, T[] table, int first, int last) {
        int up = first;
        int down = last;
        T pivot = table[first];
        do {
            while ((up < last) && (pivot.compareTo(table[up]) >= 0)) {
                map.put("Comparisons", map.get("Comparisons") + 2);
                up++;
            }
            while (pivot.compareTo(table[down]) < 0) {
                map.put("Comparisons", map.get("Comparisons") + 1);
                down--;
            }
            if (up < down) {
                map.put("Comparisons", map.get("Comparisons") + 1);
                swap(map, table, up, down);
            }
        } while (up < down); // Repeat while up is left of down. // Exchange table[first] and table[down] thus putting the // pivot value where it belongs.
        map.put("Comparisons", map.get("Comparisons") + 1);
        swap(map, table, first, down);
        // Return the index of the pivot value.
        return down;
    }

    private static <T extends Comparable<T>> void swap(Map<String, Integer> map, T[] table, int first, int down) {
        T temp = table[first];
        table[first] = table[down];
        table[down] = temp;
        map.put("Exchanges", map.get("Exchanges") + 1);
    }
}
