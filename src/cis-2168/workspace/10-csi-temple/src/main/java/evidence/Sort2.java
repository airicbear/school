package evidence;

import java.util.HashMap;
import java.util.Map;

public class Sort2 {

    public static <T extends Comparable<T>> Map<String, Integer> sort(T[] table) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Comparisons", 0);
        map.put("Exchanges", 0);
        // A table with one element is sorted already.
        if (table.length > 1) {
            long startTime = System.nanoTime();
            map.put("Comparisons", map.get("Comparisons") + 1);
            // Split table into halves.
            int halfSize = table.length / 2;
            T[] leftTable = (T[]) new Comparable[halfSize];
            T[] rightTable = (T[]) new Comparable[table.length - halfSize];
            System.arraycopy(table, 0, leftTable, 0, halfSize);
            System.arraycopy(table, halfSize, rightTable, 0, table.length - halfSize);
            // Sort the halves.
            sort(leftTable);
            sort(rightTable);
            // Merge the halves.
            method1(map, table, leftTable, rightTable);
            map.put("Time", (int)(System.nanoTime() - startTime));
        }
        return map;
    }

    private static <T extends Comparable<T>> void method1(Map<String, Integer> map, T[] outputSequence, T[] leftSequence, T[] rightSequence) {
        int i = 0;
        // Index into the left input sequence.
        int j = 0;
        // Index into the right input sequence.
        int k = 0;
        // Index into the output sequence.
        // While there is data in both input sequences
        while (i < leftSequence.length && j < rightSequence.length) {
            map.put("Comparisons", map.get("Comparisons") + 2);
            // Find the smaller and
            // insert it into the output sequence.
            if (leftSequence[i].compareTo(rightSequence[j]) < 0) {
                map.put("Comparisons", map.get("Comparisons") + 1);
                outputSequence[k++] = leftSequence[i++];
            } else {
                outputSequence[k++] = rightSequence[j++];
            }
        }
        // assert: one of the sequences has more items to copy.
        // Copy remaining input from left sequence into the output.
        while (i < leftSequence.length) {
            map.put("Comparisons", map.get("Comparisons") + 1);
            outputSequence[k++] = leftSequence[i++];
        }
        // Copy remaining input from right sequence into output.
        while (j < rightSequence.length) {
            map.put("Comparisons", map.get("Comparisons") + 1);
            outputSequence[k++] = rightSequence[j++];
        }
    }
}
