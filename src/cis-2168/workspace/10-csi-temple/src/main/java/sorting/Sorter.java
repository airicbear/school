package sorting;

import java.util.Map;

public interface Sorter {
    <T extends Comparable<T>> Map<String, Integer> sort(T[] list);
}
