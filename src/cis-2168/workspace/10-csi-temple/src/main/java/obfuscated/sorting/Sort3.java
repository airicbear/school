package obfuscated.sorting;

import java.util.HashMap;
import java.util.Map;

public class Sort3 implements Sorter {

    private static final String COMPARISONS = "comparisons";
    private static final String EXCHANGES = "exchanges";

    @Override
    public <T extends Comparable<T>> Map<String, Integer> sort(T[] t) {
        Map<String, Integer> profiler = new HashMap<>();
        profiler.put(COMPARISONS, 0);
        profiler.put(EXCHANGES, 0);
        sort(profiler, t, 0, t.length - 1);
        return profiler;
    }

    private <T extends Comparable<T>> void sort(Map<String, Integer> m, T[] t, int a, int b) {
        if (a < b) {
            m.put(COMPARISONS, m.get(COMPARISONS) + 1);
            int a1 = sortHelper1(m, t, a, b);
            sort(m, t, a, a1 - 1);
            sort(m, t, a1 + 1, b);
        }
    }

    private <T extends Comparable<T>> int sortHelper1(Map<String, Integer> m, T[] t, int a, int b) {
        sortHelper3(m, t, a, b);
        sortHelper2(m, t, a, (a + b) / 2);

        T t1 = t[a];
        int a1 = a;
        int b1 = b;
        do {
            while (a1 < b && t1.compareTo(t[a1]) >= 0) {
                m.put(COMPARISONS, m.get(COMPARISONS) + 2);
                a1++;
            }
            while (t1.compareTo(t[b1]) < 0) {
                m.put(COMPARISONS, m.get(COMPARISONS) + 2);
                b1--;
            }
            if (a1 < b1) {
                m.put(COMPARISONS, m.get(COMPARISONS) + 1);
                sortHelper2(m, t, a1, b1);
            }
            m.put(COMPARISONS, m.get(COMPARISONS) + 1);
        } while (a1 < b1);

        sortHelper2(m, t, a, b1);

        return b1;
    }

    private <T> void sortHelper2(Map<String, Integer> m, T[] t, int a, int b) {
        T temp = t[a];
        t[a] = t[b];
        t[b] = temp;
        m.put(EXCHANGES, m.get(EXCHANGES) + 1);
    }

    private <T extends Comparable<T>> void sortHelper3(Map<String, Integer> m, T[] t, int a, int b) {
        int middle = (a + b) / 2;
        if (t[a].compareTo(t[b]) >= 0) {
            m.put(COMPARISONS, m.get(COMPARISONS) + 1);
            sortHelper2(m, t, a, b);
        }
        if (t[middle].compareTo(t[b]) >= 0) {
            m.put(COMPARISONS, m.get(COMPARISONS) + 1);
            sortHelper2(m, t, middle, b);
        }
        if (t[a].compareTo(t[b]) >= 0) {
            m.put(COMPARISONS, m.get(COMPARISONS) + 1);
            sortHelper2(m, t, a, b);
        }
    }
}
