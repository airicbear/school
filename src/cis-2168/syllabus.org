#+title: CIS 2168 Syllabus
#+subtitle: CIS 2168 Data Structures \\
#+subtitle: Fall 2020
#+author: Daniel Yee
#+email: daniel.yee@temple.edu

* Description

  This course teaches students how to apply data structures
  to solve problems efficiently through proper analyses of
  the advantages and disadvantages of their different
  implementations.
  Topics include generic, inheritance, lists, time complexity /
  Big-O, stacks and queues, recursion, trees, heaps, sorting
  algorithms, and graphs.

** Temple and COVID-19

   Temple University's motto is Perseverance Conquers, and we
   will meet the challenges of the COVID pandemic with
   flexibility and resilience.
   The university has made plans for multiple eventualities.
   Working together as a community to deliver a meaningful
   learning experience is a responsibility we all share: we're
   in this together so we can be together.

* Credit Hours

  4

* Time and Place

  All classes, midterm, and final will be conducted via Zoom.

** Lecture

   Zoom (All meeting links are in the Canvas Zoom tab.) \\
   Tuesday 5:30 PM -- 8:00 PM

** Lab

   Zoom (All meeting links are in the Canvas Zoom tab. \\
   Thursday 5:30 PM -- 7:20 PM

* Prerequisites

  A grade of C or better in CIS 1166 and 1068.
  It is assumed that you know how to use Java and have
  started to learn how to think abstractly.
  You may not be prepared if you only had a C in 1068.

* Textbook

  Data Structures: Abstraction and Design Using Java, Third
  Edition \\ Elliot B. Koffman, Paul A. T. Wolfgang, Wiley

  The textbook is available through the Temple Bookstore both
  as paperback and digital editions.
  The digital edition is significantly less expensive than
  the paperback.

  Lectures will roughly follow the material presented in the book.
  You are still responsible for reading and understanding the
  material in the book.

* Office Hours

  Please email to schedule a time to meet via Zoom.
  The email should state the following information:
  provide at least four meeting time options,
  why you want to meet,
  and any relevant code files or documents.

  Please note that my office phone (215-204-4050) is reserved
  for emergencies only.

* Grades

  Letter grades will be assigned according to the standard
  grading levels: A ≥ 90, B ≥ 80, etc.
  Plus/minus grading will be used.
  In general, you need above a 70 to pass the class.

  | Participation        | 10% |
  | Labs and Assignments | 30% |
  | Quizzes              | 10% |
  | Midterm              | 15% |
  | Final Project        | 20% |
  | Final                | 15% |

  There may be extra credit opportunities available on exams
  and assignments, but extra credit will not be given to make
  up your grade.
  However, your lowest assignment will usually be dropped.
  Your final grade in the class will be the average of each
  item, weighted accordingly.

* Attendance Protocol and Your Health

  If you feel unwell, you should not come to campus, and you will not be penalized for your
  absence. Instructors are required to ensure that attendance is recorded for each in-person or
  synchronous class session. The primary reason for documentation of attendance is to facilitate
  contact tracing, so that if a student or instructor with whom you have had close contact tests
  positive for COVID-19, the university can contact you. Recording of attendance will also provide
  an opportunity for outreach from student services and/or academic support units to support
  students should they become ill. Faculty and students agree to act in good faith and work with
  mutual flexibility. The expectation is that students will be honest in representing class
  attendance.

** Illness

   If you are sick and contagious, please do not come to class. Please ask your fellow students for
   class notes and/or ask me for what material was covered in class. Official medical excuses from 
   your doctor or the student health center will be accepted. You will be excused from any
   attendance, participation and quiz grades. Exams will only be rescheduled, if you request one
   from me.

** Other Absences

   All other absences will be considered on a case-by-case basis. At minimum, you will need to
   provide some form of official documentation verifying your situation or presence at a University
   hosted event (i.e., departmental job fair).

* Technology Specifications

  You will need the following for this course:

  - a computer or laptop capable of running Eclipse,
    IntelliJ IDEA, or another Java development IDE

  - internet access

  - webcam

  - microphone

  Limited resources are available for students who do not have the technology they need for
  class. Students with educational technology needs, including no computer or camera or
  insufficient Wifi-access, should submit a request outlining their needs using the
  [[https://deanofstudents.temple.edu/news/student-emergency-aid-fund][Student Emergency Aid Fund]] form.
  The University will endeavor to meet needs, such as with a longterm loan of a laptop or
  Mifi device, a refurbished computer, or subsidized internet access.

* Participation

  You are expected to participate in the discussion boards and in-class activities and discussions.
  Completing the assigned reading and videos will increase the value you receive from the
  lectures and your classmates and the likelihood of succeeding in the class.

** Discussion Boards

   Each week, you are required to participate on the Canvas discussion boards. You are
   responsible for reading and following the Discussion Board rubric for grading details and the
   Discussion Board Rules of Conduct.

* Quizzes

  Weekly quizzes on the assigned reading and lecture videos will be given at the beginning of
  each lecture on Canvas. These are designed to allow you and myself to assess whether you are
  adequately preparing each week.

* Assignments and Labs

  Assignments and labs are intrinsically tied. Assignments are posted every week on Wednesday
  and will be due Thursday the following week.

  Lab sessions are dedicated time for you to work on assignments. The TA will explain the
  assignment at the beginning of lab, review any relevant topics, and will be available to aid you
  and answer any questions you have. The TA will only help you discover the assignment
  solutions -- they will not provide direct answers.

** Compilation

   Programming assignments are expected to compile. If your program does not compile and it is
   sent to us without informing us of a compilation issue, this indicates that you did not take the
   time to check your own work. You will receive a zero for the assignment if your program fails to
   compile and you did not warn us.

   If you have trouble resolving a compilation issue:

   - Read the error and find the line number.

   - Check your IDE for error hints (squiggly or red highlights).

   - Post your issue on the discussion board.

   - Google the error code or error message.

   - Email your TA or me.

** File Formats

   When submitting programming assignments, you must submit your source code and not the
   compiled program. Your source code must be a .java file, not a .class file and may be zipped. If
   you submit any other file type, you will receive a zero for that assignment.

** Submitting Assigments

   To receive a grade on your assignment, it must be:

   1. Submitted online to Canvas for official documentation
      of submitted assignment.

   2. Present your assignment to me or a TA.
      Presenting your work involves showing that your program
      works, explaining pieces of your source code, and answering
      some questions.
      This may be done during office hours or during a scheduled
      appointment.

   Late assignments will be accepted with a penalty up to a
   *maximum of one week* after the due date, as described below.

** Late Policy

   Late assignments will be deducted 5% per day.

   It is still your responsibility to demo late assignments.
   Any assignments that are not demoed before the final exam
   review will receive a 0.
   Late grades are determined by when you turn it in, not when
   you demo it, but the sooner you demo, the sooner you can
   correct any possible errors.

** Exams

   Practice exams will be provided and reviewed. These practice exams will be an excellent
   approximation of the actual exam, so be sure to use them. Please do not miss any exam.
   Makeup exams will be given on a case by case basis. You can expect one midterm exams and a
   final, with the midterm exam occurring during the week 6 lab. This is subject to change.

** Final Exam

   The final exam will be on December 15th, 5:45 PM - 7:45 PM. Please log into the scheduled
   Zoom meeting early so you can resolve any technical issues and be prepared when the exam
   starts. The final has a reputation for being difficult. This is due to the breadth of material
   covered and the complications of studying during the final period. 

   All courses must follow the University Final Exam schedule. If you have an exam conflict and the
   other course is not following the schedule, you MUST be allowed to take the other exam at an
   alternate time. Your advisor may be able to help resolve any issues.

   If you have more than two finals on the same day, university policy allows you to request that
   your professor provide a different time to take your final

** Remote Proctoring Statement

   Zoom, Proctorio or a similar proctoring tool may be used to proctor exams and quizzes in this
   course. These tools verify your identity and record online actions and surroundings. It is your
   responsibility to have the necessary government or school issued ID, a laptop or desktop
   computer with a reliable internet connection, the Google Chrome and Proctorio extension, a
   webcam/built-in camera and microphone, and system requirements for using Proctorio, Zoom,
   or a similar proctoring tool. Before the exam begins, the proctor may require a scan of the room
   in which you are taking the exam.

* Statement on Recording and Distribution of Records

  Lecture sessions will be recorded and will be made available as soon as possible for each
  module. These recordings are made available so students can review material, attend the
  session in cases of technology issues, illness or other excused absence. Therefore, students are
  not allowed to make recordings of the lecture except in cases of an approved accommodation
  from the Office of Disability Resources (DRS).

  Any recordings permitted in this class can only be used for the student’s personal educational
  use. Students are not permitted to copy, publish, or redistribute audio or video recordings of
  any portion of the class session to individuals who are not students in the course or academic
  program without the express permission of the faculty member and of any students who are
  recorded. Distribution without permission may be a violation of educational privacy law, known
  as [[https://www2.ed.gov/policy/gen/guid/fpco/ferpa/index.html][FERPA]] as well as certain copyright laws. Any recordings made by the instructor or university
  of this course are the property of Temple University.

* Class Conduct

  In order to maintain a safe and focused learning environment, we must all comply with the four
  public health pillars: wearing face coverings, maintaining physical distancing, washing our hands
  and monitoring our health. It is also important to foster a respectful and productive learning
  environment that includes all students in our diverse community of learners. Our differences,
  some of which are outlined in the University's nondiscrimination statement, will add richness to
  this learning experience. Therefore, all opinions and experiences, no matter how different or
  controversial they may be perceived, must be respected in the tolerant spirit of academic
  discourse.

  Treat your classmates and instructor with respect in all communication, class activities, and
  meetings. You are encouraged to comment, question, or critique an idea but you are not to
  attack an individual. Please consider that sarcasm, humor and slang can be misconstrued in
  online interactions and generate unintended disruptions. Profanity should be avoided as should
  the use of all capital letters when composing responses in discussion threads, which can be
  construed as “shouting” online. Remember to be careful with your own and others’ privacy. In
  general, have your behavior mirror how you would like to be treated by others.

* Academic Honesty

  The course’s philosophy on academic honesty is best stated as “be reasonable.” We recognize
  that interactions with classmates and others can facilitate mastery of the course’s material.
  However, there remains a line between enlisting the help of another and submitting the work
  of another.

  The essence of all work that you submit must be your own. Collaboration on assignments is not
  permitted, except to the extent that you may ask classmates and others for help so long as that
  help does not reduce to another doing your work for you. Generally, when asking for help, you
  may show your code to others, but you may not view theirs, so long as you and they respect
  this policy’s other constraints. Collaboration on the course’s quizzes and exams is not permitted
  at all. Collaboration on the course’s final project is permitted to the extent prescribed by its
  specification.

* Regret Clause

  If you commit some act that is not reasonable but bring it to my attention within 72 hours, you
  will receive disciplinary action that may include an unsatisfactory or failing grade for work
  submitted, but I will not refer the matter for further disciplinary action except in cases of
  repeated acts.

  Below are rules of thumb that inexhaustively characterize acts that the I consider reasonable
  and not reasonable. If in doubt as to whether some act is reasonable, do not commit it until you
  solicit and receive approval in writing. Acts considered not reasonable are handled seriously
  and may result in dismissal from the course and referred to the Dean of Students. If a matter is 
  referred to the Dean of Students for disciplinary action and the outcome is punitive, I reserve
  the right to impose local disciplinary action on top of that outcome that may include an
  unsatisfactory or failing grade for work submitted or for the course itself.

** Reasonable

   - Communicating with classmates about problems in English (or some other spoken
     language), and properly citing those discussions.

   - Discussing the course’s material with others in order to understand it better.

   - Sending or showing code that you’ve written to someone, possibly a classmate, so that
     they might help you identify and fix a bug, provided you properly cite the help.

   - Helping a classmate identify a bug in their code at office hours, elsewhere, or even
     online, as by viewing, compiling, or running their code after you have submitted that
     portion yourself. Add a citation to your own code of the help you provided and
     resubmit. (Make sure you indicate this in the comments when you submit your code to
     Canvas so we don’t grade your assignment prematurely.)

   - Incorporating a few lines of code that you find online or elsewhere into your own code,
     provided that those lines are not themselves solutions to assigned problems and that
     you cite the lines’ origins.

   - Submitting the same or similar work that you have submitted previously to my course.

   - Asking for help from the instructor or TA or receiving help from the instructor or TA
     during the quizzes or exams.

   - Turning to the web or elsewhere for instruction beyond the course’s own, for
     references, and for solutions to technical difficulties, but not for outright solutions to
     assignments or your own final project.

   - Whiteboarding solutions to assignments with others using diagrams or pseudocode but
     not actual code.

   - Working with (and even paying) a tutor to help you with the course, provided the tutor
     does not do your work for you.

** Not Reasonable

   - Accessing a solution to an assignment prior to it
     released by the instructor.

   - Accessing or attempting to access, without permission,
     an account not your own.

   - Asking a classmate to see their solution to an assignment.

   - Discovering but failing to disclose to the instructor
     bugs in the course's software that affect scores.

   - Decompiling, deobfuscating, or disassembling the instructor's
     solutions to the assignments.

   - Failing to cite (as with comments) the origins of code
     or techniques that you discover outside of the course's
     own lessons and integrate into your own work, even while
     respecting this policy's other constraints.

   - Giving or showing to a classmate a solution to an
     assignment when it is, they, and not you, who is
     struggling to solve it.

   - Looking at another individual's work during the
     quizzes or exams.

   - Communicating with others during quizzes or exams
     whether by computer, phone, or otherwise.

   - Manipulating or attempting to manipulate scores artificially,
     as by exploiting bugs or formulas in the course's software.

   - Paying or offering to pay an individual for work that you
     may submit as (part of) your own.

   - Paying or offering to pay an individual for work that you
     may submit as (part of) your own.

   - Providing or making available solutions to assignments to
     individuals who might take this course in the future.

   - Searching for or soliciting outright solutions to assignments
     online or elsewhere.

   - Splitting an assignment's workload with another individual
     and combining your work.

   - Submitting (after possibly modifying) the work of another
     individual beyond the few lines allowed herein.

   - Submitting work to this course that you intend to use
     outside of the course (e.g., for a job) without prior
     approval from the instructor.

   - Turning to humans (besides the instructor or TA) for help
     or receiving help from humans (besides the instructor or TA)
     during the quizzes or exam.

   - Viewing another's solution to an assignment and basing your
     own solution on it.

* Disabilities

  Please bear in mind that COVID-19 may result in a need for new or additional accommodations.

  Any student who has a need for accommodations based on the impact of a documented
  disability or medical condition should contact Disability Resources and Services (DRS) in 100
  Ritter Annex (drs@temple.edu; 215-204-1280) to request accommodations and learn more
  about the resources available to you. If you have a DRS accommodation letter to share with me,
  or you would like to discuss your accommodations, please contact me as soon as practical. I will
  work with you and with DRS to coordinate 2 reasonable accommodations for all students with
  documented disabilities. All discussions related to your accommodations will be confidential.

* Disclaimer

  The syllabus is here to serve as a guide and may be subject to changes. Up-to-date information,
  assignments, and class material can be found on online. This syllabus may be updated to reflect
  changes.

* Students and Faculty Academic Rights and Responsibilities

  Freedom to teach and freedom to learn are inseparable facets of academic freedom. The
  University has a policy on Student and Faculty Academic Rights and Responsibilities (Policy
  #03.70.02) which can be accessed at policies.temple.edu.

* Student Support Services

  The following academic support services are available to support you:

  - [[https://studentsuccess.temple.edu/][Student Success Center]]

  - [[https://library.temple.edu/webpages/remote-learner-support][University Libraries]]

  - [[http://www.temple.edu/vpus/research/index.html][Undergraduate Research Support]]

  - [[https://www.temple.edu/life-at-temple/students/careers-and-internships/career-center][Career Center]]

  - [[https://counseling.temple.edu/access-services][Tuttleman Counseling Services]]

  - [[https://disabilityresources.temple.edu/][Disability Resources and Services]]

  If you are experiencing food insecurity or financial struggles, Temple provides resources and
  support. Notably, the Temple University [[https://studentcenter.temple.edu/cherry-pantry][Cherry Pantry]] and the Temple University
  [[https://careteam.temple.edu/emergency-student-aid-0][Emergency Student Aid Program]] are in operation as well as a variety of resources from the
  [[https://studentaffairs.temple.edu/][Office of Student Affairs]].

* Tips for Succeeding

  - Don't be afraid to ask questions in class.
    If you have a question, I guarantee another student
    has the same question.

  - Get into the habit of studying a couple of days early.

  - Do your homework.

  - Give yourself more time than you think you need.

  - Use a clear, easy-to-read, monospaced font while coding.

  - Do your homework.

  - Ask questions.
    Take advantage of office hours.

  - Do your homework.
    Most students who turn in all their homework manage to earn
    A's and B's, even when they had a bad day on an exam.
