#+setupfile: ~/.emacs.d/org-templates/level-1.org
#+title: Spring 2020 Syllabus: Two Sections

The Good Life: Humanities Seminar IH851

* General Information
  :PROPERTIES:
  :CUSTOM_ID: general-information
  :END:

    - Time: TR 17:30-19:00
    - Sections: 079, 085
    - Office: Anderson 736
    - IH Website: [[https://www.courses.temple.edu.ih]]

    - Dr. Aldona Middlesworth, Instructor in the Program
    - Office Hours: TR 10:30-10:30 and 15:00-16:00 and by appointment
    - Email: [[mailto:amiddles@temple.edu][amiddles@temple.edu]]

* Required Texts
  :PROPERTIES:
  :CUSTOM_ID: required-texts
  :END:

  Purchase the exact edition listed below otherwise the translationg and pagination
  will not be the same.
  Not only were these editions used to construct this syllabus, but discussion will
  go more smoothly if we all have the same translation and the same page numbers to
  refer to.
  All of these texts are available at the campus bookstore in the SAC; please get
  them as soon as possible because if you wait too long sometimes the Bookstore
  returns unbought copies to the publishers.

  1. /Genesis/.
     In /Holy Bible: New Revised Standard Version/.
     Zondervan Publishing, 1989.
     /ISBN 031090238X/.
     (On Canvas).

  2. /The Epic of Gilgamesh/.
     Trans. Andrew George.
     Penguin Classics, 2003.
     ISBN 0140449191.

  3. /Love's Alchemy: Poems from the Sufi Tradition/.
     Trans. David and Sabrineh Fideler.
     New World Library, 2006.
     ISBN 10: 1-57731-535-9.

  4. Ch'Eng-En, Wu.
     /Monkey: Folk Novel of China/.
     Trans. Arthur Waley.
     Grove Press, 1943.
     ISBN 13: 978-0-8021-3086-0.

  5. Plato.
     /The Trials of Socrates: Six Classic Texts/.
     /3^{rd} edition/.
     /Trans. and Intro. C. D. C. Reeve/.
     Hackett Publishing Company, Inc.
     ISBN13-978-0-087220-589-5.

  6. Kafka, Franz.
     /The Sons/.
     Schocken Books, 1989.
     ISBN 0-8052-0886-0.

  7. Kang, Han.
     /The Vegetarian: A Novel/.
     Trans. Deborah Smith.
     Hogarth Press, 2007.
     ISBN 978-1-101-90611-8.

  8. Occasional short articles, essays, poems that I will either post on Canvas,
     distribute in class as a hard copy or provide a URL.

* Course Description/Overview:
  :PROPERTIES:
  :CUSTOM_ID: course-description-overview
  :END:

  IH 851 is the first half of a year-long course designed to introduce students
  to some of the fundamental concepts of civilization and culture.
  Through the use of primary texts, we will gain a better understanding of key
  ideas and values of people in the past and in other countries and how these
  have shaped the world we live in today.
  Through the analysis and evaluation of these challenging primary texts and
  their historical/social context, students are introduced to core humanities
  concepts.
  *The particular focus for our class this semester* will be the issue of what
  constitutes a good life.
  We will ask ourselves how we learn to live in a way that will lead to lifelong
  fulfillment both for ourselves as individuals and for the community/society as
  a whole.
  Where and from whom do we learn how to live?
  Our parents?
  Religion?
  Spiritual practices?
  Culture?
  Creativity?
  Material goods/Technology?
  The arts: music, film++?
  Our peers?
  Our body?
  Other species?
  Social activism?
  Each of our course texts will address one or another of these.

* Course Goals
  :PROPERTIES:
  :CUSTOM_ID: course-goals
  :END:

  The Intellectual Heritage Program has identified six main aims of the program:
  aka "What students should be able to do upon completion of the two-semester
  course."

  *Students should be able to*

  1. Read unfamiliar and problematic texts that are theoretically, historically
     or culturally challenging.

  2. Recognize abstractions, large ideas and implications associated with difficult
     texts.

  3. Make connections across disciplines, across history and cultural boundaries.

  4. Construct positions, arguments and interpretations through textual analysis.

  5. Produce thoughtful writing that reflects persuasive positions and implements
     the conventions of academic discourse.

  6. View the world in radically and culturally different ways.

  We will explore our texts in order to reach these goals and improve skills and
  will look at the texts both for their themes and their style, that is both
  content and form, in other words what is said and how it is said.
  Study of these diverse texts will help us to understand various aspects of the
  cultures that produced them, plus the relevance of these works to our own
  contemporary culture.

* Protocols of Our Course
  :PROPERTIES:
  :CUSTOM_ID: protocols-of-our-course
  :END:

** Objectives
   :PROPERTIES:
   :CUSTOM_ID: objectives
   :END:

   This course is designed to enable us to gain access to texts that have profoundly
   shaped the perception of the world we live in.
   While reading them, it is important for us to debate the issues addressed by the
   authors.
   We ought to remember that analytic and critical skill can only develop through
   close inquiry into the material.
   Be ready to do a good bit of writing both in class and out.

** Goals
   :PROPERTIES:
   :CUSTOM_ID: goals
   :END:

   1. Explore perspectives offered by the significant works of particularly the
      Western but *also* global traditions.

   2. Develop critical thinking skills.

   3. Discover and reflect on ideas that did and continue to influence the world.

   4. Enhance reading and writing skills

   5. Become familiar with the discourses of power and how they relate to knowledge.

   6. Work on effective oral expression.

   7. Come alive with thought and feeling.

** On Critical Thinking
   :PROPERTIES:
   :CUSTOM_ID: on-critical-thinking
   :END:

   According to Dr. bell hooks:
   "The most exciting aspect of critical thinking in the classroom is that it
   calls for initiative from everyone, actively inviting all students to think
   passionately and to share ideas in a passionate, open manner.
   When everyone in the classroom, teacher and students, recognizes that they
   are responsible for creating a learning community together, learning is at
   its most meaningful and useful.
   In such a community of learning there is no failure.
   Everyone is participating and sharing whatever resource is needed at a given
   moment in time to ensure that we leave the classroom knowing that critical
   thinking empowers us."
   (hooks, bell. /Teaching Critical Thinking: Practical Wisdom/)

** Guidelines
   :PROPERTIES:
   :CUSTOM_ID: guidelines
   :END:

   1. More than four (4) absences, you fail the course.
      Attendance is taken at the beginning of each class. \\
      *Please note:* Missing more than two classes will surely affect your final
      grade detrimentally, by as much as half a grade.

   2. Lateness to class will be penalized; be courteous and come to class on time.

   3. No Incomplete final grades given for this class.

   4. Submit plagiarized work, and you fail.
      Please read the section on [[*Plagiarism][plagiarism]] in this syllabus.

   5. No late paper submissions.
      Only exception: Medical reasons verified by a note from your physician.
      Without this, late papers will be marked down by half a letter grade per
      day late.

   6. Submitted work must be computer printed and delivered as a hard copy unless
      specified.

   7. *Civility and professional behavior are expected.*
      *Our class is a Tech Detox Zone:*
      *No phones out, either on your desk, lap or hands.*
      *No laptops (unless specfically allowed).*
      *You must have a hard copy of the text or a print-out where I provide it on Canvas or with a URL.*
      *Turn your phone off so it does not even vibrate!*
      *You must put your phone away in your bag when class begins;*
      *if you have it out, I will call you at the end of class and note it on your attendance card.*
      *After the first incident, your participation grade will go down ONE grade each time.*
      *(I'm serious about eliminating major distractions during our time together.)*
      *ALSO, I expect that you will listen while other students and the instructor are speaking,*
      *that is not engage in side individual conversations with the student sitting next to you.*
      *If you find you must leave before the class is over, notify the instructor first.*

   8. Always bring your copy of the text that we are reading that day to class sessions.
      If you do not have it and I notice, you will be marked absent for that class period.

* PROBLEMS
  :PROPERTIES:
  :CUSTOM_ID: problems
  :END:

  If you have any problems with the readings or any aspect of our class, contact me
  immediately.
  Send an email or talk with me during office hours.
  Please do not hesitate!
  I am interested in your success and want our class to be one that you value.
  My office hours are listed on the first page, feel free to drop in.
  I am available at other times with a prior appointment.

** Student Code of Conduct
   :PROPERTIES:
   :CUSTOM_ID: student-code-of-conduct
   :END:

   "Every student must conduct himself or herself with respect and courtesy
   in compliance with Temple University policies and the Procedure Manual at
   [[https://secretary.temple.edu/sites/secretary/files/policies/03.70.12.pdf]]

** Resources
   :PROPERTIES:
   :CUSTOM_ID: resources
   :END:

   1. IH Tutors: IH Lounge, 2^{nd} Floor, Anderson Hall

   2. The Writing Center, Charles Library Student Success Center, 215-204-0702.

   3. University Counseling Services

** Disability Statement
   :PROPERTIES:
   :CUSTOM_ID: disability-statement
   :END:

   This course is open to all students who meet the academic requirements
   for participation.
   Any student who has a need for accommodation based on the impact of a
   disability, contact the Disability Resources and Services at 215-204-1280
   at 100 Ritter Annex in order to obtain a documented disability form,
   which will be emailed to me, listing the type of accommodations you require.
   Also, notify me in person so together we can coordinate reasonable
   accommodations.

** Religious Holidays
   :PROPERTIES:
   :CUSTOM_ID: religious-holidays
   :END:

   If you will be observing religious holidays this semester which may prevent
   attending a regularly scheduled class or interfere with fulfilling a
   particular course requirement, I will offer you the opportunity to make up
   the class or course requirement if you inform me
   *within two weeks of the beginning of the semester*
   of the dates of the holidays.
   
** Plagiarism
   :PROPERTIES:
   :CUSTOM_ID: plagiarism
   :END:

   There are strict university guidelines on plagiarism.
   Cheating, copying published material without proper attribution,
   using internet papermills, using papers from previous courses,
   or copying fellow students' work constitute fraud and carry
   significant penalties, such as an F in the course, and in the
   worst case scenario expulsion from the university.
   Please check out the following site for the university's
   statement on plagiarism.
   [[https://bulletin.temple.edu/undergraduate/about-temple-university/student-responsibilities/#academichonesty]]
   *In our class,* you must cite all sources that you use in your
   essays, including journal articles, books, ebooks, websites,
   television shows, podcasts, videos, magazine or newspaper
   articles or any other source whose ideas or material you draw
   on, using the MLA citation style.

** Please Note
   :PROPERTIES:
   :CUSTOM_ID: please-note
   :END:

   - Last day to drop or add a course: Monday, January 27

   - Last day to withdraw from a course: Wednesday, March 18

** Tasks/Grading Policy
   :PROPERTIES:
   :CUSTOM_ID: tasks-grading-policy
   :END:

   *One paper which must be submitted to SafeAssign*
   *(On our course Canvas)*
   *the night before bringing it as a hard copy to class.*
   *After it is returned to you graded, it must be revised.*
   *Suggested paper topics will be distributed a week in advance.*
   *Failure to submit the paper and the revision will result in a failed grade for the course.*
   *The original draft which includes my comments must be submitted with the rewrite.*

   | Essay                   | 25% | (plus revision---mandatory to pass the course)                                                                                                                        |
   | Two closed-book quizzes | 25% | (each announced beforehand, make-up with doctor's note)                                                                                                               |
   | Final Project           | 25% |                                                                                                                                                                       |
   | Class Participation     | 25% | (*attendance, one oral news presentation,* speaking up, (listening and responding when your instructor and fellow students speak), courtesy, enthusiasm, punctuality) |

** Class Participation
   :PROPERTIES:
   :CUSTOM_ID: class-participation
   :END:

   By this, I mean coming to each class *with the text,* having read the assignment
   for that day and being ready to discuss it.
   Our class is comprised of some lecture, more small group work and much large
   group discussion.
   To prepare for this, make notes, questions and comments for the next class.
   This will most definitely help you during class discussions.
   Reflect also on how the ideas in the assignment for a particular day compare to
   ideas developed and discussed in previous assignments and classes.
   In other words, make connections.
   Prior to many classes, I will ask you to summarize in writing a passage in the
   text for the upcoming class that I will collect.
   *Or I will ask you to generate questions for the coming day's text assignment,*
   *which we will then proceed to consider in class.*
   *We will at times use your questions as the basis for exploring the text.*

** How IH 851 is Different from Other Courses
   :PROPERTIES:
   :CUSTOM_ID: how-ih-851-is-different-from-other-courses
   :END:

   The overall theme of this course is the world in which we live and how
   we come to grips with the complexity of that world.
   Our emphasis will be on knowledge and truth, or "truths" many of which
   cannot be seen or appreciated with the senses or through the social
   means that people use to negotiate complex situations in which they
   find they must interact with others.

** Essay Requirement
   :PROPERTIES:
   :CUSTOM_ID: essay-requirement
   :END:

   One formal essay and its rewrite are required to pass this course;
   failure to hand them in will result in faliing the course.
   The night before submitting them in class as a paper copy, you
   must upload them to SafeAssign.
   (The link will be provided under Assignments on Canvas.)
   I will specify the topics and length requirement beforehand;
   however, general guidelines for the essay are as follows:

   - The essay must be revised.
     The original with my comments submitted with rewrite.
     Also, a cover page with the title of the essay;
     title page is not numbered.

   - Please do not submit the essay in a folder.

   - Essays need to be double-spaced (even between paragraphs),
     12 font type,
     Times New Roman font,
     standard margins=0.75-1.25

   - Double-sided pages are fine

   If you consult sources other than our class text, include a
   Works Consulted page.

* CALENDAR
  :PROPERTIES:
  :CUSTOM_ID: calendar
  :END:

  *Please note: These dates are subject to some changes.*
  *(Reading must be completed for the date assigned)*

  | January  |  14 | Introduction to the Course/to One another/Overview/What we bring to the table |
  |          |  16 | Look at the Syllabus/In-class writing---response essay                        |
  |          |     |                                                                               |
  |          |  21 | The Epic of Gilgamesh                                                         |
  |          |  23 | Continue                                                                      |
  |          |     |                                                                               |
  |          |  28 | +Complete+ No class                                                           |
  |          |  30 | No class                                                                      |
  |          |     |                                                                               |
  | February |   4 | Genesis in /The Holy Bible/                                                   |
  |          |   6 | Continue                                                                      |
  |          |     |                                                                               |
  |          |  11 | Love's Alchemy (Selections)                                                   |
  |          |  13 | Continue                                                                      |
  |          |     |                                                                               |
  |          |  18 | Complete                                                                      |
  |          |  20 | Monkey                                                                        |
  |          |     |                                                                               |
  |          |  25 | Continue                                                                      |
  |          |  27 | Complete                                                                      |
  |          |     |                                                                               |
  | March    | 2-8 | Spring Break---No classes                                                     |
  |          |  10 | The Apology (in /The Trials of Socrates/)                                     |
  |          |  12 | Continue                                                                      |
  |          |     |                                                                               |
  |          |  17 | Continue                                                                      |
  |          |  19 | Complete                                                                      |
  |          |     |                                                                               |
  |          |  24 | The Judgment (in Kafka's /The Sons/)                                          |
  |          |  26 | Continue                                                                      |
  |          |     |                                                                               |
  | April    |  31 | Continue                                                                      |
  |          |   2 | Complete                                                                      |
  |          |     |                                                                               |
  |          |   7 | The Vegetarian (Part 1)                                                       |
  |          |   9 | Continue                                                                      |
  |          |     |                                                                               |
  |          |  14 | The Vegetarian (Part 2)                                                       |
  |          |  16 | Continue                                                                      |
  |          |     |                                                                               |
  |          |  21 | The Vegetarian (Part 3)                                                       |
  |          |  23 | Last Class---Wrap Up                                                          |
  |          |     |                                                                               |
  |          |  28 | University wide Study Day                                                     |
  |          |  29 | Final week begins (No final in our class)                                     |
  |          |     |                                                                               |
  | May      |   5 | Finals end                                                                    |
  |          |   8 | Final grades submitted to the University                                      |

* Missed Class=Missed Material and Notes
  :PROPERTIES:
  :CUSTOM_ID: missed-class
  :END:

  A missed class means missed material; as I do not carry around handouts
  and assignments day after day, those that are not available on Canvas
  should be copied from a fellow student.
  Before you leave class, make friends with someone in our class and
  exchange email addresses and/or cell numbers.
  If you miss a class, contact them to ask them to send a picture of the
  assignment or handout.
  *Remember that if you miss a class, you are still responsible for the*
  *material covered during that class so you will need to find out what*
  *you missed by contacting another student.*

* How To Excel in Our Class
  :PROPERTIES:
  :CUSTOM_ID: how-to-excel-in-our-class
  :END:

  1. *Do the Reading.*
     Because so much of our work will be comparing works to one another
     and discussing them *within the context of current events,* it is
     important that you read all the assignments carefully before class
     so that you can discuss them intelligently and in addition do well
     on the two closed-book quizzes.

  2. *Do the Writing*
     Much of the success in this course depends on writing, but a type
     that is demanding, is critical of the readings, and that poses
     questions that you attempt to answer.

  3. *Come to class.*
     If you skip the reading, don't skip the class.
     (That's missing out twice.)
     If you're going to be late, come anyway.

  4. *Email me.*
     Never feel that you're interrupting me.

* End note
  :PROPERTIES:
  :CUSTOM_ID: end-note
  :END:

  *I consider this syllabus a contract between you and me;*
  *returning to our next class meeting signifies that you*
  *agree to the requirements listed here.*

  *Welcome to the course!*
