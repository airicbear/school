#+title: Dreams

* Epic of Gilgamesh

** Gilgamesh's Third Dream (p. 33)

   #+begin_quote
   'The dream I had was an utter confusion:
     heaven cried aloud, while the earth did rumble.
   The day grew still, darkness came forth,
     there was a flash of lightning, fire broke out.

   '[/The flames/] flared up, death rained down. 
     ...and the flashes of fire went out,
   [where] it had fallen turned into cinders.
     [You were] born in the wild, can we take counsel?'

   [Having heard the words of his friend,]
     Enkidu gave the dream meaning, saying to Gilgamesh:
   '[My friend,] your dream is a good omen, fine is [its
     message/.]'

   'We draw, my friend, ever nearer the forest,
     the dreams are close, the battle soon.
   You will see the radiant auras of the god,
     of *Humbaba, whom in your thoughts you fear so much.

   'Locking horns like a bull you will batter him,
     and force his head down with your strength.
   The old man you saw is your powerful god,
     the one who begot you, divine Lugalbanda.'
   #+end_quote

** Enkidu's Final Dream (p. 59)

   #+begin_quote
   'The heavens thundered, the earth gave echo,
     and there was I, standing between them.
   A man there was, grim his expression,
     just like a Thunderbird his features were frightening.

   'His hands were a lion's paws, his claws an eagle's talon,
     he seized me by the hair, he overpowered me.
   I struck him, but back he sprang like a skipping rope,
     he struck me, and like a raft capsized me.

   'Underfood [he] crushed me, like a mighty wild bull,
     [/drenching/] my body with poisonous slaver.
   "Save me, my friend!......"
     You were afraid of him, but you......

   * * *

     '[/He struck me and/] turned me into a dove.

   '[He bound] my arms like the wings of a bird,
     to lead me captive to the house of darkness, seat of Irkalla:
   to the house which none who enters ever leaves,
     on the path that allows no journey back,

   'to the house whose residents are deprived of light,
     where soil is their sustenance and clay their food,
   where they are clad like birds in coats of feathers,
     and see no light, but dwell in darkness.

   'On door [and bolt the dust lay thick,]
     on the House [of Dust was poured a deathly quiet.]
   In the House of Dust that I entered,

   'I looked around me, saw the "crowns" in a throng,
     there were the crowned [heads] who'd ruled the land since days
       of yore,
   who served the roast [at the] tables of Anu and Enlil,
     who'd proffered baked bread, and poured them cool water from
       skins.

   'In the House of Dust that I entered,
     there were the /en/-priests and /lagar/-priests,
   there were lustration-priests and /lumahhu/-priests,
     there were the great gods' /gudapsû/-priests,

   'there was Etana, there was Shakkan,
     [there was] the queen of the Netherworld, the goddess
       Ereshkigal.
   Before her sat [Belet]-ṣeri, the scribe of the Netherworld,
     holding [a tablet], reading aloud in her presence.

   '[She raised] her head and she saw me:
     "[Who was] it fetched this man here?
   [Who was it] broguht here [/this fellow?/]"'

   The remainder of Enkidu's vision of hell is lost.
   At the end of his speech he commends himself to Gilgamesh:

   'I who [endured] all hardships [with you,]
     remember [me, my friend,] don't [forget] all I went through!'

   Gilgamesh:

   'My friend saw a vision which will never [/be equalled/!]'

   The day he had the dream [his strength] was exhausted,
     Enkidu was cast down, he lay one day sick [and then a
       second.]
   Enkidu [lay] on his bed, [his sickness /worsened/,]
     a third day and a fourth day, [the sickness of Enkidu /worsened/.]

   A fifth day, a six and a seventh, an eighth, a ninth [and a tenth,]
     the sickness of Enkidue /worsened/...
   An eleventh day and a twelf,......
     Enkidu [lay] on the bed,......
   He called for Gilgamesh [/and spoke to his friend/:]

   '[/My god/] has taken against me, my friend,...,
     [/I do not die] like one who [falls] in the midst of battle.
   I was afraid of combat, and......
     My friend, one who [falls] in combat [/makes his name/,]
   But I, [/I do not fall/] in [/combat, and shall make not my name/.]'

   The description of Enkidu's final death throes, which no doubt
   filled the remaining thirty or so lines of Tablet VII, is still
   to be recovered.
   #+end_quote

* The Vegetarian (translated by Deborah Smith)

** Dream 1 (p. 17)

   #+begin_quote
   Dark woods.
   No people.
   The sharp-pointed leaves on the trees, my torn feet.
   This place, almost remembered, but I'm lost now.
   Frightened.
   Cold.
   Across the frozen ravine, a red barn-like building.
   Straw matting flapping limp across the door.
   Roll it up and I'm inside, it's inside.
   Long bamboo sticks strung with great blood-red
   gashes of meat, blood still dripping down.
   Try to push past the meat, there's no end to the
   meat, and no exit.
   Blood in my mouth, blood-soaked clothes sucked onto
   my skin.

   Somehow a way out.
   Running, running through the valley, then suddenly
   the woods open out.
   Trees thick with leaves, springtime's green light.
   Families picnicking, little children running about,
   and that smell, that delicious smell.
   Almost painfully vivid.
   The babbling stream, people spreading out rush mats
   to sit on, snacking on kimbap.
   Barbecuing meat, the sounds of singing and happy
   laughter.
   
   But the fear.
   My clothes still wet with blood.
   Hide, hide behind the trees.
   Crouch down, don't let anybody see.
   My bloody hands.
   My bloody mouth.
   In that barn, what had I done?
   Pushed that red raw mass into my mouth, felt it
   squish against my gums, the roof of my mouth,
   slick with crimson blood.

   Chewing on something that felt so real, but
   couldn't have been, it couldn't.
   My face, the look in my eyes... my face,
   undoubtedly, but never seen before.
   Or no, not mine, but so familiar... nothing makes
   sense.
   Familiar and yet not... that vivid, strange,
   horribly uncanny feeling.
   #+end_quote

** Dream 2 (p. 24)

   #+begin_quote
   The morning before I had the dream,
   I was mincing frozen meat---remember?
   You got angry.

   "Damn it, what the hell are you doing
   squirming like that?
   You've never been squeamish before."

   If you knew how hard I've always
   worked to keep my nerves in check.
   Other people just get a bit flustered,
   but for me everything gets confused,
   speeds up.
   Quick, quicker.
   The hand holding the knife was working
   so quickly, I felt heat prickle the
   back of my neck.
   My hand, the chopping board, the meat,
   and then the knife, slicing cold into
   my finger.

   A drop of red blood already blossoming
   out of the cut.
   Rounder than round.
   Sticking the finger in my mouth calmed
   me.
   The scarlet color, and now the taste,
   sweetness masking something else, left
   strangely pacified.

   Later that day, when you sat down to a
   meal of bulgogi, you spat out the
   second mouthful and picked out
   something glittering.

   "What the hell is this?" you yelled.
   "A chip off the knife?"

   I gazed vacantly at your distorted
   face as you raged.

   "Just think what would have happened
   if I'd swallowed it!
   I was this close to dying!"

   Why didn't this agitate me like it
   should have done?
   Instead, I became even calmer.
   A cool hand on my forehead.
   Suddenly, everything around me began
   to slide away, as though pulled back
   on an ebbing tide.
   The dining table, you, all the
   kitchen furniture.
   I was alone, the only thing remaining
   in all of the infinite space.

   Dawn of the next day.
   The pool of blood in the barn... I
   first saw the face reflected there.
   #+end_quote

** Dream 3 (p. 33)

   #+begin_quote
   Dreams of murder.

   Murderer or murdered... hazy distinctions,
   boundaries wearing thin.
   Familiarity bleeds into strangeness,
   certainty becomes impossible.
   Only the violence is vivid enough to stick.
   A sound, the elasticity of the instant
   when the metal struck the victim's head...
   the shadow that crumpled and fell gleams
   cold in the darkness.

   They come to me now more times than I can
   count.
   Dreams overlaid with dreams, a palimpsest
   of horror.
   Violent acts perpetrated by night.
   A hazy feeling I can't pin down... but
   remembered as blood-chillingly definite.

   Intolerable loathing, so long suppressed.
   Loathing I've always tried to mask with
   affection.
   But now the mask is coming off.

   That shuddering, sordid, gruesome, brutal
   feeling.
   Nothing else remains.
   Murderer or murdered, experience too vivid
   to not be real.
   Determined, disillusioned.
   Lukewarm, like slightly cooled blood.

   Everything starts to feel unfamiliar.
   As if I've come up to the back of
   something.
   Shut up behind a door without a handle.
   Perhaps I'm only now coming face-to-face
   with the thing that has always been here.
   It's dark.
   Everything is being snuffed out in the
   pitch-black darkness.
   #+end_quote

** Dream 4 (p. 38)

   #+begin_quote
   Dreams of my hands around someone's throat,
   throttling them, grabbing the swinging ends
   of their long hair and hacking it all off,
   sticking my finger into their slippery
   eyeball.

   Those drawn-out waking hours, a pigeon's
   dull colors in the street and my resolve
   falters, my fingers flexing to kill.
   Next door's cat, its bright tormenting eyes,
   my fingers that could squeeze that
   brightness out.
   My trembling legs, the cold sweat on my brow.
   I become a different person, a different
   person rises up inside me, devours me, those
   hours...

   Saliva pooling over my mouth.
   Along the length of my tought to my lips,
   slick with saliva.
   Leaking out between my lips, trickling down. \\
   \\
   \\

   If only I could sleep.
   If I could shrug off consciousness for even
   just an hour.
   The house is cold on all these nights, more
   nights than I can count, when I wake up and
   pace about in bare feet.
   Chill like rice or soup that has been left
   to go cold.
   Nothing is visible outside the black window.
   The dark front door rattles now and then,
   but no one comes to knock on the door or
   anything like that.
   By the time I come back to bed and put my
   hand under the quilt, all the warmth is gone. \\
   \\
   \\

   Sleeping in five-minute snatches.
   Slipping out of fuzzy consciousness, it's
   back---the dream.
   Can't even call it that now.
   Animal eyes gleaming wild, presence of blood,
   unearthed skull, again those eyes.
   Rising up from the pit of my stomach.
   Shuddering awake, my hands, need to see
   my hands.
   Breathe.
   My fingernails still soft, my teeth still
   gentle.
 
   Can only trust my breasts now.
   I like my breasts, nothing can be killed
   by them.
   Hand, foot, tongue, gaze, all weapons from
   which nothing is safe.
   But not my breasts.
   With my round breasts, I'm okay.
   Still okay.
   So why do they keep on shrinking?
   Not even round anymore.
   Why?
   Why am I changing like this?
   Why are my edges all sharpening--what
   am I going to gouge?
   #+end_quote

** Dream 6 (p. 54)

   #+begin_quote
   I don't know why that woman is crying.
   I don't know why she keeps staring at
   my face, either, as though she wants
   to shallow it.
   Or why she strokes the bandage on my
   wrist with her trembling hands.

   My wrist is okay.
   It doesn't bother me.
   The thing that hurts is my chest.
   Something is stuck in my solar plexus.
   I don't know what it might be.
   It's lodged there permanently these days.
   Even though I've stopped wearing a bra, I
   can feel this lump all the time.
   No matter how deeply I inhale, it doesn't
   go away.

   Yells and howls, threaded together layer
   upon layer, are enmeshed to form that lump.
   Because of meat.
   I ate too much meat.
   The lives of the animals I ate have all
   lodged there.
   Blood and flesh, all those butchered
   bodies are scattered in every nook and
   cranny, and though the physical remnants
   were excreted, their lives still stick
   stubbornly to my insides.

   One time, just one more time, I want to shout.
   I want to throw myself through the pitch-black window.
   Maybe that would finally get this lump out of my body.
   Yes, perhaps that might work.

   Nobody can help me.
   Nobody can save me.
   Nobody can make me breathe.
   #+end_quote
