#+title: IH851 FINAL PROJECT ASSIGNMENT

*Due date Thursday, April 30th*

* Introduction

  As you already know, the topic for your final project is DREAMS, that is
  you are being asked to select a couple of the dreams in the /Epic of
  Gilgamesh/ or a couple of the many dreams in /The Vegetarian by Han
  Kang./ The dreams in these texts come in different guises---prophetic,
  symbolic, informative, soothing, nightmarish, life-changing,
  wish-fulfillment and so forth. Of these guises, how do the dreams you
  select to discuss function? Some of the dreams in these two texts that
  we've read can be seen actually even in conversation with each other.

  In the Kang text, Yeong-hye has about eight dreams (that are rendered in
  italics) in Part 1 and her husband Mr. Cheong even has one. Read each of
  these carefully then select a couple of them to focus on.

  In the Epic, Gilgamesh has several dreams as well that his mother
  interprets then in a later tablet on their way to the sacred forest he
  has more dreams that Enkidu interprets. Before his death, Enkidu too has
  a dream in which the gods decree that he must die. Read these dreams
  carefully and select two or three that seem most interesting to you.

* Description

  *I'm offering two possible ways, two options, to do your final project.
  They are these:*

** Option # 1:

   Select two dreams, describe and analyze each, comparing and contrasting
   each to the other to see what they might reveal about the dreamer.
   Reflect on the symbolism of each of the dreams, what they might reveal
   about the dreamer also the circumstances the dreamers find themselves
   in. You may, for example, compare the fearful dreams Gilgamesh has on
   the way to the sacred forest *to* the dreams that Yeong-hye has
   concerning eating meat. Her dreams lead her eventually to wanting to
   become a plant, in fact a tree. You may choose to compare Gilgamesh's
   fearful dreams on the way to kill Humbaba and chop down the sacred cedar
   trees to Yeong-hye's fearful dreams about eating meat. Whichever dreams
   you select, make connections between them. (Remember too the book review
   that you were asked to write a diagnostic essay about during the first
   week of classes. This was about the effect of climate change on nature
   including on trees.)

   If you choose this option as a response, write a five-paragraph essay
   comparing two dreams, with a thesis in bold face, exploring what the
   dreams have in common.

** Option # 2:

   Instead of writing an essay, you may wish to submit a visual or musical
   response as your project.

   1. That is a painting, watercolor, drawing, sculpture for example that
      you create *or* a page or two of a graphic novel or a series of
      comics with stick figures speaking in bubbles to each other.
      (Remember that in the first part of The Vegetarian, we're told that
      writing Yeong-hye's occupation is writing dialogue in bubbles for
      comic strips. Google and access XKCD for examples of webcomics.) One
      of my students last semester drew a poster size depiction of
      Yeong-hye with tree branches emerging from her abdomen. Very
      beautiful. He donated it to me to inspire future students (meaning
      you). And I saved it to bring to class to show. However, alas it is
      in my TU office. If you choose this option, you must send an image of
      your creation to me through Canvas *WITH a one-page long description
      of how your creation relates to a couple of the dreams in our texts.*

   2. Or stream to me several pieces of music that evoke the feeling of
      Yeong-hye's dreams, or those of Gilgamesh. These can be from other
      artists or music that you compose and play. You would need to stream
      the music to me via Canvas *also WITH a one-page long description of
      how each of the several pieces of music relates to a couple of the
      dreams in our texts. Also identifying the composer/artist.*

* Note

  *This final project is worth 25% of your final grade, so do your best!
  Remember that it must be submitted on Canvas on Thursday, April
  30^{th}.*
