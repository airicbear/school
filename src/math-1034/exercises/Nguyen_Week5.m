%% Week 5 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-04-09
%

%% Exercise 1
syms x;
S1 = x ^ 2 * (4 * x ^ 2 - 8 * x - 3) + 3 * (8 * x - 9);
S2 = (2 * x - 3) ^ 2 + 4 * x;
a = simplify(S1 * S2);
b = simplify(S1 / S2);
c = simplify(S2 - S1);
d = double(subs(c, 7));
disp(a);
disp(b);
disp(c);
disp(d);

%% Exercise 2
syms x y;
S = sqrt(y) + x;
T = y - sqrt(y) * x + x ^ 2;
a = S * T;
b = double(subs(a, {x,y}, {5,4}));
disp(a);
disp(b);

%% Exercise 3
syms x;
a = diff(poly2sym(poly([-3, 1, -0.5, 2, 4])));
disp(a);
b = factor(x^6 - 2*x^5 - 39*x^4 + 20*x^3 + 404*x^2 + 192*x - 576);
disp(b);

%% Exercise 4
syms x;
f = (x^2 - 4*x) * (x^2 - 4*x + 1) - 20;
a = expand(f);
b = f / (x^2 - 4*x + 4);
c = factor(f);
d = solve(c(2));
e = simplify(subs(f, d));
disp(a);
disp(b);
disp(c);
disp(d);
disp(e);

%% Exercise 5
syms x;
a = sin(3*x) - 3*sin(x) + 4*sin(x)^3;
b = (1/2)*sin(6*x) - (3*sin(x) - 4*sin(x)^3)*(4*cos(x)^3 - 3*cos(x));
disp(simplify(a));
disp(simplify(b));

%% Exercise 6
syms x y z;
a = tan(3*x) - (3*tan(x) - tan(x)^3)/(1 - 3*tan(x)^2);
b = sin(x+y+z) - sin(x)*cos(y)*cos(z) - cos(x)*sin(y)*cos(z) - cos(x)*cos(y)*sin(z) + sin(x)*sin(y)*sin(z);
disp(simplify(a));
disp(simplify(b));

%% Exercise 7
% The Hill equation
syms a b T T0 v;
H = (T + a)*(v + b) - (T0 + a)*b;
vmax = solve(subs(H, T, 0), v);
disp(H);
disp(vmax);

v2 = simplify(solve(subs(H, b, (vmax * a) / T0), v));
disp(v2);
disp(simplify(v2 - ((a * (T0 - T)) / (T0 * (T + a))) * vmax));

%% Exercise 8
syms x;
S = (6*sin(x)^2) / (3*sin(x) + 1)^2;
figure;
ezplot(S, [0, pi]);

I = int(S, 0, pi);
disp(I);

%% Exercise 9
syms x y;
e1 = (x - 1)^2/6^2 + y^2/3^2 - 1;
e2 = (x + 2)^2/2^2 + (y-5)^2/4^2 - 1;
figure;
ezplot(e1);
hold on;
ezplot(e2);

[x, y] = solve(e1, e2, x, y);
x = double(x);
y = double(y);

fprintf("The coordinates at which the two ellipses intersect are (%.2f, %.2f) and (%.2f, %.2f)\n",...
    x(1), y(1), x(2), y(2));

%% Exercise 10
syms x y(x);
Dy = diff(y);
y = dsolve(Dy == exp(y)*cos(x));
disp(y);
disp(simplify(diff(y) - cos(x)*exp(y)));

%% Exercise 11
% Spread of an infection
syms t x(t) R N;
Dx = diff(x);
x = dsolve(Dx == -R*x*(N + 1 - x), x(0) == N);
disp(x);
dx = diff(x, t, 2);
disp(solve(dx, t));

%% Exercise 12
% Damped free vibrations
syms m c k t x(t);
Dx = diff(x);
DDx = diff(x, t, 2);
x = dsolve(m*DDx + c*Dx + k*x == 0, x(0) == 0.18, Dx(0) == 0);
v = diff(x);

% Case (a)
figure;
ezplot(subs(x, {m, c, k}, {10, 3, 28}), [0,20]);
hold on;
ezplot(subs(v, {m, c, k}, {10, 3, 28}), [0,20]);

% Case (b)
figure;
ezplot(subs(x, {m, c, k}, {10, 50, 28}), [0,10]);
hold on;
ezplot(subs(v, {m, c, k}, {10, 50, 28}), [0,10]);

%% Exercise 13
syms x n;
f = sin(x) / x;
disp(limit(f, x, 0));

an = (-1)^n / (n^2 + 1);
disp(limit(an, n, Inf));

f = acos((1 - sqrt(x)) / (1 - x));
disp(limit(f, x, 1, 'left'));
disp(limit(f, x, 1, 'right'));
disp('It is continuous provided that you have f(1) = 1.');

Sn = n^2 / 2^n;
disp(symsum(Sn,n,0,Inf));
