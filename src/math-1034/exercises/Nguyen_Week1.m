%% Week 1 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-03-16
%

%% Exercise 1
x = [1 2 3 4 5];
y = [2 4 6 8 10];

z = ((x + y) .^ 2) ./ (x - y);
w = x .* log(x .^ 2 + y .^ 2) + sqrt((y .^ 3) ./ ((y - x) .^ 2));

disp(z);
disp(w);

%% Exercise 2
% Determine the angle between two vectors by using the dot product
r1 = [6 -3 2];
r2 = [2 9 10];
angle = acosd((dot(r1, r2)) ./ (sqrt(dot(r1, r1)) * sqrt(dot(r2, r2))));

disp(angle);

%% Exercise 3
crossprod = @(u, v) [u(2) * v(3) - u(3) * v(2), u(3) * v(1) - u(1) * v(3), u(1) * v(2) - u(2) * v(1)];

i = [1 0 0];
j = [0 1 0];
k = [0 0 1];

disp("crossprod");
disp(crossprod(i, j));
disp(crossprod(j, k));

disp("cross");
disp(cross(i, j));
disp(cross(j, k));

%% Exercise 4
% Area of a triangle
area = @(A, B, C) (sqrt(dot(cross(B - A, C - A), cross(B - A, C - A))) / 2);

A = [0 0 0];
B = [0 1 0];
C = [0 0 1];

disp(area(A, B, C));

%% Exercise 5
a = [7 -4 6];
b = [-4 7 5];
c = [5 -6 8];

disp(cross(a, cross(b, c)));
disp(b .* dot(a, c) - c .* dot(a, b));

%% Exercise 6
% Gosper's approximation for factorials
gospers = @(n) sqrt(((2 * n) + (1 / 3)) * pi) * (n ^ n) * exp(-n);

n = 19;
trueval = factorial(n);
approxval = gospers(n);
error = ((trueval - approxval) / trueval);

fprintf("Approximate value: %.4f.\n", approxval);
fprintf("True value: %d.\n", factorial(n));
fprintf("Error: %f.\n", error);

%% Exercise 7
% Volume of liquid in a tank
a = 8;
d = 6;
h = 12;
r = d / 2;
H = a + (r * 2);

% Cubic inches to liters
incu2liters = @(x) x / 61.024;

% Volume functions
vcyl = @(r, h) pi * r ^ 2 * h;
vsph = @(r) (4 / 3) * pi * r ^ 3;
vsphcap = @(r, h) ((pi * h ^ 2) / 3) * ((3 * r) - h);
tankvol = @(r, h) vcyl(r, h - d) + vsph(r);
Vtank = incu2liters(tankvol(r, H));

if h > 0 && h <= r
    % Spherical cap
    V = vsphcap(r, h);
elseif h > r && h <= H - r
    % Hemisphere + cylinder
    V = (vsph(r) / 2) + vcyl(r, h - r);
elseif h > H - r && h <= H
    % Sphere + cylinder
    V = vsph(r) + vcyl(r, H - d) - vsphcap(r, H - h);
else
    V = 0;
end

V = incu2liters(V);

fprintf('The volume of the liquid at height %d inches in a tank (height = %d, radius = %d, volume = %.4f) is %.4f liters.\n', h, H, r, Vtank, V);

%% Exercise 8
% Quadratic formula
quad = @(x) (-x(2) + sqrt((x(2) ^ 2) - (4 * x(1) * x(3)))) / (2 * x(1));

a = 1;
b = 5;
c = 4;
x1 = quad([a b c]);
x2 = c / (a * x1); % Viet's formula

disp([x2; x1]);
disp(roots([a b c]));

%% Exercise 9
x = linspace(0, 10, 100);
y = exp(-0.5 * x) .* sin(x);

figure;
plot(x, y, 'b', 'LineWidth', 2);

hold on;
y = exp(-0.5 * x) .* cos(2 * x);
plot(x, y, '--r', 'LineWidth', 3);

legend('\(y(x) = e^{-0.5x} \sin(x)\)',...
       '\(y(x) = e^{-0.5x} \cos(2x)\)',...
       'Interpreter', 'LaTeX');
title('Two functions plotted on the same figure');
xlabel('\(x\)', 'Interpreter', 'LaTeX');
ylabel('\(y\)', 'Interpreter', 'LaTeX');

%% Exercise 10
x = linspace(-2, 8, 200);
y = ((x .^ 2) - (6 * x) + 5) ./ (x - 3);

figure;
plot(x, y);
axis([-2 8 -10 10]);

%% Exercise 11
% Butterfly curve
t = linspace(0, 2 * pi, 360);
x = @(t) sin(t) .* (exp(cos(t)) - (2 * cos(4 * t)) - (sin(t / 12) .^ 5));
y = @(t) cos(t) .* (exp(cos(t)) - (2 * cos(4 * t)) - (sin(t / 12) .^ 5));

figure;
plot(x(t), y(t));

hold on;
t = linspace(0, 10 * pi, 3600);
plot(x(t), y(t));

%% Exercise 12
% Plot of an astroid
t = linspace(0, 2 * pi, 360);
x = @(t) cos(t) .^ 3;
y = @(t) sin(t) .^ 3;
f = @(x, y) (x .^ 2) .^ (1 / 3) + (y .^ 2) .^ (1 / 3) - 1; 

figure;
plot(x(t), y(t));

hold on;
fimplicit(f);

%% Exercise 13
% Shape of a heart
t = linspace(-1, 1, 360);
y = @(t) [sqrt(1 - t .^ 2) + (t .^ 2) .^ (1 / 3)
          sqrt(1 - t .^ 2) + (t .^ 2) .^ (1 / 3)];
f = @(x, y) x .^ 2 + (y - (x .^ 2) .^ (1 / 3)) .^ 2 - 1;

figure;
plot(t, y(t), 'r');

hold on;
fimplicit(f, 'r');

%% Exercise 14
% Shape of a pretzel
t = linspace(-4, 4, 360);
x = @(t) (3.3 - 0.3 * t .^ 2) .* cos(t);
y = @(t) (3.3 - 0.4 * t .^ 2) .* sin(t);

figure;
plot(x(t), y(t));

%% Exercise 15
% Contributions to going-away present
contrib = [15 5 10 5 15];
percent = contrib * 100 / sum(contrib);
labels = {'George', 'Sam', 'Betty', 'Charlie', 'Suzie'};

fprintf('Sam contributed %d%%.\n', percent(2));

figure;
pie(contrib);
legend(labels);

%% Exercise 16
% What does this mystery function do
% function outvar = mystery(x, y)
%     if y == 1
%         outvar = x;
%     else
%         outvar = x + mystery(x, y - 1);
%     end
% end
disp('This function returns the product of x and y.');
disp('For example,');
fprintf('mystery(%d, %d) = %d\n', 4, 5, mystery(4, 5));
fprintf('mystery(%d, %d) = %d\n', 12, 12, mystery(12, 12));

%% Exercise 17
% Fibonacci numbers
n = 30;
tic;
fprintf('fib(%d) = [ %s]\n', n, sprintf('%d ', fib(n)));
toc;

tic;
fprintf('fibrecur(%d) = [ ', n);
for i = 1:n
    fprintf('%d ', fibrecur(i));
end
fprintf(']\n');
toc;

%% Exercise 18
% Collatz sequence
seq = collatz(1000);
fprintf('Collatz sequence (n = 1000): [ %s].\n', sprintf('%d ', seq));

figure;
plot(seq);

%% Exercise 19
% Triangular numbers
nums = trinums(5);
fprintf('Triangular numbers (m = 5): [ %s].\n', sprintf('%d ', nums));

%% Exercise 20
% Fern plot
p = [0.85 0.92 0.99 1.00];
A1 = [0.85 0.04; -0.04 0.85]; b1 = [0; 1.6];
A2 = [0.20 -0.26; 0.23 0.22]; b2 = [0; 1.6];
A3 = [-0.15 0.28; 0.26 0.24]; b3 = [0; 0.44];
A4 = [0 0; 0 0.16];

val = rand;
n = 100000;

x = zeros(n, 1);
x(1) = 0.5;
y = zeros(n, 1);
y(1) = 0.5;

for i = 1:n
    if (val < p(1))
        tmp = A1 * [x(i); y(i)] + b1;
    elseif (val < p(2))
        tmp = A2 * [x(i); y(i)] + b2;
    elseif (val < p(3))
        tmp = A3 * [x(i); y(i)] + b3;
    else
        tmp = A4;
    end
    x(i + 1) = tmp(1);
    y(i + 1) = tmp(2);
    val = rand;
end

figure;
plot(x, y, 'go', 'LineWidth', 1);

%% Exercise 21
% Sierpenski's triangle
p = [0.25 0.5 0.75 1.00];
A = [0.5 0
     0 0.5];
b1 = [0; 0];
b2 = [0.5; 0];
b3 = [0.25; sqrt(3) / 4];
val = rand;
n = 100000;

x = zeros(n, 1);
y = zeros(n, 1);

for i = 1:n
    if (val < p(1))
        tmp = A * [x(i); y(i)] + b1;
    elseif (val < p(2))
        tmp = A * [x(i); y(i)] + b2;
    elseif (val < p(3))
        tmp = A * [x(i); y(i)] + b3;
    else
        tmp = A;
    end
    x(i + 1) = tmp(1);
    y(i + 1) = tmp(2);
    val = rand;
end

figure;
plot(x, y, 'go', 'LineWidth', 1);

%% Functions
function outvar = mystery(x, y)
    if y == 1
        outvar = x;
    else
        outvar = x + mystery(x, y - 1);
    end
end

function y = fib(n)
    if n == 2
        y = [1 1];
    elseif n == 1
        y = [1];
    elseif n == 0
        y = [0];
    elseif n < 0
        y = [];
    else
        x = [1 1 2];
        for i = 3:n
            x(i) = sum(x(i - 1) + x(i - 2));
        end
        y = x;
    end
end

function y = fibrecur(n)
    if n == 0
        y = 0;
    elseif n == 1
        y = 1;
    else
        y = fibrecur(n - 2) + fibrecur(n - 1);
    end
end

function seq = collatz(n)
    seq = [];
    while n > 1
        if mod(n, 2) == 0
            n = n / 2;
        else
            n = (3 * n) + 1;
        end
        seq = [seq n];
    end
end

function nums = trinums(m)
    tmp = [];
    for n = 1:(m * (m + 1) / 2)
        tmp = [tmp, n * (n + 1) / 2];
    end
    nums = tmp;
end