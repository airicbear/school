function nums = trinums(m)
    tmp = [];
    for n = 1:(m * (m + 1) / 2)
        tmp = [tmp, n * (n + 1) / 2];
    end
    nums = tmp;
end