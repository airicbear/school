%% Week 2 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-03-19
%

%% Exercise 1
A = [ 1 4 1
      1 3 2
     -1 2 7];
B = [ 1 0  1
      2 5 12
     -9 1  1];

disp((A * B) ^ (-1));
disp(B ^ (-1) * A ^ (-1));

%% Exercise 2
A = [ 1 3  -1 -9
      0 3   0  1
     12 8 -11  0
      2 1   5  3];

disp(inv(A));

%% Exercise 3
% Equation 1
A = [2  1
     3 -9];
b = [5
     7];

disp(A \ b);

% Equation 2
A = [12 -5 0
     -3  4 7
      6  2 3];
b = [11
     -3
     22];

disp(A \ b);

% Equation 3
A = [ 6 -3  4
     12  5 -6
     -5  2  6];
b = [ 41
     -26
      16];

disp(A \ b);

%% Exercise 4
A = [1 -5 -2;
     6  3  1;
     7  3 -5];

b = [11;
     13;
     10];

f = @(s) A \ (b * s);
c = linspace(-10, 10, 1000);
cx = zeros(length(c), 1);
cy = zeros(length(c), 1);
cz = zeros(length(c), 1);

for i = 1:length(c)
    tmp = f(c(i));
    cx(i) = tmp(1);
    cy(i) = tmp(2);
    cz(i) = tmp(3);
end

figure;
hold on;
plot(c, cx);
plot(c, cy);
plot(c, cz);

%% Exercise 5
% Find coefficients of cubic polynomial passing 4 points
px = [1 2 4 5];
py = [6 38 310 580];

% a * x^3 + b * x^2 + c * x + d
% a * 1^3 + b * 1^2 + c * 1 + d = 6
% a * 2^3 + b * 2^2 + c * 2 + d = 38
% a * 4^3 + b * 4^2 + c * 4 + d = 310
% a * 5^3 + b * 5^2 + c * 5 + d = 580
A = [px(1)^3 px(1)^2 px(1) 1
     px(2)^3 px(2)^2 px(2) 1
     px(3)^3 px(3)^2 px(3) 1
     px(4)^3 px(4)^2 px(4) 1];
b = [6; 38; 310; 580];
c = A \ b;
x = linspace(0, 6, 100);
y = c(1) * x .^ 3 + c(2) * x .^ 2 + c(3) * x + c(4);

figure;
hold on;
plot(px, py, 'or', 'MarkerFaceColor', 'red');
plot(x, y, 'k');

%% Exercise 6
% Alice, Bob and Carol buy fruits. Find each fruit's price.
% Alice:  3a 12b 1c = 2.36
% Bob:   12a  0b 2c = 5.26
% Carol:  0a  2b 3c = 2.77
A = [ 3 12 1
     12  0 2
      0  2 3];
b = [2.36; 5.26; 2.77];
c = A \ b;
format bank
fprintf("Apples cost $%.2f. Bananas cost $%.2f. Cantaloupes cost $%.2f.\n", c(1), c(2), c(3));

%% Exercise 7
% Kirchoff's voltage law
% i4 = i1 - i2
% i5 = i2 - i3
R = [5 100 200 150 250 250000];
v = [100 50];
A = [(R(1) + R(4))                -R(4)            0
             -R(4) (R(2) + R(4) + R(5))         -R(5)
                 0                -R(5) (R(3) + R(5))];
b = [v(1); 0; -v(2)];
i = A \ b;
fprintf('i1 = %0.2f.\n', i(1));
fprintf('i2 = %0.2f.\n', i(2));
fprintf('i3 = %0.2f.\n', i(3));
fprintf('i4 = %0.2f.\n', i(1) - i(2));
fprintf('i5 = %0.2f.\n', i(2) - i(3));

%% Exercise 8
% Hilbert matrices
n = 100; % Warning: takes a few seconds
format shortg
A = double(inv(sym(hilb(n))));
B = inv(hilb(n));
disp(max(max(abs(A - B))));
disp('The Hilbert matrix is highly sensitive to floating-point arithmetic because it involves very precise floating point numbers.')

%% Exercise 9
A = [3  5  6
     1 -2 -2];
b = [4; 10];
disp(pinv(A) * b);
disp(A \ b);

%% Exercise 10
c = [9 10];
A = [1 1
     1 2
     1 5];
b1 = [1; 3; c(1)];
b2 = [1; 3; c(2)];

% c = 9
% Exact solution
c1 = A \ b1;
disp(c1);
disp(A * c1 - b1);

% c = 10
% No solution, but the solution has smallest magnitude for Ac - b
c2 = A \ b2;
disp(c2);
disp(A * c2 - b2);
