%% Week 6 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-04-18
%

%% Exercise 1
user_input = input('Enter a string: ', 's');
c = input('Enter a character in the string: ', 's');
fprintf('The letter %s occurred %d times in the string "%s."\n', c, length(strfind(user_input, c)), user_input);

%% Exercise 2
sentence = input('Enter a sentence: ', 's');
disp(sort(strsplit(sentence)));

%% Exercise 3
strs = input('Enter a series of strings: ', 's');
disp(sort(strsplit(lower(strs))));

%% Exercise 4
str = input('Enter a string: ', 's');
head = upper(str(1));
rest = lower(str(2:end));
disp(strcat(head, rest));

%% Exercise 5
% Logical array - alphanumeric
str = input('Enter a string: ', 's');
disp(isstrprop(str, 'alphanum') == 0 & isspace(str) == 0);

%% Exercise 6
% Logical array - vowels
str = input('Enter a string: ', 's');
disp(ismember(str, 'aeiou'));

%% Exercise 7
str = input('Enter a string: ', 's');
n = input('Enter a number: ');
while (n > 127)
    n = 32 + (n - 127);
end
new_str = str;
for i = 1:length(str)
    if (new_str(i) + n > 127)
        new_str(i) = 32 + (mod(new_str(i) + n - 32, 127 - 32));
    else
        new_str(i) = new_str(i) + n;
    end
end
disp(new_str);

%% Exercise 8
M = cellstr(char(randi([32, 127], 10, 50)));
disp(M);
disp(newline);
for i=1:length(M)
    for j=1:length(M{i})
        if (M{i}(j) >= 'A' && M{i}(j) <= 'Z')
            M{i}(j) = '#';
        end
    end
end
disp(M);

%% Exercise 9
% Determine if a given email is registered under the '.edu' domain
str = input('Enter an email address: ', 's');
email = strtok(str);
domain = email((strfind(email, '@') + 1):end);
tld = domain((strfind(domain, '.') + 1):end);
if (strcmpi(tld, 'edu'))
    disp('EDU');
else
    disp('NON-EDU');
end

%% Exercise 10
str = input('Enter a short sentence: ', 's');
disp(strrep(str,' ','-'));

%% Exercise 11
names = {'Harry', 'Xavier', 'Sue'};
verbs = {'loves', 'eats'};
nouns = {'baseballs', 'rocks', 'sushi'};
fprintf('%s %s %s\n', names{randi(length(names))},...
        verbs{randi(length(verbs))}, nouns{randi(length(nouns))});

%% Exercise 12
v = {'xyz', 33.3, 2:6, 'a' < 'c'};
celldisp(reshape(transpose(v), 2, 2));

%% Exercise 13
% Distance between two points
point = struct('x', [], 'y', []);
point(1).x = input('Point 1 x: ');
point(1).y = input('Point 1 y: ');
point(2).x = input('Point 2 x: ');
point(2).y = input('Point 2 y: ');
d = sqrt((point(2).x - point(1).x)^2 + (point(2).y - point(1).y)^2);
fprintf('The distance between the points (%d, %d) and (%d, %d) is %.2f.\n',...
        point(1).x, point(1).y, point(2).x, point(2).y, d);

%% Exercise 14
s = input('Enter a struct: ');
if isstruct(s)
    disp(fieldnames(s));
else
    disp('Error: input not a struct');
end

%% Exercise 15
imaginary.r = input('Enter the real part: ', 's');
imaginary.i = input('Enter the imaginary part: ', 's');
fprintf('The complex number is %s + i%s\n', imaginary.r, imaginary.i);

%% Exercise 16
p = input('Enter a directory path: ', 's');
d = dir(p);
fprintf("The total size of all files in the directory %s is %d.\n",...
        p, sum([d.bytes]));

%% Exercise 17
% Perform computations on data from a file

% Write to data.txt
data = [55 42 98
        51 39 95
        63 43 94
        58 45 90];
fid = fopen('data.txt', 'w');
for r=1:length(data)
    fprintf(fid, '%d %d %d\n', data(r,:));
end
fclose(fid);

% Read from data.txt
fid = fopen('data.txt', 'r');

c1 = [];
c2 = [];
c3 = [];

while (~feof(fid))
    c1 = [c1; fscanf(fid, '%2d', 1)];
    c2 = [c2; fscanf(fid, '%2d', 1)];
    c3 = [c3; fscanf(fid, '%2d', 1)];
end
fclose(fid);

disp('Means of columns:');
disp([mean(c1) mean(c2) mean(c3)]);

A = [c1 c2 c3];
disp('Sums of columns:');
disp([sum(A(:,1)) sum(A(:,2)) sum(A(:,3))]);

%% Exercise 18
% Read data from a biomedical experiment

% Write the data to 'patwts.dat'
patwts = {'Darby' 'George' 166.2
          'Helen' 'Dee' 143.5
          'Giovanni' 'Lupa' 192.4
          'Cat' 'Donovan' 215.1};
fid = fopen('patwts.dat', 'w');
for r=1:length(patwts)
    fprintf(fid, "%s %s %.1f", patwts{r,:});
    if (r < length(patwts))
        fprintf(fid, '\n');
    end
end
fclose(fid);

% Read the data from 'patwts.dat'
fid = fopen('patwts.dat', 'r');

firstname = {};
lastname = {};
weight = {};
i = 1;

while (~feof(fid))
    str = fscanf(fid, '%s', 1);
    firstname{i} = str;
    str = fscanf(fid, '%s', 1);
    lastname{i} = str;
    val = fscanf(fid, '%f', 1);
    weight{i} = val;
    i = i + 1;
end
fclose(fid);

data = {firstname; lastname; weight};
for i=1:length(data{1})
    fprintf('%s, %s %.1f\n', data{2}{i}, data{1}{i}, data{3}{i});
end
fprintf('The average weight is %.2f.\n', mean([data{3}{:}]));
disp('File close successful');

%% Exercise 19
x = input('Enter a vector for x: ');
y = input('Enter a vector for y: ');
z = input('Enter a vector for z: ');
fid = fopen('points.txt', 'w');
for i=1:min([length(x) length(y) length(z)])
    fprintf(fid, '%s %.1f %s %.1f %s %.1f\n', 'x',...
            x(i), 'y', y(i), 'z', z(i));
end
fclose(fid);

%% Exercise 20
% Population data spreadsheet
Year = [1920; 1940; 1960; 1980; 2000];
Population = [4021; 8053; 14994; 9942; 3385];
data = table(Year, Population);
writetable(data, 'popdata.xls');
data = readtable('popdata.xls');
figure;
plot(data.Year, data.Population, '-o', 'MarkerFaceColor', 'b');
title('Population of a small town over time');
xlabel('Year');
ylabel('Population');
grid on;
