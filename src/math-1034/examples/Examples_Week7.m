%% Week 7 Examples (Data Analysis and Statistics, Images and Animation)
%
% Author: Eric Nguyen
%
% Date: 2020-04-23
%

%% Statistical functions
A = [12 8 18
     15 9 22
     12 5 19
     14 8 23
     12 6 22
     11 9 10];

x = A(:,1);
disp(x');
disp(mean(x));
disp(median(x));
disp(mode(x));
disp(range(x));
disp(std(x));
disp(var(x));
[max_val, max_loc] = max(x);
[min_val, min_loc] = min(x);
disp(sum(x));
disp(cumsum(x)');
disp(prod(x));
disp(cumprod(x));
z = [22 1 8 35 44 9];
[x,j] = sort(z);
[x,j] = sort(z, 'ascend');
[x,j] = sort(z, 'descend');
disp(diff(x));
disp(trapz(z));

x = A(:,1);
y = filter(ones(1,4), 4, x);

%% Histogram
y=[92 94 93 96 93 94 95 96 91 93 95 95 95 92 93 94 91 94 92 93];
histogram(y);

%% Bar plot
m = 100;
x = 91:96;
y = [13, 15, 22, 19, 17, 14]/m;
bar(x,y);

%% Other way
m = 100;
y = [91*ones(1,13)...
     92*ones(1,15)...
     93*ones(1,22)...
     94*ones(1,19)...
     95*ones(1,17)...
     96*ones(1,14)];

x = 91:96;
[z,x] = hist(y,x);

%% Monte Carlo
n = 1000;
x = rand(n, 1);
y = rand(n, 1);
is_inside = (x .^ 2 + y .^2 < 1);
percentage = sum(is_inside) / n;
pi_estimate = percentage * 4;

%% Construct a binary image
A = eye(4);

figure;
imshow(imresize(A, 128, 'nearest'));

%% Gray scale image
A = imread('parrot.jpg');

figure;
imshow(A);

C = rgb2gray(A);
figure;
imshow(C);

figure;
imagesc(C);

%% Size of the image
A = imread('parrot.jpg');
[M,N] = size(A);
C = imresize(A, [M/2, N/5]);
imshow(C);

%% RGB image
A = imread('parrot.jpg');
disp(size(A));
disp(A(1,1,:));

%% Converting an RGB image to half grey
A = imread('parrot.jpg');
B = rgb2gray(A);
[M,N] = size(B);
[x,y] = meshgrid(1:N, 1:M);
idx = (x - 1) / (N - 1) - (y - M) / (1 - M) < 0;
r = A(:,:,1); r(idx) = B(idx);
g = A(:,:,2); g(idx) = B(idx);
b = A(:,:,3); b(idx) = B(idx);

C = cat(3,r,g,b);

figure;
subplot(1,2,1);
imshow(A);
title('rgb image');

subplot(1,2,2);
imshow(C);
title('grey+rgb image');

%% ASCII picture
I = imread('parrot.jpg');
[M,N] = size(I);
I = imresize(I, 0.05);
imshow(I);
A = rgb2gray(I);
imshow(A);
S = '#n*:. ';
C = rgb2ind(I, length(S));
disp(S(C+1));

%% Animation
figure;
hold on;
h1 = plot(0, 0, 'r.', 'MarkerSize', 100);
h2 = plot(0, 0, 'b.', 'MarkerSize', 100);
h3 = plot(0, 0, 'g.', 'MarkerSize', 100);
plot([0 100 100 0 0], [0 0 100 100 0], 'k-');
axis([-3 103 -3 103]);

axis square off;


for i=1:100
    set(h1, 'XData', i/2, 'YData', i/2);
    set(h2, 'YData', i);
    set(h3, 'XData', i);
    pause(0.02);
end

hold off;

for i=1:100
    set(h1, 'XData', 50+i/2, 'YData', 50+i/2);
    set(h2, 'XData', i);
    set(h3, 'YData', i);
    pause(0.02);
end
