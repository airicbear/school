%% Examples Week 1
%
% Author: Eric Nguyen
%

%% Vectors and matrices

%% Definition of vectors and matrices

x = [1 2 3];
y = [4; 5; 6];
z = [1 2 3
     4 5 6];
t = 1:4;
u = 0:0.1:1;

%% Particular matrices

I = eye(5, 5);
X = zeros(5, 3);
Y = zeros(size(y));
A = ones(6, 2);

%% Coefficients access

z(2, 2)
x(2:3)
z(1:1, 1:2)
u(t)
z(t(1:2), x(2))
z(3)
z(2, :)
z(:, 2)

%% Vector operations

z'
x+y'
z*y

%% Element-wise operations

x .^ 2
x ./ y'
1 ./ x
z + 1

%% Matrix operations

A = [1 2 3
     0 0 1
     1 0 0];
B = [-1 -2 -3
      0  0 -1
     -1  0  0];

A * B
A .* B
A ^ 2
A ^ (-2)
A'

%% Functions on matrices

sum(A, 1)
prod(A, 1)
size(A, 1)
reshape(A, 9, 1)

%% Example 1

A = ones(4) * 2;
B = ones(4) * 2;
A(1, 1) = 1;
A(2, 2) = 1;
A(3, 3) = 1;
A(4, 4) = 1;
disp(A);
disp(B);

% repmat(1:4, 4, 1);

%% Graphics

%% Graphs of functions

x = linspace(0, 2 * pi, 100);
y = sin(x);
z = cos(x);

plot(x, y);
loglog(x, abs(y));
semilogx(x, y);
semilogy(x, y);

plot(x, z);
hold on;
plot(x, y);
clf;
plot(x, y, x, z);
clf;
plot(x, y, 'r', x, z, 'b--');

% Text and legends

legend('sinus', 'cosinus');
xlabel('x');
ylabel('f(x)');
title('Trigonometric functions');

% Customizing axis

axis off;
axis on;
axis equal
grid on;

% Partitioning windows

figure;
subplot(1, 3, 1);
plot(x, y);
subplot(1, 3, 2);
plot(x, y, 'r--');
subplot(1, 3, 3);
plot(x, y, 'go');

%% MATLAB programming

%% Script execution

script

%% Functions execution

f([1 2 3])

%% Anonymous functions

f = @(x) x .^ 3;

a = 1;
b = 2;
c = 1;
pol = @(x) (a * x ^ 2) + (b * x) + c;
pol(2)
a = 10;
pol(2)
