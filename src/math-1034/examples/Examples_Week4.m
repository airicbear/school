%% Week 4 Examples
%
% Author: Eric Nguyen
%
% Date: 2020-03-31
%

%% Solving an equation with one variable
figure;
fplot(@(x) x .* exp(-x) - 0.2, [0 8]);
hold on;
fplot(@(x) 0 + 0 * x, [0 8], '--');

% first root
x1 = fzero(@(x) x * exp(-x) - 0.2, 0.9);
disp(x1);

% second root
x2 = fzero(@(x) x * exp(-x) - 0.2, 4);
disp(x2);

%% Finding a minimum or maximum of a function
f = @(x) x.^3 - 12 .* x.^2 + 40.25 .* x - 36.5;
[xmin, fval] = fminbnd(f, 3, 8);

figure;
fplot(f, [0 8]);
hold on;
plot(xmin, fval, 'o');

%% Max
f = @(x) x .* exp(-x) - 0.2;
g = @(x) -f(x);
[xmax, gval] = fminbnd(g, 0, 8);
fval = -gval;

figure;
fplot(f, [0 8]);
hold on;
plot(xmax, fval, 'o');

%% Integration
f = @(x) sqrt(1 - x.^2);
disp(integral(f, -1, 1));

%% Integration with Trapezoidal Method
x = linspace(-1, 1, 1000);
y = sqrt(1 - x.^2);
disp(trapz(x, y));

%% Solve first-order ODE
f = @(x,y) (x.^3 - 2*y) / x;
y0 = 4.2;
[x,y] = ode45(f, 1:0.1:3, y0);

figure;
plot(x,y,'--r');
xlabel('x');
ylabel('y');
title('Numerical approximation of y(x)');
