%% Week 3 Examples
%
% Author: Eric Nguyen
%
% Date: 2020-03-26
%

%% Polynomials

my_poly = [1 2 3 4];
disp(polyval(my_poly, 3));

x = [1 -1 2 5 6 9];
disp(polyval(my_poly, x));

%% Symbolic polynomials
disp(poly2sym(my_poly));

%% Plotting polynomials
x = linspace(0, 2, 100);
my_poly = [6 3 -7 0.4];
y = polyval(my_poly, x);

figure;
plot(x, y);
grid on;

% Find roots
zeros = roots(my_poly);
disp(zeros);

figure;
stem(zeros);
title('Stem plot for the zeros of the polynomial');

%% Polynomial exact fitting
x = 0:3;
y = [-5 -6 -1 16];
V = vander(x);
a = V \ y';
disp(polyval(a, x));

%% Polynomial best fitting
x = [0 1 2 3 4 5 6];
y = [0 1 3.3 2.2 5.6 4.4 0];

figure;
plot(x, y, '*k');

p = polyfit(x, y, 3);
x1 = linspace(0, 7, 100);
y1 = polyval(p, x1);

hold on;
plot(x, y, '*r', x1, y1);

%% Interpolation and splines fitting
figure;
x = 1:6;
y = [1 0 4.4 0 5.5 0];
x1 = linspace(1, 6, 100);

subplot(1, 3, 1);
y1 = interp1(x, y, x1, 'linear');
plot(x, y, '*r', x1, y1);

subplot(1, 3, 2);
y2 = interp1(x, y, x1, 'nearest');
plot(x, y, '*r', x1, y2);

subplot(1, 3, 3);
y3 = interp1(x, y, x1, 'pchip');
plot(x, y, '*r', x1, y3);

figure;
y1 = spline(x, y, x1);
plot(x, y, '*r', x1, y1);
