* What might you want to communicate about with your professor through email?

  1. I won't be in class tomorrow.
  2. A question that might have a short answer.
  3. Set up an office hour.
  4. What material will be on the test? 
  5. Group project arrangements
  6. HW due date extension
  7. I want a better grade
  8. I don't understand the material
  9. I need a letter of recommendation

* "Good" Email

  Hi Dr. Sivek,

  Could you please clarify for me whether we will have to memorize the Quotient Rule for the upcoming exam?

  Thanks,
  Eric

* "Bad" Email

  Yo Jeremy, what's the test gonna be on?

  - Nate

* Good Office Hours

  - Specific Questions
  - Reasonable Time
  - Polite + _Patient_
