%% Week 3 Examples
%
% Author: Eric Nguyen
%

%% Example 1
figure;
x = 0:6;
y = [0 1 0 -1 0 1 0];
p = plot(x, y);

%% Example 2
figure;
hold on;
plot(x, y, 'k');
plot(x, y, 'or');
plot(x, y, '-.', 'Color', [165/255,42/255,42/255]);

%% Example 3
figure;
hold on;
plot(x, y, 'k', 'LineWidth', 3);
plot(x, y, 'or', 'MarkerFaceColor', 'r', 'MarkerSize', 30);

%% Example 4
figure;
x = 0:7;
y = sin((pi / 2) * x);
plot(x, y, 'k', 'LineWidth', 2);

figure;
x = 0:0.5:7;
y = sin((pi / 2) * x);
plot(x, y, 'k', 'LineWidth', 2);

figure;
x = 0:0.1:7;
y = sin((pi / 2) * x);
plot(x, y, 'k', 'LineWidth', 2);

figure;
hold on;
x = 0:0.01:7;
y = sin((pi / 2) * x);
plot(x, y, 'k', 'LineWidth', 2);

y = cos((pi / 2) * x);
plot(x, y, '--r', 'LineWidth', 2);

legend('sin(x)', 'cos(x)');
%% Example 5
t = 0:0.01:7;
y1 = cos(t);
y2 = -(1 / 3) * exp(-2 * t);
y3 = (t + 1) .* exp(-t);
y4 = (2 / sqrt(3)) .* exp(-t / 2) .* cos(((sqrt(3) / 2) * t) - (pi / 6));

figure;
hold on;
grid on;
title('Solutions of Spring-mass Equation', 'Interpreter', 'latex', 'FontSize', 16);
xlabel('$t$', 'Interpreter', 'latex');
ylabel('Position', 'Interpreter', 'latex', 'FontSize', 16);
plot(t, y1, 'Color', 'k',                 'LineWidth', 2);
plot(t, y2, 'Color', 'b',                 'LineWidth', 2);
plot(t, y3, 'Color', [128/255,0,128/255], 'LineWidth', 2, 'LineStyle', '--');
plot(t, y4, 'Color', 'r',                 'LineWidth', 2);

legend('$$\cos(t)$$',...
    '$$-\frac{1}{3} e^{-2t}$$',...
    '$$(t + 1) e^{-t}$$',...
    '$$\frac{2}{\sqrt{3}} e^{-t / 2} \cos{\left(\frac{\sqrt{3}}{2} t - \frac{\pi}{6}\right)}$$',...
    'Interpreter', 'latex');
axis([-0.5, 7.5, -1.5, 1.5]);

%% Example 6
x = 0.1:0.1:100;
y = 10 .^ (-x + 2);

figure;
plot(x, y);

figure;
semilogy(x, y);

figure;
semilogx(x, y);

figure;
loglog(x, y);

figure;
hold on;
subplot(2,2,1);
plot(x, y);
subplot(2,2,2);
semilogy(x, y);
subplot(2,2,3);
semilogx(x, y);
subplot(2,2,4);
loglog(x, y);

%% Example 7
t = 0:0.1:pi;
x = 0.7 * sin(10 * t);
y = 1.2 * sin(8 * t);
plot(x, y);
axis([-1.5, 1.5, -1.5, 1.5]);

%% Interacting with Scripts Examples

%% Example 1
a = input('a: ');
b = input('b: ');
c = input('c: ');

d = b ^ 2 - 4 * a * c;
x1 = (-b + sqrt(d)) / (2 * a);
x2 = (-b - sqrt(d)) / (2 * a);
fprintf('\nRoots of coefficients a = %d, b = %d, and c = %d:\n\n', a, b, c);
disp(x1);
disp(x2);

%% Example 2
t = input('Total points: ');
e = input('Earned points: ');
r = (e / t) * 100;
disp(r);

%% Example 3
line_spec = input('LineSpecifier: ', 's');
line_width = input('LineWidth: ');
x = linspace(-2 * pi, 2 * pi, 100);
y = sin(x);
plot(x, y, line_spec, 'LineWidth', line_width);

%% Example 4
fprintf('My grade is %.2f%%\n', r);

%% Example 5
n = 1:12;
fprintf('%d \t %.2f\n', [n; sqrt(n)]);
