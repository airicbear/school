%% Week 5 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-02-18
%

%% Chapter 4 Exercises

%% Exercise 1
x = input("Enter an 'x': ", 's');

if (x ~= 'x')
    disp('Error');
end

%% Exercise 3
% Calculates and prints the speed of a sound
% given a temperature entered by the user
temp = input('Temperature (C°) in the range 0 to 50: ');

if ((temp < 0) || (temp > 50))
    disp('Error in temperature');
else
    speed = 331 + 0.6 * temp;
    fprintf("For a temperature of %.1f, the speed is %.1f\n", temp, speed);
end

%% Exercise 8
% Area of a rhombus
d1 = input('Length of first diagonal: ');
d2 = input('Length of second diagonal: ');

if (d1 < 1 || d2 < 1)
    disp('Error: invalid length');
else
    A = (d1 * d2) / 2;
    fprintf('The area of the rhombus is %.1f.\n', A);
end

%% Exercise 10
c = input('Color of the cosine plot as a single character: ', 's');
x = linspace(-2 * pi, 2 * pi, 50);

disp(c == 'r')

if (c == 'r')
    plot(x, cos(x), 'Color', 'r', 'Marker', '*');
else
    plot(x, cos(x), 'Color', 'g', 'Marker', '+');
end

%% Exercise 14
x = input('x values: ');

if (any(x == 0))
    disp(-99);
else
    disp(x .^ 2 + (1 ./ x));
end

%% Exercise 15
% pH level
pH = input('pH: ');

if (pH < 0 || pH > 14)
    disp('Error: invalid pH')
elseif (pH < 7)
    fprintf('pH level %d is acidic.\n', pH);
elseif (pH > 7)
    fprintf('pH level %d is basic.\n', pH);
else
    fprintf('pH level %d is neutral.\n', pH);
end

%% Exercise 20
n = randi([-100, 100]);

if (mod(n, 2) == 0)
    fprintf('%d is an even number.\n', n)
else
    fprintf('%d is an odd number.\n', n)
end

%% Exercise 26
menu = ["1. Cylinder", "2. Circle", "3. Rectangle"];
disp('Menu');
disp(menu(1));
disp(menu(2));
disp(menu(3));

shape = input('Please choose one: ');

switch shape
  case 1
    h = input('Enter the height of the cylinder: ');
    r = input('Enter the radius of the cylinder: ');
    A = 2 * pi * r * (h + r);
  case 2
    r = input('Enter the radius of the circle: ');
    A = pi * (r ^ 2);
  case 3
    l = input('Enter the length of the rectangle: ');
    w = input('Enter the width of the rectangle: ');
    A = l * w;
  otherwise
    disp('Error: invalid choice');
    return
end

fprintf('The area is %.2f.\n', A);

%% Aditional Exercises

%% Additional Exercise 1
% Beaufort Scale
beaufort = input('Enter an integer from 0-12 representing the Beaufort number: ');

switch beaufort
  case 0
    classification = 'no wind';
  case 1
    classification = 'light air';
  case 2
    classification = 'light breeze';
  case 3
    classification = 'gentle breeze';
  case 4
    classification = 'moderate breeze';
  case 5
    classification = 'fresh breeze';
  case 6
    classification = 'strong breeze';
  case 7
    classification = 'moderate gale';
  case 8
    classification = 'fresh gale';
  case 9
    classification = 'strong gale';
  case 10
    classification = 'whole gale';
  case 11
    classification = 'storm';
  case 12
    classification = 'hurricane';
end

disp(classification);

switch beaufort
  case 0
    classification = 'no wind';
  case {2,3,4,5,6}
    classification = 'breeze';
  case {7,8,9,10}
    classification = 'gale';
  case 11
    classification = 'storm';
  case 12
    classification = 'hurricane';
end

disp(classification);

%% Additional Exercise 2
% Cost of renting a car
type = input('Type of car: ', 's');
days = input('Number of days: ');
miles = input('Number of miles: ');

switch type
  case 'Sedan'
    if ((days > 0) && (days < 7))
        cost = 79 * days + 0.69 * (miles - 80);
    elseif ((days >= 7) && (days < 30))
        cost = 69 * days + 0.59 * (miles - 100);
    elseif (days >= 30)
        cost = 59 * days + 0.49 * (miles - 120);
    else
        disp('Error');
    end
  case 'SUV'
    if ((days > 0) && (days < 7))
        cost = 84 * days + 0.74 * (miles - 80);
    elseif ((days >= 7) && (days < 30))
        cost = 74 * days + 0.64 * (miles - 100);
    elseif (days >= 30)
        cost = 64 * days + 0.54 * (miles - 120);
    else
        disp('Error');
    end
end

fprintf('$%.2f\n', cost);