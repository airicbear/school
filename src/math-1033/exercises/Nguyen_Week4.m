%% Week 4 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-02-11
%

%% Three-Dimensional Plotting Exercises

%% Exercise 1
% Create a 3D plot of a staircase modeled by given parametric equations
n = 4;
h = 50;
t = linspace(0, 2 * pi * n, 1000);
r = (h .* (2 + 5 .* sin(t ./ 8))) ./ 10;
x = r .* cos(t);
y = r .* sin(t);

figure;
plot3(x, y, t);

%% Exercise 2
x = linspace(-2, 2, 100);
y = linspace(-2, 2, 100);

[X Y] = meshgrid(x, y);
Z = -1.4 .* X .* Y .^ 3 + 1.4 .* Y .* X .^ 3;

figure;
surf(X, Y, Z);

%% Exercise 3
x = linspace(-pi, pi, 100);
y = linspace(-pi, pi, 100);

[X Y] = meshgrid(x, y);
Z = cos(0.7 * X + 0.7 * Y) .* cos(0.7 * X - 0.7 * Y);

figure;
surf(X, Y, Z);

%% Exercise 4
% Contour of the streamlines for uniform flow over a cylinder
x = linspace(-3, 3, 100);
y = linspace(-3, 3, 100);

[X Y] = meshgrid(x, y);
psi = Y - (Y ./ (X .^ 2 + Y .^ 2));
Z = X .^ 2 + Y .^ 2;

figure;
hold on;
contour(X, Y, psi, 100);
contour(X, Y, Z, [1 1]);

%% Exercise 5
% The Verhulst model
N_0 = 10;
r = 0.1;
t = linspace(0, 100, 100);
N_infty = linspace(100, 1000, 100);

[X Y] = meshgrid(t, N_infty);
N = N_0 ./ ((N_0 ./ Y) + (1 - (N_0 ./ Y)) .* exp(-r .* X));

figure;
waterfall(X, Y, N);
xlabel('t');
ylabel('Maximum');
zlabel('Population');

%% Exercise 6
% Position of the ball as function of time
v0_1 = 20; % initial velocity
theta = 30;
alpha = 25;
g = 9.81;
n = 0.8; % rebound velocity

% Initial throw
figure;
vx_1 = v0_1 * sind(theta) * cosd(alpha);
vy_1 = v0_1 * sind(theta) * sind(alpha);
vz_1 = v0_1 * cosd(theta);
t_hit_1 = (2 * vz_1) / g;
t_1 = 0:0.1:t_hit_1;
x_1 = vx_1 * t_1;
y_1 = vy_1 * t_1;
z_1 = vz_1 * t_1 - (1 / 2) * g * t_1 .^ 2;
plot3(x_1, y_1, z_1);

% First bounce
grid on;
hold on;
v0_2 = 20 * n;
vx_2 = v0_2 * sind(theta) * cosd(alpha);
vy_2 = v0_2 * sind(theta) * sind(alpha);
vz_2 = v0_2 * cosd(theta);
t_hit_2 = (2 * vz_2) / g;
t_2 = 0:0.1:(t_hit_2);
x_2 = x_1(end) + vx_2 * t_2;
y_2 = y_1(end) + vy_2 * t_2;
z_2 = z_1(end) + vz_2 * t_2 - (1 / 2) * g * t_2 .^ 2;
plot3(x_2, y_2, z_2);

% Second bounce
v0_3 = 20 * n ^ 2;
vx_3 = v0_3 * sind(theta) * cosd(alpha);
vy_3 = v0_3 * sind(theta) * sind(alpha);
vz_3 = v0_3 * cosd(theta);
t_hit_3 = (2 * vz_3) / g;
t_3 = 0:0.1:(t_hit_3);
x_3 = x_2(end) + vx_3 * t_3;
y_3 = y_2(end) + vy_3 * t_3;
z_3 = z_2(end) + vz_3 * t_3 - (1 / 2) * g * t_3 .^ 2;
plot3(x_3, y_3, z_3);

% Third bounce
v0_4 = 20 * n ^ 3;
vx_4 = v0_4 * sind(theta) * cosd(alpha);
vy_4 = v0_4 * sind(theta) * sind(alpha);
vz_4 = v0_4 * cosd(theta);
t_hit_4 = (2 * vz_4) / g;
t_4 = 0:0.1:(t_hit_4);
x_4 = x_3(end) + vx_4 * t_4;
y_4 = y_3(end) + vy_4 * t_4;
z_4 = z_3(end) + vz_4 * t_4 - (1 / 2) * g * t_4 .^ 2;
plot3(x_4, y_4, z_4);

% Fourth bounce
v0_5 = 20 * n ^ 4;
vx_5 = v0_5 * sind(theta) * cosd(alpha);
vy_5 = v0_5 * sind(theta) * sind(alpha);
vz_5 = v0_5 * cosd(theta);
t_hit_5 = (2 * vz_5) / g;
t_5 = 0:0.1:(t_hit_5);
x_5 = x_4(end) + vx_5 * t_5;
y_5 = y_4(end) + vy_5 * t_5;
z_5 = z_4(end) + vz_5 * t_5 - (1 / 2) * g * t_5 .^ 2;
plot3(x_5, y_5, z_5);

%% Relational Expressions Exercises

%% Exercise 1
d = 6;
e = 4;
f = -2;

a = d + f >= e > d - e;
b = e > d > f;
c = e - d <= d - e == f / f;
d = (d / e * f < f) > -(e - d) / f;
disp([a b c d]);

%% Exercise 2
v = [-2 4 1 0 2 1 2];
w = [2 5 0 1 2 -1 3];

a = ~v == ~w;
b = w >= v;
c = v > -w;
d = v > ~-w;
disp([a; b; c; d]);

%% Exercise 3
u = v(find([v <= w]));
disp(u);

%% Exercise 4

a = 0 || 7 && 9 && -3;
% 1 && 9 && -3
% 1 && -3
% 1

b = 7 > 6 && ~0 <= 2;
% 1 && ~0 <= 2
% 1 <= 2
% 1

c = ~4 < 5 || 0 >= 12 / 6;
% 1 || 0 >= 2
% 1 >= 2
% 1 

d = -7 < -5 < -2 && 2 + 3 <= 15 / 3;
% 1 < -2 && 5 <= 5
% 0 && 1
% 0

disp([a b c d]);

%% Exercise 5
NYC = [33 33 18 29 40 55 19 22 32 37 58 54 51 52 45 41 45 39 36 45 33 18 19 19 28 34 44 21 23 30 39];
DEN = [39 48 61 39 14 37 43 38 46 39 55 46 46 39 54 45 52 52 62 45 62 40 25 57 60 57 20 32 50 48 28];

% (a)
nyc_avg_temp = sum(NYC) / sum(logical(NYC));
den_avg_temp = sum(DEN) / sum(logical(DEN));

% (b)
num_nyc_abv_avg = sum(NYC > nyc_avg_temp);
num_den_abv_avg = sum(DEN > den_avg_temp);

% (c)
num_den_abv_nyc = sum(DEN > NYC);

disp(nyc_avg_temp);
disp(den_avg_temp);
disp(num_nyc_abv_avg);
disp(num_den_abv_avg);
disp(num_den_abv_nyc);

%% Exercise 6
male_age = 19;
H_resting_male = 64;
I_male = 0.65;
H_max_male = 203.7 / (1 + exp(0.033 * (male_age - 104.3)));
H_training_male = (H_max_male - H_resting_male) * I_male + H_resting_male;

female_age = 20;
H_resting_female = 56;
I_female = 0.8;
H_max_female = 190.2 / (1 + exp(0.0453 * (female_age - 107.5)));
H_training_female = (H_max_female - H_resting_female) * I_female + H_resting_female;

disp(H_training_male);
disp(H_training_female);