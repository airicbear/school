%% Week 2 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-01-23
%

%% Chapter 2 Exercise 42

A = [ 1 4
      3 2 ];
  
B = [ 2 1 3
      1 5 6
      3 6 0 ];

C = [ 3 2 5
      4 1 2 ];

% Results
res1 = 3 * A;
res2 = A * C;
res3 = C * B;

%% Chapter 2 Exercise 43

A = [ 4 1 -1
      2 3  0];
  
B = [ 1 4 ];

C = [ 2
      3 ];

% Results
% res1 = A * B;
% Can't be done!
% The number of columns in matrix A is not the same as the
% number of rows in matrix B.

res2 = B * C;
res3 = C * B;

%% Chapter 2 Exercise 27

x = linspace(-pi, pi, 20);
y = sin(x);

%% Chapter 2 Exercise 29

x = 3:2:11;
y = sum(x);

%% Chapter 2 Exercise 30

x = 1 ./ (1:5);
y = sum(x);

%% Chapter 2 Exercise 31

n = 3:2:9;
d = 1:4;

s = sum(n / d);

%% Chapter 2 Exercise 32

A = randi(5,2,3);
p_col = prod(A);
p_rows = prod(transpose(A));

%% Chapter 2 Exercise 33

v = randi(20, 1, 6);
min_v = min(v);
max_v = max(v);
cs_v = cumsum(v);

%% Chapter 2 Exercise 38

A = [ 1  2 3
      4 -1 6 ];

B = [ 2 4 1
      1 3 0 ];
  
op1 = A + B;
op2 = A - B;
op3 = A .* B;

%% Chapter 2 Exercise 40
% Find the total pay for every employee

v = [33 10.5 40 18 20 7.5];
hours = v(1:2:end);
pay = v(2:2:end);
total_pay = hours .* pay;

%% Chapter 2 Exercise 41
% Find the volume of each trial

r = [5.499 5.498 5.5 5.5 5.52 5.51 5.5 5.48];
h = [11.1 11.12 11.09 11.11 11.11 11.1 11.08 11.11];

v = pi * (r .^ 2) .* h;

%% Chapter 2 Additional Exercise
% Solve system of equations

A = [ 1  3 2
      2 -2 1
      3  1 2 ];

B = [  3
       8
      -1 ];

x = A \ B;

%% Chapter 2 Exercise 29

%% Chapter 2 Exercise 30

%% Chapter 2 Exercise 31

%% Chapter 2 Exercise 32

%% Chapter 2 Exercise 33

%% Chapter 2 Exercise 38

%% Chapter 2 Exercise 40

%% Chapter 2 Exercise 41

%% Chapter 2 Additional Exercise
