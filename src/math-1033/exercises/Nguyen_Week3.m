%% Week 3 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-02-02
%

%% Chapter 12 Exercise 13
T = 100;
k = 0.6;
t = linspace(1, 10, 100);
S1 = 50;
S2 = 20;

T1 = S1 + (T - S1) * exp(-k * t);
T2 = S1 + (T - S2) * exp(-k * t);

figure;
hold on;
xlabel('Time (minutes)', 'Interpreter', 'latex', 'FontSize', 16);
ylabel('Temperature of the object', 'Interpreter', 'latex', 'FontSize', 16);
title("Newton's Law of Cooling", 'Interpreter', 'latex', 'FontSize', 16);
plot(t, T1);
plot(t, T2);

%% Chapter 12 Exercise 14
x = linspace(2, 5, 100);

figure;
plot(x, x, 'LineWidth', randi(10));

%% Chapter 12 Additional Exercise 1
x = linspace(-7, 7, 100);
f = 3 * cos(1.7 * x) .* exp(-0.3 * x) + 2 * sin(1.4 * x) .* exp(0.3 * x);

figure;
plot(x, f);

%% Chapter 12 Additional Exercise 2
x = [-1  1  1 -1 -1];
y = [-1 -1  1  1 -1];

figure;
plot(x, y);
grid on;
axis([-2  2 -2  2]);
%% Chapter 12 Additional Exercise 3
% Glue method
x = linspace(-1, 1, 100);
y1 = sqrt(1 - x .^ 2);
y2 = -sqrt(1 - x .^ 2);

%figure;
%hold on;
%plot(x, y1);
%plot(x, y2);

% Parameterization method
t = linspace(0, 2 * pi, 100);
x = cos(t);
y = sin(t);

figure;
hold on;
plot(x, y);
axis([-2  2 -2  2]);

%% Chapter 12 Additional Exercise 4
t = linspace(0, 10 * pi, 1000);
x = t .* cos(t);
y = t .* sin(t);

figure;
plot(x, y);

%% Chapter 12 Additional Exercise 5
t = 10:10:70;
H = [9 22 44 63 80 94 97];

figure;
hold on;
plot(t, H, '-k');
plot(t, H, 'or', 'MarkerFace', 'r');
grid on;
legend('$$H(t) = \frac{100.8}{1 + 23e^{-0.093t}}$$',...
    'Height every 10 days (inches)',...
    'Interpreter', 'latex', 'Location', 'southeast', 'FontSize', 16);
title('Height of A Sunflower Plant Over Time',...
    'Interpreter', 'latex', 'FontSize', 16);
xlabel('Time (days)', 'Interpreter', 'latex', 'FontSize', 16);
ylabel('Height (inches)', 'Interpreter', 'latex', 'FontSize', 16);

%% Chapter 3 Exercise 5
real_number = input('Enter a real number: ');
fprintf('%.2f\n', real_number);

%% Chapter 3 Exercise 9
user_string = input('Enter a string: ', 's');
fprintf("Your string was: '%s'\n", user_string);

%% Chapter 3 Exercise 10
b = input('Enter the first side: ');
c = input('Enter the second side: ');
alpha = input('Enter the angle between them: ');
a = sqrt((b ^ 2) + (c ^ 2) - (2 * b * c * cosd(alpha)));
fprintf("The third side is %.3f\n", a);

%% Chapter 3 Additional Exercise 1
% Radioactive decay of a material over time
half_life = input("Half-life of the material (in years): ");
A0 = input("Current amount of the material (in pounds): ");
t = input("Number of years from now: ");
k = log(2) / half_life;
A = A0 * exp(-k * t);
fprintf("The amount of the material after %d years is %d pounds.\n", t, A);

%% Chapter 3 Additional Exercise 2
% Value of a savings account over time
P = input("Initial investment: ");
t = input("Number of years since initial investment: ");
r = input("Yearly interest rate as a percentage: ");
n = input("Number of times investment is compounded per year: ");
V = P * (1 + ((r / 100) / n)) ^ (n * t);
fprintf("$%.2f investment at a yearly interest rate of %.2f%% compounded %d times per year is $%.2f.\n", P, r, n, V)