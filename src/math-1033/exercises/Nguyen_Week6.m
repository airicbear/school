%% Week 6 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-02-20
%

%% Chapter 5 Exercises

%% Exercise 5
n = 11;
disp(sum(1:2:n))

%% Exercise 10
vec = [5.5 11 3.45];
count = 1;
for n = vec
    fprintf('Element %d is %.2f.\n', count, n);
    count = count + 1;
end

%% Exercise 16
n = 5;
for i = 1:n
    for j = 1:i
        fprintf('%d ', i * j);
    end
    fprintf('\n');
end

%% Exercise 20
n = 6249;
for i = 1:n
    fprintf('(1/e = %.4f) ≈ %.4f\n', exp(-1), (1 - (1 / i)) ^ i)
end
fprintf('n = %d\n', n);

%% Exercise 26
vec = rand(10, 1);
disp(sum(vec));

%% Exercise 27
vec = rand(10, 1);
disp(cumprod(vec));

%% Exercise 28
mat = rand(3);
disp(mat .* 2);

%% Exercise 33
grade = input('Quiz grade: ');
valid = 0:0.5:10;
while (grade ~= valid)
    grade = input('Quiz grade: ');
end
disp(grade)

%% Exercise 41
MIN = 1;
MAX = 100;
good = 0:5;
okay = 5:10;
continuePlaying = 1;
numGames = 0;
totalGuesses = 0;

while (continuePlaying)
    count = 0;
    n = randi([MIN,MAX]);
    guess = input(sprintf('Guess a number between %d and %d: ', MIN, MAX));
    
    while (guess ~= n)
        if (guess > n)
            disp('Your guess was too high.');
        else
            disp('Your guess was too low.');
        end
        count = count + 1;
        guess = input(sprintf('Guess a number between %d and %d: ', MIN, MAX));
    end

    fprintf('You guessed %d times.\n', count);

    if (any(count == good))
        disp('You guessed really well!');
    elseif (any(count == okay))
        disp('Good job guessing, but you could have done better.');
    else
        disp('You took way too long to guess the number.');
    end
    totalGuesses = totalGuesses + count;
    numGames = numGames + 1;
    
    continuePlaying = input('Do you want to play again? (1 or 0): ');
end

fprintf('You guessed %d times in total for %d games, averaging in around %d guesses per game.\n', totalGuesses, numGames, totalGuesses / numGames);

%% Additional Exercises

%% Additional Exercise 1
% Drug concentration
D = 150;
V = 50;
ka = 1.6;
ke = 0.4;

% (a)
t = 0:0.1:10;
C = (D / V) * (ka / (ka - ke)) * (exp(-ke .* t) - exp(-ka .* t));

figure;
plot(t, C);
xlabel('Time (hr)');
ylabel('Concentration of the drug');
title('Concentration of a drug in the body over time');

%% Additional Exercise 2
N = 10000;
x = zeros(N,1);
y = zeros(N,1);

for n = 1:N
    a = randi([1,3]);
    switch a
      case 1
        x(n+1) = 0.5 * x(n);
        y(n+1) = 0.5 * y(n);
      case 2
        x(n+1) = 0.5 * x(n) + 0.25;
        y(n+1) = 0.5 * y(n) + sqrt(3) / 4;
      case 3
        x(n+1) = 0.5 * x(n) + 0.5;
        y(n+1) = 0.5 * y(n);
    end
end

figure;
plot(x, y, '.', 'MarkerSize', 10);