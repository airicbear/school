#+title: People, Places, and Environment
#+subtitle: Department of Architecture and Environmental Design \\
#+subtitle: Tyler School of Art and Architecture
#+author: Temple University
#+date: Autumn 2019

* General Information

  - Course sequence and title: CTRP 0807 People, Places, and Environment

  - Meeting place: 162 Beury Hall

  - Date and time:

    - August 27th to December 5th

    - Tuesday and Thursday 9:30 a.m. to 10:50 a.m.

  - Professor: William J. Cohen, Ph.D., FAICP

    - E-mail: wjcohen@temple.edu

  - Course coordinator: David R. Guinnup, B.S., AICP

    - E-mail: david.guinnup@temple.edu

* Overview of Course

  People, Places, and Environment is a US Society Gen/Ed course and as such is
  geared to develop your understanding of American history, society, culture and
  political systems. Additionally, this Gen/Ed course will enhance your critical thinking
  skills; information literacy; ability to examine historical events through a variety of
  interdisciplinary disciplines; understanding of historical and contemporary issues in
  context; and engagement, both locally and globally, in relevant issues of our day.

  Specifically, this course is designed to make connections--to focus on how people
  have adapted to their place, have modified their natural environment and created
  settlement patterns that form our cities, suburbs, and regions. We will review past as
  well as current trends and seek an understanding of future possibilities.

  To reach this objective there are three dominant perspectives or themes that will be
  studied throughout the semester.

  First, we will all need to step back for a moment and examine ourselves--and
  introspectively think about our values, feelings, and goals in life. It should not be a
  painful moment, but a revelation of what we truly value. Only then can we broaden our
  search to understand people, places, and environment.

  Second, we will examine how communities have evolved through cultural
  perspectives--principally the American experience--and what that means in how /people/
  have adapted to their /place/, and in the process impacted the natural /environment/. This
  perspective has relevance to how we will plan, design, and build our future
  communities.

  Third, we will expand that first perspective to explore the concept of ecology as it
  originated in the biological sciences and how it has profound application to the
  planning and designing of present and future communities. The approach known as
  /ecological planning/ holds the key to creating sustainable and enduring communities of
  the future. So, we will push our examination to seek out new approaches, tools, and
  techniques that can integrate nature, technology, and humanity to better plan and build
  communities in the 21^{st} Century.

  The course is divided into five parts to bring these three perspectives into focus and
  include:

** Part 1 Discovering Ourselves: How We Learn What to Value

   Open up your mind; relax; step away from the whirlwind of modern technology;
   look up from the cell phone; transport yourself back in time and you might discover
   your real self--or, what you might want your real self to be.

** Part 2 Towns, Cities, and Suburbia: America's Man-Made Landscape

   This will provide a critical assessment of America’s man-made landscape--our
   cities, towns, and suburban settlements. We want to know, how did things become
   the way they are? Is the American suburb really the ideal that we are told it is
   supposed to be? What we will review will be hard hitting and a little disturbing.

** Part 3 Exploring Multiple Dimensions of People, Places, and Environment

   In this part of the course we will engage several views and perspectives--past,
   present, and future--that sharpen the connections of people, places, and
   environment. Understanding the city as a cultural expression, reviewing the
   foundations of a Garden City, and tying this together with the current movement
   toward building sustainable communities will be highlighted. And, will the city as
   we know it survive in the future?

** Part 4 Reshaping the Ethos of the 21^{st} Century City

   Here we will explore some basic ethical premises, as well as attitudes, that have
   influenced how people relate to their environment. We will see how some of the
   basic “laws of ecology” can be incorporated into planning and designing cities of
   tomorrow. And, we will pursue a search for a /Second Enlightenment/ highlighted by
   real examples of seeking “new rules” to create enduring (or sustainable) cities in the
   21^{st} Century.

** Part 5 Collaborative Thinking and Research on Relevant Issues of the Day

   Finally, the course will allow each student to examine and research specific relevant
   issues that affect the lives of how people live in places and impact their environment.

* Required Text

  There will be a single text that will contain all of the reading material for the course,
  and will be referenced in the syllabus as the /Reader/. The complete citation is:
  /People, Places, and Environment Reader/, edited by William J. Cohen (New York: McGraw-Hill, 2014).

* Course Format and Organization

  The course will be conducted in a lively and engaging manner. I will complement the
  reading material with a number of visual presentations. This will include PowerPoint
  and videos. The information presented in these formats should inspire each person to
  master the content of the course and come to a new threshold of understanding about
  /people/, /places/, and /environment/.

  The readings are organized in a way that will fuse perspectives from the past,
  present, and future. This will allow for an intermingling of ideas, concepts, and
  alternatives to give us a better understanding of the relationship of people to places and
  their environment.

* Course Requirements

  The course requirements are the following: _Attendance_ is required. There will be
  /Two Examinations/, the preparation of a /People, Places, and Environment Portfolio/, and a
  /Short Essay/ on a selected, relevant issue of the day. There will be no quizzes or final
  examination. Each of the four graded assignments will comprise 25% of the final grade.

** Attendance

   Your presence at every class is necessary in order to follow the continuity of the
   course and the material that we will cover.

   Attendance is also important to capture the notes from my lectures. If for any
   reason you miss a lecture do not ask me, “What did I miss?” _I do not give personal tutorials_.
   Moreover, my _lectures are not posted for electronic access_ on
   Canvas, so you will have to ask a fellow student to fill you in on what you missed.

** Examinations

   The two examinations will be short answer and multiple choices, and possibly a
   drawing example. You will be allowed to bring your textbooks and lecture notes
   for reference. This is generally referred to as an “open book exam.” So, while you
   don’t have to /memorize/ specifics in a traditional sense, your success in the
   examinations will be predicated on your /understanding/ of concepts and
   relationships. If you do not keep up with the reading assignments or attend class
   on a regular basis you will have a major disadvantage when exam time comes.
   Cramming for the exams will also place you at a disadvantage.

   Each examination will consist of twenty questions.

** People, Places, and Environment Portfolio

   Each person will prepare a /People, Places, and Environment Portfolio/, which will be
   your individual creation and contain a collection of visual material that
   illustrates or describes people, places, and environment. Sources could be
   newspapers, magazines, your own photography, or anything else you discover.
   Most students access images from the Internet and incorporate them into the
   content of the Portfolio.

   You can select _any subject_ that you feel illustrates the relationship of people,
   places, and environment.

   We will discuss the preparation of the Portfolio in class on October 1^{st}.

*** Organization of the Portfolio

    Follow these organizational guidelines to prepare the portfolio.

    - _Title page_ -- consists of the title, your name, the course name and
      section number, and the date of submission (nothing else).

    - _Statement of focus_ -- the subject or issue that the Portfolio
      represents, and why you selected it. This statement must be no
      longer than 1 page.

    - _Content_ -- the visual material that may consist of downloaded
      images from the Internet, clippings, or your own photography.
      There should be a minimum of 6 individual images and a
      maximum of 12. You decide to print as color or black and white.

    - _Statement of relationship_ -- describes or analyzes how your content
      (the visual material) relates to people, places, and environment. Go
      back and re-read the “Overview of the Course,” in the beginning of
      the syllabus. Now, think about your Portfolio focus, look at the
      content, and draw some analytical conclusions--this would be your
      statement of relationship. This statement must be no longer than 1
      page.

*** Guidelines for Preparing the Portfolio

    The Portfolio is intended to be both a creative and substantive project.
    Think about it as a professional presentation.

    That means *no “cutesy” embellishments* such as fancy binders, ribbons,
    colored paper, or anything that would detract the reader from the content
    and form of the Portfolio. A single staple in the upper left hand corner is
    all the binding you need. Keep it straightforward, clean, and simple in
    appearance.

    Also, some students think by *“padding”* the portfolio with additional
    information or content will result in a better presentation. Actually, it will
    detract from your presentation, and frankly, will not be read. So, follow these
    guidelines and do not try to do more than the requirements call for.

    *Captions* under the content images are not required, but are highly suggested to
    add clarity to your presentation. If you use captions remember--just a few key
    words to identify the image, not a mini essay. A caption that exceeds one line
    will not impress the reader and most likely will not be read.

    *Sources* that you may have utilized for the content (for example, images
    downloaded from the World Wide Web) _do not need to be cited_. Most
    often you will use generic or universally circulated images or pictures.

*** Evaluating the Portfolio

    The Portfolio will be evaluated using the following criteria:

    - Clarity of the statement of focus.

    - Variety or visual interest of assembled content sources and degree
      to which the content is supportive or related to the statement of
      focus of the portfolio.

    - Relevance of statement of relationship to people, places, and environment.

    - Neatness and general appearance of presentation.

** Collaborative Think Tank and Essay

   In the final part of the course the entire class will become a “Collaborative Think
   Tank.” What this means is that everyone will select a topic from the following list
   and prepare a brief (3 to 5 pages) essay. You may incorporate images, graphs,
   photographs, figures, or tables, but you need to have _3 to 5 pages of substantive writing_.
   Only use essential visual material.

   - *Energy and the Environment:* Seeking alternatives for the present and future.
   - *Water:* Exploring dimensions of an indispensable resource.
   - *Land and Food:* The essentials to live an optimal human life.
   - *Recycling:* From local to global.
   - *Transportation:* Moving people and goods from place to place.

   We will have discussions on each of the topics (issues) and the structure will be to
   facilitate student inquiry into the specific topic of your choice. Your participation
   will rely on accumulated information and knowledge acquired from the preceding
   four parts of the course. You should start by reviewing previous readings and
   lectures that are relevant to your topic. Next, read the essay in Part 5 of the /Reader/
   (beginning on page 169) that is relevant to your chosen Think Tank topic. This will
   give you a perspective on the issue. A modest amount of additional research will
   follow. We will discuss the requirements of this assignment more fully on
   November 14^{th}.

   In the end the Collaborative Think Tank will offer to each student a cumulative
   learning experience to bring together a full comprehension of people, places, and
   environment.

*** Outline for the Think Tank Essay

    The essay must be between 3 and 5 pages of text and double-spaced. Use
    the following outline as a guide as you structure your essay and make sure you list
    each of these subdivisions.

    - _Title Page_ -- Consists of the title, your name, the course name and section, and
      the date of submission. Nothing else.

    - _Focus of Essay_ -- A brief statement that summarizes the subject of the essay and
      why it has been chosen.

    - _Research and Analysis_ -- The main discussion that incorporates materials used,
      interviews conducted, and your perspective on the topic.

    - _Conclusion_ -- Summarize your findings, draw a conclusion and include a brief
      statement to answer the question, “what have I learned about my topic that
      relates to people, places, and environment?”

    - _References_ -- All references consulted should be appropriately cited and listed.
      Shorten your citations for electronic sources (i.e., Web sites or other information
      retrieved from the Internet) and give the date you accessed the information.
      Make sure you have a subject or title rather than just a Web address.

*** Evaluating the Think Tank Essay

    The essay will be evaluated using the following criteria:

    - Clarity of the focus of the essay.

    - Depth of research including the sources consulted and inclusion of
      any interviews or surveys undertaken.

    - Writing style that is easy to read and convincing to the reader.

    - References clearly cited.

*** Attendance Requirements

    Participation is the key to your gaining the most from the learning experience, since
    the format is based on collaboration and exploration. We will offer guidelines and
    provide perspectives on the Think Tank topic--you will be expected to respond.

    Therefore, attendance is mandatory and will be recorded. That means you need to
    attend _all three sessions_. A single unexcused absence will result in your essay grade
    being reduced by \(\frac{1}{2}\). That means if your essay is assessed as a B, your actual grade will
    be B-. Two unexcused absences will result in a full grade deduction. Missing all three
    sessions--unexcused--will result in a two full grade deduction for the essay.

* Course Learning Goals

  Upon successful completion of this course students will:

  1. Have an understanding and a critical awareness of the growth and
     development of American cities, towns, and suburbs.

  2. Be aware of the multiple and inevitable connections of people, places, and
     their environment.

  3. Understand the relevance of ecology as it provides a basis to plan, design, and
     build cities of the future.

  4. Be cognizant of the challenges in building and maintaining communities in
     the 21^{st} Century that will be predicated on understanding and respecting the
     limits of our natural resources.

* University and Course Policies

** Statement on Academic Freedom

   Freedom to teach and freedom to learn are inseparable facets of academic
   freedom. The University has adopted a policy on Student and Faculty
   Academic Rights and Responsibilities (Policy #03.70.02) which can be
   accessed through the following link:

   https://secretary.temple.edu/sites/secretary/files/policies/03.70.02.pdf

** Accessibility/Disability/Religious Observance/Other

   Temple University is committed to the inclusion of students with disabilities and
   provides accessible instruction, including accessible technology and instructional
   materials.

   The process for requesting access and accommodations for this class is:

   1. Advise me of the need for access or accommodations;

   2. Contact Disability Resources and Services at 100 Ritter Annex;

   3. This office will consult with me as needed about essential components of
      the program;

   4. Disability Resources and Services will forward to me an accommodation
      letter.

   Further, students who anticipate an absence due to a scheduled religious
   observance, or if you are participating in an athletic event should either see me in
   class or contact me by e-mail if there will be conflicts with meeting course
   requirements, especially examinations. Arrangements can be made to
   accommodate your needs, but only if you let me know in advance.

   The same holds true if you have an unavoidable personal or family
   commitment or situation that arises during the semester.

** Plagiarism

   In academia plagiarism is the worst sin anyone can commit. Plagiarism is
   nothing less than cheating and taking someone else’s work and calling it
   your own without citation or attribution. If there is any detection or
   evidence of plagiarism during the semester I will make a judgment of the
   nature of the infraction and the student will receive a failing grade in the
   assignment and be subject to a failing grade in the course.

** Using Electronic Devices and Equipment is Prohibited

   I want to make it perfectly clear from the start that *during class all electronic devices and equipment are not to be used.*
   That means all cell phones, SmartPhones, iPads, and lap top computers *shut off and put away.*

* Schedule of Classes and Reading Assignments

** Part 1 Discovering Ourselves: How We Learn What to Value

   - August 27^{th} Introduction to course

   - August 29^{th} Discovery through experience

     - /Volver a no saber/

     - Selections from the /Reader/: Loren Eiseley, Aldo Leopold, and Tom Brown.

** Part 2 Towns, Cities, and Suburbia: America's Man-Made Landscape

   - September 3^{rd} A Cultural Perspective of People, Places, and Environment

     - Selections from the /Reader/: James Howard Kunstler and Withold Rybczynski.

   - September 5^{th} An Interpretation of Ordinary Landscapes

     - Selection from the /Reader/: Dolores Hayden.

   - September 10^{th} The Geometry of Urban Form: the Grid and the Circle

     - “Tricks of the Trade”

     - Selection from the /Reader/: Anthony Flint.

   - September 12^{th} Looking Back to the Village and Finding Place in America

     - A presentation about the availability of the University Writing Center

     - Selections from the /Reader/: Suzanne Sutro and Ray Oldenburg.

   - September 17^{th} From Sprawl to Smart Growth

     - Selections from the /Reader/: Oliver Gillham and Jonathan Barnett.

   - September 19^{th} New Urbanism: Adapting Traditional Approaches

     - Selections from the /Reader/: Elizabeth Plater-Zyberk and Peter Calthorpe.

   - September 24^{th} Save Our Land Save Our Towns

     - Video about sprawl and a story of hope why America’s towns can be re-built and the countryside preserved.

     - No reading assigned

   - September 26^{th} *First Examination*

     - To cover the readings in Parts 1 and 2, lectures, and video.

** Part 3 Exploring Multiple Dimensions of People, Places, and Environment

   - October 1^{st} How to do the People, Places, and Environment Portfolio

     - Review of the First Examination.

   - October 3^{rd} The Cultural City and the Garden City

     - Selections from the /Reader/: Lewis Mumford and Frederic Osborn.

   - October 8^{th} Climate Change Impacts on People, Places, and Environment

     - A guest Presenter, Mr. Ronald Glass will present a contrarian perspective

     - Selection from the /Reader/: Joel Kotkin

   - October 10^{th} Building Diverse, Sustainable, and Healthy Cities

     - Selections from the /Reader/: Jane Jacobs, Jan Gehl, and William Mitchell.
   
** Part 4 Reshaping the Ethos of the 21^{st} Century City

   - October 15^{th} The Land Ethic: The Beginning of the Essential Trilogy.

     - Selection from the /Reader/: Aldo Leopold.

   - October 17^{th} The Tragedy and the Tyranny: Completing of the Essential Trilogy

     - Selections from the /Reader/: Garrett Hardin and William Odum.
     - *Due Date for People, Places, and Environment Portfolio*

   - October 22^{nd} Ecology 101

     - The Four Laws of Ecology

     - The Ecological Footprint

     - Selections from the /Reader/: Barry Commoner; Mathis Wackernagel and William Rees.

   - October 24^{th} Fusing Ecology in Planning and Designing Cities and Regions

     - Doing a Land Suitability Analysis

     - Selection from the /Reader/: Ian L. McHarg.

   - October 29^{th} Timeless Cities, Eco-Villages, and Biomimicry

     - Selections from the /Reader/: David Mayernik, Jim Antoniou, and Janine Benyus.

   - October 31^{st} The “Outlaw Designer”

     - A video interview with architect James Wines.

     - Selections from the /Reader/: James Wines and Phil Cousineau.

   - November 5^{th} The Search for the Second Enlightenment

     - Selection from the /Reader/: William Cohen

   - November 7^{th} Integrating Nature, Technology, and Humanity

     - Ecological Design: Inventing the Future Video.

     - Return of the Portfolio.

   - November 12^{th} *Second Examination*

     - To cover the readings in Parts 3 and 4, lectures, and videos.

** Part 5 Collaborative Thinking and Research on Relevant Issues of the Day

   - November 14^{th} Organization of the Collaborative Think Tank

     - Review of Second Examination

     - Organization of the Think Tank

   - November 19^{th} Getting Started: Using the Library and Writing the Essay

     - Presentation: Librarian Jill Luedke -- using library resources (Greenr)

     - Presentation: Professor David Guinnup -- “Tips and Traps” in preparing the Think Tank Essay

   - November 21^{st} Overview of Topics

     - Energy and the Environment -- Professor Guinnup

     - Land and Food -- Professor Cohen

     - Water -- Professor Guinnup

     - Recycling -- Professor Cohen

     - Transportation -- Professor Guinnup

   - November 25^{th} to December 1^{st} Fall Break (no classes)

   - December 3^{rd} Student Topics

     - Random selection of students to discuss their topic

   - December 5^{th} Conclusion and Final Wrap-up

     - *Think Tank Essay Due*
