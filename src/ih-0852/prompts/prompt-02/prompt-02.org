#+TITLE: Prompt 2: Obsession
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
#+LATEX_HEADER: \usepackage[doublespacing]{setspace}
#+LATEX_HEADER: \frenchspacing
#+LATEX_CLASS_OPTIONS: [12pt]
#+EXPORT_FILE_NAME: Prompt02_NguyenEric
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{automata}
#+LATEX_HEADER: \usetikzlibrary{trees}
#+LATEX_HEADER: \usetikzlibrary{graphs.standard}

In his book, /The Starry Messenger/, Galileo obsessively and compulsively studies the lunar surface and far away stars using his new "spyglass" telescope which would allow him to zoom into a distant image of the sky as much as 20 times closer than the naked eye.
His obsession and compulsion led him to make great discoveries about the Moon, the stars, and from those the universe.
We can define obsession as being constantly preoccupied with something to an extreme extent and compulsion as the strong, irresistable urge to do something.
Seeing as Galileo's obsession and compulsion led him to making such great discoveries, it raises the question, are these qualities needed to become a better scientist?

Certainly obsession and compulsion does indeed enable one to become a better scientist in some cases as shown by Galileo.
We can notice that these qualities may lead to better scientists especially in studying the behavior of a system as this process often involves a repetitive and meticulous study of the behavior over a certain period of time.
In fact, it is quite common in scientific work for a scientist to come up with a hypothesis and then run through many experiments to determine whether or not the stated hypothesis is valid for a definite conclusion to arise from it.
Apparently then, these qualities are operationalized during the experimental phase of the scientific method where obsession and compulsion can be leveraged to constantly observe and record data with great attention to detail.
From these reasons, we can confirm that such qualities do prove to be useful when doing science in most cases.

What separates obsessive-compulsive *disorder* (OCD) and obsessive compulsive *traits* is that OCD is an version of excessive perfectionism for trivial, arbitrary tasks whereas obsessive compulsive traits enable a person to vigorously and diligently work towards a certain goal in mind.
Clearly, Galileo displayed obsessive compulsive traits, not OCD, seeing that his obsession and compulsion paved the way for him to study and draw conclusions about the universe---a goal which he had set out for himself to accomplish.
His dedication to his work enhanced by these traits show that on an emotional level he may have felt that it was his moral duty to carry out the experiments that he did.
I imagine that he turned the spyglass towards the celestial realm in an effort to determine whether or not certain beliefs regarding the universe at the time were actually true.

I suppose a time when I practiced obsession-compulsion was back in twelfth grade of highschool to freshman year of college.
During this time, I was very involved in the practice of desktop ``ricing'', a process involving modifying the desktop screen of my laptop in such a manner that it would be more visually appealing and have useful functionality according to my needs.
I wanted my laptop to look the best it could but also be the most efficient it could be for my work.
To do this, I was constantly looking online to see other people's ``riced'' desktops and take inspiration from them to work my own.
I also had learned much about different computer programs such as i3wm, LaTeX, Emacs, and Org-mode all of which heavily contributed to how I use my computer today to take notes and even write this very essay.
Indeed, I did incorporate some steps from the scientific method to achieve this. (1) First asking myself what I wanted my computer to be able to do, (2) determining whether it was even possible or not based on the available technology, (3) researching how to achieve such a goal given that it is possible, (4) trying it out on my computer for myself and continue to retry if it does not work, and (5) concluding whether or not I achieved what I initially wanted and if it was actually going to be beneficial for me.
An specific example of this would be the time when I researched the LaTeX package, TikZ, to (1) create beautiful diagrams to display on my school assignments. (2) I determined it was possible based on online responses. (3) I researched on how to create diagrams in TikZ. (4) I continued to configure my computer environment until (5) it finally worked and I used it to create several diagrams to include in my final project for my discrete mathematics course.
Figure 1 shows one of said diagrams, a Markov chain modeling the probability for a drunkard to walk to the next corner of the building.

#+CAPTION: A TikZ diagram created for my discrete mathematics final project.
\begin{figure}
\[\begin{tikzpicture}[shorten >=1pt,->]
\tikzstyle{vertex} = [draw,circle]
\foreach \name/\angle/\text in {P-1/234/s_1, P-2/162/s_2,
P-3/90/s_3, P-4/18/s_4, P-5/-54/s_5}
\node[vertex,xshift=6cm,yshift=.5cm] (\name) at (\angle:3cm) {$\text$};
\foreach \from/\to in {1/2,2/3,3/4,4/5,5/1}
{ \draw (P-\from) to [bend left] node [fill=white,midway] {0.5} (P-\to); \draw (P-\to) to [bend left] node [fill=white,midway] {0.5} (P-\from); }
\end{tikzpicture}\]
\end{figure}
