#+TITLE: Final Review
#+AUTHOR: Eric Nguyen


* Problem 1

Determine whether the given pair of graphs is isomorphic.
Exhibit an isomorphism or provide a rigorous argument that none exists.

\begin{align*}
G &= \begin{bmatrix}
0 & 1 & 1 & 1 & 1 \\
1 & 0 & 1 & 1 & 1 \\
1 & 1 & 0 & 0 & 0 \\
1 & 1 & 0 & 0 & 0 \\
1 & 1 & 0 & 0 & 0
\end{bmatrix} \\
H &= \begin{bmatrix}
0 & 1 & 1 & 1 & 1 \\
1 & 0 & 1 & 1 & 1 \\
1 & 1 & 0 & 0 & 0 \\
1 & 1 & 0 & 0 & 0 \\
1 & 1 & 0 & 0 & 0
\end{bmatrix}
\end{align*}

* Problem 2

Prove by induction that \(n^2 - 7n + 12\) is nonnegative whenever \(n\) is an integer with \(n \geq 3\).

- Basis step: show \(P(3)\) is true.
  
  \begin{align*}
  P(3) &= (3)^2 - 7(3) + 12 \geq 0 \\
       &= 9 - 21 + 12 \geq 0 \\
       &= -12 + 12 \geq 0 \\
       &= 0 \geq 0 \implies T
  \end{align*}

- Inductive hypothesis: assume that \(P(k)\) is true for an arbitrary fixed integer \(k \geq 3\).

  \[P(k) = k^2 - 7k + 12 \geq 0\]

- Inductive step: show that \(P(k) \implies P(k + 1)\) is true.
  
  \begin{align*}
    P(k + 1) &= (k + 1)^2 - 7(k + 1) + 12 \geq 0 \\
         &= k^2 + 2k + 1 - 7k - 7 + 12 \geq 0 \\
         &= (k^2 - 7k + 12) + 2k - 6 \geq 0 \\
         &= P(k) + 2k - 6 \geq 0 \\
         &= 2k \geq 6 \\
         &= k \geq 3
  \end{align*}

  This completes the inductive step.

- By mathematical induction, \(P(n)\) is true for all integers \(n\) with \(n \geq 3\).
  
* Problem 3

a) How many vertices and how many edges are in this graph?
b) Is this graph planar? Justify your answer.
c) Does this graph have an Euler circuit? Justify your answer.
d) What is the chromatic number of this graph?
   
** Graph 1: \(K_5\)

a) There are 5 vertices and 10 edges.
b) Not planar.
   Draw a planar representation of the subgraph \(K_4\).
   Show that it is impossible to add another vertex in any of the available regions without crossing.
c) Yes. Each of its vertices has even degree.
d) 5.

** Graph 2: \(C_4\)

a) There are 4 vertices and 4 edges.
b) It is planar. It is obvious from the graph that no edges cross.
c) Yes. Each of its vertices has even degree.
d) 2.

** Graph 3: \(K_{5,5}\)

a) There are 10 vertices and 25 edges.
b) It is not planar because of a non-planar subgraph, \(K_{3,3}\).
c) No.
   There exists a vertex with odd degree.
d) 2. Bipartite graph.
   
* Problem 4

For the web graph shown below write the link matrix A that expresses the system of PageRank linear equations in the form \(Ax = x\), where \(x = \begin{bmatrix}x_1 & x_2 & x_3 & x_4 & x_5\end{bmatrix}^T\).
Is the matrix \(M = (1 - m)A + mS\) for \(m = 0.25\) column-stochastic? Justify your answer.

\begin{align*}
M &= 0.75 A + 0.25 S \\
0.75 + 0.25 &= 1
\end{align*}

* Problem 5

Use the method of Gaussian elimination to find \(x\) for the system of linear equations \(Ax = b\), where \(A\) and \(b\) are given below.
Show your work.

\[A = \begin{bmatrix} 2 & 4 & 6 \\ 1 & 3 & 5 \\ 2 & 6 & 11 \end{bmatrix}, b = \begin{bmatrix} 10 \\ 4 \\ 6 \end{bmatrix}\]


\begin{align*}
Ax = b &\implies \begin{bmatrix} 2 & 4 & 6 & 10 \\ 1 & 3 & 5 & 4 \\ 2 & 6 & 11 & 6 \end{bmatrix} \\
\frac{1}{2} R_1 \to R_1 &\implies \begin{bmatrix} 1 & 2 & 3 & 5 \\ 1 & 3 & 5 & 4 \\ 2 & 6 & 11 & 6 \end{bmatrix} \\
-R_1 + R_2 \to R_2 &\implies \begin{bmatrix} 1 & 2 & 3 & 5 \\ 0 & 1 & 2 & -1 \\ 2 & 6 & 11 & 6 \end{bmatrix} \\
-2R_1 + R_3 \to R_3 &\implies \begin{bmatrix} 1 & 2 & 3 & 5 \\ 0 & 1 & 2 & -1 \\ 0 & 2 & 5 & -4 \end{bmatrix} \\
-2R_2 + R_3 \to R_3 &\implies \begin{bmatrix} 1 & 2 & 3 & 5 \\ 0 & 1 & 2 & -1 \\ 0 & 0 & 1 & -2 \end{bmatrix} \\
-2R_2 + R_1 \to R_1 &\implies \begin{bmatrix} 1 & 0 & -1 & 7 \\ 0 & 1 & 2 & -1 \\ 0 & 0 & 1 & -2 \end{bmatrix} \\
-2R_3 + R_2 \to R_2 &\implies \begin{bmatrix} 1 & 0 & -1 & 7 \\ 0 & 1 & 0 & 3 \\ 0 & 0 & 1 & -2 \end{bmatrix} \\
R_3 + R_1 \to R_1 &\implies \begin{bmatrix} 1 & 0 & 0 & 5 \\ 0 & 1 & 0 & 3 \\ 0 & 0 & 1 & -2 \end{bmatrix} \\
\end{align*}

\begin{align*}
x_1 &= 5 \\
x_2 &= 3 \\
x_3 &= -2
\end{align*}

\[x = \begin{bmatrix} 5 \\ 3 \\ -2 \end{bmatrix}\]

* Problem 7

Find the eigenvalues and eigenvectcors of these two matrices.
Show your work.

\[A = \begin{bmatrix} 1 & 4 \\ 2 & 3 \end{bmatrix} \text{ and } A + I = \begin{bmatrix} 2 & 4 \\ 2 & 4 \end{bmatrix}\]

\begin{align*}
\det(A - \lambda I) &= \begin{vmatrix} 1 - \lambda & 4 \\ 2 & 3 - \lambda \end{vmatrix} \\
&= (1 - \lambda)(3 - \lambda) - (4)(2) \\
&= 3 - 4 \lambda + \lambda^2 - 8 \\
&= \lambda^2 - 4 \lambda - 5 \\
&= (\lambda + 1)(\lambda - 5) \\
&\implies \lambda_1 = -1, \; \lambda_2 = 5
\end{align*}

\begin{align*}
(A - \lambda_1 I)x = 0 &\implies \begin{bmatrix} 1 - (-1) & 4 & 0 \\ 2 & 3 - (-1) & 0 \end{bmatrix} \\
&\implies \begin{bmatrix} 2 & 4 & 0 \\ 2 & 4 & 0 \end{bmatrix} \\
-R_1 + R_2 \to R_2 &\implies \begin{bmatrix} 2 & 4 & 0 \\ 0 & 0 & 0 \end{bmatrix} \\
\frac{1}{2} R_1 \to R_1 &\implies \begin{bmatrix} 1 & 2 & 0 \\ 0 & 0 & 0 \end{bmatrix}
\end{align*}

\begin{align*}
x_1 + 2x_2 &= 0 \\
x_1 &= -2x_2 \\
x_2 &\text{ is free}
\end{align*}

\begin{align*}
x &= x_2 \begin{bmatrix} -2 \\ 1 \end{bmatrix}
\end{align*}

\begin{align*}
(A - \lambda_2 I) x = 0 &\implies \begin{bmatrix} 1 - (5) & 4 & 0 \\ 2 & 3 - (5) & 0 \end{bmatrix} \\
&\implies \begin{bmatrix} -4 & 4 & 0 \\ 2 & -2 & 0 \end{bmatrix} \\
-\frac{1}{4} R_1 \to R_1 &\implies \begin{bmatrix} 1 & -1 & 0 \\ 2 & -2 & 0 \end{bmatrix} \\
-2R_1 + R_2 \to R_2 &\implies \begin{bmatrix} 1 & -1 & 0 \\ 0 & 0 & 0 \end{bmatrix}
\end{align*}

\begin{align*}
x_1 &= x_2 \\
x_2 &\text{ is free}
\end{align*}

\[x = x_2 \begin{bmatrix} 1 \\ 1 \end{bmatrix}\]

\begin{align*}
\det((A + I) - \lambda I) &= \begin{vmatrix} 2 - \lambda & 4 \\ 2 & 4 - \lambda \end{vmatrix} \\
&= (2 - \lambda)(4 - \lambda) - (2)(4) \\
&= 8 - 6 \lambda + \lambda^2 - 8 \\
&= \lambda(\lambda - 6) \\
&\implies \lambda_1 = 0, \; \lambda_2 = 6
\end{align*}

\begin{align*}
((A + I) - \lambda_1 I)x = 0 &\implies \begin{bmatrix} 2 & 4 & 0 \\ 2 & 4 & 0 \end{bmatrix} \\
&\implies x = \begin{bmatrix} -2 \\ 1 \end{bmatrix}
\end{align*}

\begin{align*}
((A + I) - \lambda_2 I)x = 0 &\implies \begin{bmatrix} 2 - (6) & 4 & 0 \\ 2 & 4 - (6) & 0 \end{bmatrix} \\
&\implies \begin{bmatrix} -4 & 4 & 0 \\ 2 & -2 & 0 \end{bmatrix} \\
&\implies x = \begin{bmatrix} 1 \\ 1 \end{bmatrix}
\end{align*}

* Problem 8

Find the eigenvalues and the eigenvectors of matrix A.
Show your work.

\[A = \begin{bmatrix} 1 & 0 & 0 \\ 1 & 2 & 0 \\ 2 & 3 & 3 \end{bmatrix}.\]

\begin{align*}
\det(A - \lambda) &= (1 - \lambda)(2 - \lambda)(3 - \lambda) \\
&\implies \lambda_1 = 1, \; \lambda_2 = 2, \; \lambda_3 = 3
\end{align*}

\begin{align*}
(A - \lambda_1 I)x = 0 &\implies \begin{bmatrix} 1 - (1) & 0 & 0 & 0 \\ 1 & 2 - (1) & 0 & 0 \\ 2 & 3 & 3 - (1) & 0 \end{bmatrix} \\
&\implies \begin{bmatrix} 0 & 0 & 0 & 0 \\ 1 & 1 & 0 & 0 \\ 2 & 3 & 2 & 0 \end{bmatrix} \\
R_2 \leftrightarrow R_3 &\implies \begin{bmatrix} 2 & 3 & 2 & 0 \\ 1 & 1 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{bmatrix} \\
R_1 \leftrightarrow R_2 &\implies \begin{bmatrix} 1 & 1 & 0 & 0 \\ 2 & 3 & 2 & 0 \\ 0 & 0 & 0 & 0 \end{bmatrix} \\
-2R_1 + R_2 \to R_2 &\implies \begin{bmatrix} 1 & 1 & 0 & 0 \\ 0 & 1 & 2 & 0 \\ 0 & 0 & 0 & 0 \end{bmatrix} \\
-R_2 + R_1 \to R_1 &\implies \begin{bmatrix} 1 & 0 & -2 & 0 \\ 0 & 1 & 2 & 0 \\ 0 & 0 & 0 & 0 \end{bmatrix}
\end{align*}

\begin{align*}
x_1 &= 2x_3 \\
x_2 &= -2x_3 \\
x_3 &\text{ is free}
\end{align*}

\[x = x_3 \begin{bmatrix} 2 \\ -2 \\ 1 \end{bmatrix}\]

\(\lambda_2\) and \(\lambda_3\) produce no solutions.
You can verify this using Gaussian Elimination.

* Problem 9

Find the matrix \(A\) that performs those transformations, in order, on the Cartesian plane.
To which point is the point (-2, 1) mapped by this transformation.

a) horizontal stretch by a factor of 3
b) reflection across the line \(y = x\)

\begin{align*}
A_1 &= \begin{bmatrix} 3 & 0 \\ 0 & 1 \end{bmatrix} \\
A_2 &= \begin{bmatrix} 0 & 1 \\ 1 & 0 \end{bmatrix} \\
A &= A_2 A_1 \\
&= \begin{bmatrix} 0 & 3 \\ 1 & 0 \end{bmatrix}
\end{align*}

* Problem 10

Find the standard matrix \(A\) for the given linear transformation \(T\).

\begin{align*}
T\left(\begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix}\right) &= \begin{bmatrix} x_1 + 2x_2 - 3x_3 \\ 0 \\ x_1 + 4x_3 \\ 5x_2 + x_3 \end{bmatrix}
\end{align*}

\begin{align*}
T\left(\begin{bmatrix} 1 \\ 0 \\ 0 \\ 0 \end{bmatrix}\right) &= \begin{bmatrix} 1 \\ 0 \\ 1 \\ 0 \end{bmatrix} \\
T\left(\begin{bmatrix} 0 \\ 1 \\ 0 \\ 0 \end{bmatrix}\right) &= \begin{bmatrix} 2 \\ 0 \\ 0 \\ 5 \end{bmatrix} \\
T\left(\begin{bmatrix} 0 \\ 0 \\ 1 \\ 0 \end{bmatrix}\right) &= \begin{bmatrix} -3 \\ 0 \\ 4 \\ 1 \end{bmatrix} \\
T\left(\begin{bmatrix} 0 \\ 0 \\ 0 \\ 1 \end{bmatrix}\right) &= \begin{bmatrix} 0 \\ 0 \\ 0 \\ 0 \end{bmatrix}
\end{align*}

\[A = \begin{bmatrix} 1 & 2 & -3 & 0 \\ 0 & 0 & 0 & 0 \\ 1 & 0 & 4 & 0 \\ 0 & 5 & 1 & 0 \end{bmatrix}\]

* Problem 11

#+begin_src julia
function minmaxavg(a)
    max = -Inf
    min = Inf
    for n ∈ a
        if n < min
            min = n
        end
        if n > max
            max = n
        end
    end
    (max + min) / 2
end
#+end_src

Its worst case time complexity in terms of the number of comparisons is \(2n\) since the list is only traversed once however makes two comparisons for each iteration.

* Problem 12

Let \(f(n) = 2n \log(n^2 + 3) + 7n + 1\).
What is big-O estimate of \(f(n)\)?
Be sure to specify the values of the witnesses \(C\) and \(k\).

\begin{align*}
f(n) &< 2n \log(n^2 + 3) + 7n + 1 \\
&< 2n \log(n^2 + n^2) + 7n + n \\
&< 2n \log(2n^2) + 8n \\
&< 2n \log(n^3) + 8n \\
&< 6n \log(n) + 8n \\
&< 6n^2 + 8n \\
&< 6n^2 + 8n^2 \\
&< 14n^2
\end{align*}

\begin{align*}
3 &< n^2 \\
1 &< n \\
2n^2 &< n^3 \\
n &< n^2 \\
\log(n) &< n
\end{align*}

\[C = 14, \; k = 3 \text{ from } 2n^2 < n^3\]
