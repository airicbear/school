#+TITLE: Exercises Hartman
#+AUTHOR: Eric Nguyen

* Matrix Arithmetic

** Vector Solutions to Linear Systems

*** Exercise 18

Matrix \(A\) and vector \(\vec{b}\) are given below.

1. Solve the equation \(A\vec{x} = 0\), where 0 is a zero vector.

2. Solve the equation \(A\vec{x} = b\).

In each of the above, be sure to write your answer in vector format.
Show your work.

\[A = \begin{bmatrix}2 & 2 & 2 \\ 5 & 5 & -3\end{bmatrix}, \;\vec{b} = \begin{bmatrix}3 \\ -3\end{bmatrix}\]

**** Problem 1

\begin{align*}
A\vec{x} = 0 &\implies \begin{bmatrix}
2 & 2 & 2 & 0 \\
5 & 5 & -3 & 0
\end{bmatrix} \\
\frac{1}{2} R_1 \to R_1 &\implies
\begin{bmatrix}
1 & 1 & 1 & 0 \\
5 & 5 & -3 & 0
\end{bmatrix} \\
-5R_1 + R_2 \to R_2 &\implies
\begin{bmatrix}
1 & 1 & 1 & 0\\
0 & 0 & -8 & 0
\end{bmatrix} \\
-\frac{1}{8} R_2 \to R_2 &\implies
\begin{bmatrix}
1 & 1 & 1 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix} \\
R_1 - R_2 \to R_1 &\implies
\begin{bmatrix}
1 & 1 & 0 & 0 \\
0 & 0 & 1 & 0
\end{bmatrix}
\end{align*}

\begin{align*}
x_1 + x_2 &= 0 \\
x_2 &\text{ is free} \\
x_3 &= 0
\end{align*}

\begin{align*}
x_1 &= -x_2 \\
x_2 &\text{ is free} \\
x_3 &= 0
\end{align*}

\begin{align*}
\vec{x} =
\begin{bmatrix}
x_1 \\ x_2 \\ x_3
\end{bmatrix} =
\begin{bmatrix}
-x_2 \\ x_2 \\ 0
\end{bmatrix} =
x_2 \begin{bmatrix}
-1 \\ 1 \\ 0
\end{bmatrix}
\end{align*}

**** Problem 2

\begin{align*}
A\vec{x} = \vec{b} &\implies
\begin{bmatrix}
2 & 2 & 2 & 3 \\
5 & 5 & -3 & -3
\end{bmatrix} \\
\frac{1}{2} R_1 \to R_1 &\implies
\begin{bmatrix}
1 & 1 & 1 & \frac{3}{2} \\
5 & 5 & -3 & -3
\end{bmatrix} \\
-5R_1 + R_2 \to R_2 &\implies
\begin{bmatrix}
1 & 1 & 1 & \frac{3}{2} \\
0 & 0 & -8 & -\frac{21}{2}
\end{bmatrix} \\
-\frac{1}{8} R_2 \to R_2 &\implies
\begin{bmatrix}
1 & 1 & 1 & \frac{3}{2} \\
0 & 0 & 1 & \frac{21}{16}
\end{bmatrix} \\
R_1 - R_2 \to R_1 &\implies
\begin{bmatrix}
1 & 1 & 0 & \frac{3}{16} \\
0 & 0 & 1 & \frac{21}{16}
\end{bmatrix}
\end{align*}

\begin{align*}
x_1 + x_2 &= \frac{3}{16} \\
x_2 &\text{ is free} \\
x_3 &= \frac{21}{16}
\end{align*}

\begin{align*}
x_1 &= -x_2 + \frac{3}{16} \\
x_2 &\text{ is free} \\
x_3 &= \frac{21}{16}
\end{align*}

\begin{align*}
\vec{x} &= \begin{bmatrix}-x_2 + \frac{3}{16} \\ x_2 \\ \frac{21}{16}\end{bmatrix} \\
&= \begin{bmatrix}-x_2 \\ x_2 \\ 0\end{bmatrix} +
\begin{bmatrix}\frac{3}{16} \\ 0 \\ \frac{21}{16}\end{bmatrix} \\
&= x_2 \begin{bmatrix}-1 \\ 1 \\ 0\end{bmatrix} +
\begin{bmatrix}\frac{3}{16} \\ 0 \\ \frac{21}{16}\end{bmatrix}
\end{align*}
