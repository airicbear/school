#+title: Paper #2: Analysis of a Topic

* Requirements

  - Your paper must be 5-7pages in length, not including your Works Cited page

  - You must cite at least six sources that you find on your own (6+ total)

  - You must cite all sources properly using MLA Style for in-text citation and Works Cited page

* Description

  Write a paper in which you *analyze a topic of your choice related to surveillance*.
  The purpose of your paper will be to describe what research and writing exists about your topic, to evaluate the status of the topic right now, in fall of 2019, and to offer your position on the future of the discourse on this topic (that is, how, why, and where do you expect future researchers or writers to enter into this discourse in the next few years?).
  Another way to put this is that you’ll be describing and synthesizing the scholarly conversation about your topic, which is a vital step in constructing academic arguments of your own in the future.

  This paper is a step up from your first paper in which you analyzed a single author’s argument about a topic.
  Here, you will demonstrate that you understand and can appropriately evaluate the current shape of a topic you’ve chosen /as if/ you were going to make a full argument of your own.
  In so doing, you will also demonstrate that you understand the research process and can effectively evaluate library and internet sources.
  This paper’s process, particularly its emphasis on reviewing the existing literature, will be valuable when you get to Paper #3 where you will make your own full argument on a topic of your choice.

  This is a relatively common assignment that you may see again in your studies at Temple University.
  It is often called a “literature review” or a “survey of the field,” and the audience for your paper will again be academic.
  That is, you are writing to fellow scholars who are interested in your topic and who are looking to understand a full picture of the literature about it.

  The steps for researching and writing this paper are as follows, though you may need to circle back through them multiple times as you work on this assignment.
  As will be described in our library sessions, the research process is iterative, which means that it is not a one-and-done process.

  1. Select a clear, specific topic related to surveillance about which scholars and other writers have published in the last 10-15 years.
     We will brainstorm topics together in class, and our readings will also provide you with some good ideas.
     You will not search only for your exact topic, but you’ll need to read widely about your topic in a variety of fields and contexts.

  2. Using the library and/or Google Scholar, you will examine and evaluate published research and writing about your topic.
     Spend time exploring and figuring out who these writers and researchers are:

     - What fields are they in?

     - What publications are they using?

     - How often does your topic come up in non-scholarly venues, like newspapers, magazines, and online sites?

     - Follow the links other scholars have provided and look through their Works Cited pages to see what those scholars read and found influential.

  3. Once you have a sense of the published literature about your topic, make sense of what you’ve read.

     - What are the various arguments or perspectives you’ve seen and how do they relate to one another?

     - Which sources are most representative or best and would be most useful in a research paper about your topic?

     - Which sources are outliers or most innovative, and why?
       Which sources are most traditional and mainstream?

     - Do any of the sources you’ve read suggest solutions to problems or pose new problems that future researchers will need to examine?

     - What questions arise as you make sense of these sources?
       What is missing from this discourse?

  You may wonder: am I making an argument of my own in this paper?

  Yes! But it’s an argument about the state of the scholarly conversation of the topic you’ve researched, not an argument of your own about the topic itself.

  For example, if my topic is the use of FaceApp as a surveillance tool, my research would find a wide range of sources, all of which are related to the use of apps as surveillance.
  /A simple search for “FaceApp” would not give me many sources, but a search for other, related topics, will produce a lot of excellent research material/.
  In popular sources, I would find experts warning people not to use such apps because they compromise our private information, but I would also find lots of people gleefully sharing their FaceApp pictures on Twitter and Instagram.
  I might also find articles in psychological literature about the entertainment value of using apps and connecting with social media.
  I might find sources in the political science world talking about Cambridge Analytica and/or the Russian influence on technology in the US, and they might speculate or prove that apps are invasive compromises on personal and national security.
  Perhaps I would also find scholars in computer science talking about AI and the promise of algorithms like FaceApp, and this conversation might be quite complex and technical in nature.
  I would likely have had to go through several stages of research, because articles just about FaceApp might be hard to find, but articles with a wider focus on apps and security are likely to be my best sources.

  After considering these (and other) scholarly conversations, I might wind up at an argument like this:
  /Experts tell us that FaceApp and similar programs are dangerous, but many of us still download and use them for entertainment purposes all the time/.
  /Congress and others have called for investigations of such programs because of the dangers they pose to our personal and national security/.
  /But most interestingly, the scholarship of experts talking to other experts about biometric surveillance suggests that pop-culture warnings about FaceApp are many years too late because biometric surveillance is already highly advanced, thoroughly integrated into many of our public and private systems, and probably impossible to avoid/.

  Yes, this statement of argument may seem long, but it’s an argument that accurately captures the current scholarly conversation and puts the information into an order that someone not in the conversation will understand.
  My paper would lead readers through the research I have found, and I would demonstrate that this research is valuable and complex.

  Toward the end of my paper, I might wish to weigh in on the topic as a result of my research.
  Perhaps I’m not worried about about FaceApp, even after all my reading and thinking about it.
  Perhaps I am excited about the future of programming with biometric algorithms, and if I were to write a longer paper about this topic, I’d be arguing for such programming, not against it.
  So my argument might still be similar to the one in italics above, but my conclusion could say something like: yes, experts are correct that we cannot avoid biometric surveillance, but we do not need to be passive in the face of biometric surveillance.
  We can become coders ourselves and change the discourse from within the industry.
  We can find ways to hack the systems and resist the biases ourselves, and new scholarship is starting to share how and where we can do so.
  This might be an ideal way to end my argument because I would be pointing to new scholarship and showing where the scholarly conversation might go next, which is exactly what this sort of paper should do.
