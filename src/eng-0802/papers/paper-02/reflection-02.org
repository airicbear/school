Essay Reflection

1. What was your process for writing the paper? What part of that
   process worked well, and what part of the process did you find to be
   challenging?

My process for writing the paper was to find keywords in the article,
read the paragraphs in which I found those words, and then see how I
could include that in my paper.

The part of that process that worked well is that I easily found things
to write about.

The challenging part was that I did not dedicate enough time to write
this paper and did not put enough effort into this paper.

2. If you were to grade your paper on a scale from 1-10 (where 10 means
   that it's a perfect draft and needs no further revision, and 1 means
   that it's a very early draft and needs substantial revision to become
   a passable paper), where would you grade your current draft and why?
   (Note, don't worry---I will not necessarily agree or disagree with
   your grading of the paper, but it's useful to compare your impression
   of the paper with my feedback).

I would grade my current draft as a 3 because I did not write about the
differences between the articles, I did not read the articles enough
that I understood them well, and towards the end of the paper, I wrote
down random things, but I wrote the paper at the minimum required
length, with much of the content being quotes from the articles.

3. What sort of feedback would you most like to get from me about the
   paper?

I would like to get feedback on how I can write better introductions and
conclusions and point out the differences between the articles.
