#+title: Library Workshops

* General workshop

** Source 1

   | Source URL          | [[http://web.b.ebscohost.com/ehost/detail/detail?vid=0&sid=c8aa46c2-dc2f-486c-a667-35d3024fd8ec%40pdc-v-sessmgr06&bdata=JnNpdGU9ZWhvc3QtbGl2ZSZzY29wZT1zaXRl#AN=129918138&db=cax][URL]]                                                                  |
   | Author              | Kwok Choon, Mary Jane                                                |
   | Publication         | Canadian Journal of Communication                                    |
   | Year of publication | 2018                                                                 |
   | Information         | Document Type, Subject Terms, Keywords, Reviews, Abstract, ISSN, DOI |
   | Usage               |                                                                      |

** Source 2

   | Source URL          |   |
   | Author              |   |
   | Publication         |   |
   | Year of publication |   |
   | Information         |   |
   | Usage               |   |

** Source 3

   | Source URL          |   |
   | Author              |   |
   | Publication         |   |
   | Year of publication |   |
   | Information         |   |
   | Usage               |   |

** Source 4

   | Source URL          |   |
   | Author              |   |
   | Publication         |   |
   | Year of publication |   |
   | Information         |   |
   | Usage               |   |

* Synthesizing Sources

** List the authors and titles for 3 of your research sources here in the order in which they were published (oldest to newest):

*** 1

**** Authors

     Elizabeth Stoycheff and G. Scott Burgess and Maria Clara Martucci

**** Title

     Online censorship and digital surveillance: the relationship between suppression technologies and democratization across countries

*** 2

**** Authors

     Tanczer, Leonie Maria and McConville, Ryan and Maynard, Peter

**** Title

     Censorship and Surveillance in the Digital Age: The Technological Challenges for Academics

*** 3

**** Author

     Xiao Qiang

**** Title

     President XI’s Surveillance State

** What elements do these sources have in common? Be as specific as possible.

   These sources all have in common the idea of how digital surveillance restricts people and democracy.

** What elements of these sources are most different (Audience? Genre? Content? Purpose?)

   These sources differ in specific populations.

* Talk To A Database

** Learn to Speak the Language of Search

   1. In your group, talk about your assigned topic.

   2. Spend 5-7 minutes doing background research on the topic.
      You can search anywhere on the web or library website.

   3. Prepare to share your answers with the class.
      You may assign a reporter to share the answers.

** Brainstorm

   | Questions to consider                                                                                                                                                                                                | What keywords would I use to find out more? |
   |----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------------------------------------------|
   | *What* are the most important issues related to this topic? Are there synonyms for any of the words? Are there specialized terms that experts on this topic might use? What aspects of this topic are being ignored? |                                             |
   | *Who* are the important people associated with this topic? Who writes about this topic, and whose voices are not being heard?                                                                                        |                                             |
   | *When* did important historical events happen that might have influenced this topic? Is there a time of year when this topic is more frequently discussed in the media?                                              |                                             |
   | *Where* are the important places that might have influenced this topic? What kinds of places are they? Where can I read about this topic in the media?                                                               |                                             |
   | *Why* does this matter? Why should people care about this topic? *How* does the information I have found change what I thought about this topic? How and why do different authors discuss this topic differently?    |                                             |

** Get Help With Your Assignments

   - Research Guide: https://guides.temple.edu/english802

   - Visit the *One Stop Assistance Desk:*
     We're open every day!

   - Ask a Librarian
     IM, TEXT and EMAIL:
     https://library.temple.edu/contact-us
