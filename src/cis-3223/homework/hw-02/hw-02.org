#+TITLE: HW2
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Problem 1.2

We can express numbers up to \(b^k - 1\) for number in base \(b\) with at most \(k\) digits.
From this expression, we find that a number in base \(b\) requires at least \(k = \lceil \log_b{(N + 1)} \rceil\) number of digits to represent an integer \(N \geq 0\).
Then we want to show that a binary integer of \(\lceil \log_2{(N + 1)} \rceil\) digits is at most four times as long as the corresponding decimal integer of \(\lceil \log_{10}{(N + 1)} \rceil\) digits.

The ratio of these two lengths is
\[\frac{\log_2{(N)}}{\log_{10}{(N)}} = \log_2{(10)} \approx 3.3219 \ldots\]
by the change of base rule for logarithms where we omit any constants and the ceiling of the values as these do not affect the ratio much for large \(N\).

Given that this ratio is less than four, it is apparent that the length of a binary number cannot be larger than four times the length as its corresponding decimal integer.

* Problem 1.7

The recursive multiplication algorithm on page 25 would take \(O(m^2)\) for a multiplication of two numbers with \(n\) and \(m\) numbers of bits, respectively.
For each recursive call \(m\) is halved, thereby decreasing the number of bits in the second number to \(m - 1\) bits giving \(m\) number of recursive calls.
Within a recursive call, we have \(O(m)\) bit operations from a division by 2, a test for odd/even, a multiplication by 2, and a possible addition.
Since there are \(m\) recursive calls where each recursive call requires \(O(m)\) operations, we find that the recursive multiplication algorithm takes \(O(m^2)\) time.

* Problem 1.8

For each recursive call, we reduce the number of bits in the dividend \(x\) by one, giving \(O(n)\) recursive calls.
A recursive call has a division by 2, two multiplications by 2, one check for odd, one check for inequality, and a possible one or two additions which gives \(O(n)\) bit operations.
Then the total time for the recursive division algorithm is \(O(n^2)\). \\
\\
#+begin_proof
\noindent /Proof/: We prove the correctness of the algorithm using induction on size \(n\) of the bitlength of \(x + 2\), i.e. \(n = \lceil \log_2{(x + 1)} \rceil + 2\).

\noindent /Precondition/:

    - \(x \in \mathbb{Z}\), \(y \in \mathbb{Z}\), \(y > 0\)

\noindent /Postcondition/:

    - \(x = yq + r\) for some quotient \(q\) and remainder \(r\) and \(r < y\).

\noindent /Base case/: \(n = 2\), i.e., \(x = 0\). The algorithm terminates and returns (0, 0). \\

\noindent /Inductive step/: Let \(n > 2\), i.e., \(x > 1\) and suppose that the postcondition holds for all inputs of size \(k\) for \(1 \leq k < n\) (IH). \\

\noindent /Case 1/ \(x\) is even. Recursively applying the division algorithm on \(\frac{x}{2}\) by \(y\), we obtain \(\frac{x}{2} = qy + r \implies x = 2qy + 2r\). Thus, our quotient and remainder would be \(2q\) and \(2r\) respectively by IH. \\

\noindent /Case 2/ \(x\) is odd. Recursively applying the division algorithm on \(\frac{x - 1}{2}\), we obtain \(\frac{x - 1}{2} = qy + r \implies x = 2qy + 2r + 1\). Thus, our quotient and remainder would be \(2q\) and \(2r + 1\) respectively by IH. \\

Finally, the algorithm normalizes the remainder such that the postcondition \(r < y\) holds.
By proof of induction, we prove that the recursive division algorithm is correct.
#+end_proof

* Problem 1.18

** Factorization

\begin{align*}
588 &= 2 \cdot 2 \cdot 3 \cdot 7 \cdot 7 = 2^2 \cdot 3 \cdot 7^2 \\
210 &= 2 \cdot 5 \cdot 3 \cdot 7
\end{align*}

Product of common factors is \(2 \cdot 3 \cdot 7 = 42\).
So, \(\gcd(210, 588) = 42\).

#+begin_comment
\begin{align*}
    588 &= 2 \cdot 210 + 168 \\
    210 &= 1 \cdot 168 + 42 \\
    168 &= 4 \cdot 42 + 0
\end{align*}

\[\gcd(210, 588) = 42\]

\begin{align*}
42 &= 42 - 0 \\
42 &= 42 - (168 - 4 \cdot 42) = -1 \cdot 168 + 5 \cdot 42 \\
42 &= -1 \cdot 168 + 5 (210 - 1 \cdot 168) = 5 \cdot 210 - 6 \cdot 168 \\
42 &= 5 \cdot 210 - 6(588 - 2 \cdot 210) = -6 \cdot 588 + 17 \cdot 210
\end{align*}

So, for \(\gcd(210, 588) = 42 = d = ax + by\) where \(a = 588\), \(b = 210\), then \(x\) and \(y\) values that satisfy this condition are \(x = -6\) and \(y = 17\).
Evaluating the expression \(d = ax + by = (588)(-6) + (210)(17) = 42\) we verify that 42 is indeed \(\gcd(210, 588)\).
#+end_comment

** Euclid's algorithm

\begin{align*}
    \gcd(210, 588) &= \gcd(588, 210) \\
                   &= \gcd(210, 168) \\
                   &= \gcd(168, 42) \\
                   &= \gcd(42, 0) \\
                   &= 42
\end{align*}

* Problem 1.27

- \(p = 17\)

- \(q = 23\)

- \(N = 391\)

- \(e = 3\)

- \((p - 1)(q - 1) = 352\)

- \(d = 3^{-1} \bmod 352 = 235\)

- The encryption of the message \(M = 41\) is \(M^e \bmod N \implies 41^3 \bmod 391 = 105\)

* Problem 1.28

- \(p = 7\)

- \(q = 11\)

- \(N = pq = 77\)

- Choose \(e = 7\) where \(\gcd(e, (p - 1)(q - 1)) = \gcd(7,60) = 1\) i.e. \(e\) is relatively prime to \((p - 1)(q - 1)\).

- Then \(d = 7^{-1} \bmod 60\).
  To find \(d\) we use the Extended Euclidean algorithm to calculate \(e\) inverse modulo \((p - 1)(q - 1)\).

  \begin{align*}
    60 &= 8 \cdot 7 + 4 \\
    7 &= 1 \cdot 4 + 3 \\
    4 &= 1 \cdot 3 + 1 \\
    3 &= 3 \cdot 1 + 0
  \end{align*}

  \begin{align*}
    1 &= 1 - 0 \\
    1 &= 1 - (3 - 3 \cdot 1) = -1 \cdot 3 + 4 \cdot 1 \\
    1 &= -1 \cdot 3 + 4(4 - 1 \cdot 3) = 4 \cdot 4 - 5 \cdot 3 \\
    1 &= 4 \cdot 4 - 5(7 - 1 \cdot 4) = -5 \cdot 7 + 9 \cdot 4 \\
    1 &= -5 \cdot 7 + 9(60 - 8 \cdot 7) = 9 \cdot 60 - 77 \cdot 7
  \end{align*}

  \begin{align*}
    9 \cdot 60 - 77 \cdot 7 &= 1 \\
    -77 \cdot 7 &\equiv 1 \bmod 60 \\
    &\equiv -17 \bmod 60 \\
    &\equiv 43 \bmod 60 \\
    &\implies d = 43
  \end{align*}

- So \(e = 7\), \(d = 43\)
