#+TITLE: Homework 7
#+AUTHOR: Eric Nguyen
#+OPTIONS:
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Problem 6.2

This algorithm finds the optimal route of hotel stops and returns the indices of each hotel stop in the route.

#+begin_src julia
function hotelopt(a::Vector{Int})
    penalties = [(200 - x)^2 for x in a]
    path = fill(1, length(a))

    # Update the penalties and the path
    for i in 1:length(a)
        for j in 1:i
            penalty = (200 - (a[i] - a[j]))^2
            if penalties[i] > penalties[j] + penalty
                penalties[i] = penalties[j] + penalty
                path[i] = j + 1
            end
        end
    end

    # Trace the path to output the optimal route
    optroute = []
    index = length(a)
    while index >= 1
        pushfirst!(optroute, index)
        index = path[index] - 1
    end

    return optroute
end
#+end_src

The running time of this algorithm is \(O(n^2)\).

* Problem 6.3

#+begin_src julia
function maxprofit(m::Vector{Int}, p::Vector{Int})

end
#+end_src

* Problem 6.8
* Problem 6.21
