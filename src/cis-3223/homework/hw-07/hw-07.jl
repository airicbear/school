### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ a1e16b84-92a3-11eb-1496-4bcbdfbcaada
md"# Homework 7

Name: Eric Nguyen

Date: April 1, 2021

Language: Julia"

# ╔═╡ 76e2afa8-92a3-11eb-1669-e798530495cb
md"## Problem 6.2

This algorithm finds the optimal route of hotel stops and returns the indices of each hotel stop in the route. It's running time is $O(n^2)$ from the nested loop."

# ╔═╡ 61665d58-92a3-11eb-1369-39b09cda79b9
function hotelopt(a::Vector{Int})
    penalties = [(200 - x)^2 for x in a]
    path = fill(1, length(a))

    # Update the penalties and the path
    for i in 1:length(a)
        for j in 1:i
            penalty = (200 - (a[i] - a[j]))^2
            if penalties[i] > penalties[j] + penalty
                penalties[i] = penalties[j] + penalty
                path[i] = j + 1
            end
        end
    end

    # Trace the path to output the optimal route
    optroute = []
    index = length(a)
    while index >= 1
        pushfirst!(optroute, index)
        index = path[index] - 1
    end

    return optroute
end

# ╔═╡ 0499e328-92a4-11eb-28ff-7f578b590f4d
hotels = [50, 100, 200, 300, 400, 500, 600] # Mock data

# ╔═╡ 0cfe139a-92a4-11eb-38ab-ef8d609ffb64
hotelopt(hotels) # Optimal route for mock data

# ╔═╡ 78267efe-92a5-11eb-133f-07c5b083d83e
md"## Problem 6.3

This algorithm computes the maximum expected total profit for Yuckdonald's given the constraints. It's running time is $O(n^2)$ from the nested loop."

# ╔═╡ 7ff1940c-92a5-11eb-17fd-a73255d94cc5
function maxprofit(m::Vector{Int}, p::Vector{Int}, k::Int)
	P = fill(0, length(p))
	
	for i in 1:length(p)
		for j in 1:i
			profit = (m[i] - m[j] >= k) ? p[i] : 0
			
			if P[i] < P[j] + profit
				P[i] = P[j] + profit
			end
			
			if P[i] < p[i]
				P[i] = p[i]
			end
		end
	end
	
	return P[end]
end

# ╔═╡ ea34f1ba-92aa-11eb-2069-2d77d559d3fc
begin # Test using mock data
	miles = [1, 3, 5, 7]
	profit = [4, 10, 5, 2]
	k = 3
	maxprofit(miles, profit, k)
end

# ╔═╡ 1073fec0-92dd-11eb-3988-ef6baa2cd444
md"## Problem 6.8

This algorithm computes the longest common substring between two strings $a$ and $b$.
It's running time is $O(mn)$ where $n$ and $m$ are the lengths of each string respectively."

# ╔═╡ 2abb6d54-92dd-11eb-04f0-b567123af3a3
function lcs(a::String, b::String)
	L = fill(0, (length(a), length(b))) # n × m matrix
	l = 0 								# length of the longest common substring
	s = ""								# the longest common substring
	
	for i in 1:length(a)
		for j in 1:length(b)
			if a[i] == b[j]
				
				# Update the matrix
				if i == 1 || j == 1
					L[i, j] = 1
				else
					L[i, j] = L[i - 1, j - 1] + 1
				end
				
				# Update the lcs
				if L[i, j] > l
					l = L[i, j]
					s = a[i - l .+ 1:i]
				elseif L[i, j] == l
					s = s * a[i - l .+ 1:i]
				end
				
			else
				L[i, j] = 0
			end
		end
	end
	
	return s
end

# ╔═╡ eb22a3ee-92df-11eb-2d14-e92686ec65bf
# Test 1
lcs("ABC123ABC", "123ABCABC")

# ╔═╡ f1f649b8-92e0-11eb-2cce-73403301aea7
# Test 2
lcs("here is a sequence of characters", "here's another sequence of characters")

# ╔═╡ 33d669d8-92e1-11eb-1d47-3be4ec881438
md"## Problem 6.21

This algorithm computes the size of the minimum vertex cover for an undirected tree $T = (V, E)$ which is implemented as an adjacency list using a hash table. We compute it using the relation $$I(u) = \min \left\{1 + \underset{\text{grandchildren } w \text{ of } u}{\sum} I(w), \underset{\text{children } w \text{ of } u}{\sum} I(w)\right\}$$ where $I(u) = \text{minvc}(T, u)$.

It's running time is $O(|V| + |E|)$."

# ╔═╡ 3b87093c-92e1-11eb-3718-dd49f48fa012
begin
	function minvc(T::Dict, root)
		cache = Dict(collect(keys(T)) .=> -1) # minvc is initially -1 for each node
		return minvc(T, root, cache)
	end

	function minvc(T::Dict, root, cache)
		# If the minvc is not yet cached, then compute it
		if cache[root] == -1
			excludeRoot = length(T[root])
			includeRoot = 1
			
			for child in T[root]
				for grandchild in T[child]
					excludeRoot += minvc(T, grandchild, cache)
				end
				
				includeRoot += minvc(T, child, cache)
			end
			
			cache[root] = min(includeRoot, excludeRoot)
		end
		
		return cache[root]
	end
end

# ╔═╡ d067ab66-92e5-11eb-103c-6526d0acd892
# Example tree 1
T1 = Dict(
	:A => [:B, :C, :D],
	:B => [],
	:C => [],
	:D => [:F],
	:F => [],
)

# ╔═╡ f76fe426-92e5-11eb-293e-dd0561cc3e37
# Test tree 1
minvc(T1, :A)

# ╔═╡ befed088-92f5-11eb-061d-273dd3a0e341
# Example tree 2
T2 = Dict(
	:A => [:B],
	:B => [:C],
	:C => [:D],
	:D => [:F],
	:F => [:G],
	:G => [],
)

# ╔═╡ da5c0f08-92f5-11eb-1ff5-03017e12c4c6
# Test tree 2
minvc(T2, :A)

# ╔═╡ Cell order:
# ╟─a1e16b84-92a3-11eb-1496-4bcbdfbcaada
# ╟─76e2afa8-92a3-11eb-1669-e798530495cb
# ╠═61665d58-92a3-11eb-1369-39b09cda79b9
# ╠═0499e328-92a4-11eb-28ff-7f578b590f4d
# ╠═0cfe139a-92a4-11eb-38ab-ef8d609ffb64
# ╟─78267efe-92a5-11eb-133f-07c5b083d83e
# ╠═7ff1940c-92a5-11eb-17fd-a73255d94cc5
# ╠═ea34f1ba-92aa-11eb-2069-2d77d559d3fc
# ╟─1073fec0-92dd-11eb-3988-ef6baa2cd444
# ╠═2abb6d54-92dd-11eb-04f0-b567123af3a3
# ╠═eb22a3ee-92df-11eb-2d14-e92686ec65bf
# ╠═f1f649b8-92e0-11eb-2cce-73403301aea7
# ╟─33d669d8-92e1-11eb-1d47-3be4ec881438
# ╠═3b87093c-92e1-11eb-3718-dd49f48fa012
# ╠═d067ab66-92e5-11eb-103c-6526d0acd892
# ╠═f76fe426-92e5-11eb-293e-dd0561cc3e37
# ╠═befed088-92f5-11eb-061d-273dd3a0e341
# ╠═da5c0f08-92f5-11eb-1ff5-03017e12c4c6
