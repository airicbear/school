#+TITLE: HW 03
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Problem 2.3

** Part (a)

\begin{align*}
  T(n) &\leq 3T(n/2) + cn \\
       &\leq 3[3T(n/4) + cn/2] + cn = 9T(n/4) + \left(\frac{3}{2} + 1\right) cn \\
       &\leq 9[3T(n/8) + cn/4] + \frac{5}{2} cn = 27T(n/8) + \left(\frac{9}{4} + \frac{3}{2} + 1\right) cn \\
       &\leq 27[3T(n/16) + cn/8] + \frac{19}{4} cn = 81T(n/16) + \left(\frac{27}{8} + \frac{9}{4} + \frac{3}{2} + 1\right) cn \\
       &\;\;\vdots
\end{align*}

The general \(k\)th term is

\begin{align*}
T(n) &\leq 3^k T(n/2^k) + \frac{1 - \left(\frac{3}{2}\right)^k}{1 - \frac{3}{2}}cn \\
&\leq 3^k T(n/2^k) - 2 + 2 \left(\frac{3}{2}\right)^k cn \\
\end{align*}

Plugging in \(k = \log_{\frac{3}{2}} n\), we get

\begin{align*}
T(n) &\leq 3^{\frac{1}{\log_3{\left(\frac{3}{2}\right)}}}n T\left(\frac{n}{2^{\frac{1}{\log_2{\left(\frac{3}{2}\right)}}}n}\right) - 2 + 2cn^2 \\
     &\leq 3^{\frac{1}{\log_3{\left(\frac{3}{2}\right)}}}n T\left(2^{\frac{1}{\log_2{\left(\frac{3}{2}\right)}}}\right) - 2 + 2cn^2 \\
&= O(n^2)
\end{align*}

** Part (b)

\begin{align*}
T(n) &\leq T(n - 1) + 1 \\
&\leq [T(n - 2) + 1] + 1 = T(n - 2) + 2 \\
&\leq [T(n - 3) + 1] + 2 = T(n - 3) + 3 \\
&\leq [T(n - 4) + 1] + 3 = T(n - 4) + 4 \\
&\;\;\vdots
\end{align*}

The general \(k\)th term is \(T(n - k) + k\).

Plugging in \(k = n\), we get \(T(n) \leq T(0) + n = O(n)\).

* Problem 2.5 (a to j)

** (a) \(T(n) = 2T(n / 3) + 1\)

\begin{align*}
T(n) &= 2T(n/3) + 1 \\
     &= 2[2T(n/9) + 1] + 1 = 4T(n/9) + 2 \\
     &= 4[2T(n/27) + 1] + 2 = 8T(n/27) + 3 \\
     &= 8[2T(n/81) + 1] + 3 = 16T(n/27) + 4 \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(2^{k-1}T(n/3^k) + k\).

- \(a = 2\), \(b = 3\), \(f(n) = 1\), \(k = \log_3{2}\).

- \(T(n) = \Theta(n^{\log_3{2}})\) from Case 1 of the Master Theorem.

** (b) \(T(n) = 5T(n / 4) + n\)

\begin{align*}
T(n) &= 5T(n/4) + n \\
     &= 5[5T(n/16) + n] + n = 25T(n/16) + 2n \\
     &= 25[5T(n/64) + n] + 2n = 125T(n/64) + 3n \\
     &= 125[5T(n/256) + n] + 3n = 625T(n/256) + 4n \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(5^k T(n/4^k) + kn\).

- \(a = 5\), \(b = 4\), \(f(n) = n\), \(k = \log_4{5}\).

- \(T(n) = \Theta(n^{\log_4{5}})\) from Case 1 of the Master Theorem.

** (c) \(T(n) = 7T(n / 7) + n\)

\begin{align*}
  T(n) &= 7T(n/7) + n \\
       &= 7[7T(n/49) + n] + n = 49T(n/49) + 2n \\
       &= 49[7T(n/343) + n] + 2n = 343T(n/343) + 3n \\
       &= 343[7T(n/2401) + n] + 3n = 2401T(n/2401) + 4n \\
\end{align*}

The general \(k\)th term is \(7^k T(n/7^k) + kn\).

- \(a = 7\), \(b = 7\), \(f(n) = n\), \(k = \log_7{7} = 1\).

- \(T(n) = \Theta(n)\) from Case 1 of the Master Theorem.

** (d) \(T(n) = 9T(n / 3) + n^2\)

\begin{align*}
T(n) &= 9T(n/3) + n^2 \\
     &= 9[9T(n/9) + n^2] + n^2 = 81T(n/9) + 2n^2 \\
     &= 81[9T(n/27) + n^2] + 2n^2 = 729T(n/27) + 3n^2 \\
     &= 729[9T(n/81) + n^2] + 3n^2 = 6561T(n/81) + 4n^2 \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(9^k T(n/3^k) + kn^2\).

- \(a = 9\), \(b = 3\), \(f(n) = n^2\), \(k = \log_3{9} = 2\).

- \(T(n) = \Theta(n^2)\) from Case 3 of the Master Theorem.

** (e) \(T(n) = 8T(n / 2) + n^3\)

\begin{align*}
T(n) &= 8T(n/2) + n^3 \\
     &= 8[8T(n/4) + n^3] + n^3 = 64T(n/4) + 2n^3 \\
     &= 64[8T(n/8) + n^3] + 2n^3 = 512T(n/8) + 3n^3 \\
     &= 512[8T(n/16) + n^3] + 3n^3 = 4096T(n/16) + 4n^3 \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(8^k T(n/2^k) + kn^3\).

- \(a = 8\), \(b = 2\), \(f(n) = n^3\), \(k = \log_2{8} = 3\).

- \(T(n) = \Theta(n^3)\) from Case 3 of the Master Theorem.

** (f) \(T(n) = 49T(n / 25) + n^{3/2} \log{n}\)

\begin{align*}
T(n) &= 49T(n/25) + n^{3/2} \log{n} \\
     &= 49[49T(n/625) + n^{3/2} \log{n}] + n^{3/2} \log{n} = 2401T(n/625) + 2n^{3/2} \log{n} \\
     &= 2401[49T(n/15625) + n^{3/2} \log{n}] + 2n^{3/2} \log{n} = 117649T(n/15625) + 3n^{3/2} \log{n} \\
     &= 117649[49T(n/390625) + n^{3/2} \log{n}] + 3n^{3/2} \log{n} = 5764801T(n/390625) + 4n^{3/2} \log{n} \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(49^k T(n/25^k) + k n^{3/2} \log{n}\).

- \(a = 49\), \(b = 25\), \(f(n) = n^{3/2} \log{n}\), \(k = \log_{25}{49}\).

- \(T(n) = \Theta(n^{3/2} \log^{2}{n})\) from Case 2 of the Master Theorem.

** (g) \(T(n) = T(n - 1) + 2\)

\begin{align*}
T(n) &= T(n - 1) + 2 \\
     &= [T(n - 2) + 2] + 2 = T(n - 2) + 4 \\
     &= [T(n - 3) + 2] + 4 = T(n - 3) + 6 \\
     &= [T(n - 4) + 2] + 6 = T(n - 4) + 8 \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(T(n - k) + 2k\).

- \(T(n) = \Theta(n)\) since the tree depth \(2k\) dominates the number of subproblems \(1\).

** (h) \(T(n) = T(n - 1) + n^c\), *where \(c \geq 1\) is a constant*

\begin{align*}
T(n) &= T(n - 1) + n^c \\
     &= [T(n - 2) + n^c] + n^c = T(n - 2) + 2n^c \\
     &= [T(n - 3) + n^c] + 2n^c = T(n - 3) + 3n^c \\
     &= [T(n - 4) + n^c] + 3n^c = T(n - 4) + 4n^c \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(T(n - k) + kn^c\).

- \(T(n) = \Theta(n^c)\) from Case 3 of the Master Theorem.

** (i) \(T(n) = T(n - 1) + c^n\), *where \(c > 1\) is some constant*

\begin{align*}
T(n) &= T(n - 1) + c^n \\
     &= [T(n - 2) + c^n] + c^n = T(n - 2) + 2c^n \\
     &= [T(n - 3) + c^n] + 2c^n = T(n - 3) + 3c^n \\
     &= [T(n - 4) + c^n] + 3c^n = T(n - 4) + 4c^n \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(T(n - k) + kc^n\).

- \(T(n) = \Theta(c^n)\) since the tree depth \(kc^n\) dominates the number of subproblems \(1\).

** (j) \(T(n) = 2T(n - 1) + 1\)

\begin{align*}
T(n) &= 2T(n - 1) + 1 \\
     &= 2[2T(n - 2) + 1] + 1 = 4T(n - 2) + 3 \\
     &= 4[2T(n - 3) + 1] + 3 = 8T(n - 3) + 7 \\
     &= 8[2T(n - 4) + 1] + 7 = 16T(n - 4) + 15 \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(T(n) = 2^k T(n - k) + 2k - 1\).

- \(T(n) = \Theta(2^n)\) since the number of subproblems \(2^k\) dominates the tree depth \(2k - 1\).

* Problem 2.12

#+begin_example
function f(n)
  if n > 1:
    print_line(‘‘still going’’)
    f(n/2)
    f(n/2)
#+end_example

This function has \(a = 2\) subproblems where each subproblem size decreases by a factor of \(b = 2\) and the work done in each subproblem is \(f(n) = 1\) from one comparison.
Then using the Master Theorem \(T(n) = aT\left(\frac{n}{b}\right) + f(n)\), a recurrence for the given function would be \[T(n) = 2T(n/2) + 1\]

\begin{align*}
T(n) &= 2T(n/2) + 1 \\
     &= 2[2T(n/4) + 1] + 1 = 4T(n/4) + 2 \\
     &= 4[2T(n/8) + 1] + 2 = 8T(n/8) + 3 \\
     &= 8[2T(n/16) + 1] + 3 = 16T(n/16) + 4 \\
     &\;\;\vdots
\end{align*}

The general \(k\)th term is \(2^k T(n/2^k) + k\).

* Problem 2.14

First, perform mergesort on the array, making the algorithm \(O(n \log{n})\).
Next, allocate a new array with the same size, making the algorithm \(O(n \log{n} + n)\).
Next, traverse through the original array making \(O(n)\) comparisons and setting the current value to the value at the next index in the new array if it is not a duplicate of the last element in the new array.
This makes our algorithm \(O(n \log{n} + n + n)\) which is just \(O(n \log{n})\).
