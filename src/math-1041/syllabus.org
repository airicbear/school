#+setupfile: ~/.emacs.d/org-templates/level-1.org
#+title: 2020 Spring Course Syllabus - Mathematics 1041.002

* General Information
  :PROPERTIES:
  :CUSTOM_ID: general-information
  :END:

  - Course: Mathematics 1041.002

  - Course Title: Calculus I

  - Time: MWF 9:20-10:30am

  - Place: Wachman 011

  - Instructor: Brandi Henry

  - Instructor Office: Wachman 523

  - Instructor Email: brandi.henry@temple.edu

  - Course Web Page: https://math.temple.edu/ugrad/coordinated/1041/

  - Office Hours:

  - Monday 2:00-3:00pm

  - Thursday 3:00-4:00pm (in MCC Wachman Hall Room 1036)

  - Friday 10:30-11:30am

  - Prerequisites: A math placement test score of 9-14-9 or higher, a grade of C or higher in Math 1022, or transfer credit for Math 1022.

  - Textbook: Calculus: Early Transcendentals by James Stewart; 8th Edition, Cengage Learning. IMPORTANT: together with the textbook you need to have access to WebAssign because part of your homework will be assigned there. IF YOU TOOK MATH 1041 IN OR AFTER THE FALL 2015, YOU SHOULD ALREADY HAVE THIS BOOK AND THE WebAssign ACCESS. If it is not the case, you can purchase a standalone WebAssign Instant Access (it is called WebAssign Instant Access for Calculus, Multi-Term Courses, 1st Edition) and use the electronic version of the textbook (eBook). Or you can purchase the Bundle that contains both the paper textbook (loose leaf version) and the WebAssign Printed Access Card for Stewart's Calculus: Early Transcendentals; 8th Edition; Multi-Term. The publisher (Cengage) created a special site for Temple students where you can buy either of these two versions for a discounted price ($125 for the standalone access and about $144 for the Bundle): http://services.cengagebrain.com/course/site.html?id=3646046. You can also buy the Bundle at the Main Campus Temple Bookstore (http://www.temple.edu/bookstore), but it will cost $172. Please DO NOT buy any EARLIER edition of this book. Please also DO NOT buy any used books since they will not let you access WebAssign (the WebAssign codes ARE NOT transferable).

* Course Goals
  :PROPERTIES:
  :CUSTOM_ID: course-goals
  :END:

  Students will be able to:

  1. Evaluate limits - including those of indeterminate form - using the appropriate tools.

  2. Compute derivatives of polynomial, trig, exponential, inverse, and composite functions using the standard tools of differential Calculus.

  3. Apply the computation of derivatives to practical problems including optimization and related rates.

  4. Understand the basic relationship between the derivative and the integral and evaluate some simple integrals using the Fundamental Theorem of Calculus.

* Topics Covered
  :PROPERTIES:
  :CUSTOM_ID: topics-covered
  :END:

  The three central objects of the semester are: limits, derivatives, and integrals.
  In studying these objects, we will see the connections between them, how to compute different types of them, and how to apply such calculations to some practical problems.

* Course Grading
  :PROPERTIES:
  :CUSTOM_ID: course-grading
  :END:

  Review Quiz: 1%,

  WebAssign Homework: 4%

  Quizzes/HW: 13%

  Test 1: 24%

  Test 2: 24%

  Comprehensive Final Exam: 34%

  Correspondence between the numerical and letter grades: 93-100 A, 90-92 A-, 87-89 B+, 83-86 B, 80-82 B-, 77-79 C+, 73-76 C, 70-72 C-, 65-69 D+, 55-64 D, 50-54 D-, 0-49 F.

* Exam Dates
  :PROPERTIES:
  :CUSTOM_ID: exam-dates
  :END:

  Test 1 - Wednesday, February 19, (5:50 - 7:20 pm)

  Test 2 - Wednesday, April 8, (5:50 - 7:20 pm)

  Final Exam - Wednesday, April 29, (3:30 - 5:30 pm)

  Exam rooms will be announced at least one week in advance of the exam.

* Attendance Policy
  :PROPERTIES:
  :CUSTOM_ID: attendance-policy
  :END:

  Attendance is required.
  Students who miss more than 6 classes without an excuse will have their grades lowered by one notch (e.g., from B to B-).
  Those who miss more than 12 classes without an excuse will have their grades lowered by two notches, etc.
 
* Final Exam
  :PROPERTIES:
  :CUSTOM_ID: final-exam
  :END:

  Final Exam - Wednesday, April 29, (3:30 - 5:30 pm).
  Exam rooms will be announced at least one week in advance of the exam.
  Important: please note that if you miss the final exam and do not make alternative arrangements before the grades are turned in, your grade for the course will be F.
 
* Common Review Quiz
  :PROPERTIES:
  :CUSTOM_ID: common-review-quiz
  :END:

  Review Quiz will be given on WebAssign on the first week of classes.
  It will be based on precalculus material.
  Students who perform poorly on Review Quiz will be recommended to move to Math 1022, Precalculus.
  The score for Review Quiz will count for 1% of your Course Average.
 
* Homework
  :PROPERTIES:
  :CUSTOM_ID: homework
  :END:

  A list of homework problems from the textbook will be distributed;
  a certain (specified) part of the homework assignments you will need to do on WebAssign.
  Our class key for WebAssign is temple 5418 7819.
 
* Quizzes
  :PROPERTIES:
  :CUSTOM_ID: quizzes
  :END:

  There will be a 15-20 min.
  quiz every week (when there is no midterm).
  Each quiz will consist of problems similar to the ones assigned from the textbook.
  There will be no make-up quizzes.
  The lowest two scores for the quizzes will be dropped.
 
* Calculators
  :PROPERTIES:
  :CUSTOM_ID: calculators
  :END:

  NO CALCULATORS or CHEAT SHEET will be allowed on any exams or in-class quizzes.
 
* Make Up Policy
  :PROPERTIES:
  :CUSTOM_ID: make-up-policy
  :END:

  Make ups of exams will only be given in cases of DOCUMENTED EMERGENCIES (sickness, car accident, a death in the family, etc).
  It is the student's responsibility to contact his/her instructor RIGHT AWAY, preferably by e-mail, in the case of a missed exam.
  DOCUMENTATION OF EMERGENCY IS REQUIRED.
  ALL MAKE UPS MUST BE TAKEN WITHIN TWO DAYS OF THE EXAM DATE.
 
* CANVAS
  :PROPERTIES:
  :CUSTOM_ID: canvas
  :END:

  This is a registered CANVAS course.
  Please check CANVAS daily for important announcements.
  I highly recommend you adjusting your Canvas settings to receive notifications when announcements are posted.
  There will occasionally be assignments through Canvas (short quizzes, surveys, etc), which will be apart of your quiz average.

* Exam Security Policy
  :PROPERTIES:
  :CUSTOM_ID: exam-security-policy
  :END:

  Under no circumstances is the use of personal electronic devices such as phones, computers, smart watches allowed during exams.
  All such items must be stowed away and out of sight for the duration of the exam.
  Any student found with such a device during an exam will not be allowed to complete the test, will receive a score of ZERO for the test, and will be reported to the Student Code of Conduct Board.
  In addition, under no circumstances, will students be allowed to bring in any kind of papers for use during the exam.
  Should a student need extra paper during a test, the proctor will provide it.
  Any student found using papers that they themselves brought into the exam will not be allowed to complete the test, will receive a score of ZERO for the test, and will be reported to the Student Code of Conduct Board.

* Academic Support
  :PROPERTIES:
  :CUSTOM_ID: academic-support
  :END:

  The Student Success Center (SSC) and the Math TA and CA Consulting Center (MCC) both provide excellent support services for this course throughout the semester.
  Information regarding the services these centers provide will be posted on the course Canvas page.
  Students are strongly encouraged to take advantage of these services!
  In addition, this semester the SSC will be hosting PASS study sessions.
  These sessions will start in approximately week 3 and more information will be posted to CANVAS at that time.
  Study halls specifically for this course will also be held on a weekly basis.
  Please see the CANVAS site for more information.

  Any student who has a need for accommodations based on the impact of a documented disability or medical condition should contact Disability Resources and Services (DRS) in 100 Ritter Annex ([[mailto:drs@temple.edu][drs@temple.edu]]; 215-204-1280) to request accommodations and learn more about the resources available to you.
  If you have a DRS accommodation letter to share with me, or you would like to discuss your accommodations, please contact me as soon as practical.
  I will work with you and with DRS to coordinate reasonable accommodations for all students with documented disabilities.
  All discussions related to your accommodations will be confidential.

  Freedom to teach and freedom to learn are inseparable facets of academic freedom.
  The University has adopted a policy on Student and Faculty Academic Rights and Responsibilities (Policy # 03.70.02) which can be accessed [[http://policies.temple.edu/][here]].
  (Links to an external site.)

  Students will be charged for a course unless dropped by the Drop/Add deadline date.
  Check the [[http://www.temple.edu/registrar/documents/calendars][University calendar]] (Links to an external site.) for exact dates.

  During the Drop/Add period, students may drop a course with no record of the class appearing on their transcript.
  Students are not financially responsible for any courses dropped during this period.
  In the following weeks prior to or on the withdrawal date students may withdraw from a course with the grade of "W" appearing on their transcript.
  After the withdrawal date students may not withdraw from courses.
  Check the [[http://www.temple.edu/registrar/documents/calendars][University Calendar]] (Links to an external site.) (Opens in new tab/window) for exact dates.
  [[https://bulletin.temple.edu/undergraduate/academic-policies/withdrawal-policies/][See the full policy by clicking here]].
  (Links to an external site.) (Opens in new tab/window)

  The grade "I" (an "incomplete") is only given if students cannot complete the course work due to circumstances beyond their control.
  It is necessary for the student to have completed the majority of the course work with a passing average and to sign an incomplete contract which clearly states what is left for the student to do and the deadline by which the work must be completed.
  The incomplete contract must also include a default grade that will be used in case the "I" grade is not resolved by the agreed deadline.
  [[http://bulletin.temple.edu/undergraduate/academic-policies/incomplete-coursework/][See the full policy by clicking here]].
  (Links to an external site.) (Opens in new tab/window)
