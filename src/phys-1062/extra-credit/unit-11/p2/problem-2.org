#+TITLE: Problem 2
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent Instructions: Solve the problem below using the IRDEA framework.

\noindent *Problem 2:* Cosmic microwave background radiation fills all space with an average energy density of \(4 \times 10^{-14} \text{ J/m}^3\).
(a) Find the peak value of the electric field associated with this radiation.
(b) How far (in km) from a 7.5-kW radio transmitter emitting uniformly in all directions would you find a comparable value?

1) Interpret the problem by identifying applicable concepts and principles

   - Assume that the CMB is faint plane EM plane wave filling all space.

   - In part (b) use the model of spherical wave with the power emitted uniformly in all directions.

2) Represent the problem in terms of symbols and diagrams

   - \(\bar{u} = 4 \times 10^{-14} \text{ J/m}^3\)

   - \(\epsilon_0 = 8.85 \times 10^{-12} \text{ C}^2 / \text{N m}^2\)

   - \(P_{\text{source}} = 7.5 \text{ kW} = 7500 \text{ W}\)

   - Targets: (a) \(E_0 = \;?\) (b) \(r = \;?\)

3) Develop a plan for solving the problem (VERBALLY EXPLAIN YOUR PLAN, No calculations in this step!)

   Part (a)

   - \(E_0 = \;?\) Relationship between electric and magnetic fields
     \[E_0 = c B_0\]

   - \(c = \;?\) Speed of light equation
     \[c = \frac{1}{\sqrt{\epsilon_0 \mu_0}}\]

   - \(B_0 = \;?\) Average density equation
     \begin{align*}
     \bar{u} &= \frac{1}{2} (u_B + u_E) \\
     \bar{u} &= \frac{1}{2} \left(\frac{{B_0}^2}{2\mu_0} + \frac{1}{2} \epsilon_0 {E_0}^2\right) \\
     2\bar{u} &= \frac{{B_0}^2}{2\mu_0} + \frac{1}{2} \epsilon_0 {E_0}^2 \\
     4\bar{u} &= \frac{{B_0}^2}{\mu_0} + \epsilon_0 {E_0}^2 \\
     B_0 &= \sqrt{4\bar{u} \mu_0 - \epsilon_0 \mu_0 {E_0}^2}
     \end{align*}

   - Substitute \(c\) and \(B_0\) into \(E_0\) to get
     \begin{align*}
       E_0 &= \left(\frac{1}{\sqrt{\epsilon_0 \mu_0}}\right) \sqrt{4\bar{u} \mu_0 - \epsilon_0 \mu_0 {E_0}^2} \\
       {E_0}^2 &= \frac{4 \bar{u} \mu_0 - \epsilon_0 \mu_0 {E_0}^2}{\epsilon_0 \mu_0} \\
       {E_0}^2 &= \frac{4 \bar{u} - \epsilon_0 {E_0}^2}{\epsilon_0} \\
       {E_0}^2 \epsilon_0 &= 4 \bar{u} - \epsilon_0 {E_0}^2 \\
       2{E_0}^2 \epsilon_0 &= 4 \bar{u} \\
       E_0 &= \sqrt{\frac{2\bar{u}}{\epsilon_0}} \quad \text{(Final expression)}
     \end{align*}

   Part (b)

   - \(r = \;?\) Intensity from a point source
     \[I = \frac{P_{\text{source}}}{4\pi r^2} \implies r = \sqrt{\frac{P_{\text{source}}}{4 \pi I}}\]

   - \(I = \;?\) Intensity of an electromagnetic wave
     \[I = \frac{c\epsilon_0}{2} {E_0}^2\]

   - Substitute \(I\) into \(r\) to get
     \begin{align*}
     r &= \sqrt{\frac{P_{\text{source}}}{4 \pi \left(\frac{c \epsilon_0}{2} {E_0}^2\right)}} \\
     &= \sqrt{\frac{P_{\text{source}}}{2 \pi c \epsilon_0 {E_0}^2}} \quad \text{(Final expression)}
     \end{align*}

4) Evaluate your answer

   - Part (a):
     \[E_0 = \sqrt{\frac{2(4 \times 10^{-14})}{8.85 \times 10^{-12}}} = 0.095 \text{ V/m}\]

   - Part (b):
     \[r = \sqrt{\frac{7500}{2\pi(3.00 \times 10^8)(8.85 \times 10^{-12})(0.095)}} = 7052 \text{ m} \approx 7.1 \text{ km}\]

5) Assess your answer (is your answer properly stated with proper units and sig fig or precision, is your answer reasonable?)

   - Units in V/m for part (a) and km for (b).

   - 2 sig. figs.

   - The peak electric field is small (\(E_0 \ll 1\)) and the distance from the radio transmitter is large (\(r \gg 1\)).
