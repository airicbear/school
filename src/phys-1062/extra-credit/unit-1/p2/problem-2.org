#+TITLE: Problem 2
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\begin{wrapfigure}[5]{r}
\centering
\includegraphics[width=0.3\textwidth]{problem-2-figure.png}
\end{wrapfigure}
\noindent *Instructions:* Solve the problem below using the IRDEA framework.

\noindent *Problem 2:* 8.0 g of helium gas follows the process \(1 \to 2 \to 3\) shown in figure.
Find \(p_2\) (in atm) and \(V_3\) (in m\(^3\)).

1. Interpret the problem by identifying applicable concepts and principles (no equations here)

   1) \(V_3\) in \(\text{m}^3\) and \(\text{p}_2\) in atm

   2) Assume that the helium gas is an ideal gas

   3) Process \(1 \to 2\) is isochoric

   4) Process \(2 \to 3\) is isothermal

   5) Helium is a monoatomic gas.
      Use the counting moles method to determine \(n\).
      Use Table 18.2 to determine that the atomic mass number of helium is 4.

   6) Prediction: \(p_2\) should be several atm higher than \(p_1\) given a large increase in temperature from an isochoric process. I can't make many predictions about \(V_3\) other than \(V_3 = V_1 > V_2\).

2. Represent the problem in terms of symbols and diagrams.

      \begin{align*}
        p_1 &= 2 \text{ atm} \\
        p_3 &= p_1 \\
        R &= 8.31 \\
        1 \text{ atm} &= 101.3 \text{ kPa} \\
        M &= 8 \text{ g} \\
        T_1 &= 37 + 273 \text{ K} \\
        T_2 &= 657 + 273 \text{ K} \\
        M_{\text{mol}} &= 4 \text{ g}
      \end{align*}

3. Develop a plan for solving the problem (Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics; No calculations in the development stage)

   - Determine \(n\) from \(n = \frac{M}{M_{\text{mol}}}\).

   - For the isochoric process from \(1 \to 2\):

     + Find \(p_2\) using the ideal gas law \(\frac{p_1 V_1}{T_1} = \frac{p_2 V_2}{T_2}\).

     + Solve for \(p_2\), obtaining \(p_2 = \frac{p_1 T_2}{T_1}\).

   - For the isothermic process from \(2 \to 3\):

     + Find \(V_3\) from the ideal-gas law \(p_3 V_3 = nRT_3\).

     + Note that \(T_3 = T_1\) and \(p_3 = p_1\) from the figure.

     + Solving for \(V_3\), we get \(V_3 = \frac{nRT_3}{p_3}\).

4. Evaluate your answer (No units involve when performing calculations)

   \begin{align*}
     n &= \frac{M}{M_{\text{mol}}} = \frac{8}{4} = 2 \text{ mol}
   \end{align*}

   \begin{align*}
     p_{2} &= \frac{p_1 T_2}{T_1} \\
     &= \frac{2 (657 + 273)}{37 + 273} \\
     &= 6 \text{ atm}
   \end{align*}

   \begin{align*}
     p_{3} &= 2 \times 101325 = 202650 \\
     V_{3} &= \frac{nRT_3}{p_3} \\
     &= \frac{2(8.31)(37 + 273)}{202650} \\
     &= 0.0254 \\
     &\approx 0.03 \text{ m}^{3}
   \end{align*}

5. Assess your answer (Is the answer properly stated? Are units and sig fig or precision correct? Is the answer reasonable? Complete?)

   - Correct units as asked for in the question. \(p_2\) in atm and \(V_3\) in m^3.

   - 1 sig. fig. from the given atmospheric pressure (2 atm).

   - The answer is reasonable for \(p_2 > p_1\).
     However, I am not able to verify whether the result for \(V_3\) is reasonable.
