#+TITLE: Problem 4
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent Instructions: Solve the problem below using the IRDEA framework.

\noindent *Problem 4:* A conducting bar of length L and mass m rests at the left end of the two frictionless rails of length d in Figure below.
A uniform magnetic field of strength B points upward.
(a) In which direction (into or out of the page), will a current through the conducting bar cause the bar to experience a force to the right?
(b) Find an expression for the bar's speed as it leaves the rails at the right end.

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - Use RHR given direction of magnetic field and magnetic force to obtain direction of the current.

   - Assume the same physics laws for a current-carrying wire apply to the current-carrying conducting bar.

   - Use Newton's 2nd Law to relate velocity to magnetic field and current through magnetic force equation for a current-carrying wire.

   - Use a kinematic equation to find an expression for acceleration in terms of velocity.

   - Predict: A current running through the conducting bar will be directed into the page to experience a force to the right.

2) Represent the problem in terms of symbols and diagrams (do not forget about the target quantity and informative diagrams/sketches)

   #+ATTR_LATEX: :width 8cm
   [[./sketch.png]]

   - \(\Delta x = d\)

   - Target: \(v_f = \;?\)

3) Develop a plan for solving the problem (Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics.)

   - \(F = \;?\) Newton's Law and magnetic force on a current-carrying wire
     \[F = ma \text{ and } F = \vec{I}L \times \vec{B} = ILB \text{ from } \vec{I} \perp \vec{B}\]

   - \(a = \;?\) Use kinematics equation
     \[{v_f}^2 = {v_0}^2 + 2a \Delta x \implies a = \frac{1}{2 \Delta x} {v_f}^2\]

   - Equate the two expressions for force and solve for velocity
     \begin{align*}
        ma &= ILB \\
        m \left(\frac{1}{2 \Delta x} {v_f}^2\right) &= ILB \\
        v_f &= \sqrt{\frac{2ILB\Delta x}{m}} \quad\text{(Final expression)}
     \end{align*}

4) Evaluate your answer

   a) The current flows into the page.

   b) Substitute for \(\Delta x = d\)
     \[v_f = \sqrt{\frac{2ILBd}{m}}\]

5) Assess your answer (the answer properly stated? answer reasonable? complete?)

   - The RHR can be used to verify the current's direction using the direction of the magnetic field and magnetic force.

   - The bar's speed is inversely proportional with the square root of the mass.

   - The bar's speed is proportional with the square root of: the strength of the magnetic field, the current going through the bar, the length of the bar, and the distance which the bar travels.

   - The units are in m/s. \[\sqrt{\frac{ATm^2}{kg}} = m \sqrt{\frac{A(kg\cdot s^{-2} \cdot A^{-1})}{kg}} = \frac{m}{s}\]
