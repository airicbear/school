#+TITLE: Problem 01
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent *Instructions:* Solve the problem below using the IRDEA framework.

\noindent *Problem 1:* The wire in Figure below has linear charge density \(\lambda\).
Find an expression for the electric potential at the center of the semicircle?

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - Assume the charge is uniformly distributed along the wire because it is thin.

   - We will use the superposition principle.

2) Represent the problem in terms of symbols and diagrams.

   [[./diagram.png]]

   - \(Q\), \(\lambda\), \(R\), \(k\)

   - Target: \(V(P) = V_c + V_l = \;?\)

3) Develop a plan for solving the problem and execute it. (*Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics*; Derive a final expression for your answer.)

   - The wire's potential is the sum of the potential of the semicircle portion \(V_c\) and the linear portion \(V_l\).

   - Divide the wire into small segments, each carrying the charge \(dq\).

   - The density \(\lambda\) is defined as the ratio between the charge \(Q\) and distance \(L\) (in terms of \(R\)).

   - For the semicircle, we have \(\lambda = \frac{Q}{\pi R} \implies Q = \lambda \pi R\).

   - For the linear portion we have \(\lambda = \frac{Q}{R} \implies Q = \lambda R\) or \(dq = \lambda \; dr\).

   - The potential of the semicircle portion is then
     \[V_c = \int \frac{k \;dq}{R} = k \frac{Q}{R} = k \frac{(\lambda \pi R)}{R} = k \lambda \pi\]

   - The potential of the linear portion is
     \[2V_l = 2\int \frac{k \;dq}{R} = 2k \int_R^{3R} \frac{\lambda \;dr}{R} = 2k \lambda \ln{r}\;\Big|_R^{3R} = 2k \lambda \ln{\left(\frac{3R}{R}\right)} = 2k \lambda \ln{(3)}\]

   - The sum of these two potentials is then
     \[V(P) = V_c + 2V_l = k\lambda(\pi + 2 \ln{3})\]

4) Assess your answer (Is the answer properly stated? Are units and sig fig or precision correct? Is the answer reasonable? Complete?)

   - No sig figs required.

   - Units are in volts.

   - Potential is a scalar.
