#+TITLE: Problem 2
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent Instructions: Solve the problem below using the IRDEA framework.

\noindent *Problem 2:* You're writing the instruction manual for a power saw, and you have to specify the maximum permissible length for an extension cord made from 18-gauge copper wire (diameter 1.0 mm).
The saw draws 13.0 A and needs a minimum of 115 V across its motor when the outlet supplies 120 V.
What do you specify for the maximum length extension cord, given that they come in 25-foot increments?
*Hint: 1 ft = 0.3048 m.*

1) Interpret the problem by identifying applicable concepts and principles

   - Find the max length.
   - The uniform model will translate the max length into max resistance.
   - Ohm's law will translate the max resistance into max voltage per wire.
   - Predict: max length should be a positive number.

2) Represent the problem in terms of symbols and diagrams
[[./diagram.png]]
\begin{align*}
\rho_{\text{Cu}} &= 1.7 \times 10^{-8} \\
\Delta V_{\text{max}} &= 2.5 \text{ V} \\
I &= 13 \text{ A} \\
r &= 0.5 \text{ mm} \\
L_{\text{max}} &= \;?
\end{align*}

1) Develop a plan for solving the problem (VERBAL EXPLANATIONS. NO CALCULATIONS in this STAGE!)

   - \(R = \;?\) Calculate resistance from Ohm's law: \(R = \frac{\Delta V}{I}\)

   - \(A = \;?\) Circular cross section \(A = \pi r^2\)

   - Solve for length from uniform model
     \begin{align*}
     R &= \frac{\rho L}{A} \\
     L_{\text{max}} &= \frac{RA}{\rho} \\
       &= \frac{\Delta V A}{I \rho} \\
       &= \frac{\Delta V \pi r^2}{I \rho}
     \end{align*}

   - Final expression: \(\frac{\Delta V \pi r^2}{I \rho}\)

4) Evaluate your answer
   \[L_{\text{max}} = 8.88 \text{ m} \times \frac{1 \text{ ft}}{0.3048 \text{ m}} = 29.1 \text{ ft}\]

5) Assess your answer (the answer properly stated with proper units and sig fig or precision, is your answer complete?)
   - 3 sig. figs.
   - Units in feet.
   - Reasonable answer for an extension cord length.
