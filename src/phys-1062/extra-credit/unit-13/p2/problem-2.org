#+TITLE: Problem 2
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent Instructions: Solve the problem below using the IRDEA framework.

\noindent *Problem 2:* Figure P33.56 shows the light intensity on a screen behind a single slit.
The wavelength of the light is 600 nm and the slit width is 0.15 mm.
What is the distance from the slit to the screen?

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - Use the width of the central maximum for a single slit diffraction pattern.

   - Predict: The distance from the slit to the screen should be much larger than the slit width.

2) Represent the problem in terms of symbols and diagrams (do not forget about the target quantity and a convenient coordinate system)

   #+ATTR_LATEX: :width 8cm
   [[./diagram.png]]

   - \(\lambda = 600 \text{ nm} = 6 \times 10^{-7} \text{ m}\)

   - \(a = 0.15 \text{ mm} = 0.00015 \text{ m}\)

   - \(w = 1 \text{ cm} = 0.01 \text{ m}\)

   - \(L = \;?\) (Target)

3) Develop a plan for solving the problem (Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics.)

   - \(L = \;?\) Width of central maximum
     \[w = \frac{2 \lambda L}{a} \implies L = \frac{wa}{2 \lambda}\]

4) Evaluate your answer

   \[L = \frac{(0.01)(0.00015)}{2(6 \times 10^{-7})} = 1.3 \text{ m}\]

5) Assess your answer (the answer properly stated? answer reasonable? complete?)

   - 2 sig fig.

   - Units in m

   - Distance from slit to screen is much larger than slit width
