#+TITLE: Problem 1
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent *Instructions:* Solve the problem below using the IRDEA framework.

\noindent *Problem 1:* At what temperature (in °C) do hydrogen molecules have the same rms speed as nitrogen molecules at 100°C?

1) Interpret the problem by identify applicable concepts and principles (no equations here).

   - This problem involves the concept of the rms speed.

   - Predict: The temperature for the hydrogen molecules is going to be less than the temperature of the nitrogen molecules.

2) Represent the problem in terms of symbols and diagrams.

   - \(m(H_2) = 2 \text{ u}\)

   - \(m(N_2) = 28 \text{ u}\)

   - \(T(N_2) = 100 + 273.15 = 373.15 \text{ K}\)

   - \(T(H_2) = \; ?\)

3) Develop a plan for solving the problem (*Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics*; No calculations in the development stage)

   \begin{align*}
     v_{\text{rms}} &= \sqrt{\frac{3k_{B}T}{m}} \\
     v_{\text{rms}_{H_2}} &= v_{\text{rms}_{N_2}} \\
     \sqrt{\frac{3k_{B}T(H_{2})}{m(H_{2})}} &= \sqrt{\frac{3k_{B}T(N_{2})}{m(N_{2})}} \\
     \frac{3k_B T(H_2)}{m(H_2)} &= \frac{3k_B T(N_2)}{m(N_2)} \\
     T(H_2) &= \frac{T(N_2) m(H_2)}{m(N_2)}
   \end{align*}

4) Evaluate your answer (No units involved when performing calculations)

   \begin{align*}
     T(H_2) &= \frac{(373.15)(2)}{28} \\
            &\approx 27 \text{ K} \\
            &\approx -246.5 \\
            &\approx -250^{\circ}\text{C}
   \end{align*}

5) Assess (Is the answer properly stated? Are units and sig fig or precision correct? Is the answer reasonable? Complete?).

   - The answer is reasonable: the temperature for the hydrogen molecules is less than that of the nitrogen molecules.

   - Units in °C as asked for in the question.

   - Use 2 sig. figs. for a reasonably precise answer.
