#+TITLE: Problem 2
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent *Instructions:* Solve the problem below using the IRDEA framework.

\noindent *Problem 2:* A mixture of monatomic and diatomic gases has specific-heat ratio \(\gamma = 1.52\).
What fraction of its molecules are monatomic?

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - Monatomic gases have three degrees of freedom from translation.

   - Diatomic gases have five degrees of freedom from translation and rotation.

   - The sum of the fraction of monatomic molecules and the fraction of diatomic molecules is one.

   - Predict: the answer should be less than or equal to one since it is a fraction.

2) Represent the problem in terms of symbols (for all knowns and the target quantity) and diagrams. (no equations here)

   - \(C_{V_m} = \frac{3}{2} R\)

   - \(C_{V_d} = \frac{5}{2} R\)

   - \(C_V = C_{V_m} + C_{V_d} = \frac{3}{2} R x_1 + \frac{5}{2} R x_2\)

   - \(\gamma = 1.52\)

   - Target: \(x_1 = \frac{n_1}{n_1 + n_2}\)

3) Develop a symbolic plan for solving the problem (In addition verbally explain what you are doing by identifying the relevant physics model or fact of mathematics; No calculations in the development stage).
   Whenever possible derive a final expression for your plan.

   - The sum of the fraction of monatomic and diatomic molecules is \(x_1 + x_2 = 1\).
     Using this, we can write \(x_2\) in terms of \(x_1\), \(x_2 = 1 - x_1\).

   - We want to put the equation \(\gamma = \frac{C_P}{C_V}\) into terms of \(x_1\) and solve for \(x_1\).

   - Use \(C_P = C_V + R\) to put the equation in terms of \(C_V\) and from \(C_V\) in terms of \(x_1\).

   \begin{align*}
     \gamma &= \frac{C_P}{C_V} \\
     \gamma &= \frac{C_V + R}{C_V} \\
     \gamma &= 1 + \frac{R}{C_V} \\
     \gamma &= 1 + \frac{R}{\frac{3}{2} R x_1 + \frac{5}{2} R x_2} \\
     \gamma &= 1 + \frac{R}{\frac{3}{2} R x_1 + \frac{5}{2} R (1 - x_1)} \\
     \gamma &= 1 + \frac{R}{\frac{3}{2} R x_1 + \frac{5}{2} R - \frac{5}{2} R x_1} \\
     \gamma &= 1 + \frac{R}{-Rx_{1} + \frac{5}{2}R} \\
     \gamma &= 1 + \frac{R}{-Rx_{1} + \frac{5}{2}R}
   \end{align*}

   \begin{align*}
     (\gamma - 1) \left(-Rx_{1} + \frac{5}{2} R\right) &= R \\
     -Rx_{1} \gamma + \frac{5}{2} R \gamma + Rx_{1} - \frac{5}{2} R &= R \\
     -x_{1} \gamma + \frac{5}{2} \gamma + x_{1} - \frac{5}{2} &= 1 \\
     x_{1} (-\gamma + 1) &= 1 - \frac{5}{2} \gamma + \frac{5}{2} \\
     x_{1} &= \frac{\frac{7}{2} - \frac{5}{2} \gamma}{-\gamma + 1}
   \end{align*}

4) Evaluate your answer (No units involved in the evaluation)

   \begin{align*}
      x_1 &= \frac{\frac{7}{2} - \frac{5}{2} (1.52)}{-1.52 + 1} \\
      x_1 &\approx 0.577 = 57.7\%
   \end{align*}

5) Assess your answer (Is the answer properly stated? Are units and sig fig correct., Is the answer reasonable? complete?)

   - No units necessary for this question.

   - Three sig figs from \(\gamma = 1.52\).

   - Reasonable: \(x_1 \leq 1\) since it is a fraction.
