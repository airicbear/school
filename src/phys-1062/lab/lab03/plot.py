#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def dataframe(file):
    """
    Get pandas dataframe from file
    """
    return pd.read_csv(file)


def predict(x_data, y_data):
    """
    Linear regression on x and y
    """

    model = np.polyfit(x_data, y_data, 1)
    return np.poly1d(model)


def plot(files):
    """
    Plot csv file and print out relevant information
    """

    i = 0
    predictions = []
    for file in files:
        data = dataframe(file)
        pressure = data.iloc[:, 0]
        temperature = data.iloc[:, 1]
        fit = predict(pressure, temperature)
        x_axis = np.linspace(0, max(pressure), 1000)
        abs_zero = fit(0)
        i += 1
        print(f'Run {i}: {fit}')
        print(f'Run {i} prediction: {abs_zero}')
        predictions.append(abs_zero)
        plt.scatter(pressure, temperature, label=f'Run {i}')
        plt.plot(x_axis, fit(x_axis))

    print(f'Average absolute zero: {np.mean(predictions)}')
    print(f'Standard deviation absolute zero: {np.std(predictions)}')
    plt.legend(loc="upper left")
    plt.title('Absolute Zero')
    plt.xlabel('Pressure (kPa)')
    plt.ylabel('Temperature (°C)')
    plt.savefig('plot.png')
    plt.show()


if __name__ == '__main__':
    plot(['run01.csv', 'run02.csv', 'run03.csv'])
