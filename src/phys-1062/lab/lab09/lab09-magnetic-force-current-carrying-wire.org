#+TITLE: Lab 09 Magnetic Force Current Carrying Wire
#+AUTHOR: Eric Nguyen, Zack Waxler, John Lauderdale
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Introduction

In this lab, we study the factors which affect the magnitude and direction of the force on a current-carrying wire and practice using the right-hand rule to determine the direction of force on a wire in a magnetic field.

* Procedure

** Apparatus

iOLab, hookup wire, battery, permanent magnets, tape

** Part I: Force on a wire in a magnetic

a) Orient the iOLab face up according to Figure 1 in the lab manual.
   Move the permanent hook magnet up and down above the magnetometer.
   Determine the direction of the magnetic field of the hook by observing the magnetometer's \(B_z\) value.
   If it increases, it is pointing in the positive \(z\)-direction.
   If it decreases, it is pointing in the negative \(z\)-direction.
   Include this finding in the lab report.

b) Tightly screw the eye screw into the force sensor.

c) Determine whether the iOLab's force sensor records pushing or pulling as positive by displaying the force graph, rezeroing the force sensor, and pulling gently on the screw.

d) Place the hook on the eye screw according to Figure 2 in the lab manual.

e) Prepare to run a wire with current past the magnet.

f) Predict the direction of the force on the wire using the right-hand rule with the current flowing downwards past the magnet and using the known direction of the magnetic field.

g) The measurement shown on the force graph will be the force applied on the magnet.
   Using Newton's Third Law, take the opposite force to be the force applied on the wire.
   Make a table of predictions of the force on the magnet for each direction of current.

h) Grab a hookup wire and battery and bend the wire such that it is perpendicular to the direction of magnetic field produced by the magnet as shown in Figure 3 of the lab manual.
   Taping the wire down will help keep it stable.

i) Display the force graph and enable autoscaling via the gear icon.
   Additionally, increase the smoothing from 1 to 25 and rezero the sensor.

j) Click record and repeatedly tap the wire against the battery and then flip the current flow and then stop recording after a few seconds.

k) Determine if there is a clear indication of force on the magnet for both current directions.
   If not, repeat the experiment.

l) In the data table, include the direction of the force acted on the magnet and the force acted on the wire as well as the strength of each force.

* Precautions

- Make sure the data matches the predictions and expectations (i.e. the data is reasonable).

- Make sure that the proper units are used.

- Make sure that the sensors are calibrated.

- Make sure that the graphs are configured accordingly.

* Error

Given the sensitivity of the force sensor and the imprecise manner through which the experiment is carried out, there is some inherent noise collected by the force sensor.
However, this error is negligible in the ultimate conclusions that we arrive to in this lab.

* Data

** Direction of force

The force sensor of my particular iOLab measures pulling as positive and pushing as negative.

** Prediction of the force on the wire

Using the right-hand rule, given that the direction of the current is crosses the magnet in the negative \(x\)-direction and the magnetic field is in the negative \(z\)-direction, the force on the wire should be in the negative \(y\)-direction.

** Forces table

| \(\vec{I}\) direction | \(\vec{B}\) direction | \(\vec{F}_{\text{wire}}\) prediction | \(\vec{F}_{\text{magnet}}\) prediction | \(\vec{F}_{\text{wire}}\) actual | \(\vec{F}_{\text{magnet}}\) actual | \(F_{\text{wire}}\) |
|-----------------------+-----------------------+--------------------------------------+----------------------------------------+----------------------------------+------------------------------------+---------------------|
| \(-x\)                | \(-z\)                | \(-y\)                               | \(+y\)                                 | \(-y\)                           | \(+y\)                             | \(-0.015\)          |
| \(+x\)                | \(-z\)                | \(+y\)                               | \(-y\)                                 | \(+y\)                           | \(-y\)                             | \(+0.015\)        |

#+CAPTION: Graph of force on the magnet
#+ATTR_LATEX: :width 16cm
[[./force-graph.png]]

** Amps of current

Assuming the length of the wire is \(1 \text{ cm} = 0.01 \text{ m}\) and the magnet has a surface field of about \(B = 6000 \text{ Gauss} = 0.6 \text{ T}\), using the fact that our force was \(0.015 \text{ N}\) we can use the \(F = ILB\) equation to determine the amps of current flowing through the wire.

\begin{align*}
F &= ILB \\
0.015 &= I(0.01)(0.6) \\
I &= \frac{0.015}{(0.01)(0.6)} = 2.5 \text{ A}
\end{align*}

* Questions

** Question 1

The direction of force is indeed in agreement with my prediction.
In my case, my eye screw and permanent hook magnet is oriented such that its magnetic field is pointing towards the ground in the negative \(z\)-axis.
Then to produce a force on the wire along the \(y\)-axis, I needed to provide a current perpendicular current along the \(x\)-axis.
For a current flowing toward the negative \(x\)-axis and the magnetic field in the negative \(z\)-direction, I can use the right-hand rule to determine that the force on the wire must point in the negative \(y\)-axis.
Then for a current flowing toward the positive \(x\)-axis and the magnetic field in the negative \(z\)-direction, I determine that the force on the wire must point in the positive \(y\)-axis.

** Question 2

I did indeed observe a change in direction of the force on the wire when I reversed the direction of the current.
This observation agrees with the right-hand rule as explained in Question 1.

** Question 3

If we were to double over the wire as shown in the Figure provided, the force would be zero.
Considering the current vector, it would cancel itself out to a zero vector \(\vec{I} = \vec{0}\) hence by the force equation for a current-carrying wire \(\vec{F} = \vec{I}L\times \vec{B}\) the force must be zero.

* Conclusion

In this lab, we verified that magnetic fields indeed exert a force on a current-carrying wire.
We experimented using the iOLab's force sensor, a magnet, and a current-carrying wire to determine the strength and direction of the force exerted on the wire and we discovered that the direction of the current affects the direction of the force.

Our results matched according to our expectations as we predicted that, given a negative direction of the magnetic field, a negative direction of the current would result in a force exerted along the negative perpendicular direction.
Again, a positive direction of the current would result in a positive direction of the force exerted on the wire.

We can easily reproduce our results given that the procedure is short and simple and leaves little room for error.

Again, there were very few sources of error in this lab aside from the negligible error in the force sensor.
The main sources of error simply would be due to misconfiguration of the iOLab, especially in the display of the graphs.
