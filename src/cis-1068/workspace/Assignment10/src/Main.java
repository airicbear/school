import java.io.*;

/**
 * @author Eric Nguyen
 * Section 008
 * Assignment 10 
 * 2020-04-15
 *
 * Main class to play the game.
 *
 */
class Main {
  public static void main(String[] args) throws FileNotFoundException {
    Game game = new Game("big_word_list.txt");
    game.play();
  }
}
