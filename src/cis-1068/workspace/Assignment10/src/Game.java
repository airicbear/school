import java.util.*;
import java.io.*;

/**
 * @author Eric Nguyen
 *
 * Section 008
 * Assignment 10 
 * 2020-04-15
 *
 * Hangman game, except the correct answer is
 * changed every time the player guesses.
 *
 */
public class Game {
  private static final int RAND_MIN = 4;
  private static final int RAND_MAX = 13;
  private static final int DEFAULT_NUM_GUESSES = 6;
  private static final int CLEAR_CONSOLE_HEIGHT = 50;
  private static final char WORD_MODE = '?';
  private static final String INFO_WORD_MODE = "Enter '" + WORD_MODE + "' to guess a word.";
  private static final String INFO_GUESSED_LETTERS = "Guessed letters: ";
  private static final String INFO_GUESSED_WORDS = "Guessed words: ";
  private static final String INFO_NUM_GUESSES = "Number of guesses remaining: ";
  private static final String INFO_HINT = "Hint: ";
  private static final String PROMPT_LETTER = "Guess a letter: ";
  private static final String PROMPT_WORD = "Guess a word: ";
  private static final String PROMPT_PLAY_AGAIN = "Play again? (y/n): ";
  private static final String LOADING_PROGRESS = "Loading... ";
  private static final String LOADING_DONE = "Done.";
  private static final String MESSAGE_INVALID_INPUT = "Please enter a letter.";
  private static final String MESSAGE_ALREADY_GUESSED = "You already guessed that letter.";
  private static final String MESSAGE_CORRECT_ANSWER = "The correct answer was: ";
  private static final String MESSAGE_LOSE = "You ran out of guesses!";
  private static final String MESSAGE_WIN = "Congratulations. You won!";
  private static final String MESSAGE_INVALID_RESPONSE_PLAY_AGAIN = "Please type 'y' for \"yes\" or 'n' for \"no\".";
    
  private int answerLength;
  private int numGuesses; 
  private ArrayList<String> rawDictionary;
  private ArrayList<String> dictionary;
  private ArrayList<Character> guessedLetters; 
  private ArrayList<String> guessedWords; 
  private String currentAnswer;
  private Scanner user;
  private boolean playerWon;

  public Game() {
    this.numGuesses = DEFAULT_NUM_GUESSES;
    this.guessedLetters = new ArrayList<Character>();
    this.guessedWords = new ArrayList<String>();
    this.user = new Scanner(System.in);
    this.answerLength = this.nextAnswerLength();
  }

  public Game(ArrayList<String> dictionary) {
    this();
    System.out.print(LOADING_PROGRESS);
    this.rawDictionary = dictionary;
    this.dictionary = this.removeWordsDiffLength(dictionary, this.answerLength);
    this.currentAnswer = this.nextWord(this.dictionary);
    System.out.print(LOADING_DONE);
  }

  public Game(String filename) throws FileNotFoundException {
    this();
    System.out.print(LOADING_PROGRESS);
    this.rawDictionary = this.fileToList(filename);
    this.dictionary = this.removeWordsDiffLength(this.rawDictionary, this.answerLength);
    this.currentAnswer = this.nextWord(this.dictionary);
    System.out.print(LOADING_DONE);
  }

  public boolean play() {
    // Main game loop
    if (this.numGuesses > 0 && !this.playerWon) {
      System.out.println(this.info());
      char guess = this.promptUserLetter();

      // Guessing a word? if not guess a letter
      if (guess == WORD_MODE) {
        this.playerWon = this.handleWordGuess(this.promptUserWord(), this.guessedWords, this.currentAnswer);
      } else {
        this.playerWon = this.handleLetterGuess(guess);
      }

      // Shrink the dictionary and choose a new answer
      if (!this.playerWon) {
        this.dictionary = this.removeWordsNoMatch(this.dictionary, this.guessedLetters);
        this.currentAnswer = this.nextWord(this.dictionary);
      }

      return this.play();
    }

    // Game over, try again?
    if (!this.playerWon) {
      System.out.println(MESSAGE_LOSE);
      System.out.println(MESSAGE_CORRECT_ANSWER + this.currentAnswer);
    } else {
      System.out.println(MESSAGE_WIN);
    }

    return this.playAgain();
  }

  private boolean playAgain() {
    // ask
    System.out.print(PROMPT_PLAY_AGAIN);
    String response = user.next();

    // yes
    if (response.equalsIgnoreCase("y")) {
      Game newGame = new Game(this.rawDictionary);
      return newGame.play();
    }

    // not yes or no
    if (!response.equalsIgnoreCase("y") && !response.equalsIgnoreCase("n")) {
      System.out.println(MESSAGE_INVALID_RESPONSE_PLAY_AGAIN);
      return this.playAgain();
    }

    // no
    user.close();
    return false;
  }

  /**
   * @returns A string that provides information on
   * - how to input words
   * - the guessed letters
   * - the guesssed words
   * - the number of guesses
   * - a hint to the answer
   */
  private String info() {
    String info = this.clearConsole()
      + INFO_WORD_MODE + '\n' + '\n'
      + INFO_GUESSED_LETTERS + this.guessedLetters + '\n'
      + INFO_GUESSED_WORDS + this.guessedWords + '\n'
      + INFO_NUM_GUESSES + this.numGuesses + '\n'
      + INFO_HINT + this.hint();
    return info;
  }

  /**
   * Determine whether a guessed letter should
   * 1. Be added to the guessed letters list
   * 2. Decrement the number of guesses
   */
  private boolean handleLetterGuess(char guess) {
    this.guessedLetters.add(guess);
    if (this.currentAnswer.indexOf(guess) == -1) {
      this.numGuesses = this.decrementNumGuesses(this.numGuesses);
    }
    return this.correctAnswer(this.currentAnswer, this.answerLength, this.guessedLetters);
  }

  /**
   * Determine whether a guessed word should
   * 1. Be added to the guessed words list
   * 2. Decrement the number of guesses
   *
   * Additionally, it removes the guessed word
   * from the dictionary.
   */
  private boolean handleWordGuess(String wordGuess, ArrayList<String> guessedWords, String answer) {
    guessedWords.add(wordGuess);
    if (!answer.equalsIgnoreCase(wordGuess)) {
      this.numGuesses = this.decrementNumGuesses(this.numGuesses);
      this.dictionary = this.removeWord(this.dictionary, wordGuess);
      return false;
    } else {
      return true;
    }
  }

  /**
   * Get the user's input as a character
   */
  private char promptUserLetter() {
    String userInput = getUserInput(PROMPT_LETTER);
    if (invalidLetterInput(userInput)) {
      return this.promptUserLetterAgain(MESSAGE_INVALID_INPUT);
    } else if (alreadyGuessedLetters(userInput.charAt(0))) {
      return this.promptUserLetterAgain(MESSAGE_ALREADY_GUESSED);
    }
    return userInput.charAt(0);
  }

  private char promptUserLetterAgain(String message) {
    System.out.println(this.info());
    System.out.println(message);
    return promptUserLetter();
  }

  /**
   * Get the user's input as a word
   */
  private String promptUserWord() {
    String userInput = getUserInput(PROMPT_WORD);
    if (this.invalidWordInput(userInput)) {
      return this.promptUserWord();
    }
    return userInput;
  }

  /**
   * @returns a random word in the dictionary
   */
  private String nextWord(ArrayList<String> dictionary) {
    Random r = new Random();
    int i = r.nextInt(dictionary.size()) + 1;
    String word = dictionary.get(i - 1);
    return word;
  }

  /**
   * @returns a random word length
   */
  private int nextAnswerLength() {
    Random r = new Random();
    int length = r.nextInt((RAND_MAX - RAND_MIN) + 1) + RAND_MIN;
    return length;
  }

  /**
   * @returns the dictionary containing only the words that have the same length
   * as the specified length
   */
  private ArrayList<String> removeWordsDiffLength(ArrayList<String> dictionary, int length) {
    ArrayList<String> tmp = new ArrayList<String>();
    for (int i = 0; i < dictionary.size(); i++) {
      if (dictionary.get(i).length() == length) {
        tmp.add(dictionary.get(i));
      }
    }
    return tmp;
  }

  private ArrayList<String> removeWord(ArrayList<String> list, String word) {
    ArrayList<String> newList = list;
    while (newList.contains(word)) {
      newList.remove(newList.indexOf(word));
    }
    return newList;
  }

  private ArrayList<String> removeWordsNoMatch(ArrayList<String> dictionary, ArrayList<Character> letters) {
    ArrayList<String> newList = dictionary;
    for (int i = 0; i < newList.size(); i++) {
      String word = newList.get(i);
      for (int j = 0; j < word.length(); j++) {
        if (this.nonMatching(letters, word, j)) {
          newList.remove(i);
          i--;
          break;
        }
      }
    }
    return newList;
  }

  private boolean nonMatching(ArrayList<Character> letters, String word, int i) {
    return (word.charAt(i) != hint().charAt(i) && hint().charAt(i) != '_')
      || (letters.contains(word.charAt(i)) && hint().charAt(i) != word.charAt(i));
  }

  private String hint() {
    return this.hint(this.guessedLetters, this.currentAnswer);
  }

  private String hint(ArrayList<Character> letters, String word) {
    String hint = "";
    for (int i = 0; i < word.length(); i++) {
      char c = word.charAt(i);
      if (letters.contains(c)) {
        hint += c;
      } else {
        hint += "_";
      }
    }
    return hint;
  }

  private String getUserInput(String prompt) {
    System.out.print(prompt);
    String userInput = this.user.next();
    return userInput.toLowerCase();
  }

  /**
   * @returns an ArrayList of each line of the file
   */
  private ArrayList<String> fileToList(String filename) throws FileNotFoundException {
    Scanner s = new Scanner(new File(filename));
    ArrayList<String> newList = new ArrayList<String>();
    while (s.hasNextLine()) {
      newList.add(s.nextLine());
    }
    s.close();
    return newList;
  }

  private boolean alreadyGuessedLetters(char letter) {
    return guessedLetters.contains(letter);
  }

  /**
   * @param input The letter that the user guessed.
   * @return True if input length > 1 or the input is not a character
   * except in the case that the input matches the special word mode
   * character.
   */
  private boolean invalidLetterInput(String input) {
    return (input.length() > 1 || !isAlphabetic(input.charAt(0)))
      && input.charAt(0) != WORD_MODE;
  }

  /**
   * @param input The word that the user guessed.
   * @return True if each character in the input is alphabetic.
   */
  private boolean invalidWordInput(String input) {
    for (int i = 0; i < input.length(); i++) {
      if (!isAlphabetic(input.charAt(i))) {
        return true;
      }
    }
    return false;
  }

  private boolean isAlphabetic(char c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
  }

  private boolean correctAnswer(String answer, int answerLength, ArrayList<Character> guessed) {
    for (int i = 0; i < answerLength - 1; i++) {
      if (!guessed.contains(answer.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  private String clearConsole() {
    String s = "";
    for (int i = 0; i < CLEAR_CONSOLE_HEIGHT; i++) {
      s += '\n';
    }
    return s;
  }

  private int decrementNumGuesses(int numGuesses) {
    if (numGuesses > 0) {
      return numGuesses - 1;
    } else {
      return 0;
    }
  }
}
