=====ANSWER KEY=====
Test (44 points)

Question 1 (1 point)
1 is greater than 0. True or False?

1. **** True ****
2. False

Question 2 (25 points)
Which of these countries are from Australia?

1. Hawaii
2. Japan
3. Indonesia
4. New Zealand
5. **** Australia ****
6. Cambodia
7. Madagascar
8. Greenland
9. Norway
10. Argentina

Question 3 (10 points)
Who lives in a pineapple under the sea?

1. Peter Griffin
2. Scooby Doo
3. **** Spongebob Squarepants ****
4. Eric Cartman

Question 4 (1 point)
The typical Rubik's Cube has ___six___ sides.

Question 5 (1 point)
___Abraham Lincoln___ was the 16th US President.

Question 6 (1 point)
Is this an objective question?

**** Yes ****

Question 7 (5 points)
What is love?

There is no correct answer.