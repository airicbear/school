/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Tests the Questions classes to make sure they work properly.
 *
 */

import java.io.*;

class Driver {
  public static void main(String[] args) throws FileNotFoundException {
    // Hard-coded questions
    Question[] questions = questions();
    testTest(questions);
    testTestBank(questions);

    // Read questions from file
    testTestBank();
  }
  private static Question[] questions() {
    MultipleChoiceQuestion question1 = new MultipleChoiceQuestion("1 is greater than 0. True or False?", new String[]{"True", "False"}, "True");
    MultipleChoiceQuestion question2 = new MultipleChoiceQuestion(25, 3, 0, "Which of these countries are from Australia?", new String[]{"Hawaii", "Japan", "Indonesia", "New Zealand", "Australia", "Cambodia", "Madagascar", "Greenland", "Norway", "Argentina"}, "Australia");
    MultipleChoiceQuestion question3 = new MultipleChoiceQuestion(10, 2, 0, "Who lives in a pineapple under the sea?", new String[]{"Peter Griffin", "Scooby Doo", "Spongebob Squarepants", "Eric Cartman"}, "Spongebob Squarepants");
    FillInTheBlankQuestion question4 = new FillInTheBlankQuestion("The typical Rubik's Cube has ", "six", " sides.");
    FillInTheBlankQuestion question5 = new FillInTheBlankQuestion("", "Abraham Lincoln", " was the 16th US President.");
    ObjectiveQuestion question6 = new ObjectiveQuestion(1, 1, 3, "Is this an objective question?", "Yes");
    Question question7 = new Question(5, 3, 10, "What is love?");
    Question[] questions = {question1, question2, question3, question4, question5, question6, question7};
    return questions;
  }
  private static void testTest(Question[] questions) throws FileNotFoundException {
    // Sum the points of the questions
    int totalPoints = 0;
    for (int i = 0; i < questions.length; i++) {
      totalPoints += questions[i].getPoints();
    }

    // Make the test
    Test test = new Test(questions, totalPoints);

    // Print the test and its answer key
    System.out.println(test);
    System.out.println();
    System.out.println(test.answerKey());

    // Write the test to a file
    test.writeTo("test.txt");

    // Write the test answer key to a file
    test.writeAnswerKeyTo("answer_key.txt");
  }
  private static void testTestBank(Question[] questions) {
    TestBank testBank = new TestBank(questions);
    Test test = testBank.generateTest(3);
    System.out.println();
    System.out.println("=====TEST BANK=====");
    System.out.println(test);
    System.out.println();
    System.out.println(test.answerKey());
  }
  private static void testTestBank() throws FileNotFoundException {
    TestBank testBank = new TestBank("example_questions.txt");
    Test test = testBank.generateTest(3);
    System.out.println();
    System.out.println(testBank);
    System.out.println(test);
    System.out.println(test.answerKey());
  }
}
