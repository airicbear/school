/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Stores a collection of questions and generates randomized tests using those questions.
 * Capable of reading in properly formatted input text files for questions.
 * The necessary format varies on the type of question desired.
 * All parameters are delimited by a new line.
 * Every question must be provided its type, points, difficulty, answer space, and question text.
 * For example, to create a new objective question, ObjectiveQuestion(3, 3, 5, "The sky is blue.", "Yes"),
 * I would first need to specify the following
 * TYPE
 * POINTS
 * DIFFICULTY
 * ANSWERSPACE
 * QUESTIONTEXT
 * so in this case, it would look like
 * OQ
 * 3
 * 3
 * 5
 * The sky is blue.
 * Yes
 *
 * Example: new FillInTheBlankQuestion(2, 2, 0, "The ocean is made of ", "water", ".")
 * FITBQ
 * 2
 * 2
 * 0
 * The ocean is made of 
 * water
 * .
 *
 * Note how this is a bit different.
 * Fill in the blank questions are constructed via two separate questionTexts with
 * a correctAnswer squished in between them; so here we specify the questionText1 first
 * and then the correctAnswer and then finally the questionText2.
 *
 * Example: new MultipleChoiceQuestion(5, 3, 0, "What color are bananas?", new String[]{"Green","Yellow"}, "Yellow")
 * MCQ
 * 5
 * 3
 * 0
 * What color are bananas?
 * Yellow
 * 2
 * Green
 * Yellow
 *
 * Note: The number '2' after the correctAnswer ('yellow') specifies the length of possibleAnswers.
 * Afterwards, we specify all of the possible answers.
 * 
 */

import java.util.Scanner;
import java.util.Random;
import java.io.*;

class TestBank {
  private static final String[] QUESTION_IDENTIFIERS = {
    "Q",     // Question
    "OQ",    // ObjectiveQuestion
    "FITBQ", // FillInTheBlankQuestion
    "MCQ"    // MultipleChoiceQuestion
  };
  private Question[] questions;
  private String filename;
  private int numQuestions;
  public TestBank(Question[] questions) {
    this.questions = questions;
    this.numQuestions = questions.length;
  }
  public TestBank(String filename) throws FileNotFoundException {
    this.filename = filename;
    this.numQuestions = this.countQuestions();
    this.questions = readQuestions(filename);
  }
  public String toString() {
    String title = "TEST BANK\n=========";
    String qtns = "Questions (" + this.questions.length + ")\n---------\n";
    for (int i = 0; i < this.questions.length; i++) {
      qtns += "[" + i + "] " + this.questions[i].getQuestionText() + "\n";
    }
    return title + "\n" + (filename != null ? "Filename: " + filename : "") + "\n" + qtns;
  }

  /* Return the number of questions found in a questions input file */
  public int countQuestions() throws FileNotFoundException {
    int count = 0;
    Scanner s = new Scanner(new File(this.filename));
    while (s.hasNextLine()) {
      String nextLine = s.nextLine();
      if (this.isQuestion(nextLine)) {
        count++;
      }
    }
    s.close();
    return count;
  }

  /* Read in questions from a file */
  public Question[] readQuestions(String filename) throws FileNotFoundException {
    Question[] qtns = new Question[numQuestions];
    Scanner s = new Scanner(new File(filename));
    int qtnIndex = 0;
    while (s.hasNextLine() && qtnIndex < qtns.length) {
      String nextLine = s.nextLine();
      if (this.isQuestion(nextLine)) {
        Question q;
        int points = s.nextInt();
        int difficulty = s.nextInt();
        int answerSpace = s.nextInt();
        s.nextLine();
        String questionText = s.nextLine();
        if (nextLine.equalsIgnoreCase("OQ")) {
          String correctAnswer = s.nextLine();
          q = new ObjectiveQuestion(points, difficulty, answerSpace, questionText, correctAnswer);
        } else if (nextLine.equalsIgnoreCase("FITBQ")) {
          String correctAnswer = s.nextLine();
          String questionText2 = s.nextLine();
          q = new FillInTheBlankQuestion(points, difficulty, answerSpace, questionText, correctAnswer, questionText2);
        } else if (nextLine.equalsIgnoreCase("MCQ")) {
          String correctAnswer = s.nextLine();
          int numPossibleAnswers = s.nextInt();
          String[] possibleAnswers = new String[numPossibleAnswers];
          s.nextLine();
          for (int i = 0; i < numPossibleAnswers; i++) {
            possibleAnswers[i] = s.nextLine();
          }
          q = new MultipleChoiceQuestion(points, difficulty, answerSpace, questionText, possibleAnswers, correctAnswer);
        } else {
          q = new Question(points, difficulty, answerSpace, questionText);
        }
        qtns[qtnIndex] = q;
        qtnIndex++;
      }
    }
    return qtns;
  }

  /* Generate a test using randomly selected questions stored in the test bank */
  public Test generateTest(int numTestQuestions) {
    Random rand = new Random();
    if (numTestQuestions > this.questions.length) {
      numTestQuestions = this.questions.length;
    } else if (numTestQuestions < 0) {
      return new Test(new Question[0], 0);
    }
    Question[] testQuestions = new Question[numTestQuestions];
    for (int i = 0; i < testQuestions.length; i++) {
      Question randomQuestion = this.questions[rand.nextInt(this.questions.length)];
      while(this.contains(testQuestions, randomQuestion)) {
        randomQuestion = this.questions[rand.nextInt(this.questions.length)];
      }
      testQuestions[i] = randomQuestion;
    }
    Test randomTest = new Test(testQuestions, sumPoints(testQuestions));
    return randomTest;
  }
  private int sumPoints(Question[] questions) {
    int sum = 0;
    for (int i = 0; i < questions.length; i++) {
      sum += questions[i].getPoints();
    }
    return sum;
  }
  private boolean contains(Question[] questions, Question q) {
    for (int i = 0; i < questions.length; i++) {
      if (questions[i] != null && questions[i].equals(q)) {
        return true;
      }
    }
    return false;
  }
  private boolean isQuestion(String line) {
    for (int i = 0; i < QUESTION_IDENTIFIERS.length; i++) {
      if (line.equalsIgnoreCase(QUESTION_IDENTIFIERS[i])) {
        return true;
      }
    }
    return false;
  }
}
