/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Formats a set of questions into a readable test and its answer key.
 *
 */

import java.io.*;

class Test {
  private Question[] questions;
  private int totalPoints;
  Test(Question[] questions, int totalPoints) {
    this.questions = questions;
    this.totalPoints = totalPoints;
  }
  public String toString() {
    String s = "Test (" + this.totalPoints + " point" + (this.totalPoints > 1 ? "s" : "") + ")\n\n";
    for (int i = 0; i < questions.length - 1; i++) {
      s += "Question " + (i + 1)
        + " (" + questions[i].getPoints() + " point" + (questions[i].getPoints() > 1 ? "s" : "") + ")" + "\n"
        + questions[i] + "\n\n";
    }
    s += "Question " + questions.length
      + " (" + questions[questions.length - 1].getPoints() + " point" + (questions[questions.length - 1].getPoints() > 1 ? "s" : "") + ")" + "\n"
      + questions[questions.length - 1];
    return s;
  }
  public String answerKey() {
    String s = "=====ANSWER KEY=====\nTest (" + this.totalPoints + " point" + (this.totalPoints > 1 ? "s" : "") + ")\n\n";
    for (int i = 0; i < questions.length - 1; i++) {
      s += "Question " + (i + 1)
        + " (" + questions[i].getPoints() + " point" + (questions[i].getPoints() > 1 ? "s" : "") + ")" + "\n"
        + questions[i].answerToString() + "\n\n";
    }
    s += "Question " + questions.length
      + " (" + questions[questions.length - 1].getPoints() + " point" + (questions[questions.length - 1].getPoints() > 1 ? "s" : "") + ")" + "\n"
      + questions[questions.length - 1].answerToString();
    return s;
  }
  public void writeTo(String filename) throws FileNotFoundException {
    PrintStream output = new PrintStream(new File(filename));
    output.print(this.toString());
  }
  public void writeAnswerKeyTo(String filename) throws FileNotFoundException {
    PrintStream output = new PrintStream(new File(filename));
    output.print(this.answerKey());
  }
}
