/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Organizes all Question-related data and operations into a class.
 *
 */

class Question {
  private static final int MIN_DIFFICULTY = 1;
  private static final int MAX_DIFFICULTY = 3;
  protected static final int DEFAULT_POINTS = 1;
  protected static final int DEFAULT_DIFFICULTY = 1;
  protected static final int DEFAULT_ANSWER_SPACE = 0;
  protected int points;
  protected int difficulty;
  protected int answerSpace;
  protected String questionText;
  public Question(int points, int difficulty, int answerSpace, String questionText) {
    this.points = points;
    if (difficulty < MIN_DIFFICULTY) {
      this.difficulty = MIN_DIFFICULTY;
    } else if (difficulty > MAX_DIFFICULTY) {
      this.difficulty = MAX_DIFFICULTY;
    } else {
      this.difficulty = difficulty;
    }
    this.answerSpace = answerSpace;
    this.questionText = questionText;
  }
  public Question(String questionText) {
    this.points = DEFAULT_POINTS;
    this.difficulty = DEFAULT_DIFFICULTY;
    this.answerSpace = DEFAULT_ANSWER_SPACE;
    this.questionText = questionText;
  }
  public String toString() {
    String lines = "";
    for (int i = 0; i < this.answerSpace; i++) {
      lines += "\n";
    }
    return questionText + lines;
  }
  public String getQuestionText() {
    return this.questionText;
  }
  public int getPoints() {
    return this.points;
  }
  public String answerToString() {
    return this.questionText + "\n\nThere is no correct answer.";
  }
  public boolean equals(Question other) {
    return (this.points == other.points
            && this.difficulty == other.difficulty
            && this.answerSpace == other.answerSpace
            && this.questionText.equals(other.questionText));
  }
}
