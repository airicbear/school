/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Extends the ObjectiveQuestion to have multiple possible answers.
 *
 */

class MultipleChoiceQuestion extends ObjectiveQuestion {
  private String[] possibleAnswers;
  public MultipleChoiceQuestion(int points, int difficulty, int answerSpace, String questionText, String[] possibleAnswers, String correctAnswer) {
    super(points, difficulty, answerSpace, questionText, correctAnswer);
    this.possibleAnswers = possibleAnswers;
  }
  public MultipleChoiceQuestion(String questionText, String[] possibleAnswers, String correctAnswer) {
    super(questionText, correctAnswer);
    this.possibleAnswers = possibleAnswers;
  }
  public String toString() {
    String options = "";
    for (int i = 0; i < this.possibleAnswers.length - 1; i++) {
      options += (i + 1) + ". " + this.possibleAnswers[i] + "\n";
    }
    options += this.possibleAnswers.length + ". " + this.possibleAnswers[this.possibleAnswers.length - 1];
    return this.questionText + "\n\n" + options;
  }
  public String answerToString() {
    String options = "";
    for (int i = 0; i < this.possibleAnswers.length - 1; i++) {
      if (this.possibleAnswers[i].equalsIgnoreCase(this.correctAnswer)) {
        options += (i + 1) + ". **** " + this.possibleAnswers[i] + " ****\n";
      } else {
        options += (i + 1) + ". " + this.possibleAnswers[i] + "\n";
      }
    }
    if (this.possibleAnswers[this.possibleAnswers.length - 1].equalsIgnoreCase(this.correctAnswer)) {
      options += this.possibleAnswers.length + ". **** " + this.possibleAnswers[this.possibleAnswers.length - 1] + " ****";
    } else {
      options += this.possibleAnswers.length + ". " + this.possibleAnswers[this.possibleAnswers.length - 1];
    }
    return questionText + "\n\n" + options;
  }
}
