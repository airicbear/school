package cis1068.stringPracticeAssignment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class StringPracticeTest extends StringPractice {

  @Test
  public void testIsFace() {
    assertTrue(StringPractice.isFace('a'));
    assertTrue(StringPractice.isFace('A'));
    assertTrue(StringPractice.isFace('k'));
    assertTrue(StringPractice.isFace('K'));
    assertTrue(StringPractice.isFace('q'));
    assertTrue(StringPractice.isFace('A'));
    assertTrue(StringPractice.isFace('j'));
    assertTrue(StringPractice.isFace('J'));
    assertFalse(StringPractice.isFace('f'));
    assertFalse(StringPractice.isFace('G'));
    assertFalse(StringPractice.isFace('0'));
  }

  @Test
  public void testIndexOfFirstFaceString() {
    assertEquals(-1, StringPractice.indexOfFirstFace(""));
    assertEquals(0, StringPractice.indexOfFirstFace("abalone"));
    assertEquals(6, StringPractice.indexOfFirstFace("trepidation"));
    assertEquals(3, StringPractice.indexOfFirstFace("workbook"));
  }

  @Test
  public void testIndexOfFirstFaceStringInt() {
    assertEquals(1, StringPractice.indexOfFirstFace("squidward"));
    assertEquals(1, StringPractice.indexOfFirstFace("SQUIDWARD"));
    assertEquals(2, StringPractice.indexOfFirstFace("vikinglike"));
    assertEquals(2, StringPractice.indexOfFirstFace("VIKINGLIKE"));
    assertEquals(0, StringPractice.indexOfFirstFace("kafkaesque"));
    assertEquals(0, StringPractice.indexOfFirstFace("KAFKAESQUE"));
    assertEquals(-1, StringPractice.indexOfFirstFace("homer"));
  }

  @Test
  public void testIndexOfLastFace() {
    assertEquals(6, StringPractice.indexOfLastFace("squidward"));
    assertEquals(6, StringPractice.indexOfLastFace("SQUIDWARD"));
    assertEquals(8, StringPractice.indexOfLastFace("vikinglike"));
    assertEquals(8, StringPractice.indexOfLastFace("VIKINGLIKE"));
    assertEquals(7, StringPractice.indexOfLastFace("kafkaesque"));
    assertEquals(7, StringPractice.indexOfLastFace("KAFKAESQUE"));
    assertEquals(-1, StringPractice.indexOfLastFace("homer"));
  }

  @Test
  public void testReversed() {
    assertEquals("cba", StringPractice.reversed("abc"));
    assertEquals("a", StringPractice.reversed("a"));
    assertEquals("dcba", StringPractice.reversed("abcd"));
  }

  @Test
  public void testNumOccurrences() {
    assertEquals(2, StringPractice.numOccurrences("banana", "na"));
    assertEquals(2, StringPractice.numOccurrences("mississippi", "ss"));
    assertEquals(0, StringPractice.numOccurrences("undertow", "sushi"));
  }

  @Test
  public void testSameInReverse() {
    assertTrue(StringPractice.sameInReverse("peep"));
    assertTrue(StringPractice.sameInReverse("madam"));
    assertTrue(StringPractice.sameInReverse("rotator"));
    assertFalse(StringPractice.sameInReverse("blue"));
    assertFalse(StringPractice.sameInReverse("aspirin"));
    assertFalse(StringPractice.sameInReverse("ab"));
    assertTrue(StringPractice.sameInReverse("a"));
  }

  @Test
  public void testWithoutFaces() {
    assertEquals("suidwrd", StringPractice.withoutFaces("squidward"));
    assertEquals("SUIDWRD", StringPractice.withoutFaces("SQUIDWARD"));
    assertEquals("viinglie", StringPractice.withoutFaces("vikinglike"));
    assertEquals("VIINGLIE", StringPractice.withoutFaces("VIKINGLIKE"));
    assertEquals("fesue", StringPractice.withoutFaces("kafkaesque"));
    assertEquals("FESUE", StringPractice.withoutFaces("KAFKAESQUE"));
    assertEquals("homer", StringPractice.withoutFaces("homer"));
  }

  @Test
  public void testContainsOnlyFaces() {
    assertTrue(StringPractice.containsOnlyFaces("kQjj"));
    assertTrue(StringPractice.containsOnlyFaces("JAK"));
    assertFalse(StringPractice.containsOnlyFaces("spring break"));
    assertFalse(StringPractice.containsOnlyFaces("temple"));
  }

  @Test
  public void testContainsNoFaces() {
    assertTrue(StringPractice.containsNoFaces("beguile"));
    assertTrue(StringPractice.containsNoFaces("chess"));
    assertTrue(StringPractice.containsNoFaces("cider"));
    assertTrue(StringPractice.containsNoFaces("unsupported"));
    assertTrue(StringPractice.containsNoFaces("visitor"));
    assertFalse(StringPractice.containsNoFaces("viking"));
    assertFalse(StringPractice.containsNoFaces("ajax"));
  }

  @Test
  public void testLastFirst() {
    assertEquals("Sqigmann, Andrew", StringPractice.lastFirst("Andrew Sqigmann"));
    assertEquals("Squarepants, Spongebob", StringPractice.lastFirst("Spongebob Squarepants"));
    assertEquals("Pines, Stanley", StringPractice.lastFirst("Stanley Pines"));
  }
}
