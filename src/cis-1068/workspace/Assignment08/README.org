#+title: CIS 1068 Assignment 8 Warm Up with Objects

Due: Wednesday, March 25

70 points (+ up to 15 extra credit)

The purpose of this assignment is to give you practice implementing your
own classes. It also provides extra practice with arrays.

* Task
  :PROPERTIES:
  :CUSTOM_ID: task
  :END:

Implement a class Task, which is used to represent a job that should be
done. It should contain the following private fields:

- name text description of what job should be done (e.g., take out the
  trash, finish 1068 homework).
- priority, a non-negative integer which stores the importance of a
  Task.
- estMinsToComplete, a non-negative integer which holds the anticipated
  number of minutes it should take to complete the task
- *extra credit:* whenDue a
  [[https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html][java.time.LocalDateTime]]
  object representing the date and time when the task should be
  completed

Implement at least the following public methods within the Task class:

- a constructor, which initializes each of the fields
- accessor methods for each of the fields. Use the naming convention
  getXXX() where XXX is the name of the field, e.g., you'll write a
  method called getName() which returns the name of the Task.
- mutator methods for each of the fields except for priority. Use the
  naming convention setXXX() where XXX is the name of the field, e.g.,
  you'll write a method called setName(String newName), which would
  update the name field of the Task to newName.
- toString( ), which returns a String representation of the Task.
- increasePriority(int amount) increases the priority level by amount.
  If a negative amount is specified, the method returns without making
  any changes.
- decreasePriority(int amount) decreases the priority level by amount.
  If the decrease is more than the current value of priority (and
  subtracting this amount would result in a negative priority), priority
  should be set to 0.
- *extra credit:* overdue() returns true if the current date/time is
  later than whenDue or false otherwise.

For example, we should be able to do something like the following:

#+BEGIN_EXAMPLE
      // creates a new Task object to remind you
      // to finish your 1068 homework. It has priority 3
      // and I anticipate that it's going to take 120 minutes
      Task doMyHW = new Task("finish 1068 homework", 3, 120);


      // if we're doing extra credit, we might instead write:
      Task postHW = new Task("post 1068 homework", 1, LocalDateTime.of(2019, 3, 23, 13, 0), 180);

      // which would be to remind me to post the 1068 homework assignment.
      // This has priority 1, should take me 180 minutes
      // and it's due March 23, 2019 at 1 PM (i.e., 13:00)
#+END_EXAMPLE

Write a very short driver program (a separate class containing a main())
to test your code to make sure it's working. You are not required to
write JUnit tests.

* HoneyDoList
  :PROPERTIES:
  :CUSTOM_ID: honeydolist
  :END:

Implement a class HoneyDoList, which is used to manage a collection of
Task. It should contain the following private fields:

- tasks[] an array of Task
- numTasks, a non-negative integer storing the number of items contained
  in tasks[].
- INITIAL_CAPACITY, a constant non-negative integer. This is how large
  the task array should be at creation time.

Implement at least the following public methods within the HoneyDo
class:

- a constructor, which creates the tasks[] array, giving it the capacity
  of INITIAL_CAPACITY, and setting numTasks to 0.
- toString( ), which returns a String representation of each Task in
  tasks[]. Do not include in the String null for entries in tasks[] that
  are empty.
- find(String name), which returns the index of the first occurence of a
  Task whose name matches the name specified ignoring case. Recall that
  there is an equalsIgnoreCase() method in the String class. The method
  should return -1 if no match is found.
- addTask(), which appends a new Task to the occupied end of tasks[]. If
  tasks[] is already full when you attempt to add the new Task, a new
  array is created, all of the items are copied from the old array into
  the new one, and tasks[] is set to point to the new array.
- totalTime() which returns the total time in minutes required to
  complete all tasks in the list.
- shortestTime() which returns the index of the task that should take
  the least amount of time to finish or -1 if the list is empty.
- completeTask(int index), which removes and returns the Task at the
  specified index shifting all subsequent tasks in the array one
  position to the left. If index is invalid, returns null.
  [[https://cis.temple.edu/~jfiore/2020/spring/1068/assignments/08/#shift_task][See
  example]].
- *extra credit:* overdueTasks() returns an array of Task consisting of
  only the items in tasks[] that are overdue.

For example, we should be able to do something like the following:

#+BEGIN_EXAMPLE
     // create a new empty list
     HoneyDoList honeydo = new HoneyDoList();

     System.out.println(honeydo);     
     honeydo.addTask(new Task("take aspirin", 1, 120));
     System.out.println(honeydo);

     // print the item in the list which should
     // take the least amount of time
     System.out.println(honeydo.shortestTime()); // calls toString() in Task
#+END_EXAMPLE

Write a very short driver program to test your code to make sure it's
working. You are not required to write JUnit tests.

* Implementation Notes
  :PROPERTIES:
  :CUSTOM_ID: implementation-notes
  :END:

One of the purposes of this assignment is give you practice managing
fixed-sized arrays. Although you may use the Arrays class, please
refrain from using ArrayList or any other class in Java's Collections
Framework. (We'll get to these soon 🤞.)

** Multiple Files in a Single Eclipse Project
   :PROPERTIES:
   :CUSTOM_ID: multiple-files-in-a-single-eclipse-project
   :END:

You'll implement each class as its own separate file within the same
Eclipse project. Create each class as you have all semester, by
right-clicking on the package icon under the src directory, and choosing
New, then Class. Be sure that each class you pick falls under the same
package (in Eclipse, the package icon is a square with a large plus sign
superimposed). The Package Explorer view of your project should look
something like this:

[[./CIS%201068%20Assignment%208.%20Warm%20Up%20with%20Objects._files/eclipse_project_view.png]]
As you're working through the assignment testing your class
implementations, you'll end up with two completely different classes
that have a main(). When we try to run the project, how do we make sure
that eclipse runs the one we intend? Right click (or command-click on a
Mac) on the file you wish to run, then choose "Run As", then "Java
Application". Alternatively, you can go to the Run menu, choose Run
Configurations, and under the Main tab, insert the class whose main you
want to run in the text field for Main class.

* Shifting Tasks
  :PROPERTIES:
  :CUSTOM_ID: shift_task
  :END:

Suppose that we have a HoneyDoList with a capacity of 10 items and
contains 4. We can visualize it this way:

[[./CIS%201068%20Assignment%208.%20Warm%20Up%20with%20Objects._files/task_list_before_delete.png]]
Suppose that we then complete task 1. Each subsequent item is shifted
down one position and a null reference is pulled in from the end.

[[./CIS%201068%20Assignment%208.%20Warm%20Up%20with%20Objects._files/task_list_after_delete.png]]
