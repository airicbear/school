/**
 * Eric Nguyen
 * Section 008
 * Assignment 8 Warm Up with Objects
 * 2020-03-22
 *
 * Task class to organize all data and operations associated with individual tasks.
 */

import java.time.LocalDateTime;

public class Task {

  private String name;
  private int priority;
  private int estMinsToComplete;
  private LocalDateTime whenDue;

  public Task(String name, int priority, int estMinsToComplete) {
    this.name = name;
    this.priority = priority;
    this.estMinsToComplete = estMinsToComplete;
    this.whenDue = null;
  }

  public Task(String name, int priority, LocalDateTime whenDue, int estMinsToComplete) {
    this.name = name;
    this.priority = priority;
    this.estMinsToComplete = estMinsToComplete;
    this.whenDue = whenDue;
  }
  
  public String getName() {
    return this.name;
  }
  
  public int getPriority() {
    return this.priority;
  }
  
  public int getEstMinsToComplete() {
    return this.estMinsToComplete;
  }
  
  public LocalDateTime getWhenDue() {
    return this.whenDue;
  }

  public void setName(String newName) {
    this.name = newName;
  }

  public void setEstMinsToComplete(int newEstMinsToComplete) {
    this.estMinsToComplete = newEstMinsToComplete;
  }

  public void setWhenDue(LocalDateTime newWhenDue) {
    this.whenDue = newWhenDue;
  }
  
  public String toString() {
    return "Task("
      + "name = "
      + this.name
      + ", priority = "
      + this.priority
      + ", estMinsToComplete = "
      + this.estMinsToComplete
      + ", whenDue = "
      + this.whenDue
      + ")";
  }

  public void increasePriority(int amount) {
    if (amount > 0) {
      this.priority = this.priority - amount;
    }
  }

  public void decreasePriority(int amount) {
    if (amount > 0) {
      this.priority = this.priority + amount;
    }
  }

  public boolean overdue() {
    return LocalDateTime.now().isAfter(this.whenDue);
  }

}
