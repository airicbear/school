/**
 * Eric Nguyen
 * Section 008
 * Assignment 8 Warm Up with Objects
 * 2020-03-22
 *
 * Basic testing for the HoneyDoList class.
 */

import java.time.LocalDateTime;

public class HoneyDoMain {
  public static void main(String[] args) {
    HoneyDoList honeydo = new HoneyDoList();

    System.out.println("TEST EMPTY LIST");
    System.out.println(honeydo);
    System.out.println();

    System.out.println("TEST ADD TASK");
    honeydo.addTask(new Task("drink water", 1, LocalDateTime.now(), 20));
    honeydo.addTask(new Task("celebrate birthday in Minecraft", 3, 120));
    honeydo.addTask(new Task("eat rice", 3, 30));
    honeydo.addTask(new Task("check instagram", 5, 2));		
    honeydo.addTask(new Task("read the epic of gilgamesh", 1, 200));
    System.out.println(honeydo);
    System.out.println();

    System.out.println("TEST FIND");
    testFind(honeydo, "eat rice");
    System.out.println();

    System.out.println("TEST TOTAL TIME");
    testTotalTime(honeydo);
    System.out.println();

    System.out.println("TEST SHORTEST TIME");
    testShortestTime(honeydo);
    System.out.println();

    System.out.println("TEST COMPLETE TASK");
    testCompleteTask(honeydo, 3);
    System.out.println();

    System.out.println("TEST OVERDUE TASKS");
    testOverdueTasks(honeydo);
    System.out.println();
  }

  public static void testFind(HoneyDoList list, String query){
    int index = list.find(query);
    System.out.println("The task \""
                       + query
                       + "\" from the list "
                       + (index >= 0 ?
                          "is at index "
                          + index
                          + "."
                          : "does not exist."));
    System.out.println(list);
  }

  public static void testTotalTime(HoneyDoList list) {
    System.out.println("It will take about "
                       + list.totalTime()
                       + " minutes to finish all of the tasks in the list.");
    System.out.println(list);
  }

  public static void testShortestTime(HoneyDoList list) {
    int index = list.shortestTime();
    System.out.println("The quickest task is at index " + index + ".");
    System.out.println(list);
  }

  public static void testCompleteTask(HoneyDoList list, int index) {
    System.out.println("Before: ");
    System.out.println(list);
    
    list.completeTask(index);
    System.out.println("After: ");
    System.out.println(list);
  }

  public static void testOverdueTasks(HoneyDoList list) {
    System.out.println(HoneyDoList.tasksToString(list.overdueTasks(), list.getNumOverdueTasks()));
  }
}
