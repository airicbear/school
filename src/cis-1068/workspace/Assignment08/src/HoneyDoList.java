/**
 * Eric Nguyen
 * Section 008
 * Assignment 8 Warm Up with Objects
 * 2020-03-22
 *
 * HoneyDoList class to organize and manipulate lists of Tasks.
 */

public class HoneyDoList {
  private Task[] tasks;
  private int numTasks;
  private int numOverdueTasks;
  private static final int INITIAL_CAPACITY = 10;

  public HoneyDoList() {
    this.tasks = new Task[INITIAL_CAPACITY];
    this.numTasks = 0;
    this.numOverdueTasks = 0;
  }

  public static String taskToString(Task[] tasks, int index) {
    return "  - ["
      + tasks[index].getPriority()
      + "] "
      + tasks[index].getName()
      + " ("
      + tasks[index].getEstMinsToComplete()
      + " mins"
      + (tasks[index].getWhenDue() != null ?
         ", due "
         + tasks[index].getWhenDue().toLocalDate() : "")
      + ")";
  }

  public static String tasksToString(Task[] tasks, int numTasks) {
    if (numTasks <= 0) {
      return "There currently are no tasks.";
    }
    String s = "Tasks:\n";

    for (int i = 0; i < numTasks - 1; i++) {
      s += taskToString(tasks, i) + "\n";
    }
    s += taskToString(tasks, numTasks - 1);
    return s;
  }

  public String toString() {
    return tasksToString(this.tasks, this.numTasks);
  }

  public int find(String name) {
    for (int i = 0; i < this.numTasks; i++) {
      if (this.tasks[i].getName().equalsIgnoreCase(name)) {
        return i;
      }
    }
    return -1;
  }

  public void addTask(Task task) {
    if (this.numTasks < INITIAL_CAPACITY) {
      this.tasks[numTasks] = task;
    }
    this.numTasks++;
  }

  public int totalTime() {
    int total = 0;
    for (int i = 0; i < this.numTasks; i++) {
      total += this.tasks[i].getEstMinsToComplete();
    }
    return total;
  }

  public int shortestTime() {
    int min = this.tasks[0].getEstMinsToComplete();
    int index = 0;
    for (int i = 1; i < this.numTasks; i++) {
      if (this.tasks[i].getEstMinsToComplete() < min) {
        min = this.tasks[i].getEstMinsToComplete();
        index = i;
      }
    }
    return index;
  }

  public Task completeTask(int index) {
    if (index < 0 || index > this.numTasks - 1) {
      return null;
    }

    Task pop = this.tasks[index];
    for (int i = index; i < this.numTasks - 1; i++) {
      this.tasks[i] = this.tasks[i + 1];
    }
    this.tasks[numTasks - 1] = null;
    this.numTasks--;
    return pop;
  }

  public Task[] overdueTasks() {
    Task[] overdue = new Task[this.numTasks];
    int overdueIndex = 0;
    for (int i = 0; i < numTasks; i++) {
      if (this.tasks[i].getWhenDue() != null && this.tasks[i].overdue()) {
        overdue[overdueIndex] = this.tasks[i];
        overdueIndex++;
      }
    }
    this.numOverdueTasks = overdueIndex;
    return overdue;
  }

  public int getNumOverdueTasks() {
    return this.numOverdueTasks;
  }
}
