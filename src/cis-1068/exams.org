#+title: Exams

These are my CIS 1068 exam solutions.
Warning: they are not the best.

* Midterm 1B

** Problems 1-17, Multiple choice

| question | correct answer | your answer | points |
|----------+----------------+-------------+--------|
|        1 | B              | B           |      1 |
|        2 | A              | A           |      1 |
|        3 | B              | B           |      1 |
|        4 | B              | B           |      1 |
|        5 | A              | D           |      0 |
|        6 | E              | E           |      6 |
|        7 | D              | B           |      0 |
|        8 | C              | C           |      6 |
|        9 | C              | C           |      1 |
|       10 | B              | B           |      1 |
|       11 | C              | A           |      0 |
|       12 | C              | C           |      6 |
|       13 | A              | A           |      1 |
|       14 | D              | D           |      6 |
|       15 | D              | D           |      6 |
|       16 | B              | D           |      0 |
|       17 | B              | B           |      6 |
|          |                |             |     43 |

*** 5.

     (6 points) What is printed by by the following?

     #+begin_src java :eval never
       public class WhatsPrinted {
           public static void mystery(int x, int y) {
               int z = 4;
               if (z <= x) {
                   z = x + 1;
               } else {
                   z = z + 9;
               }
               if (z <= y) {
                   y++;
               }
               System.out.println(z + " " + y);
           }

           public static void main(String []args) {
               mystery(10, 5);
               mystery(1, 14);
           }
       }
     #+end_src

     - =11 5= \\
       =13 15= ✓

     - =11 5= \\
       =13 14=

     - =13 5= \\
       =2 15=

     - =11 6= \\
       =13 15= ❌

*** 7.

    (6 points) What is printed by the following?

    #+begin_src java :eval never
      public class WhatsPrinted {
          public static void main(String args[]) {
              int x=10, y=20, z=30;

              if (x < y) {
                  System.out.println("Washington");
              } else if ((x + y) % 2 == 0) {
                  if (z >= 30 || z <= 30) {
                      System.out.print("(not Quincy) ");
                  }
                  System.out.println("Adams");
              } else if (z - 2 < y + 1) {
                  System.out.println("Jefferson");
              } else {
                  System.out.println("Madison");
              }
          }
      }
    #+end_src

    - Washington \\
      (not Quincy)

    - Washington \\
      (not Quincy) Adams ❌

    - Madison

    - Washington ✓

    - Jefferson

*** 11.

    (6 points) What is printed by the following?

    #+begin_src java :eval never
      public class WhatsPrinted {
          public static void main(String []args) {
              String s1 = "bob";
              String s2 = "lob";
              String s3 = "law";

              for (int i = 0; i < 5; i++) {
                  if (i % 2 == 0) {
                      s1 = s1 + s2;
                      i+=2;
                  } else {
                      s2 += s2;
                  }
              }
              System.out.println(s1);
          }
      }
    #+end_src

    - boblobloblawloblawlaw ❌

    - loblawlaw

    - boblobloblaw ✓

    - bobloblawloblawlawloblawlawlaw

    - loblawloblob

*** 16.

    (6 points) What is printed by the following?

    #+begin_src java :eval never
      public class WhatsPrinted {
          public static void main(String []args) {
              int i = 100;

              do {
                  System.out.println(i);
                  i++;
              } while (i < 10);
          }
      }
    #+end_src

    - infinite loop

    - 100 ✓

    - 101

    - counts upwards through all possible positive integers until overflow ❌

    - nothing

** Problems 18-19, Expression Evaluation

   Evaluate each of the following expressions.
   Write the value of each or =Error= if the expression results in an error.

   - 18. (1 point)

     =8 / 5 * 5 + 16=

     - 21

   - 19. (1 point)

     =2.0 / 5 == 2 / 5=

     - +=Error=+ =false=

   - 20. (1 point)

     ="wellspring".substring(4) + " " + "cab".charAt(2) + "streaks".substring(2, 6)=

     - spring break

** Problems 21-23, Translate

   Translate each of the following statements from English to Java.
   For example, if the English is "x is larger than 10", you'd write the Java expression =x > 10=.
   Assume that we already have =int x=, =int y=, and the =String s= and =String t= already properly declared somewhere else in our program.

   - 21. (1 point) =t= does not occur in =s=

     - =s.indexOf(t) < 0=

   - 22. (1 point) The sum of x and y is greater than the product of x and y

     - =(x + y) > (x * y)=
     
   - 23. (1 point) =s= and =t= contain the same number of characters.
     
     - =s.length == t.length=

** Problems 24-26, Short Answer

*** Problem 24
   
    (6 points)
    Write the few lines of code that print the integers between 95 and 45 that are divisible by 6 as a:

**** (a) =for= loop

     #+begin_src java :eval never
   for (int i = 90; i >= 48; i -= 6) {
       System.out.println(i);
   }
     #+end_src

**** (b) =while= loop

     #+begin_src java :eval never
   int n = 90;
   while (n >= 48) {
       System.out.println(n);
       x -= 6;
   }
     #+end_src

**** (c) =do-while= loop
    
     #+begin_src java :eval never
   int x = 90;
   do {
       System.out.println(x);
       x -= 6;
   } while (x >= 48);
     #+end_src

*** Problem 25

     (9 points)

**** (a)

     Write a method called =sumK= which is passed an =int b= (for "beginning") and an =int k=.
     The method returns the sum of the next *k* integers starting with and including =b=.
     For example, if =b = 3= and =k = 5=, the method returns 25, /i.e./, 3 + 4 + 5 + 6 + 7 = 25.
     You do not have to write a complete class.
     You do not have to use a =Scanner= to read user input from the keyboard.

     #+begin_src java :eval never
   public static int sumK(int b, int k) {
       int result = 0;
       for (int i = b; i <= (b + k); i++) {
           result += i;
       }
       return result;
   }
     #+end_src

**** (b)

     Write the single line of code that calls your method in order to calculate the sum of the
     next 10 integers starting with 15 and stores the result in an integer named =sum=.

     #+begin_src java :eval never
   int sum = sumK(15, 10);
     #+end_src

*** Problem 26

     #+begin_quote
     Note that I used =ArrayList=.
     We didn't learn about this in class yet when we took the exam.
     #+end_quote

     (12 points)
     Write a program which reads in from the user at the keyboard a series of temperatures.
     The program should continue to read in temperatures until the user enters a temperature of -100.
     The program then prints the coldest of all hot temperatures.
     Define a constant =HOT_THR= such that any temperature greater than or equal to =HOT_THR= is considered to be hot.

     If not hot temperatures have been entered, instead of printing the coldest of the hot temperatures print "No hot days."

     #+begin_src java :eval never
    import java.util.*;

    public class MinHotTemp {
        public static final int HOT_THR = 100;

        public static void main(String[] args) {
            Scanner kbd = new Scanner(System.in);
            ArrayList<Int> hotTemps = new ArrayList<Int>();
            int temp = kbd.nextInt();
            while (temp != -100) {
                if (temp >= HOT_THR) {
                    hotTemps.push(temp);
                }
                temp = kbd.nextInt();
            }
            if (hotTemps.size() == 0) {
                System.out.println("No hot days.");
            } else {
                int minHotTemp = hotTemps.get(0);
                for (int i = 1; i < hotTemps.size(); i++) {
                    if (hotTemps.get(i) <= minHotTemp) {
                        minHotTemp = hotTemps.get(i);
                    }
                }
                System.out.println(minHotTemp);
            }
        }
    }
     #+end_src


