#+title: Precalculus Homework
#+options: toc:2 num:3

#+LaTeX_HEADER: \let\originalleft\left
#+LaTeX_HEADER: \let\originalright\right
#+LaTeX_HEADER: \renewcommand{\left}{\mathopen{}\mathclose\bgroup\originalleft}
#+LaTeX_HEADER: \renewcommand{\right}{\aftergroup\egroup\originalright}
#+LaTeX_HEADER: \usepackage[margin=1in]{geometry}

-----
*Textbook:* /Stewart/Redlin/Watson’s Precalculus: Mathematics for Calculus, 7th edition, and Enhanced WebAssign, Cengage Learning/
-----

* Chapter 2: Functions & Chapter 3: Polynomial and Rational Functions

** 2.1 Functions
*** 19. $f(x) = x^2 - 6; \quad f(-3), f(3), f(0), f\left(\frac{1}{2}\right)$

$$\begin{aligned}
f(-3) &= (-3)^2 - 6 \\
&= 9 - 6 \\
&= \boxed{3}
\end{aligned}$$

$$\begin{aligned}
f(3) &= (3)^2 - 6 \\
&= 9 - 6 \\
&= \boxed{3}
\end{aligned}$$

$$\begin{aligned}
f(0) &= (0)^2 - 6 \\
&= 0 - 6 \\
&= \boxed{-6}
\end{aligned}$$

$$\begin{aligned}
f\left(\frac{1}{2}\right) &= \left(\frac{1}{2}\right)^2 - 6 \\
&= \frac{1}{4} - 6 \\
&= \frac{1}{4} - \frac{24}{4} \\
&= \boxed{-\frac{23}{4}}
\end{aligned}$$

*** 20. $f(x) = x^3 + 2x; \quad f(-2), f(-1), f(0), f\left(\frac{1}{2}\right)$

$$\begin{aligned}
f(-2) &= (-2)^3 + 2(-2) \\
&= -8 - 4 \\
&= \boxed{-12}
\end{aligned}$$

$$\begin{aligned}
f(-1) &= (-1)^3 + 2(-1) \\
&= -1 - 2 \\
&= \boxed{-3}
\end{aligned}$$

$$\begin{aligned}
f(0) &= (0)^3 + 2(0) \\
&= 0 + 0 \\
&= \boxed{0}
\end{aligned}$$

$$\begin{aligned}
f\left(\frac{1}{2}\right) &= \left(\frac{1}{2}\right)^3 + 2 \left(\frac{1}{2}\right) \\
&= \frac{1}{8} + 1 \\
&= \frac{1}{8} + \frac{8}{8} \\
&= \boxed{\frac{9}{8}}
\end{aligned}$$

*** 22. $h(x) = \frac{x^2 + 4}{5}; \quad h(2), h(-2), h(a), h(-x), h(a - 2), h(\sqrt{x})$

$$\begin{aligned}
h(2) &= \frac{(2)^2 + 4}{5} \\
&= \frac{4 + 4}{5} \\
&= \boxed{\frac{8}{5}}
\end{aligned}$$

$$\begin{aligned}
h(-2) &= \frac{(-2)^2 + 4}{5} \\
&= \frac{4 + 4}{5} \\
&= \boxed{\frac{8}{5}}
\end{aligned}$$

$$\begin{aligned}
h(a) &= \frac{(a)^2 + 4}{5} \\
&= \boxed{\frac{a^2 + 4}{5}}
\end{aligned}$$

$$\begin{aligned}
h(-x) &= \frac{(-x)^2 + 4}{5} \\
&= \boxed{\frac{x^2 + 4}{5}}
\end{aligned}$$

$$\begin{aligned}
h(a - 2) &= \frac{(a - 2)^2 + 4}{5} \\
&= \frac{(a^2 - 4a + 4) + 4}{5} \\
&= \boxed{\frac{a^2 - 4a + 8}{5}}
\end{aligned}$$

$$\begin{aligned}
h(\sqrt{x}) &= \frac{(\sqrt{x})^2 + 4}{5} \\
&= \boxed{\frac{x + 4}{5}}
\end{aligned}$$

*** 23. $f(x) = x^2 + 2x; \quad f(0), f(3), f(-3), f(a), f(-x), f\left(\frac{1}{a}\right)$

$$\begin{aligned}
f(0) &= (0)^2 + 2(0) \\
&= 0 + 0 \\
&= \boxed{0}
\end{aligned}$$

$$\begin{aligned}
f(3) &= (3)^2 + 2(3) \\
&= 9 + 6 \\
&= \boxed{15}
\end{aligned}$$

$$\begin{aligned}
f(-3) &= (-3)^2 + 2(-3) \\
&= 9 - 6 \\
&= \boxed{3}
\end{aligned}$$

$$\begin{aligned}
f(a) &= (a)^2 + 2(a) \\
&= \boxed{a^2 + 2a}
\end{aligned}$$

$$\begin{aligned}
f(-x) &= (-x)^2 + 2(-x) \\
&= \boxed{x^2 - 2x}
\end{aligned}$$

$$\begin{aligned}
f\left(\frac{1}{a}\right) &= \left(\frac{1}{a}\right)^2 + 2 \left(\frac{1}{a}\right) \\
&= \boxed{\frac{1}{a^2} + \frac{2}{a}}
\end{aligned}$$

*** 24. $h(t) &= t + \frac{1}{t}; \quad h(-1), h(2), h\left(\frac{1}{2}\right), h(x - 1), h\left(\frac{1}{x}\right)$

$$\begin{aligned}
h(-1) &= (-1) + \frac{1}{(-1)} \\
&= -1 - 1 \\
&= \boxed{-2}
\end{aligned}$$

$$\begin{aligned}
h(2) &= (2) + \frac{1}{(2)} \\
&= \frac{4}{2} + \frac{1}{2} \\
&= \boxed{\frac{5}{2}}
\end{aligned}$$

$$\begin{aligned}
h\left(\frac{1}{2}\right) &= \left(\frac{1}{2}\right) + \frac{1}{\left(\frac{1}{2}\right)} \\
&= \frac{1}{2} + 2 \\
&= \frac{1}{2} + \frac{4}{2} \\
&= \boxed{\frac{5}{2}}
\end{aligned}$$

$$\begin{aligned}
h(x - 1) &= (x - 1) + \frac{1}{(x - 1)} \\
&= \frac{(x - 1)^2}{(x - 1)} + \frac{1}{(x - 1)} \\
&= \frac{(x - 1)^2 + 1}{x - 1} \\
&= \frac{(x^2 - 2x + 1) + 1}{x - 1} \\
&= \boxed{\frac{x^2 - 2x + 2}{x - 1}} \\
\end{aligned}$$

$$\begin{aligned}
h\left(\frac{1}{x}\right) &= \left(\frac{1}{x}\right) + \frac{1}{\left(\frac{1}{x}\right)} \\
&= \frac{1}{x} + x \\
&= \frac{1}{x} + \frac{x^2}{x} \\
&= \boxed{\frac{1 + x^2}{x}}
\end{aligned}$$

*** 25. $g(x) = \frac{1 - x}{1 + x}; \quad g(2), g(-1), g\left(\frac{1}{2}\right), g(a), g(a - 1), g\left(x^2 - 1\right)$

$$\begin{aligned}
g(2) &= \frac{1 - (2)}{1 + (2)} \\
&= \boxed{-\frac{1}{3}} \\
\end{aligned}$$

$$\begin{aligned}
g(-1) &= \frac{1 - (-1)}{1 + (-1)} \\
&= \frac{2}{0} \\
&= \boxed{\text{undefined}}
\end{aligned}$$

$$\begin{aligned}
g\left(\frac{1}{2}\right) &= \frac{1 - \left(\frac{1}{2}\right)}{1 + \left(\frac{1}{2}\right)} \\
&= \frac{\frac{2}{2} - \frac{1}{2}}{\frac{2}{2} + \frac{1}{2}} \\
&= \frac{\frac{1}{2}}{\frac{3}{2}} \\
&= \frac{1}{2} \cdot \frac{2}{3} \\
&= \frac{2}{6} \\
&= \boxed{\frac{1}{3}}
\end{aligned}$$

$$\begin{aligned}
g(a) &= \frac{1 - (a)}{1 + (a)} \\
&= \boxed{\frac{1 - a}{1 + a}}
\end{aligned}$$

$$\begin{aligned}
g(a - 1) &= \frac{1 - (a - 1)}{1 + (a - 1)} \\
&= \frac{1 - a + 1}{1 + a - 1} \\
&= \boxed{\frac{2 - a}{a}}
\end{aligned}$$

$$\begin{aligned}
g(x^2 - 1) &= \frac{1 - (x^2 - 1)}{1 + (x^2 - 1)} \\
&= \frac{1 - x^2 + 1}{1 + x^2 - 1} \\
&= \boxed{\frac{2 - x^2}{x^2}}
\end{aligned}$$

*** 28. $k(x) = 2x^3 - 3x^2; \quad k(0), k(3), k(-3), k\left(\frac{1}{2}\right), k\left(\frac{a}{2}\right), k(-x), k\left(x^3\right)$

$$\begin{aligned}
k(0) &= 2(0)^3 - 3(0)^2 \\
&= 0 - 0 \\
&= \boxed{0}
\end{aligned}$$

$$\begin{aligned}
k(3) &= 2(3)^3 - 3(3)^2 \\
&= 2(27) - 3(9) \\
&= 54 - 27 \\
&= \boxed{27}
\end{aligned}$$

$$\begin{aligned}
k(-3) &= 2(-3)^3 - 3(3)^2 \\
&= 2(-27) - 3(9) \\
&= -54 - 27 \\
&= \boxed{-81}
\end{aligned}$$

$$\begin{aligned}
k\left(\frac{1}{2}\right) &= 2 \left(\frac{1}{2}\right)^3 - 3 \left(\frac{1}{2}\right)^2 \\
&= 2 \left(\frac{1}{8}\right) - 3 \left(\frac{1}{4}\right) \\
&= \frac{1}{4} - \frac{3}{4} \\
&= -\frac{2}{4} \\
&= \boxed{-\frac{1}{2}}
\end{aligned}$$

$$\begin{aligned}
k\left(\frac{a}{2}\right) &= 2 \left(\frac{a}{2}\right)^3 - 3 \left(\frac{a}{2}\right)^2 \\
&= 2 \left(\frac{a^3}{2^3}\right) - 3 \left(\frac{a^2}{2^2}\right) \\
&= 2 \left(\frac{a^3}{8}\right) - 3 \left(\frac{a^2}{4}\right) \\
&= \frac{a^3}{4} - \frac{3a^2}{4} \\\
&= \boxed{\frac{a^3 - 3a^2}{4}}
\end{aligned}$$

$$\begin{aligned}
k(-x) &= 2(-x)^3 - 3(-x)^2 \\
&= \boxed{-2x^3 - 3x^2}
\end{aligned}$$

$$\begin{aligned}
k\left(x^3\right) &= 2 \left(x^3\right)^3 - 3 \left(x^3\right)^2 \\
&= \boxed{2x^9 - 3x^6}
\end{aligned}$$

*** 31.

$$f(x) &= \begin{cases}
x^2 & \text{if } x < 0 \\
x + 1 & \text{if } x \geq 0
\end{cases}, \quad f(-2), f(0), f(1), f(2)$$

$$\begin{aligned}
f(-2) &= (-2)^2 \\
&= \boxed{4}
\end{aligned}$$

$$\begin{aligned}
f(0) &= (0) + 1 \\
&= \boxed{1}
\end{aligned}$$

$$\begin{aligned}
f(1) &= (1) + 1 \\
&= \boxed{2}
\end{aligned}$$

$$\begin{aligned}
f(2) &= (2) + 1 \\
&= \boxed{3}
\end{aligned}$$

*** 34.

$$f(x) = \begin{cases}
3x & \text{if } x < 0 \\
x + 1 & \text{if } 0 \leq x \leq 2 \\
(x - 2)^2 & \text{if } x > 2
\end{cases}, \quad f(-5), f(0), f(1), f(2), f(5)$$

$$\begin{aligned}
f(-5) &= 3(-5) \\
&= \boxed{-15}
\end{aligned}$$

$$\begin{aligned}
f(0) &= (0) + 1 \\
&= \boxed{1}
\end{aligned}$$

$$\begin{aligned}
f(1) &= (1) + 1 \\
&= \boxed{2}
\end{aligned}$$

$$\begin{aligned}
f(2) &= (2) + 1 \\
&= \boxed{3}
\end{aligned}$$

$$\begin{aligned}
f(5) &= ((5) - 2)^2 \\
&= (3)^2 \\
&= \boxed{9}
\end{aligned}$$

*** 35. $f(x) = x^2 + 1; \quad f(x + 2), f(x) + f(2)$

$$\begin{aligned}
f(x + 2) &= (x + 2)^2 + 1 \\
&= (x^2 + 4x + 4) + 1 \\
&= \boxed{x^2 + 4x + 5}
\end{aligned}$$

$$\begin{aligned}
f(x) + f(2) &= \left((x)^2 + 1\right) + \left((2)^2 + 1\right) \\
&= (x^2 + 1) + (4 + 1) \\
&= x^2 + 1 + 5 \\
&= \boxed{x^2 + 6}
\end{aligned}$$

*** 36. $f(x) = 3x - 1; \quad f(2x), 2f(x)$

$$\begin{aligned}
f(2x) &= 3(2x) - 1 \\
&= \boxed{6x - 1}
\end{aligned}$$

$$\begin{aligned}
2f(x) &= 2(3x - 1) \\
&= \boxed{6x - 2}
\end{aligned}$$

*** 37. $f(x) = x + 4; \quad f\left(x^2\right), (f(x))^2$

$$\begin{aligned}
f\left(x^2\right) &= \left(x^2\right) + 4 \\
&= \boxed{x^2 + 4}
\end{aligned}$$

$$\begin{aligned}
(f(x))^2 &= (x + 4)^2 \\
&= (x + 4) (x + 4) \\
&= \boxed{x^2 + 8x + 16}
\end{aligned}$$

*** 43. $f(x) = 5 - 2x; \quad f(a), f(a + h), \frac{f(a + h) - f(a)}{h}, \quad h \neq 0$

$$\begin{aligned}
f(a) &= 5 - 2(a) \\
&= \boxed{5 - 2a}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= 5 - 2(a + h) \\
&= \boxed{5 - 2a - 2h}
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{(5 - 2(a + h)) - (5 - 2(a))}{h} \\
&= \frac{(5 - 2a - 2h) - (5 - 2a)}{h} \\
&= \frac{5 - 2a - 2h - 5 + 2a}{h} \\
&= \frac{-2h}{h} \\
&= \boxed{-2}
\end{aligned}$$

*** 44. $f(x) = 3x^2 + 2; \quad f(a), f(a + h), \frac{f(a + h) - f(a)}{h}, \quad h \neq 0$

$$\begin{aligned}
f(a) &= 3(a)^2 + 2 \\
&= \boxed{3a^2 + 2}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= 3(a + h)^2 + 2 \\
&= 3\left[(a + h)(a + h)\right] + 2 \\
&= 3(a^2 + 2ah + h^2) + 2 \\
&= \boxed{3a^2 + 6ah + 3h^2 + 2}
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{\left(3(a + h)^2 + 2\right) - \left(3(a)^2 + 2\right)}{h} \\
&= \frac{\left(3a^2 + 6ah + 3h^2 + 2\right) - \left(3a^2 + 2\right)}{h} \\
&= \frac{3a^2 + 6ah + 3h^2 + 2 - 3a^2 - 2}{h} \\
&= \frac{6ah + 3h^2}{h} \\
&= \frac{h\left(6a + 3h\right)}{h} \\
&= \boxed{6a + 3h}
\end{aligned}$$

*** 46. $f(x) = \frac{1}{x - 1}; \quad f(a), f(a + h), \frac{f(a + h) - f(a)}{h}, \quad h \neq 0$

$$\begin{aligned}
f(a) &= \frac{1}{(a) - 1} \\
&= \boxed{\frac{1}{a - 1}}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= \frac{1}{(a + h) - 1} \\
&= \boxed{\frac{1}{a + h - 1}}
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{\left(\frac{1}{a + h + 1}\right) - \left(\frac{1}{a - 1}\right)}{h} \\
&= \frac{\frac{a - 1}{(a + h + 1)(a - 1)} - \frac{a + h + 1}{(a + h + 1)(a - 1)}}{h} \\
&= \frac{a - 1 - (a + h + 1)}{h(a + h + 1)(a - 1)} \\
&= \frac{a - 1 - a - h - 1}{h(a + h + 1)(a - 1)} \\
&= \frac{-2 - h}{h(a + h + 1)(a - 1)} \\
&= \boxed{\frac{-2 - h}{h(a + h + 1)(a - 1)}}
\end{aligned}$$

*** 47. $f(x) = \frac{x}{x + 1}; \quad f(a), f(a + h), \frac{f(a + h) - f(a)}{h}, \quad h \neq 0$

$$\begin{aligned}
f(a) &= \frac{(a)}{(a) + 1} \\
&= \boxed{\frac{a}{a + 1}}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= \frac{(a + h)}{(a + h) + 1} \\
&= \boxed{\frac{a + h}{a + h + 1}}
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{\left(\frac{a + h}{a + h + 1}\right) - \left(\frac{a}{a + 1}\right)}{h} \\
&= \frac{\left(\frac{(a + h)(a + 1)}{(a + h + 1)(a + 1)}\right) - \left(\frac{a(a + h + 1)}{(a + h + 1)(a + 1)}\right)}{h} \\
&= \frac{(a^2 + a + ah + h) - (a^2 + ah + 1)}{h(a + h + 1)(a + 1)} \\
&= \frac{a^2 + a + ah + h - a^2 - ah - a}{h(a + h + 1)(a + 1)} \\
&= \frac{h}{h(a + h + 1)(a + 1)} \\
&= \frac{1}{(a + h + 1)(a + 1)}
\end{aligned}$$

*** 49. $f(x) = 3 - 5x + 4x^2; \quad f(a), f(a + h), \frac{f(a + h) - f(a)}{h}, \quad h \neq 0$

$$\begin{aligned}
f(a) &= 3 - 5(a) + 4(a)^2 \\
&= \boxed{3 - 5a + 4a^2}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= 3 - 5(a + h) + 4(a + h)^2 \\
&= 3 - 5a - 5h + 4(a + h)(a + h) \\
&= 3 - 5a - 5h + 4(a^2 + 2ah + h^2) \\
&= \boxed{3 - 5a - 5h + 4a^2 + 8ah + 4h^2} 
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{\left(3 - 5a - 5h + 4a^2 + 8ah + 4h^2\right) - \left(3 - 5a + 4a^2\right)}{h} \\
&= \frac{3 - 5a - 5h + 4a^2 + 8ah + 4h^2 - 3 + 5a - 4a^2}{h} \\
&= \frac{-5h + 8ah + 4h^2}{h} \\
&= \frac{h(-5 + 8a + 4h}{h} \\
&= \boxed{-5 + 8a + 4h}
\end{aligned}$$

*** 55. $f(x) = \frac{1}{x - 3}$

$$\begin{align}
\boxed{\left\{x \mid x \neq 3\right\}}
\end{align}$$

*** 57. $f(x) = \frac{x + 2}{x^2 - 1}$

$$\begin{aligned}
f(x) &= \frac{x + 2}{(x + 1)(x - 1)} \\
& \boxed{\left\{x \mid x \neq \pm 1\right\}}
\end{aligned}$$

*** 58. $f(x) = \frac{x^4}{x^2 + x - 6}$

$$\begin{aligned}
f(x) &= \frac{x^4}{(x + 3)(x - 2)} \\
& \boxed{\left\{x \mid x \neq -3, 2\right\}}
\end{aligned}$$

*** 59. $f(t) = \sqrt{t + 1}$

$$\begin{aligned}
t + 1 &\geq 0 \\
t &\geq -1 \\
& \boxed{[-1, \infty)}
\end{aligned}$$

*** 60. $g(t) = \sqrt{t^2 + 9}$

$$\begin{aligned}
t^2 + 9 &\geq 0 \\
t^2 &\geq -9 \\
t &\geq \pm\sqrt{-9} \\
& \boxed{(-\infty, \infty)}
\end{aligned}$$

*** 61. $f(t) = \sqrt[3]{t - 1}$

$$\begin{aligned}
& \boxed{(-\infty, \infty)}
\end{aligned}$$

*** 64. $g(x) = \sqrt{x^2 - 4}$

$$\begin{aligned}
x^2 - 4 &\geq 0 \\
x^2 &\geq 4 \\
x &\geq \pm\sqrt{4} \\
x &\geq \pm 2 \\
& \boxed{(-\infty, -2] \cup [2, \infty)}
\end{aligned}$$

*** 65. $g(x) = \frac{\sqrt{2 + x}}{3 - x}$

$$\begin{aligned}
2 + x &\geq 0 \\
x &\geq -2 \\
x &\neq 3 \\
& \boxed{[-2, 3) \cup (3, \infty)}
\end{aligned}$$

*** 66. $g(x) = \frac{\sqrt{x}}{2x^2 + x - 1}$

$$\begin{aligned}
x &\geq 0 \\
(2x - 1)(x + 1) &\neq 0 \\
x &\neq \frac{1}{2} \\
x &\neq -1 \\
& \boxed{\left[0, \frac{1}{2}\right) \cup \left(\frac{1}{2}, \infty\right)}
\end{aligned}$$

*** 69. $f(x) = \frac{3}{\sqrt{x - 4}}$

$$\begin{aligned}
x - 4 &\geq 0 \\
x &\geq 4 \\
\sqrt{x - 4} &\neq 0 \\
x &\neq 4 \\
& \boxed{(4, \infty)}
\end{aligned}$$

*** 70. $f(x) = \frac{x^2}{\sqrt{6 - x}}$

$$\begin{aligned}
6 - x &\geq 0 \\
-x &\geq -6 \\
x &\leq 6 \\
\sqrt{6 - x} &\neq 0 \\
x &\neq 6 \\
& \boxed{(-\infty, 6)}
\end{aligned}$$

*** 80. *Area of a Sphere* \quad The surface area $S$ of a sphere is a function of its radius $r$ given by 

$$S(r) = 4 \pi r^2$$
*(a)* Find $S(2)$ and $S(3)$. \\
*(b)* What do your answers in part (a) represent? \\

(a)

$$\begin{aligned}
S(2) &= 4 \pi (2)^2 \\
&= 4 \pi (4) \\
&= \boxed{16 \pi}
\end{aligned}$$

$$\begin{aligned}
S(3) &= 4 \pi (3)^2 \\
&= 4 \pi (9) \\
&= \boxed{36 \pi}
\end{aligned}$$

(b) My answers in part (a) represent the surface areas $S$ of the spheres given their radius $r$. In this case, the radii are two and three units of measurement.

*** 85. *Income Tax* \quad In a certain country, income tax $T$ is assessed according to the following function of income $x$:

$$\begin{cases}
0 & \text{if } 0 \leq x \leq 10,000 \\
0.08x & \text{if } 10,000 < x \leq 20,000 \\
1600 + 0.15x & \text{if } 20,000 > x
\end{cases}$$
*(a)* Find $T(5,000)$, $T(12,000)$, and $T(25,000)$. \\
*(b)* What do your answers in part (a) represent? \\

(a)

$$\begin{aligned}
T(5,000) &= \boxed{\$0}
\end{aligned}$$

$$\begin{aligned}
T(12,000) &= 0.08(12,000) \\
&= \boxed{\$960}
\end{aligned}$$

$$\begin{aligned}
T(20,000) &= 1600 + 0.15(25,000) \\
&= 1600 + 3750 \\
&= \boxed{\$5350}
\end{aligned}$$

(b) My answers in part (a) represent the amount of income tax $T$ for the country given its income $x$. In this case, the incomes given are $5,000, $12,000, and $25,000.

*** 87. *Cost of a Hotel Stay* \quad A hotel chain charges $75 each night for the first two nights and $50 for each additional night's stay. The total cost $T$ is a function of the number of nights that a guest stays.

*(a)* Complete the expressions in the following piecewise defined function.

$$T(x) = \begin{cases}
\phantom{\qquad} & \text{if } 0 \leq x \leq 2 \\
\phantom{\qquad} & \text{if } x > 2
\end{cases}$$ \\
*(b)* Find $T(2)$, $T(3)$, and $T(5)$. \\
*(c)* What do your answers in part (b) represent? \\

(a)

$$T(x) = \begin{cases}
75x & \text{if } 0 \leq x \leq 2 \\
50(x - 2) + 150 & \text{if } x > 2
\end{cases}$$

(b)

$$\begin{aligned}
T(2) &= 75(2) \\
&= \boxed{\$150}
\end{aligned}$$

$$\begin{aligned}
T(3) &= 50((3) - 2) + 150 \\
&= 50 + 150 \\
&= \boxed{\$200}
\end{aligned}$$

$$\begin{aligned}
T(5) &= 50((5) - 2) + 150 \\
&= 50(3) + 150 \\
&= 150 + 150 \\
&= \boxed{\$300}
\end{aligned}$$

(c) My answers in part (b) represent the total cost $T$ given the number of nights that a guest stays $x$. In this case, the number of nights that a guest stays are two, three, and five days.

** 2.2 Graphs of Functions
*** 4. Match the function with its graph.

$$\begin{aligned}
\textbf{(a) } f(x) = x^2 &\quad \textbf{(b) } f(x) = x^3 \\
\textbf{(c) } f(x) = \sqrt{x} &\quad \textbf{(d) } f(x) = |x|
\end{aligned}$$

a. $\boxed{\text{IV}}$

b. $\boxed{\text{II}}$

c. $\boxed{\text{I}}$

d. $\boxed{\text{III}}$

*** 35. Sketch a graph of the piecewise defined function.

$$f(x) = \begin{cases}
3 & \text{if } x < 2 \\
x - 1 & \text{if } x \geq 2
\end{cases}$$

#+begin_src julia :eval no-export :results graphics :file ./figures/020235.png :exports results
  using Plots
  x = -8:12
  function f(x)
      if x < 2
          return 3
      else
          return x - 1
      end
  end

  scatter(x, f.(x), legend=false)
  plot!(x[1]:0.1:1.9, (x -> 3).(x[1]:0.1:1.9))
  scatter!([2], [3], mc=:white)
  plot!(2:0.1:x[end], (x -> x - 1).(2:0.1:x[end]))

  savefig("./figures/020235.png")
#+end_src

#+RESULTS:
[[file:./figures/020235.png]]

*** 36.

$$f(x) = \begin{cases}
1 - x & \text{if } x < -2 \\
5 & \text{if } x \geq -2
\end{cases}$$

#+begin_src julia :eval no-export :results graphics :file ./figures/020236.png :exports results
  using Plots
  x = -12:8
  function f(x)
      if x < -2
          return 1 - x
      else
          return 5
      end
  end

  scatter(x, f.(x), legend=false)
  plot!(-12:0.1:-1.9, (x -> 1 - x).(-12:0.1:-1.9))
  scatter!([-2], [3], mc=:white)
  plot!(-2:0.1:8, (x -> 5).(-2:0.1:8))

  savefig("./figures/020236.png")
#+end_src

#+RESULTS:
[[file:./figures/020236.png]]

*** 40. 

$$f(x) = \begin{cases}
-1 & \text{if } x < - 1 \\
x & \text{if } -1 \leq x \leq 1 \\
1 & \text{if } x > 1
\end{cases}$$

#+begin_src julia :eval no-export :results graphics :file ./figures/020240.png :exports results
  using Plots
  x = -10:10
  function f(x)
      if x < -1
          return -1
      elseif x <= 1
          return x
      else
          return 1
      end
  end

  scatter(x, f.(x), legend=false)
  plot!(x[1]:0.1:-0.9, (x -> -1).(x[1]:0.1:-0.9))
  scatter!([-1], [-1], mc=:white)
  plot!(-1:0.1:1, (x -> x).(-1:0.1:1))
  scatter!([1], [1], mc=:white)
  plot!(1.1:0.1:x[end], (x -> 1).(1.1:0.1:x[end]))

  savefig("./figures/020240.png")
#+end_src

#+RESULTS:
[[file:./figures/020240.png]]

*** 41. 

$$f(x) = \begin{cases}
2 & \text{if } x \leq -1 \\
x^2 & \text{if } x > -1
\end{cases}$$

#+begin_src julia :eval no-export :results graphics :file ./figures/020241.png :exports results
  using Plots
  x = -11:9
  function f(x)
      if x <= -1
          return 2
      else
          return x^2
      end
  end

  scatter(x, f.(x), legend=false)
  plot!(x[1]:0.1:-1, (x -> 2).(x[1]:0.1:-1))
  scatter!([-1], [(x -> x^2)(-1)], mc=:white)
  plot!(-0.9:0.1:x[end], (x -> x^2).(-0.9:0.1:x[end]))

  savefig("./figures/020241.png")
#+end_src

#+RESULTS:
[[file:./figures/020241.png]]

*** 45. Sketch a graph of the piecewise defined

$$f(x) = \begin{cases}
4 & \text{if } x < - 2 \\
x^2 & \text{if } -2 \leq x \leq 2 \\
-x + 6 & \text{if } x > 2
\end{cases}$$

#+begin_src julia :eval no-export :results graphics :file ./figures/020245.png :exports results
  using Plots
  x = -10:10
  function f(x)
      if x < -2
          return 4
      elseif x <= 2
          return x^2
      else
          return -x + 6
      end
  end

  scatter(x, f.(x), legend=false)
  plot!(x[1]:0.1:-1.9, (x -> 4).(x[1]:0.1:-1.9))
  scatter!([-2], [4])
  plot!(-2:0.1:2, (x -> x^2).(-2:0.1:2))
  scatter!([2], [(x -> -x + 6)(2)])
  plot!(2:0.1:x[end], (x -> -x + 6).(2:0.1:x[end]))

  savefig("./figures/020245.png")
#+end_src

#+RESULTS:
[[file:./figures/020245.png]]

*** 49. Finding Piecewise Defined Functions \quad a graph of a piecewise function is given. Find a formula for the function in the indicated form.

#+begin_src julia :eval no-export :results graphics :file ./figures/020249.png :exports results
  using Plots
  plot(-4:0.1:-2, (x -> -2).(-4:0.1:-2), legend=false, ylim=(-4,4))
  plot!(-2:0.1:2, (x -> x).(-2:0.1:2))
  plot!(2:0.1:4, (x -> 2).(2:0.1:4))

  savefig("./figures/020249.png")
#+end_src

#+RESULTS:
[[file:./figures/020249.png]]


$$\boxed{f(x) = \begin{cases}
-2 & \text{if } x < -2 \\
x & \text{if } -2 \leq x \leq 2 \\
2 & \text{if } x > 2
\end{cases}}$$

*** 50.

#+begin_src julia :eval no-export :results graphics :file ./figures/020250.png :exports results
  using Plots
  x = -4:4
  function f(x)
      if x <= -1
          return 1
      elseif x <= 2
          return -x + 1
      else
          return -2
      end
  end

  scatter(x, f.(x), legend=false, ylim=(-4,4))
  plot!(-4:0.1:-1, (x -> 1).(-4:0.1:-1))
  scatter!([-1], [2], mc=:white)
  plot!(-1:0.1:2, (x -> -x + 1).(-1:0.1:2))
  scatter!([2], [-2], mc=:white)
  plot!(2:0.1:4, (x -> -2).(2:0.1:4))

  savefig("./figures/020250.png")
#+end_src

#+RESULTS:
[[file:./figures/020250.png]]

$$\boxed{f(x) = \begin{cases}
1 & \text{if } x \leq - 1\\
-x + 1 & \text{if } -1 < x \leq 2 \\
-2 & \text{if } x > 2
\end{cases}}$$

*** 51. *Vertical Line Test* \quad Use the Vertical Line Test to determine whether the curve is a graph of a function of $x$.

**** (a)

Yes

**** (b)

No
 
*** 52.

**** (a)

No

**** (b)

Yes

** 2.3 Getting Information from the Graph of a Function
*** 7. Values of a function. \quad The graph of a function $h$ is given.

[[./figures/020307.png]]

**** (a) Find $h(-2)$, $h(0)$, $h(2)$, and $h(3)$.

#+begin_latex latex
\begin{align*}
&\boxed{h(-2) = 1} \\
&\boxed{h(0) = -1} \\
&\boxed{h(2) = 3} \\
&\boxed{h(3) = 4}
\end{align*}
#+end_latex

**** (b) Find the domain and range of $h$.

#+begin_latex latex
\begin{align*}
\boxed{D_h = [-3, 4]} \\
\boxed{R_h = [-1, 4]}
\end{align*}
#+end_latex

**** (c) Find the values of $x$ for which $h(x) = 3$.

#+begin_latex latex
\begin{align*}
\boxed{x = -3, 2, 4}
\end{align*}
#+end_latex

**** (d) Find the values of $x$ for which $h(x) \leq 3$.

#+begin_latex latex
\begin{align*}
\boxed{x = [-3, 2], 4}
\end{align*}
#+end_latex

**** (e) Find the net change in $h$ between $x = -3$ and $x = 3$.

#+begin_latex latex
\begin{align*}
h(3) - h(-3) &= 4 - 3 \\
&= \boxed{1}
\end{align*}
#+end_latex

*** 8. Values of a function. \quad The graph of a function $g$ is given.

[[./figures/020308.png]]

**** (a) Find $g(-4)$, $g(-2)$, $g(0)$, $g(2)$, and $g(4)$.

#+begin_latex latex
\begin{align*}
&\boxed{g(-4) = 3} \\
&\boxed{g(-2) = 2} \\
&\boxed{g(0) = -2} \\
&\boxed{g(4) = 0}
\end{align*}
#+end_latex

**** (b) Find the domain and range of $g$.

#+begin_latex latex
\begin{align*}
\boxed{D_g = [-4, 4]} \\
\boxed{R_g = [-2, 3]}
\end{align*}
#+end_latex

**** (c) Find the values of $x$ for which $g(x) = 3$.

#+begin_latex latex
\begin{align*}
\boxed{x = -4}
\end{align*}
#+end_latex

**** (d) Estimate the values of $x$ for which $g(x) \leq 0$.

#+begin_latex latex
\begin{align*}
\boxed{x = \left[-1, \frac{5}{3}\right], 4}
\end{align*}
#+end_latex

**** (e) Find the net change in $g$ between $x = -1$ and $x = 2$.

#+begin_latex latex
\begin{align*}
g(2) - g(-1) &= 1 - 0 \\
&= \boxed{1}
\end{align*}
#+end_latex

*** 31-34. Increasing and decreasing the graph of a function $f$ is given. Use the graph to estimate the following. (a) The domain and range of $f$. (b) The intervals on which $f$ is increasing and on which $f$ is decreasing.

[[./figures/02033134.png]]

*** 31. 
**** (a) The domain and range of $f$.

#+begin_latex latex
\begin{align*}
\boxed{D_f = [-1, 4]} \\
\boxed{R_f = [-1, 3]}
\end{align*}
#+end_latex

**** (b) The intervals on which $f$ is increasing and on which $f$ is decreasing.

$f$ is increasing at intervals \boxed{(-1, 1) \cup (2, 4)} and decreasing at intervals \boxed{(1, 2)}.

*** 32.

**** (a) The domain and range of $f$.

#+begin_latex latex
\begin{align*}
\boxed{D_f = [-2, 3]} \\
\boxed{R_f = [-2, 3]}
\end{align*}
#+end_latex

**** (b) The intervals on which $f$ increasing and on which $f$ is decreasing.

$f$ is increasing at intervals \boxed{(0, 1)} and decreasing at intervals \boxed{(-2, 0) \cup (1, 3)}.
*** 33.

**** (a) The domain and range of $f$.

#+begin_latex latex
\begin{align*}
\boxed{D_f = [-3, 3]} \\
\boxed{R_f = [-2, 2]}
\end{align*}
#+end_latex

**** (b) The intervals on which $f$ increasing and on which $f$ is decreasing.

$f$ is increasing at intervals \boxed{(-2, -1) \cup (1, 2)} and decreasing at intervals \boxed{(-3, -2) \cup (-1, 1) \cup (2, 3)}.
*** 34.

**** (a) The domain and range of $f$.

#+begin_latex latex
\begin{align*}
\boxed{D_f = [-2, 2]} \\
\boxed{R_f = [-2, 2]}
\end{align*}
#+end_latex

**** (b) The intervals on which $f$ increasing and on which $f$ is decreasing.

$f$ is increasing at intervals \boxed{(-1, 1)} and decreasing at intervals \boxed{(-2, -1) \cup (1, 2)}.
*** 43-46. Local Maximum and Minimum Values \quad The graph of a function $f$ is given. Use the graph to estimate the following. (a) All the local maximum and minimum values of the function and the value of $x$ at which each occurs. (b) The intervals on which the function is increasing and on which the function is decreasing.

[[./figures/02034346.png]]

*** 43.

**** (a) All the local maximum and minimum values of the function and the value of $x$ at which each occurs.

| Max   | Min     |
|-------+---------|
| (0,2) | (-2,-1) |
|       | (2,0)   |

**** (b) The intervals on which the function is increasing and on which the function is decreasing.

| Increasing | Decreasing   |
|------------+--------------|
| (-1,0)     | (-\infty,-1) |
| (2,\infty) | (0,2)        |

*** 44.

**** (a) All the local maximum and minimum values of the function and the value of $x$ at which each occurs.

| Max    | Min    |
|--------+--------|
| (-2,2) | (0,-1) |
| (2,1)  |        |

**** (b) The intervals on which the function is increasing and on which the function is decreasing.

| Increasing   | Decreasing |
|--------------+------------|
| (-\infty,-2) | (-2,0)     |
| (0,2)        | (2,\infty) |
|              |            |

*** 45.

**** (a) All the local maximum and minimum values of the function and the value of $x$ at which each occurs.

| Max   | Min     |
|-------+---------|
| (0,0) | (-2,-2) |
| (3,1) | (1,-1)  |

**** (b) The intervals on which the function is increasing and on which the function is decreasing.

| Increasing | Decreasing   |
|------------+--------------|
| (-2,0)     | (-\infty,-2) |
| (1,3)      | (0,1)        |
|            | (3,\infty)   |

*** 46.

**** (a) All the local maximum and minimum values of the function and the value of $x$ at which each occurs.

| Max    | Min    |
|--------+--------|
| (-2,3) | (-1,0) |
| (1,2)  | (2,-1) |

**** (b) The intervals on which the function is increasing and on which the function is decreasing.

| Increasing    | Decreasing |
|---------------+------------|
| (-\infty, -2) | (-2,-1)    |
| (-1,1)        | (1,2)      |
| (2, \infty)  |            |

** 3.7 Polynomial and Rational Inequalities
*** 1.
To solve a polynomial inequality, we factor the polynomial into irreducible factors and find all the real _*zeroes*_ of the polynomial.
Then we find the intervals determined by the real _*zeroes*_ and use test points in each interval to find the sign of the polynomial on that interval.
Let $$P(x) = x(x + 2)(x - 1)$$
Fill in the diagram below to find the intervals on which $P(x) \geq 0$.
| $x$               | - | - | + | + |
| $x + 2$           |   |   |   |   |
| $x - 1$           |   |   |   |   |
| $x(x + 2)(x - 1)$ |   |   |   |   |
*** 2.
To solve a rational inequality, we factor the numerator and the denominator into irreducible factors.
The cut points are the real *zeroes* of the numerator and the real *zeroes* of the denominator.
Then we find the intervals determined by the *rational inequality*, and we use test points to find the sign of the rational function on each interval.
*** 3. Solve the inequality.
$$(x - 3)(x + 5)(2x + 5) < 0$$

| $x = -6$           | - |
| $x = -5$           | 0 |
| $x = -4$           | + |
| $x = -\frac{5}{2}$ | 0 |
| $x = 0$            | - |
| $x = 3$            | 0 |
| $x = 4$            | + |

$$(-\infty, -5) \cup \left(-\frac{5}{2}, 3\right)$$
*** 5. 
$$(x + 5)^2 (x + 3) (x - 1) > 0$$

| $x = -6$ | + |
| $x = -5$ | 0 |
| $x = -4$ | + |
| $x = -3$ | 0 |
| $x = 0$  | - |
| $x = 1$  | 0 |
| $x = 2$  | + |

$$(-\infty, -5) \cup (-5, -3) \cup (1, \infty)$$
*** 17.
$$\frac{x - 1}{x - 10} < 0$$

| $x = 0$  | +         |
| $x = 1$  | 0         |
| $x = 2$  | -         |
| $x = 10$ | \emptyset |
| $x = 11$ | +         |

$$(1, 10)$$
*** 20.
$$\frac{4x^2 - 25}{x^2 - 9} \leq 0$$

$$\begin{align}
\frac{(2x - 5)(2x + 5)}{(x - 3)(x + 3)} &\leq 0
\end{align}$$

| $x = -4$            | +         |
| $x = -3$            | \emptyset |
| $x = -\frac{11}{4}$ | -         |
| $x = -\frac{5}{2}$  | 0         |
| $x = 0$             | +         |
| $x = \frac{5}{2}$   | 0         |
| $x = \frac{11}{4}$  | -         |
| $x = 3$             | \emptyset |
| $x = 4$             | +         |

$$\left(-3, -\frac{5}{2}\right] \cup \left[\frac{5}{2}, 3\right)$$
*** 23.
$$\frac{x^2 + 2x - 3}{3x^2 - 7x - 6} > 0$$

$$\begin{align}
\frac{(x - 1)(x + 3)}{(3x + 2) (x - 3)} &> 0
\end{align}$$

| $x = -4$           | +         |
| $x = -3$           | 0         |
| $x = -1$           | -         |
| $x = -\frac{2}{3}$ | \emptyset |
| $x = 0$            | +         |
| $x = 1$            | 0         |
| $x = 2$            | -         |
| $x = 3$            | \emptyset |
| $x = 4$            | +         |

$$(-\infty, -3) \cup \left(-\frac{2}{3}, 1\right) \cup (3, \infty)$$
*** 31.
$$\frac{(x - 1)^2}{(x + 1)(x + 2)} > 0$$

| $x = -3$           | +         |
| $x = -2$           | \emptyset |
| $x = -\frac{3}{2}$ | -        |
| $x = -1$           | \emptyset |
| $x = 0$            | +         |
| $x = 1$            | 0         |
| $x = 2$            | +         |

$$(-\infty, -2) \cup (-1, 1) \cup (1, \infty)$$
** 2.6 Transformations of Functions
*** 23. Use the graph of $y = x^2$ in Figure 4 to graph the following.
**** (a) $g(x) = x^2 + 1$
#+begin_src julia :eval no-export :results graphics :file ./figures/020623a.png :exports results
  using Plots
  x = -4:0.1:4
  f(x) = x^2
  g(x) = x^2 + 1

  plot(x, f.(x), linestyle=:dash)
  plot!(x, g.(x), ylim=(-1,4))

  savefig("./figures/020623a.png")
#+end_src

#+RESULTS:
[[file:./figures/020623a.png]]

**** (b) $g(x) = (x - 1)^2$
#+begin_src julia :eval no-export :results graphics :file ./figures/020306b.png :exports results
  using Plots

  x = -4:0.1:4
  f(x) = x^2
  g(x) = (x - 1)^2

  plot(x, f.(x), linestyle=:dash)
  plot!(x, g.(x), ylim=(-1,4))

  savefig("./figures/020306b.png")
#+end_src

#+RESULTS:
[[file:./figures/020306b.png]]

**** (c) $g(x) = -x^2$
#+begin_src julia :eval no-export :results graphics :file ./figures/020306c.png :exports results
  using Plots

  x = -4:0.1:4
  f(x) = x^2
  g(x) = -x^2

  plot(x, f.(x), linestyle=:dash)
  plot!(x, g.(x), ylim=(-4,4))

  savefig("./figures/020306c.png")
#+end_src

#+RESULTS:
[[file:./figures/020306c.png]]

**** (d) $g(x) = (x - 1)^2 + 3$
#+begin_src julia :eval no-export :results graphics :file ./figures/020306d.png :exports results

  using Plots

  x = -4:0.1:4
  f(x) = x^2
  g(x) = (x - 1)^2 + 3

  plot(x, f.(x), linestyle=:dash)
  plot!(x, g.(x), ylim=(-1,7))

  savefig("./figures/020306d.png")
#+end_src

#+RESULTS:
[[file:./figures/020306d.png]]

*** 24. Use the graph of $y = \sqrt{x}$ in Figure 5 to graph the following.
*** 25. Match the graph with the function. (See the graph of $y = |x|$ on page 96.) $y = |x + 1|$
*** 26. $y = |x - 1|$
*** 27. $y = |x| - 1$
*** 28. $y = -|x|$
*** 29.
*** 31.
*** 32.
*** 34.
*** 36.
*** 38.
*** 45.
*** 46.
*** 48.
*** 49.
*** 50.
*** 54.
*** 55.
*** 57.
*** 58.
*** 63.
*** 64.
*** 65.
*** 68.
*** 70.
*** 85.
*** 86.
*** 87.
*** 88.
*** 90.
*** 91.
*** 93.
*** 94.
*** 96.
** 3.6 Rational Functions
** 2.7 Combining Functions
** 2.8 One-to-One Functions and Their Inverses
* Chapter 4: Exponential and Logarithmic Functions
** 4.1
** 4.2
** 4.3
*** 7. Logarithmic and Exponential Forms \quad Complete the table by finding the appropriate logarithmic or exponential form of the equation, as in Example 1.
| Logarithmic form            | Exponential form        |
|-----------------------------+-------------------------|
| $\log_8{8} = 1$             | $8^1 = 8$               |
| $\log_8{64} = 2$            | $8^2 = 64$              |
| $\log_8{4} = 2/3$           | $8^{2/3} = 4$           |
| $\log_8{512} = 3$           | $8^3 = 512$             |
| $\log_8{\frac{1}{8}} = -1$  | $8^{-1} = \frac{1}{8}$  |
| $\log_8{\frac{1}{64}} = -2$ | $8^{-2} = \frac{1}{64}$ |
*** 8. Hello
*** 9.
*** 10.
*** 12.
*** 14.
*** 15.
*** 17.
*** 18.
*** 19.
*** 22.
*** 23.
*** 25.
*** 27.
*** 28.
*** 29.
*** 32.
*** 33.
*** 34.
*** 37.
*** 38.
*** 39.
*** 41.
*** 42.
*** 43.
*** 57.
*** 58.
*** 59.
*** 60.
*** 64.
*** 65.
*** 70.
*** 73.
*** 75.
*** 76.
** 4.4
** 4.5
** 4.6
* Chapter 6: Trigonometric Functions: Right Triangle Approach
** 6.1
** 6.2
** 6.3
** 6.4
* Chapter 5: Trigonometric Functions: Unit Circle Approach
** 5.3
** 5.5
* Chapter 7: Analytic Trigonometry
** 7.1
** 7.2
** 7.3
** 7.4
** 7.5
* Chapter 8: Polar Coordinates
** 8.1
* Myths about Learning Math

[[https://www.youtube.com/watch?v=X1rdZPYP588][Link to video]]

** Myths
- I'm just not a "math person"
- If I don't know how to do something right away, I never will
- Math is all about memorizing a bunch of formulas and rules
- I'll never use this
** Math is Sequential
- Builds on what you've previously learned
- Grades in previous math course
- Attendance
- Amount of time taken off between math courses
- Math placement test scores
** Math is NOT a Spectator Sport!
- Just like athletes and musicians do, you have to practice math in order to be successful come "game time."
- Do math at least every other day.
  - Homework
  - Exam reviews
  - Reviewing notes
  - Reading the book
- Create an environment that is similar to the one you'll have to take a test in.
- Learn how to speak math as a language:
  - i.e. "+" stands for addition, etc.
** College Math Classes: What to Expect
- Fast paced!
- Responsibility is on YOU
- Expect to spend 2-3 hours outside of class for every hour you're in class
- In many classes, attendance is optional
- You must have a C in order to move on to the next class
- Do NOT expect your math teacher to let you do extra credit or retake exams to get a higher grade
** General Tips
- Keep a positive attitude!
- Ace that first exam!
- Go to class on time and prepared
- Ask questions when you have them and get help right away!
- YOU can do this!
** Quiz
*** Question 1
Some people are just math people, while others aren't.
**** Answer:
- [ ] True
- [X] False
*** Question 2
As long as you can memorize formulas and rules, you will be good at math
**** Answer:
- [ ] True
- [X] False
*** Question 3
Math is all about...
**** Answer:
- [ ] Formulas and rules
- [X] Recognizing patterns and using logic
- [ ] Getting the right answer
*** Question 4
Math is sequential:
Math ______ on what you've previously learned.
**** Answer:
builds
*** Question 5
If your math placement score is on the low end of the spectrum, you should consider taking the previous math course to build your skills.
**** Answer:
- [X] True
- [ ] False
*** Question 6
How often does the video suggest you practice math?
**** Answer:
- [ ] At least once a day
- [X] At least every other day
- [ ] At least twice a week
- [ ] At least once a week
*** Question 7
How I do my homework doesn't matter.
What's important is that I got it done; even if I did it all the night before it was due.
**** Answer:
- [ ] True
- [X] False
*** Question 8
Doing your homework in a test-like environment includes:

[Select all that apply]
**** Answer:
- [X] No phone
- [X] No Facebook
- [X] No solution manual open
- [ ] No water
*** Question 9
Which of the following is a good way to learn what all the different symbols and formulas mean in math?
**** Answer:
- [X] Keep a sheet of new ones you learn and ask yourself questions about them
- [ ] Hope they are given to you on the exam
*** Question 10
College courses have less class time than in high school but, cover half the material.
**** Answer:
- [ ] True
- [X] False
*** Question 11
Attitude has very little to do with learning math.
**** Answer:
- [ ] True
- [X] False
*** Question 12
The last thing you want to do in a math class is _____ _____.
**** Answer:
fall behind
* Review
** 8.1
*** 31
$$\begin{align}
x &= r \cos{\theta} \\
&= \sqrt{2} \cos{\left(-\frac{\pi}{4}\right)} \\
&= \sqrt{2} \cdot \frac{\sqrt{2}}{2} \\
&= 1 \\
y &= r \sin{\theta} \\
&= \sqrt{2} \sin{\left(-\frac{\pi}{4}\right)} \\
&= \sqrt{2} \cdot \frac{(-\sqrt{2})}{2} \\
&= -1 \\
\left(\sqrt{2}, -\frac{\pi}{4}\right) &\Rightarrow (1, -1)
\end{align}$$
*** 33
$$\begin{align}
x &= r \cos{\theta} \\
&= 5 \cos{(5 \pi)} \\
&= 5 (-1) \\
&= -5 \\
y &= r \sin{\theta} \\
&= 5 \sin{(5 \pi)} \\
&= 0 \\
(5, 5 \pi) &\Rightarrow (-5, 0)
\end{align}$$
*** 37
$$\begin{align}
r &= \sqrt{x^2 + y^2} \\
&= \sqrt{(-1)^2 + (1)^2} \\
&= \sqrt{1 + 1} \\
&= \sqrt{2} \\
\theta &= \tan^{-1}{\left(\frac{y}{x}\right)} \\
&= \tan^{-1}{\left(\frac{1}{-1}\right)} \\
&= \tan^{-1}{(-1)} \\
&= \frac{3 \pi}{4} \\
(-1, 1) &\Rightarrow \left(\sqrt{2}, \frac{3 \pi}{4}\right)
\end{align}$$
*** 38
$$\begin{align}
r &= \sqrt{x^2 + y^2} \\
&= \sqrt{(3 \sqrt{3})^2 + (-3)^2} \\
&= \sqrt{9 \cdot 3 + 9} \\
&= \sqrt{27 + 9} \\
&= \sqrt{36} \\
&= 6 \\
\theta &= \tan^{-1}{\left(\frac{y}{x}\right)} \\
&= \tan^{-1}{\left(\frac{-3}{3 \sqrt{3}}\right)} \\
&= \tan^{-1}{\left(-\frac{1}{\sqrt{3}}\right)} \\
&= -\frac{\pi}{3} + \pi \\
&= \frac{2 \pi}{3} \\
(3 \sqrt{3}, -3) &\Rightarrow \left(6, \frac{2 \pi}{3}\right)
\end{align}$$
