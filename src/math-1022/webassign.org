#+title: MATH 1022 WebAssign Homework Solutions
#+author: Eric Nguyen
#+options: toc:1 num:2
#+latex_header: \usepackage{tikz}
#+latex_header: \usetikzlibrary{arrows}

* Algebra Review Assignment
** 1. Perform the indicated operations.

*** a. 

$$\begin{align*}
\frac{3}{4} - \frac{4}{7} &= \frac{3 \cdot 7}{4 \cdot 7} - \frac{4 \cdot 4}{7 \cdot 4} \\
&= \frac{21}{28} - \frac{16}{28} \\
&= \frac{5}{28}
\end{align*}$$

*** b.

$$\begin{align*}
2 + \frac{7}{8} - \frac{5}{6} &= \frac{2 \cdot 24}{1 \cdot 24} + \frac{7 \cdot 3}{8 \cdot 3} - \frac{5 \cdot 4}{6 \cdot 4} \\
&= \frac{48}{24} + \frac{21}{24} - \frac{20}{24} \\
&= \frac{49}{24}
\end{align*}$$

** 2. Perform the indicated operations.

*** a.

$$\begin{align*}
\frac{2}{\frac{2}{5}} - \frac{\frac{2}{5}}{2} &= 2 \div \frac{2}{5} - \frac{2}{5} \div 2 \\
&= 2 \times \frac{5}{2} - \frac{2}{5} \times \frac{1}{2} \\
&= 5 - \frac{1}{5} \\
&= \frac{5 \cdot 5}{1 \cdot 5} - \frac{1}{5} \\
&= \frac{25}{5} - \frac{1}{5} \\
&= \frac{24}{5}
\end{align*}$$

*** b.

$$\begin{align*}
\frac{\frac{2}{5} + \frac{1}{2}}{\frac{1}{10} + \frac{3}{15}} &= \frac{\frac{2 \cdot 2}{5 \cdot 2} + \frac{1 \cdot 5}{2 \cdot 5}}{\frac{1 \cdot 3}{10 \cdot 3} + \frac{3 \cdot 2}{15 \cdot 2}} \\
&= \frac{\frac{4}{10} + \frac{5}{10}}{\frac{3}{30} + \frac{6}{30}} \\
&= \frac{\frac{9}{10}}{\frac{9}{30}} \\
&= \frac{9}{10} \div \frac{9}{30} \\
&= \frac{9}{10} \times \frac{30}{9} \\
&= 3
\end{align*}$$

** 3. Express the inequality in interval notation.

$$x \leq 4$$

$$(-\infty, 4]$$

** 18.

*** a.

$$\begin{align*}
\left(\frac{a^{1/12}b^{-4}}{x^{-1}y}\right)^4 \left(\frac{x^{-3}b^{-1}}{a^{4/3}y^{1/4}}\right) &= \left(\frac{a^{4/12}b^{-16}}{x^{-4}y^4}\right) \left(\frac{1}{a^{4/3}y^{1/4}x^3b}\right) \\
&= \frac{x^4}{y^{17/4}x^3ab^{17}} \\
&= \frac{x}{y^{17/4}ab^{17}}
\end{align*}$$

*** b.

$$\begin{align*}
\frac{(4st)^{3/2}}{\left(8s^6t^{-4}\right)^{2/3}}\left(\frac{2s^{-4}}{3t^{1/3}}\right)^{-1} &= \frac{4^{3/2}s^{3/2}t^{3/2}}{8^{2/3}s^4t^{-8/3}}\left(\frac{2^{-1}s^{4}}{3^{-1}t^{-1/3}}\right) \\
&= \frac{8s^{3/2}t^{3/2 + 8/3}(3t^{1/3})}{4s^4(2s^4)} \\
&= \frac{24s^{3/2}t^{9/2 + 16/6 + 2/6}}{8s^8} \\
&= \frac{3t^{27/6}}{s^{13/2}}
\end{align*}$$

** 19.

$$\begin{align*}
(5x^3 + 3x^2 - 2x) - (x^2 + 8x + 5) &= 5x^3 + 3x^2 - 2x - x^2 - 8x - 5 \\
&= 5x^3 + 2x^2 - 10x - 5
\end{align*}$$

** 20.

$$\begin{align*}
(9t - 2)(7t - 6) &= 63t^2 - 54t - 14t + 12 \\
&= 63t^2 - 68t + 12
\end{align*}$$

** 21.

$$\begin{align*}
3(3t - 4) - (t^2 + 4) - 5t(t - 5) &= 9t - 12 - t^2 - 4 - 5t^2 + 25t \\
&= -6t^2 + 34t - 16
\end{align*}$$

** 22.

$$\begin{align*}
(4x + 1)^2 &= (4x + 1)(4x + 1) \\
&= 16x^2 + 4x + 4x + 1 \\
&= 16x^2 + 8x + 1
\end{align*}$$

** 23.

$$\begin{align*}
(x + 8)(x^2 + 4x + 5) &= x^3 + 4x^2 + 5x + 8x^2 + 32x + 40 \\
&= x^3 + 12x^2 + 37x + 40
\end{align*}$$

** 24.

$$\begin{align*}
(r - 7s)^2 &= (r - 7s)(r - 7s) \\
&= r^2 - 7rs - 7rs + 49s^2 \\
&= r^2 - 14rs + 49s^2
\end{align*}$$

** 25.

$$\begin{align*}
(x + 4)(x - 4) &= x^2 - 4x + 4x - 16 \\
&= x^2 - 16
\end{align*}$$

** 26.

$$\begin{align*}
(4y + 5)(4y - 5) &= 16y^2 - 20y + 20y - 25 \\
&= 16y^2 - 25
\end{align*}$$
 
** 27.

$$\begin{align*}
(\sqrt{x} + 6)(\sqrt{x} - 6) &= x - 6\sqrt{x} + 6\sqrt{x} - 36 \\
&= x - 36
\end{align*}$$

** 28.

$$\begin{align*}
(1 + 6x)(x^2 - 9x + 1) &= x^2 - 9x + 1 + 6x^3 - 54x^2 + 6x \\
&= 6x^3 - 53x^2 - 3x + 1
\end{align*}$$

** 29.

$$\begin{align*}
\left(\sqrt{h^4 + 1} + 1\right)\left(\sqrt{h^4 + 1} - 1\right) &= h^4 + 1 - \sqrt{h^4 + 1} + \sqrt{h^4 + 1} - 1 \\
&= h^4
\end{align*}$$

** 30. Factor the trinomial.

$$\begin{align*}
4x^2 + 29x - 63 &= (4x - 7)(x + 9)
\end{align*}$$

** 31. Factor the trinomial.

$$\begin{align*}
3x^2 - 16x + 5 &= (3x - 1)(x - 5)
\end{align*}$$

** 32. Factor the expression completely.

$$\begin{align*}
14x^3 + 63x &= 7x(2x^2 + 9)
\end{align*}$$

** 33. Factor the expression completely.

$$\begin{align*}
4x^2 - 28x - 120 &= 4(x^2 - 7x - 30) \\
&= 4(x + 3)(x - 10)
\end{align*}$$

** 34. Factor the expression completely.

$$\begin{align*}
81 - 16y^2 &= (9 - 4y)(9 + 4y)
\end{align*}$$

** 35. Factor the expression completely.

$$\begin{align*}
x^2 + 10x + 25 &= (x + 5)(x + 5) \\
&= (x + 5)^2
\end{align*}$$

** 36. Factor the expression completely.

$$\begin{align*}
t^2 - 14t + 49 &= (t - 7)(t - 7) \\
&= (t - 7)^2
\end{align*}$$

** 37. Factor the expression completely.

$$\begin{align*}
5(x^2 + 3)^4 (2x) (x - 2)^4 + (x^2 + 3)^5 (4) (x - 2)^3 &= 2 (x^2 + 3)^4 (x - 2)^3 \left[5x (x - 2) + 2(x^2 + 3)\right]
\end{align*}$$

** 38. Perform the multiplication or division and simplify.

$$\begin{align*}
\frac{x^2 - 36}{x^2 - 16} \cdot \frac{x + 4}{x + 6} &= \frac{(x + 6)(x - 6)(x + 4)}{(x + 4)(x - 4)(x + 6)} \\
&= \frac{x - 6}{x - 4}
\end{align*}$$

** 39. Perform the multiplication or division and simplify.

$$\begin{align*}
\frac{x^2 + 9x + 20}{x^2 + 5x + 6} \cdot \frac{x^2 + 6x + 8}{x^2 + 8x + 16} &= \frac{(x + 4)(x + 5) (x + 2)(x + 4)}{(x + 3)(x + 2) (x + 4)(x + 4)} \\
&= \frac{x + 5}{x + 3}
\end{align*}$$

** 40. Perform the multiplication or division and simplify.

$$\begin{align*}
\frac{x + 5}{9x^2 - 16} \div \frac{x^2 + 6x + 5}{3x^2 + 5x - 12} &= \frac{x + 5}{(3x - 4)(3x + 4)} \times \frac{(3x - 4)(x + 3)}{(x + 1)(x + 5)} \\
&= \frac{x + 3}{(3x + 4)(x + 1)}
\end{align*}$$

** 41. Perform the multiplication or division and simplify.

$$\begin{align*}
\frac{\frac{2x^2 - 11x - 6}{x^2 - 25}}{\frac{2x^2 + 13x + 6}{x^2 + x - 30}} &= \frac{2x^2 - 11x - 6}{x^2 - 25} \div \frac{2x^2 + 13x + 6}{x^2 + x - 30} \\
&= \frac{(2x + 1)(x - 6)}{(x + 5)(x - 5)} \times \frac{(x + 6)(x - 5)}{(2x + 1)(x + 6)} \\
&= \frac{x - 6}{x + 5}
\end{align*}$$
 
** 42. Perform the addition or subtraction and simplify.

$$\begin{align*}
5 + \frac{x}{x + 7} &= \frac{5 (x + 7) + x}{x + 7} \\
&= \frac{5x + 35 + x}{x + 7} \\
&= \frac{6x + 35}{x + 7}
\end{align*}$$

** 43. Perform the addition or subtraction and simplify.

$$\begin{align*}
\frac{x}{x - 8} - \frac{5}{x + 10} &= \frac{x (x + 10) - 5 (x - 8)}{(x - 8)(x + 10)} \\
&= \frac{x^2 + 10x - 5x + 40}{(x - 8)(x + 10)} \\
&= \frac{x^2 + 5x + 40}{(x - 8)(x + 10)}
\end{align*}$$

** 44. Perform the addition or subtraction and simplify.

$$\begin{align*}
\frac{7}{x^2} + \frac{9}{x^2 + x} &= \frac{7}{x^2} + \frac{9}{x(x + 1)} \\
&= \frac{7(x + 1) + 9x}{x^2(x + 1)} \\
&= \frac{7x + 7 + 9x}{x^2 (x + 1)} \\
&= \frac{16x + 7}{x^2 (x + 1)}
\end{align*}$$

** 45. Perform the addition or subtraction and simplify.

$$\begin{align*}
\frac{5}{x} + \frac{6}{x - 1} - \frac{7}{x^2 - x} &= \frac{5}{x} + \frac{6}{x - 1} - \frac{7}{x(x - 1)} \\
&= \frac{5(x - 1) + 6x - 7}{x(x - 1)} \\
&= \frac{5x - 5 + 6x - 7}{x(x - 1)} \\
&= \frac{11x - 12}{x(x - 1)}
\end{align*}$$

** 46. Simplify the compound fractional expression.

$$\begin{align*}
\frac{1 - \frac{4}{y}}{\frac{9}{y} - 1} &= \frac{y - 4}{y} \div \frac{9 - y}{y} \\
&= \frac{y - 4}{9 - y}
\end{align*}$$

** 47. Simplify the compound fractional expression.

$$\begin{align*}
\frac{1 + \frac{1}{x + 9}}{1 - \frac{1}{x + 9}} &= \frac{x + 9 + 1}{x + 9} \div \frac{x + 9 - 1}{x + 9} \\
&= \frac{x + 10}{x + 9} \times \frac{x + 9}{x + 8} \\
&= \frac{x + 10}{x + 8}
\end{align*}$$

** 48. Simplify the compound fractional expression.

$$\begin{align*}
\frac{x + \frac{y}{x}}{y + \frac{x}{y}} &= \frac{x^2 + y}{x} \div \frac{y^2 + x}{y} \\
&= \frac{x^2 + y}{x} \times \frac{y}{y^2 + x} \\
&= \frac{y(x^2 + y)}{x(y^2 + x)}
\end{align*}$$

** 49. Simplify the fractional expression.

$$\begin{align*}
\frac{\frac{1}{4 + x + h} - \frac{1}{4 + x}}{h} &= \frac{4 + x - (4 + x + h)}{h(4 + x + h)(4 + x)} \\
&= \frac{4 + x - 4 - x - h}{h(4 + x + h)(4 + x)} \\
&= \frac{-h}{h(4 + x + h)(4 + x)} \\
&= -\frac{1}{(4 + x + h)(4 + x)}
\end{align*}$$

** 50. Simplify the fractional expression.

$$\begin{align*}
\frac{\frac{1}{(x + h)^2} - \frac{1}{x^2}}{h} &= \frac{x^2 - (x + h)^2}{x^2h(x + h)^2} \\
&= \frac{x^2 - (x + h)(x + h)}{x^2h(x + h)^2} \\
&= \frac{x^2 - (x^2 + 2xh + h^2)}{x^2h(x + h)^2} \\
&= \frac{x^2 - x^2 - 2xh - h^2}{x^2h(x + h)^2} \\
&= \frac{-2xh - h^2}{x^2h(x + h)^2} \\
&= \frac{h(-2x - h)}{x^2h(x + h)^2} \\
&= \frac{-2x - h}{x^2(x + h)^2}
\end{align*}$$

** 51. Find all real solutions of the equation by factoring.

$$\begin{align*}
x^2 + x - 6 &= 0 \\
(x + 3) (x - 2) &= 0 \\
x &= -3, 2
\end{align*}$$

** 52. Find all real solutions of the equation by factoring.

$$\begin{align*}
4x^2 + 11x &= 3 \\
4x^2 + 11x - 3 &= 0 \\
(4x - 1) (x + 3) &= 0 \\
x &= -3, \frac{1}{4}
\end{align*}$$

** 53. Find all real solutions of the equation by factoring.

$$\begin{align*}
6x(x - 1) &= 2 - 5x \\
6x^2 - 6x + 5x - 2 &= 0 \\
6x^2 - x - 2 &= 0 \\
(2x + 1) (3x - 2) &= 0 \\
x &= -\frac{1}{2}, \frac{2}{3}
\end{align*}$$

** 54. Find all real solutions of the quadratic equation.

$$\begin{align*}
x^2 - 2x - 24 &= 0 \\
(x - 6) (x + 4) &= 0 \\
x &= -4, 6
\end{align*}$$

** 55. Find all real solutions of the quadratic equation.

$$\begin{align*}
x^2 - 10x + 7 &= 0 \\
x &= \frac{-(-10) \pm \sqrt{(-10)^2 - 4(1)(7)}}{2(1)} \\
&= \frac{10 \pm \sqrt{100 - 28}}{2} \\
&= \frac{10 \pm \sqrt{72}}{2} \\
&= \frac{10 \pm \sqrt{(36)(2)}}{2} \\
&= \frac{10 \pm 6\sqrt{2}}{2} \\
&= \frac{2(5 \pm 3\sqrt{2})}{2} \\
&= 5 \pm 3\sqrt{2} \\
&= 5 - 3\sqrt{2}, 5 + 3\sqrt{2}
\end{align*}$$

** 56. Find all real solutions of the equation.

$$\begin{align*}
\frac{x + 4}{x - 2} &= \frac{7}{x + 2} + \frac{24}{x^2 - 4} \\
\frac{x + 4}{x - 2} - \frac{7}{x + 2} - \frac{24}{x^2 - 4} &= 0 \\
\frac{(x + 4)(x + 2) - 7(x - 2) - 24}{(x + 2)(x - 2)} &= 0 \\
\frac{x^2 + 6x + 8 - 7x + 14 - 24}{(x + 2)(x - 2)} &= 0 \\
\frac{x^2 - x - 2}{(x + 2)(x - 2)} &= 0 \\
\frac{(x + 1)(x - 2)}{(x + 2)(x - 2)} &= 0 \\
\frac{x + 1}{x + 2} &= 0 \\
x &= -1
\end{align*}$$

** 57. Find all real solutions of the equation.

$$\begin{align*}
\sqrt{8x - 1} &= 3 \\
8x - 1 &= 9 \\
8x &= 10 \\
x &= \frac{5}{4}
\end{align*}$$

** 58. Find all real solutions of the equation.

$$\begin{align*}
x - \sqrt{9 - 3x} &= 0 \\
-\sqrt{9 - 3x} &= -x \\
\sqrt{9 - 3x} &= x \\
9 - 3x &= x^2 \\
x^2 + 3x - 9 &= 0 \\
x &= \frac{-(3) \pm \sqrt{(3)^2 - 4(1)(-9)}}{2(1)} \\
&= \frac{-3 \pm \sqrt{9 + 36}}{2} \\
&= \frac{-3 \pm \sqrt{45}}{2} \\
&= \frac{-3 \pm 3\sqrt{5}}{2} \\
\end{align*}$$

** 59. Find all real solutions of the equation.

$$\begin{align*}
x^4 - 8x^2 + 12 &= 0 \\
(x^2 - 6) (x^2 - 2) &= 0 \\
x &= \pm \sqrt{6}, \pm \sqrt{2} \\
&= -\sqrt{6}, -\sqrt{2}, \sqrt{2}, \sqrt{6}
\end{align*}$$

** 60. Find all real solutions of the equation.

$$\begin{align*}
x^4 - 5x^2 + 4 &= 0 \\
(x^2 - 1) (x^2 - 4) &= 0 \\
x &= \pm \sqrt{1}, \pm \sqrt{4} \\
&= -\sqrt{4}, -\sqrt{1}, \sqrt{1}, \sqrt{4} \\
&= -2, -1, 1, 2
\end{align*}$$

** 61. Find all real solutions of the equation.

$$\begin{align*}
\lvert3x + 1\rvert &= 5 \\
3x + 1 = 5 \\
x &= \frac{4}{3} \\
3x + 1 = -5 \\
x &= -2
\end{align*}$$

** 62. Solve the linear inequality. Express the solution using interval notation.

$$-14 < 3x + 7 \leq 13$$

$$\begin{align*}
3x + 7 &> -14 \\
3x &> -21 \\
x &> -7 \\
3x + 7 &\leq 13 \\
3x &\leq 6 \\
x &\leq 2 \\
&(-7, 2]
\end{align*}$$

** 63. Solve the linear inequality. Express the solution using interval notation.

$$\frac{1}{6} < \frac{2x - 11}{12} \leq \frac{2}{3}$$

$$\begin{align*}
\frac{2x - 11}{12} &> \frac{1}{6} \\
2x - 11 &> 2 \\
2x &> 13 \\
x &> \frac{13}{2} \\
\frac{2x - 11}{12} &\leq \frac{2}{3} \\
2x - 11 &\leq 8 \\
2x &\leq 19 \\
x &\leq \frac{19}{2} \\
&\left(\frac{13}{2}, \frac{19}{2}\right]
\end{align*}$$

** 64. Solve the nonlinear inequality. Express the solution using interval notation.

$$x(1 - 2x) \leq 0$$

$$\begin{align*}
x &\leq 0 \\
1 - 2x &\leq 0 \\
-2x &\leq -1 \\
x &\geq \frac{1}{2} \\
(-\infty, 0] &\cup \left[\frac{1}{2}, \infty\right)
\end{align*}$$

** 65. Solve the nonlinear inequality. Express the solution using interval notation.

$$x^2 - 2x - 48 \leq 0$$

$$\begin{align*}
(x + 6) (x - 8) &\leq 0
\end{align*}$$

| $x$ = -7 | + |
| $x$ = 7  | - |
| $x$ = 9  | + |

$$(-\infty, 6] \cup [8, \infty)$$

** 66. Solve the nonlinear inequality. Express the solution using interval notation.

$$(x - 9)(x + 2)^2 < 0$$

$$(x - 9) (x + 2) (x + 2) < 0$$

| $x$ = -3 | - |
| $x$ = 2  | 0 |
| $x$ = 0  | - |
| $x$ = 9  | 0 |
| $x$ = 10 | + |

$$(-\infty, 9)$$

** 67. Solve the nonlinear inequality. Express the solution using interval notation.

$$x^3 - 25x > 0$$

$$\begin{align*}
x(x^2 - 25) &> 0 \\
x(x + 5)(x - 5) &> 0
\end{align*}$$

| $x$ = -6 | - |
| $x$ = -5 | 0 |
| $x$ = -1 | + |
| $x$ = 0  | 0 |
| $x$ = 1  | - |
| $x$ = 5  | 0 |
| $x$ = 6  | + |

$$(-\infty,-6) \cup (0, 5)$$

** 68. Solve the nonlinear inequality. Express the solution using interval notation.

$$\frac{x - 3}{x + 1} \geq 0$$

| $x$ = -2 | +         |
| $x$ = -1 | \emptyset |
| $x$ = 0  | -         |
| $x$ = 3  | 0         |
| $x$ = 4  | +         |

$$[3, \infty)$$

** 69. Solve the nonlinear inequality. Express the solution using interval notation.

$$\frac{2x + 4}{x - 1} < 0$$

$$\frac{2(x + 2)}{x - 1} < 0$$

| $x$ = -3 | +         |
| $x$ = -2 | 0         |
| $x$ = 0  | -         |
| $x$ = 1  | \emptyset |
| $x$ = 2  | +         |

$$(-2, 1)$$

* 4.5 Exponential and Logarithmic Equations
** 1.
$$\begin{align}
11^{3x - 4} &= \frac{1}{11} \\
3x - 4 &= \log_{11}{\left(\frac{1}{11}\right)} \\
3x - 4 &= -1 \\
3x &= 3 \\
x &= 1
\end{align}$$
** 2.
$$\begin{align}
e^{1 - 3x} &= e^{4x - 8} \\
1 - 3x &= 4x - 8 \\
9 &= 7x \\
x &= \frac{9}{7}
\end{align}$$
** 3.
$$2^{1 - x} = 9$$
*** (a)
$$\begin{align}
1 - x &= \log_2{(9)} \\
-x &= \log_2{(9)} - 1 \\
x &= -\log_2{(9)} + 1
\end{align}$$
*** (b)
$$\approx -3.169925$$
** 4.
$$e^{1 - 7x} &= 6$$
*** (a)
$$\begin{align}
1 - 7x &= \ln{(6)} \\
-7x &= \ln{(6)} - 1 \\
x &= \frac{\ln{(6)} - 1}{-7}
\end{align}$$
*** (b)
$$\approx -0.113108$$
** 5.
$$4^x = 3^{x + 1}$$
*** (a)
$$\begin{align}
x &= \log_4{\left(3^{x + 1}\right)} \\
x &= (x + 1) \log_4{(3)} \\
x &= x \log_4{(3)} + \log_4{(3)} \\
x - x \log_4{(3)} &= \log_4{(3)} \\
x (1 - \log_4{(3)} &= \log_4{(3)} \\
x &= \frac{\log_4{(3)}}{1 - \log_4{(3)}}
\end{align}$$
*** (b)
$$\approx 3.818842$$
** 6.
$$\frac{14}{1 + e^{-x}} = 2$$
*** (a)
$$\begin{align}
14 &= 2 (1 + e^{-x}) \\
14 &= 2 + 2e^{-x} \\
2e^{-x} &= 12 \\
e^{-x} &= 6 \\
-x &= \ln{(6)} \\
x &= -\ln{(6)}
\end{align}$$
*** (b)
$$\approx 1.791759$$
** 7.
$$\begin{align}
e^{2x} - 7e^x + 6 &= 0 \\
(e^x - 6) (e^x - 1) &= 0 \\
x = \ln{(6)} &\quad x = \ln{(1)}
\end{align}$$
** 8.
$$\begin{align}
x^2 9^x - 64 (9^x) &= 0 \\
9^x (x^2 - 64) &= 0 \\
9^x (x + 8) (x - 8) &= 0 \\
x = -8 &\quad x = 8
\end{align}$$
** 9.
$$\begin{align}
\log_2{(3)} + \log_2{(x)} &= \log_2{(5)} + \log_2{(x - 2)} \\
\log_2{(3x)} &= \log_2{(5x - 10)} \\
3x &= 5x - 10 \\
2x &= 10 \\
x &= 5
\end{align}$$
** 10.
$$\begin{align}
\log_{10}{(x - 6)} + \log_{10}{(x + 3)} &= 1 \\
\log_{10}{[(x - 6)(x + 3)]} &= 1 \\
\log_{10}{(x^2 - 3x - 18)} &= 1 \\
x^2 - 3x - 18 &= 10^1 \\
x^2 - 3x - 28 &= 0 \\
(x + 4) (x - 7) &= 0 \\
x \neq -4 &\quad x = 7
\end{align}$$
** 11.
$$\begin{align}
\ln{(x - 7)} + \ln{(x + 8)} &= 1 \\
\ln{[(x - 7)(x + 8)]} &= 1 \\
\ln{(x^2 + x - 56)} &= 1 \\
x^2 + x - 56 &= e^1 \\
x^2 + x - 56 - e &= 0 \\
x &= \frac{-(1) \pm \sqrt{(1)^2 - 4(1)(-56 - e)}}{2(1)} \\
&= \frac{-1 \pm \sqrt{1 + 224 + 4e}}{2} \\
&= \frac{-1 \pm \sqrt{225 + 4e}}{2} \\
x \approx 7.1791 &\quad x \not\approx -8.1791
\end{align}$$
** 12.
$$\begin{align}
x^2 e^x - 3e^x &< 0 \\
e^x (x^2 - 3) &< 0 \\
x &< \pm\sqrt{3} \\
x > -\sqrt{3} &\quad x < \sqrt{3} \\
&(-\sqrt{3}, \sqrt{3})
\end{align}$$
** 13.
$$\begin{align}
A &= P \left(1 + \frac{r}{n}\right)^{nt} \\
(1429.74) &= (1000) \left(1 + \frac{r}{2}\right)^{(2)(4)} \\
1.42974 &= \left(1 + \frac{r}{2}\right)^8 \\
\sqrt[8]{1.42974} &= 1 + \frac{r}{2} \\
\frac{r}{2} &= \sqrt[8]{1.42974} - 1 \\
r &= 2 (\sqrt[8]{1.42974} - 1) \\
&\approx 0.0914 \text{ or } 9.14%
\end{align}$$
* 4.6 Modeling with Exponential Functions
** 1.
*** (a)
$$\begin{align}
Q(t) &= Q_0 e^{kt} \\
Q(2034 - 2011) &= (100) e^{(0.03) (2034 - 2011)} \\
&\approx 199 \text{ million people}
\end{align}$$
*** (b)
$$\begin{align}
Q(t) &= Q_0 e^{kt} \\
Q(2034 - 2011) &= (100) e^{(0.02) (2034 - 2011)} \\
&\approx 158 \text{ million people}
\end{align}$$
** 2.
*** (a)
$$\begin{align}
Q(t) &= Q_0 e^{kt} \\
Q(24) &= (23) e^{(0.15) (24)} \\
&\approx 842 \text{ bacteria}
\end{align}$$
*** (b)
$$\begin{align}
Q(t) &= Q_0 e^{kt} \\
Q(24) &= (23) e^{(0.06) (24)} \\
&\approx 97 \text{ bacteria}
\end{align}$$
** 3.
$$\begin{align}
Q_t &= Q_0 e^{\left(\frac{-\ln{(2)}}{5730}\right) t} \\
70\% \, Q_0 &= Q_0 e^{\left(\frac{-\ln{(2)}}{5730}\right) t} \\
\ln{(0.7)} &= \left(\frac{-\ln{(2)}}{5730}\right) t \\
t &= \frac{\ln{(0.7)}}{\left(\frac{-\ln{(2)}}{5730}\right)} \\
&\approx \frac{\ln{(0.7)}}{-0.000121} \\
&\approx 2948 \text{ yr}
\end{align}$$
** 4.
$$\begin{align}
Q_t &= Q_0 e^{\left(\frac{-\ln{(2)}}{5730}\right) t} \\
59\% \, Q_0 &= Q_0 e^{\left(\frac{-\ln{(2)}}{5730}\right) t} \\
\ln{(0.59)} &= \left(\frac{-\ln{(2)}}{5730}\right) t \\
t &= \frac{\ln{(0.59)}}{\left(\frac{-\ln{(2)}}{5730}\right)} \\
&\approx \frac{\ln{(0.59)}}{-0.000121} \\
&\approx 4361 \text{ yr}
\end{align}$$
* 6.1 Angle Measure
** 1.
$$\begin{align}
87^\circ \times \frac{\pi}{180^\circ} &= 1.518 \text{ radians}
\end{align}$$
** 2.
$$\begin{align}
-56^\circ \times \frac{\pi}{180^\circ} &= -0.977 \text{ radians}
\end{align}$$
** 3.
$$\begin{align}
\frac{11 \pi}{6} \times \frac{180^\circ}{\pi} &= 330^\circ
\end{align}$$
** 4.
$$\begin{align}
-\frac{3 \pi}{2} \times \frac{180^\circ}{\pi} &= 
\end{align}$$
** 5.
$$\begin{align}
\frac{\pi}{6}, \frac{13 \pi}{6}
\end{align}$$

The angles are coterminal.
** 6.
$$\begin{align}
-\frac{4 \pi}{3} + \frac{6 \pi}{3} &= \frac{2 \pi}{3}
\end{align}$$
** 7.
$$\begin{align}
\frac{23 \pi}{4} - \frac{16 \pi}{4} &= \frac{7 \pi}{4}
\end{align}$$
* 7.1 Trigonometric Identities
** 1.
$$\begin{align}
\frac{4 \sin{(x)} \sec{(x)}}{\tan{(x)}} &= 4 \sin{(x)} \left(\frac{1}{\cos{(x)}}\right) \left(\frac{\cos{(x)}}{\sin{(x)}}\right) \\
&= 4
\end{align}$$
** 2.
$$\begin{align}
\frac{1 + \sin{(u)}}{\cos{(u)}} + \frac{\cos{(u)}}{1 + \sin{(u)}} &= \frac{(1 + \sin{(u)}) (1 + \sin{(u)})}{\cos{(u)} (1 + \sin{(u)})} + \frac{\cos{(u)} \cos{(u)}}{\cos{(u)} (1 + \sin{(u)})} \\
&= \frac{1 + 2 \sin{(u)} + \sin^2{(u)} + \cos^2{(u)}}{\cos{(u)} (1 + \sin{(u)})} \\
&= \frac{2 + 2 \sin{(u)}}{\cos{(u)} (1 + \sin{(u)})} \\
&= \frac{2 (1 + \sin{(u)})}{\cos{(u)} (1 + \sin{(u)})} \\
&= \frac{2}{\cos{(u)}} \\
&= 2 \sec{(u)}
\end{align}$$
** 3.
$$\begin{align}
\frac{1}{\sec{(x)} + \tan{(x)}} + \frac{1}{\sec{(x)} - \tan{(x)}} &= 2 \sec{(x)}
\end{align}$$

$$\begin{align}
\frac{1}{\sec{(x)} + \tan{(x)}} + \frac{1}{\sec{(x)} - \tan{(x)}} &= \frac{\sec{(x)} + \tan{(x)} + \sec{(x)} - \tan{(x)}}{(\sec{(x)} + \tan{(x)}) (\sec{(x)} - \tan{(x)})} \\
&= \frac{2 \sec{(x)}}{\sec^2{(x)} - \tan^2{(x)}} \\
&= 2 \sec{(x)}
\end{align}$$
** 4.
$$\begin{align}
\frac{1}{x^2 \sqrt{16 + x^2}}, \quad x = 4 \tan{(\theta)} 
\end{align}$$

$$\begin{align}
\frac{1}{(4 \tan{(\theta)})^2 \sqrt{16 + (4 \tan{(\theta)})^2}} &= \frac{1}{(16 \tan^2{(\theta)}) \sqrt{16 + (16 \tan^2{(\theta)})}} \\
&= \frac{1}{16 \tan^2{(\theta)} \sqrt{16 (1 + \tan^2{(\theta)})}} \\
&= \frac{1}{16 \tan^2{(\theta)} \sqrt{16} \sqrt{1 + \tan^2{(\theta)}}} \\
&= \frac{1}{64 \tan^2{(\theta)} \sqrt{\sec^2{(\theta)}}} \\
&= \frac{1}{64 \tan^2{(\theta)} \sec{(\theta)}} \\
&= \frac{\cos^3{(\theta)}}{64 \sin^2{(\theta)}} \\
&= \frac{\cot^2{(\theta)} \cos{(\theta)}}{64}
\end{align}$$
** 5.
$$\begin{align}
\sqrt{25 - x^2}, \quad x = 5 \sin{(\theta)}
\end{align}$$

$$\begin{align}
\sqrt{25 - (5 \sin{(\theta)})^2} &= \sqrt{25 - (25 \sin^2{(\theta)})} \\
&= \sqrt{25 (1 - \sin^2{(\theta)})} \\
&= \sqrt{25} \sqrt{1 - \sin^2{(\theta)}} \\
&= 5 \sqrt{\cos^2{(\theta)}} \\
&= 5 \cos{(\theta)}
\end{align}$$
** 6.
$$\begin{align}
\frac{\sqrt{x^2 - 4}}{x}, \quad x = 2 \sec{(\theta)}
\end{align}$$

$$\begin{align}
\frac{\sqrt{(2 \sec{(\theta)})^2 - 4}}{(2 \sec{(\theta)})} &= \frac{\sqrt{4 \sec^2{(\theta)} - 4}}{2 \sec{(\theta)}} \\
&= \frac{\sqrt{4 (\sec^2{(\theta)} - 1)}}{2 \sec{(\theta)}} \\
&= \frac{\sqrt{4} \sqrt{\tan^2{(\theta)}}}{2 \sec{(\theta)}} \\
&= \frac{2 \tan{(\theta)}}{2 \sec{(\theta)}} \\
&= \frac{\sin{(\theta)}}{\cos{(\theta)}} \times \cos{(\theta)} \\
&= \sin{(\theta)}
\end{align}$$
** 7.
$$\begin{align}
e^{x + 2 \ln{(| \sin{(x)} |)}} &= e^x \sin^2{(x)}
\end{align}$$

$$\begin{align}
e^{x + 2 \ln{(| \sin{(x)} |)}} &= e^x e^{2 \ln{(| \sin{(x)} |)}} \\
&= e^x e^{\ln{(| \sin{(x)} |)}} e^{\ln{(| \sin{(x)} |)}} \\
&= e^x \sin^2{(x)}
\end{align}$$
* 7.2 Addition and Subtraction Formulas
** 1.
Use an Addition or Subtraction Formula to write the expression as a trigonometric function of one number.

$$\sin{\left(\frac{4 \pi}{5}\right)} \cos{\left(\frac{7 \pi}{15}\right)} - \cos{\left(\frac{4 \pi}{5}\right)} \sin{\left(\frac{7 \pi}{15}\right)}$$

$$\begin{align}
\sin{\left(\frac{4 \pi}{5} - \frac{7 \pi}{15}\right)} &= \sin{\left(\frac{12 \pi}{15} - \frac{7 \pi}{15}\right)} \\
&= \sin{\left(\frac{5 \pi}{15} \\
&= \sin{\left(\frac{\pi}{3}\right)} \\
&= \frac{\sqrt{3}}{2}
\end{align}$$
** 2.
Prove the indentity.

$$\sin{\left(x - \frac{\pi}{2}\right)} = -\cos{(x)}$$

Use the Subtraction Formula for Sine, and then simplify.

$$\begin{align}
\sin{\left(x - \frac{\pi}{2}\right)} &= (\sin{(x)}) \left(\cos{\left(\frac{\pi}{2}\right)}\right) - (\cos{(x)}) \left(\sin{\left(\frac{\pi}{2}\right)}\right) \\
&= (\sin{(x)}) (0) - (\cos{(x)}) (1) \\
&= -\cos{(x)}
\end{align}$$
* 7.3 Double-Angle, Half-Angle, and Product-Sum Form
** 1.
Find $\sin{(2x)}, \cos{(2x)}$, and $\tan{(2x)}$ from the given information.

$$\sin{(x)} = \frac{8}{17}, \quad x \text{ in Quadrant I}$$

$$\begin{align}
\cos{x} &= \frac{\sqrt{17^2 - 8^2}}{17} \\
&= \frac{\sqrt{289 - 64}}{17} \\
&= \frac{\sqrt{225}}{17} \\
&= \frac{15}{17} \\
\tan{x} &= \frac{\sin{x}}{\cos{x}} \\
&= \frac{8}{17} \div \frac{15}{17} \\
&= \frac{8}{15} \\
\sin{(2x)} &= 2 \sin{x} \cos{x} \\
&= 2 \left(\frac{8}{17}\right) \left(\frac{15}{17}\right) \\
&= \frac{240}{289} \\
\cos{(2x)} &= 1 - 2 \sin^2{x} \\
&= 1 - 2 \left(\frac{8}{17}\right)^2 \\
&= 1 - 2 \left(\frac{64}{289}\right) \\
&= \frac{161}{289} \\
\tan{(2x)} &= \frac{2 \tan{x}}{1 - \tan^2{x}} \\
&= \frac{2 \left(\frac{8}{15}\right)}{1 - \left(\frac{8}{15}\right)^2} \\
&= \left(\frac{16}{15}\right) \div \left(1 - \frac{64}{225}\right) \\
&= \frac{16}{15} \times \frac{225}{161} \\
&= \frac{240}{161}
\end{align}$$
** 2.
Find $\sin{(2x)}, \cos{(2x)}$, and $\tan{(2x)}$ from the given information.

$$\tan{(x)} = -\frac{15}{8}, \quad x \text{ in Quadrant II}$$

$$\begin{align}
h &= \sqrt{15^2 + 8^2} \\
&= \sqrt{225 + 64} \\
&= \sqrt{289} \\
&= 17 \\
\sin{x} &= \frac{15}{17} \\
\cos{x} &= -\frac{8}{17} \\
\sin{(2x)} &= 2 \sin{x} \cos{x} \\
&= 2 \left(\frac{15}{17}\right) \left(-\frac{8}{17}\right) \\
&= -\frac{240}{289} \\
\cos{(2x)} &= \cos^2{x} - \sin^2{x} \\
&= \left(-\frac{8}{17}\right)^2 - \left(\frac{15}{17}\right)^2 \\
&= \frac{64}{289} - \frac{225}{289} \\
&= -\frac{161}{289} \\
\tan{(2x)} &= \frac{2 \tan{x}}{1 - \tan^2{x}} \\
&= \frac{2 \left(-\frac{15}{8}\right)}{1 - \left(-\frac{15}{8}\right)^2} \\
&= -\frac{15}{4} \div \left(1 - \frac{225}{64}\right) \\
&= -\frac{15}{4} \times -\frac{64}{161} \\
&= \frac{240}{161}
\end{align}$$
** 3.
Evaluate the expression under the given conditions.

$$\cos{(2 \theta)}; \sin{(\theta)} = - \frac{5}{13}$$

$$\begin{align}
1 - 2 \sin^2{x} &= 1 - 2 \left(-\frac{5}{13}\right)^2 \\
&= 1 - 2 \left(\frac{25}{169}\right) \\
&= 1 - \frac{50}{169} \\
&= \frac{119}{169}
\end{align}$$
** 4.
Evaluate the expression udner the given conditions.

$$\sin{(2 \theta)}; \sin{(\theta)} = \frac{1}{5}, \quad \theta \text{ in Quadrant II}$$

$$\begin{align}
\cos{\theta} &= -\frac{\sqrt{5^2 - 1^2}}{5} \\
&= -\frac{\sqrt{24}}{5} \\
&= -\frac{\sqrt{4 \cdot 6}}{5} \\
&= -\frac{2 \sqrt{6}}{5} \\
\sin{(2 \theta)} &= 2 \sin{\theta} \cos{\theta} \\
&= 2 \left(\frac{1}{5}\right) \left(-\frac{2 \sqrt{6}}{5}\right) \\
&= -\frac{4 \sqrt{6}}{25}
\end{align}$$
** 5.
Prove the identity.

$$(\sin{(x)} + \cos{(x)})^2 = 1 + \sin{(2x)}$$

Expand the product, and use a Pythagorean Identity and a Double-Angle Formula to simplify.

$$\begin{align}
(\sin{(x)} + \cos{(x)})^2 &= \sin^2{(x)} + 2 \sin{(x)} \cos{(x)} + \cos^2{(x)} \\
&= 1 + 2 \sin{(x)} \cos{(x)} \\
&= 1 + \sin{(2x)}
\end{align}$$
** 6.
Prove the identity.

$$\frac{1 - \cos{(2x)}}{\sin{(2x)}} = \tan{(x)}$$

$$\begin{align}
\frac{1 - \cos{(2x)}}{\sin{(2x)}} &= \frac{1 - (1 - 2 \sin^2{(x)})}{2 \sin{(x)} \cdot (\cos{(x)})} \\
&= \frac{2 (\sin{(x)})^2}{2 \sin{(x)} \cos{(x)}} \\
&= \tan{(x)}
\end{align}$$
* 5.5 Inverse Trigonometric Functions and Their Graphs
** 1.
*** (a)
$$\begin{align}
\sin^{-1}{(1)} &= \frac{\pi}{2}
\end{align}$$
*** (b)
$$\begin{align}
\sin^{-1}{\left(\frac{\sqrt{3}}{2}\right)} &= \frac{\pi}{3}
\end{align}$$
*** (c)
$$\begin{align}
\sin^{-1}{(3)} &= \text{UNDEFINED}
\end{align}$$
** 2.
*** (a)
$$\begin{align}
\cos^{-1}{(-1)} &= \pi
\end{align}$$
*** (b)
$$\begin{align}
\cos^{-1}{\left(\frac{1}{2}\right)} &= \frac{\pi}{3}
\end{align}$$
*** (c)
$$\begin{align}
\cos^{-1}{\left(-\frac{\sqrt{3}}{2}\right)} &= \frac{5 \pi}{6}
\end{align}$$
** 3.
*** (a)
$$\begin{align}
\tan^{-1}{(0)} &= 0
\end{align}$$
*** (b)
$$\begin{align}
\tan^{-1}{(-\sqrt{3})} &= -\frac{\pi}{3}
\end{align}$$
*** (c)
$$\begin{align}
\tan^{-1}{\left(-\frac{\sqrt{3}}{3}\right)} &= \tan^{-1}{\left(-\frac{1}{\sqrt{3}}\right)} \\
&= -\frac{\pi}{6}
\end{align}$$
** 4.
*** (a)
$$\begin{align}
\cos^{-1}{\left(-\frac{1}{2}\right)} &= \frac{2 \pi}{3}
\end{align}$$
*** (b)
$$\begin{align}
\sin^{-1}{\left(-\frac{\sqrt{2}}{2}\right)} &= -\frac{\pi}{4}
\end{align}$$
*** (c)
$$\begin{align}
\tan^{-1}{(1)} &= \frac{\pi}{4}
\end{align}$$
** 5.
$$\begin{align}
\sin{\left(\sin^{-1}{\left(\frac{1}{4}\right)}\right)} &= \frac{1}{4}
\end{align}$$
** 6.
$$\begin{align}
\cos{\left(\cos^{-1}{\left(\frac{5}{7}\right)}\right)} &= \frac{5}{7}
\end{align}$$
** 7.
$$\begin{align}
\tan{(\tan^{-1}{(9)})} &= 9
\end{align}$$
** 8.
$$\begin{align}
\sin{\left(\sin^{-1}{\left(-\frac{1}{4}\right)}\right)} &= -\frac{1}{4}
\end{align}$$
** 9.
$$\begin{align}
\cos^{-1}{\left(\cos{\left(\frac{11 \pi}{6}\right)}\right)} &= \cos^{-1}{\left(\frac{\sqrt{3}}{2}\right)} \\
&= \frac{\pi}{6}
\end{align}$$
** 10.
$$\begin{align}
\sin^{-1}{\left(\sin{\left(\frac{4 \pi}{3}\right)}\right)} &= \sin^{-1}{\left(-\frac{\sqrt{3}}{2}\right)} \\
&= -\frac{\pi}{3}
\end{align}$$
** 11.
$$\begin{align}
\tan^{-1}{\left(\tan{\left(\frac{5 \pi}{6}\right)}\right)} &= \tan^{-1}{\left(-\frac{1}{\sqrt{3}}\right)} \\
&= -\frac{\pi}{6}
\end{align}$$
** 12.
$$\begin{align}
\cos{(\sin^{-1}{(0)})} &= \cos{(0)} \\
&= 1
\end{align}$$
** 13.
$$\begin{align}
\cos{\left(\sin^{-1}{\left(\frac{1}{2}\right)}\right)} &= \cos{\left(\frac{\pi}{6}\right)} \\
&= \frac{\sqrt{3}}{2}
\end{align}$$
** 14.
$$\begin{align}
\sin{(\tan^{-1}{(-1)})} &= \sin{\left(-\frac{\pi}{4}\right)} \\
ac{\sqrt{2}}{2}
\end{align}$$
* 6.4 Inverse Trigonometric Functions & Right Triangles
** 1.
*** (a)
$$\begin{align}
\sin^{-1}{(1)} &= \frac{\pi}{2}
\end{align}$$
*** (b)
$$\begin{align}
\cos^{-1}{(0)} &= \frac{\pi}{2}
\end{align}$$
*** (c)
$$\begin{align}
\tan^{-1}{(\sqrt{3})} &= \frac{\pi}{3}
\end{align}$$
** 2.
*** (a)
$$\begin{align}
\sin^{-1}{(0)} &= 0
\end{align}$$
*** (b)
$$\begin{align}
\cos^{-1}{(-1)} &= \pi
\end{align}$$
*** (c)
$$\begin{align}
\tan^{-1}{(0)} &= 0
\end{align}$$
** 3.
$$\begin{align}
\text{opp} &= 21 \\
\text{adj} &= 20 \\
\text{hyp} &= \sqrt{(21)^2 + (20)^2} = 29 \\
\cos{\left(\tan^{-1}{\left(\frac{21}{20}\right)}\right)} &= \frac{\text{adj}}{\text{hyp}} \\
&= \frac{20}{29}
\end{align}$$
** 4.
$$\begin{align}
\text{opp} &= 8 \\
\text{hyp} &= 17 \\
\text{adj} &= \sqrt{(17)^2 - (8)^2} = 15 \\
\sec{\left(\sin^{-1}{\left(\frac{8}{17}\right)}\right)} &= \frac{\text{hyp}}{\text{adj}} \\
&= \frac{17}{15}
\end{align}$$
** 5. $\cos{(\sin^{-1}{(x)})}$
$$\begin{align}
\text{Let } u &= \sin^{-1}{(x)} \\
\cos{u} &= \pm \sqrt{1 - \sin^2{(u)}} \\
\sin^2{u} &= \sin^{2}{(\sin^{-1}{(x)})} \\
&= x^2 \\
\cos{x} &= \sqrt{1 - x^2} \\
\end{align}$$
** 6. $\sin{(\tan^{-1}{(x)})}$
$$\begin{align}
\text{Let } u &= \tan^{-1}{(x)} \\
1 + \cot^2{x} &= \csc^2{x} \\
1 + \frac{1}{\tan^2{x}} &= \frac{1}{\sin^2{x}} \\
\frac{1 + \tan^2{x}}{\tan^2{x}} &= \frac{1}{\sin^2{x}} \\
\sin^2{x} &= \frac{\tan^2{x}}{1 + \tan^2{x}} \\
\sin{u} &= \sqrt{\frac{\tan^2{u}}{1 + \tan^2{u}}} \\
&= \sqrt{\frac{x^2}{1 + x^2}} \\
&= \frac{x}{\sqrt{1 + x^2}}
\end{align}$$
* 7.4 Basic Trigonometric Equations
** Solve the given equation.
$$\begin{align}
\cos{(\theta)} &= -1 \\
\theta &= \cos^{-1}{(-1)} \\
&= \pi + 2 \pi k \text{ rad}
\end{align}$$
** Solve the given equation.
$$\begin{align}
\tan{(\theta)} &= \frac{\sqrt{3}}{3} \\
\theta &= \tan^{-1}{\left(\frac{1}{\sqrt{3}}\right)} \\
&= \frac{\pi}{6} + \pi k
\end{align}$$
** Solve the given equation.
$$\begin{align}
\cos{(\theta)} &= \frac{1}{2} \\
\theta &= \cos^{-1}{\left(\frac{1}{2}\right)} + 2 \pi k \\
&= \frac{\pi}{3} + 2 \pi k, -\frac{\pi}{3} + 2 \pi k
\end{align}$$
*** List six specific solutions
$$\begin{align}
\frac{\pi}{3} + 2 \pi (0) &= \frac{\pi}{3} \\
\frac{\pi}{3} + 2 \pi (1) &= \frac{7 \pi}{3} \\
\frac{\pi}{3} + 2 \pi (2) &= \frac{13 \pi}{3} \\
\frac{\pi}{3} + 2 \pi (3) &= \frac{19 \pi}{3} \\
\frac{\pi}{3} + 2 \pi (4) &= \frac{25 \pi}{3} \\
\frac{\pi}{3} + 2 \pi (5) &= \frac{31 \pi}{3}
\end{align}$$
** Solve the given equation.
$$\begin{align}
\sin{(\theta)} &= -\frac{\sqrt{3}}{2} \\
\theta &= \sin^{-1}{\left(-\frac{\sqrt{3}}{2}\right)} \\
&= \frac{\pi}{3} + 2 \pi k, -\frac{\pi}{3} + 2 \pi k
\end{align}$$
*** List six specific solutions.
$$\begin{align}
-\frac{\pi}{3} + 2 \pi (0) &= -\frac{\pi}{3} \\
-\frac{\pi}{3} + 2 \pi (1) &= \frac{5 \pi}{3} \\
-\frac{\pi}{3} + 2 \pi (2) &= \frac{11 \pi}{3} \\
-\frac{\pi}{3} + 2 \pi (3) &= \frac{17 \pi}{3} \\
-\frac{\pi}{3} + 2 \pi (4) &= \frac{23 \pi}{3} \\
-\frac{\pi}{3} + 2 \pi (5) &= \frac{29 \pi}{3}
\end{align}$$
** Find all solutions of the given equation
$$\begin{align}
\sqrt{2} \cos{(\theta)} - 1 &= 0 \\
\sqrt{2} \cos{(\theta)} &= 1 \\
\cos{(\theta)} &= \frac{1}{\sqrt{2}} \\
\theta &= \cos^{-1}{\left(\frac{\sqrt{2}}{2}\right)} + 2 \pi k \\
&= \frac{\pi}{4} + 2 \pi k, 2 \pi k - \frac{\pi}{4}
\end{align}$$
** Solve the given equation
$$\begin{align}
2 \cos^2{(\theta)} - 1 &= 0 \\
2 \cos^2{(\theta)} &= 1 \\
\cos^2{(\theta)} &= \frac{1}{2} \\
\cos{(\theta)} &= \sqrt{\frac{1}{2}} \\
\theta &= \cos^{-1}{\left(\pm \sqrt{\frac{1}{2}}\right)} + \pi k \\
&= \frac{\pi}{4} + \pi k, \pi k - \frac{\pi}{4}
\end{align}$$
** Solve the given equation
$$\begin{align}
2 \cos^2{(\theta)} - \cos{(\theta)} - 1 &= 0 \\
(2 \cos{(\theta)} + 1) (\cos{(\theta)} - 1) &= 0 \\
2 \cos{(\theta)} + 1 &= 0 \\
\cos{(\theta)} &= -\frac{1}{2} \\
\theta &= \cos^{-1}{\left(-\frac{1}{2}\right)} \\
&= \frac{2 \pi}{3} + 2 \pi k, 2 \pi k - \frac{2 \pi}{3} \\
\cos{(\theta)} - 1 &= 0 \\
\cos{(\theta)} &= 1 \\
\theta &= \cos^{-1}{(1)} \\
&= 2 \pi k
\end{align}$$
** Solve the given equation
$$\begin{align}
2 \cos^2{(\theta)} - 7 \cos{(\theta)} + 3 &= 0 \\
(2 \cos{(\theta)} - 1) (\cos{(\theta)} - 3) &= 0 \\
2 \cos{(\theta)} - 1 &= 0 \\
\theta &= \cos^{-1}{\left(\frac{1}{2}\right)} \\
&= \frac{\pi}{3} + 2 \pi k, 2 \pi k - \frac{\pi}{3} \\
\cos{(\theta)} - 3 &=  0 \\
\theta &= \cos^{-1}{(3)} \\
&= \text{undefined}
\end{align}$$
* 7.5 More Trigonometric Equations
** An equation is given
$$2 \cos{(2 \theta)} - 1 = 0$$
*** (a) Find all solutions of the equation
$$\begin{align}
\cos{(2 \theta)} &= \frac{1}{2} \\
1 - 2 \sin^2{(\theta)} &= \frac{1}{2} \\
\sin^2{(\theta)} &= -\frac{\frac{1}{2} - 1}{2} \\
&= \frac{1}{4} \\
\sin{(\theta)} &= \pm\sqrt{\frac{1}{4}} \\
&= \pm\frac{1}{2} \\
\theta &= \sin^{-1}{\left(\pm \frac{1}{2}\right)} \\
&= \frac{\pi}{6} + 2 \pi k, \frac{5 \pi}{6} + 2 \pi k, 2 \pi k - \frac{\pi}{6}, 2 \pi k - \frac{5 \pi}{6}
\end{align}$$
*** (b) Find the solutions in the interval [0, 2\pi)
$$\begin{align}
\frac{\pi}{6}, \frac{5 \pi}{6}, \frac{7 \pi}{6}, \frac{11 \pi}{6}
\end{align}$$
** Use a Double- or Half-Angle Formula to solve the equation in the interval [0, 2\pi)
$$\begin{align}
\sin{(2 \theta)} + \sin{(\theta)} &= 0 \\
2 \sin{(\theta)} \cos{(\theta)} + \sin{(\theta)} &= 0 \\
\sin{(\theta)} (2 \cos{(\theta)} + 1) &= 0 \\
\sin{(\theta)} &= 0 \\
\theta &= \sin^{-1}{(0)} \\
&= 0, \pi \\
2 \cos{(\theta)} + 1 &= 0 \\
\theta &= \cos^{-1}{\left(-\frac{1}{2}\right)} \\
&= \frac{2 \pi}{3}, \frac{4 \pi}{3}
\end{align}$$
** Use a Double- or Half-Angle Formula to solve the equation in the interval [0, 2\pi)
$$\begin{align}
\cos{(2 \theta)} + \sin^2{(\theta)} &= 0 \\
1 - 2 \sin^2{(\theta)} + \sin^2{(\theta)} &= 0 \\
1 - \sin^2{(\theta)} &= 0 \\
\cos^2{(\theta)} &= 0 \\
\theta &= \cos^{-1}{(0)} \\
&= \frac{\pi}{2}, \frac{3 \pi}{2}
\end{align}$$
* 8.1 Polar Coordinates
** A point is graphed in rectangular form
$$(0, -3)$$

$$\begin{align}
r^2 &= x^2 + y^2 \\
r^2 &= (0)^2 + (-3)^2 \\
r &= \sqrt{9} \\
&= 3 \\
\tan{\theta} &= \frac{y}{x} \\
\theta &= \tan^{-1}{\left(\frac{-3}{0}\right)} \\
&= \frac{3 \pi}{2} \\
(r, \theta) &= \left(3, \frac{3 \pi}{2}\right)
\end{align}$$
** A point is graphed in polar form
$$\left(5, -\frac{2 \pi}{3}\right)$$

$$\begin{align}
x &= r \cos{\theta} \\
&= 5 \cos{\left(-\frac{2 \pi}{3}\right)} \\
&= 5 \left(-\frac{1}{2}\right) \\
&= -\frac{5}{2} \\
y &= r \sin{\theta} \\
&= 5 \sin{\left(-\frac{2 \pi}{3}\right)} \\
&= 5 \left(-\frac{\sqrt{3}}{2}\right) \\
&= -\frac{5 \sqrt{3}}{2} \\
(x, y) &= \left(-\frac{5}{2}, -\frac{5 \sqrt{3}}{2}\right)
\end{align}$$
** Find the rectangular coordinates for the point whose polar coordinates are given?
$$\left(8, \frac{\pi}{6}\right)$$

$$\begin{align}
x &= r \cos{\theta} \\
&= 8 \cos{\left(\frac{\pi}{6}\right)} \\
&= \frac{8 \sqrt{3}}{2} \\
y &= r \sin{\theta} \\
&= 8 \sin{\left(\frac{\pi}{6}\right)} \\
&= \frac{8}{2} \\
&= 4 \\
(x, y) &= \left(\frac{8 \sqrt{3}}{2}, 4\right)
\end{align}$$
** Find the rectangular coordinates for the point whose polar coordinates are given
$$(3, 3\pi)$$

$$\begin{align}
x &= r \cos{\theta} \\
&= 3 \cos{(3 \pi)} \\
&= -3 \\
y &= r \sin{\theta} \\
&= 3 \sin{(3 \pi)} \\
&= 0 \\
(x, y) &= (-1, 0)
\end{align}$$
** Convert the rectangular coordinates to polar coordinates with $r > 0$ and $0 \leq \theta < 2 \pi$
$$(-6, 6)$$

$$\begin{align}
r^2 &= x^2 + y^2 \\
r &= \sqrt{(-6)^2 + (6)^2} \\
&= \sqrt{36 \cdot 2} \\
&= 6 \sqrt{2} \\
\tan{\theta} &= \frac{y}{x} \\
\theta &= \tan^{-1}{\left(-\frac{6}{6}\right)} \\
&= \tan^{-1}{(-1)} \\
&= -\frac{3 \pi}{4} \\
(r, \theta) &= \left(6 \sqrt{2}, -\frac{\pi}{4}\right)
\end{align}$$
** Convert the rectangular coordinates to polar coordinates with $r > 0$ and $0 \leq \theta < 2 \pi$
$$(7 \sqrt{3}, -7)$$

$$\begin{align}
r^2 &= x^2 + y^2 \\
r &= \sqrt{(7 \sqrt{3})^2 + (-7)^2} \\
&= \sqrt{196} \\
&= 14 \\
\tan{\theta} &= \frac{y}{x} \\
\theta &= \tan^{-1}{\left(-\frac{7}{7 \sqrt{3}}\right)} \\
&= \tan^{-1}{\left(-\frac{1}{\sqrt{3}}\right)} \\
&= \frac{11 \pi}{6} \\
(r, \theta) &= \left(14, \frac{11 \pi}{6}\right)
\end{align}$$
** Convert the rectangular coordinates to polar coordinates with $r > 0$ and $0 \leq \theta < 2 \pi$
$$(-4, 0)$$

$$\begin{align}
r^2 &= x^2 + y^2 \\
r &= \sqrt{(-4)^2 + (0)^2} \\
&= \sqrt{16} \\
&= 4 \\
\tan{\theta} &= \frac{y}{x} \\
\theta &= \tan^{-1}{\left(\frac{0}{-4}\right)} \\
&= \tan^{-1}{(0)} \\
&= \pi \\
(r, \theta) &= (4, \pi)
\end{align}$$
