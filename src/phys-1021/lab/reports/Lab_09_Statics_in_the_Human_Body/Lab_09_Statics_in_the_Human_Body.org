#+title: Statics in the Human Body
#+author: Eric Nguyen, Kai Clark, Conley Easter
#+options: toc:nil
#+latex_header: \usepackage[margin=1in]{geometry}
* Introduction
In this lab, we simulate systems of static equilibrium found in the human body to gain a better understanding of statics.
Specifically, we look into how the body and its muscles apply torque to maintain equilibrium.
* Procedure
** Apparatus
3 mass positioners,
fulcrum with support stand,
Pasco force sensor (hand held),
set of masses and mass hangers,
meter stick.
** Part I
1. Slide the fulcrum to the center of mass of the meterstick.
2. Place a mass hanger on opposite sides of the meterstick at equal distance from the center, with one representing the weight and the other representing the head.
3. Add 150 g for the head side and add weight to the muscle side until it reaches equilibrium.
** Part II
1. Reset the meterstick to equilibrium.
2. Add a mass to represent the weight of the body and another mass at least twice as far to represent the muscle.
3. Apply a muscle force to maintain rotational equilibrium.
** Part III
1. Reset the meterstick to equilibrium.
2. Hold down the meterstick at the fulcrum and add weights at the end of the meterstick.
3. Apply a force to represent the bicep muscle and establish equilibrium.
* Precautions
- Set the system to equilibrium before applying forces.
- Apply the force of the weight before applying the force of the muscle.
- Hold down the meterstick to ensure it is not displaced.
* Error
Sources of error include not converting units, mixing up signs, and improper setup of systems.
For the class II and class III levers, we were unsure as to where the fulcrum should be in the system.
We assumed the fulcrum should be at the end of the meterstick.
* Data
** Part I
$$\begin{align}
r_\text{muscle} &= 7.75 \text{ cm} & r_\text{weight} &= 8.25 \text{ cm} \\
m_\text{muscle} &= 0.015 \text{ kg} & m_\text{weight} &= 0.015 \text{ kg}
\end{align}$$
** Part II
$$\begin{align}
r_\text{weight} &= 28.25 \text{ cm} & r_\text{muscle} &= 13.75 \text{ cm}
\end{align}$$

| Force (N) |
|-----------|
|    -0.538 |
|    -0.069 |
|    -0.067 |
** Part III
| Force (N) |
|-----------|
|    -5.370 |
|    -5.200 |
|    -5.104 |
|    -4.885 |
* Questions
** Where is the center of mass of your meterstick? How can you tell that the fulcrum is at the center of mass?
The center of mass of my meterstick is approximately 48.75 cm.
We can tell that the fulcrum is at the center of mass because the meterstick balances on the fulcrum, not tipping over.
** Make a prediction: if we add 150 g to represent the weight of the head $F_\text{weight}$, how much force does the muscle need to generate to maintain equilibrium and not tip forward or backward?
If we add 150 g to represent the weight of the head $F_\text{weight}$, my prediction is that the muscle would need to generate $0.15 \text{ kg} \times 9.8 \text{ m/s}^2 = 1.47 \text{ N}$
** What do we mean by equilibrium here? In other words, how do you know if your meter stick is in equilibrium (both rotational and translational)?
We know that our meterstick is in equilibrium because there is no translational or rotational acceleration, that is $a = 0$ and $\alpha = 0$.
** Do your results support or refute your prediction? Briefly explain.
Our results support our prediction.
The force of the muscle and the force of the weight were indeed the same so that they would maintain equilibrium.
** How would the muscle's force need to compensate to maintain equilibrium if the center of mass of the head was shifted farther away from the pivot? Support your answer with a mathematical relation (s).
If the center of mass of the head were to shift farther away from the pivot, the muscle's force would need to increase to compensate for that.

$$\begin{align}
r_\text{muscle} F_\text{muscle} &= r_\text{weight} F_\text{weight} \\
\end{align}$$
** What muscle force is needed to keep the ankle in equilibrium according to your calculation?
The tension force of the calf muscle is needed to keep the ankle in equilibrium.

$$\begin{align}
m &= 0.2658 \text{ kg} \\
F &= (0.2658 \text{ kg}) (9.8 \text{ m/s}^2) \\
&= 2.605 \text{ N}
\end{align}$$
** Was your prediction approximately correct?
No, our prediction was not approximately correct.
This is due to mechanical advantage inherent in class II levers which we failed to consider. 
** Class II levers like ankles and wheelbarrows are useful because the provide mechanical advantage, by amplifying the input force to provide a greater output force. In other words, we can lift a load without having to lift the full weight of the load. \newline\newline Prove using a mathematical relation that Class II levers provide *mechanical advantage*. You will need to use the rotational equilibrium condition. It will be helpful to draw a picture indicating the movement of the load. Also, to simplify we can assume the force is still perpendicular to $r$ by considering the case when the load just starts to move.
$$\begin{align}
\tau &= r F
\end{align}$$

Because the $r_\text{muscle}$ will be greater than $r_\text{weight}$, it will be easier to rotate as $\tau_\text{muscle}$ would be larger and $\tau_\text{weight}$ would be smaller.
** Mechanical advantage comes at a cost. If the output force to input force ratio increases when we use a lever, what decreases?
The cost of mechanical advantage is that while the output force to input force ratio increases, the output distance decreases. 
** Based on static equilibrium arguments, estimate the maximum muscle force we can apply before the meterstick lifts off of the base (think translational equilibrium here).
Using our collected data, we estimate that the maximum muscle force we can apply before the meterstick lifts off of the base to be approximately 4.9 N.
** Show using a mathematical relation why very large forces must be generated by the bicep to lift even a small weight. Be sure to accompany your proof with a short explanation.
Large forces must be generated by the bicep to lift even a small weight because it is the opposite of the class II lever, where the force applied by the muscle is closer to the axis of rotation and the weight is much farther away from the axis of rotation.

$$\begin{align}
r_\text{muscle} F_\text{muscle} &= r_\text{weight} F_\text{weight} + F_\text{gravity} \\
F_\text{muscle} &= \frac{r_\text{weight} F_\text{weight} + F_\text{gravity}}{r_\text{muscle}} \\
\end{align}$$
** The maximum contraction of the human biceps is only a few cm. Show using trigonometry how even a small bicep contraction results in a large translation of the hand.
Because trigonometric functions represent ratios, we can apply that idea to the ratio of the distances between the forces and the axis of rotation.
So, because the muscle force is much closer to the axis of rotation and the hand is much farther away, we know that the bicep will travel less and the hand will travel more.
** Back injuries are very common in humans and are often caused by lifting objects with the legs straight while leaning over; also known as “lifting with the back.” Use the concepts learned in this lab to explain why one should “lift with the legs” rather than with the back. Make sure to discuss the forces and torques involved, and how they differ in the two lifting techniques.
Lifting with your back increases the torque of gravity by moving your center of mass away from the axis of rotation, whereas lifting with your legs keeps your center of mass on the axis of rotation.
This extra torque may cause injury to the back.
* Conclusion
In doing this lab, we found that our predicted results and actual results generally aligned with each other, except for Part II.
In Part II, we failed to consider mechanical advantage, which is why our predictions did not match well with the actual results.

In any case, we learned about how our body uses statics in our everyday life.
We learned that there are three different classes of levers and how they are different from each other.
