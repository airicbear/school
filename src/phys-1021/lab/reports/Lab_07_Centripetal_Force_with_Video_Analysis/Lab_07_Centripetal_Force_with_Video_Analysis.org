#+title: Centripetal Force with Video Analysis
#+author: Eric Nguyen, Gabriela Chassedin, Nathan William Perry
#+options: toc:nil
#+latex_header: \usepackage[margin=1in]{geometry}
* Introduction
The purpose of this lab is to gain a better understanding of circular motion and specifically, the acceleration and velocity of objects moving in circular paths.
* Hypothesis
#+begin_quote
Static friction provides the centripetal force for an object resting on a spinning turntable.
#+end_quote
* Procedure
** Apparatus
Computer with PASCO Capstone software, turntable, masses for placing on turntable, smartphone
** Part I
1. Using a smartphone, take birds-eye-view video footage of a mass on a spinning turntable.
** Part II
1. Within the PASCO Capstone software, add a Movie display with your video footage.
2. Within the Movie display, use the x-y coordinate tool to define the origin. 
3. Within the Movie display, use the caliper tool to match the scale.
4. Repeatedly click the Next Frame button until just before the mass starts moving.
5. Repeatedly click on the mass's position until the mass flies off the turntable.
** Part III
1. Add a graph display of tangential velocity.
2. Find the point when the object flies off of the turntable and drag the coordinates tool there.
3. Add a graph display for angle in radians over time.
4. Find average period of motion.
5. Derive an expression for $\mu_s$.
* Precautions
- Ensure that the mass is on the correct area on the turntable.
- Spin the turntable at a reasonable speed.
* Error
Error in this lab can primarily be found in the proper usage of the Capstone software.
Specifically, correctly configuring the Movie display with the necessary tools such as the caliper tool.
* Data
file:angle_in_radians.png
[[file:tangential_velocity.png]]
* Questions
** What force or forces are acting on the object as it rotates at constant velocity? Draw a free body diagram indicating the direction of the force(s) acting on the object. (Why did we ask about constant velocity rotation?)
The force acting on the object as it rotates at constant velocity is the frictional force.
** What are some ways to test the hypothesis? Be aware that additional materials can be made available on request.
One way to test the hypothesis would be using different masses and see far each mass flies off the turntable.
Another way would be to compare the distance the mass travels when it is on different surfaces with different amounts of friction.
** What path do you expect the objects to follow as they fly off of the turntable? Is it a straightline path, or do they curve away from the turntable, or continue moving along the circle? How does this compare to the path they take before they fall off the table? If you are spinning a string with a ball at the end and the string snaps, which path does the ball take?
I expect the objects to follow a straightline path as they fly off of the turntable.
The path they take before they fall off the table would be circular.
If I were spinning a string with a ball at the end and the string snaps, the ball would follow a straightline path.
** Roughly, how does choice of origin affect the shape of the tangential velocity data curve?
The choice of origin affects the shape of the tangential velocity curve by the position at which the curve begins.
** According to Capstone, how many radians did your object pass through in the time interval you studied? (check for internal consistency: How many radians are there in a circle? Is the number you are reporting reasonable?)
According to Capstone, my object passed through 5 radians.
Theare 2\pi radians in a circle, so my number is reasonable.
** How did you obtain the average period? Briefly explain.
I obtained the average period by taking the reciprocal of the frequency.
Because the frequency is roughly 5 radians per half second, or 10 radians per second, I found the average period to be a tenth of a second per rotation.
** What expression did you obtain for centripetal force? Does the expression depend on the mass of the object?
The expression I obtained for centripetal force was $F_c = \frac{mv^2}{r}$.
It does depend on the mass of the object.
** Compare your value to published values of $\mu_s$ for similar materials (try [[https://engineeringtoolbox.com/][engineeringtoolbox.com]]). Is the value about what you would expect for the materials?
Using my derived expression, $\mu_s = \frac{F_f}{F_N}$, I found my value to be 0.07.
This value is about what I would expect for the materials.
* Conclusion
** How I tested the hypothesis
I tested my hypothesis by using different surface conditions for the turntable.
I then observed how this affected the motion of the object.
** What results results I obtained from my hypotheses
I found that static friction indeed provides the centripetal force for an object resting on a spinning turntable as the surface condition with more friction prevented the mass from flying off.
Inversely, I observed how the surface condition with less friction made the mass fly off very easily, indicating that there was no centripetal acceleration keeping the mass on the turntable.
