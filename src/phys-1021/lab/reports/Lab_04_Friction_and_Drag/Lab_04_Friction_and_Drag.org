#+title: Lab Report 4: Friction and Drag
#+author: Eric Nguyen, Kai Clark, Conley Easter
#+options: toc:nil
#+latex_header:\usepackage[margin=1in]{geometry}
* Introduction
In this lab, we observe how friction and drag affect an object's motion.
The purpose of the lab is to understand the factors which contribute to such forces and how the forces change under different conditions.
* Procedure
** Apparatus
Wood block,
adjustable inclined plane with pulley and protractor,
weight hanger and set of weights,
platform balance and level,
coffee filters,
motion detector,
Capstone software
** Part I
For Part I of the lab, we identify the force which will move the block at a constant velocity along a plane by sliding a block across a plane.
*** Part Ia
- Measure the mass of the block and have the plane inclined at 0$^\circ$. Then, place the block onto the plane with its broad side down.
- Attach the string to the block, threading it through the pulley and then attach the mass hanger to the hanging end of the string.
- Hanging different amounts of masses, determine the amount required to have the block move at a constant velocity.
- Repeat the last step, but with the block on its narrow side.
- Repeat the last step, but with the block on its broad side and masses 100 g, 200 g, and 600 g on top of the block.
- Using the results, calculate the kinetic coefficient \mu_k, average and standard deviation.
*** Part Ib
- Repeat the procedure for Part Ia, but incline the plane at angles 20$^\circ$, 30$^\circ$, 40$^\circ$ and only use the broad side of the block.
- Using the results, calculate the force of tension on the block needed to move the block up the plane at a constant velocity.
** Part II
For Part II of the lab, we determine the terminal velocity of a given number of coffee filters using a motion detector.
- Connect the motion sensor to the interface and configure the Capstone software to plot a graph of the object's position and speed over time, including a table alongside it.
- Hit the Record button and drop a coffee filter starting just below the motion sensor and stop recording once it hits the ground.
- Repeat the last step for 2, 4, 6, 8, and 10 coffee filters, saving the results for each.
- For each trial, weigh the mass of the coffee filters.
- Calculate the theoretical value of the terminal speed, assuming $C = 0.75$. Calculate the percent error against the experimental result.
- Plot ${v_t}^2$ vs. $mg$. There should be five points on the graph, one for each trial. Fit a line to the graph and find the experimental slope.
- Find the theoretical slope by rearranging the equation, $\frac{1}{2} C \rho A {v_t}^2 = mg$. Use the result to calculate $C^*$, where $C^* = C\rho$.
* Precautions
** Part I
- We hold the block back when putting on masses to ensure that the block does not move.
** Part II
- Make sure the coffee filters fall in a straight path.
* Error
In this lab, the source of errors generally are involved with the data collection part.
Particularly, in the second part of the lab, we found our results from the motion sensor to be faulty.
We can contribute this error to two primary factors.
First, we did not troubleshoot when our graphs did not look correct.
Secondly, we may have held the coffee filters too close to the motion sensor.

The same errors can be found in the first part of the lab, but for different reasons.
Namely, our methods for finding the force were imprecise in the sense that the smallest weights were 10 g.
* Data Calculations Fitting
** Part I
*** Part Ia
#+ATTR_LATEX: :environment longtable :align |p{2.25cm}|p{2.25cm}|p{2.25cm}|p{3cm}|p{3.25cm}|p{1.5cm}|
| Mass (kg)   | $m_\text{block}$ (kg) | $m_\text{hanger}$ (kg) | $F_n = (m_\text{block}) (9.8$ m/s^2) | $F_t = (m_\text{hanger}) (9.8$ m/s^2) | $\mu_k = \frac{F_t}{F_n}$ |
|-------------+-----------------------+------------------------+-------------------------------------+--------------------------------------+---------------------|
| 0   (Broad) |                 0.334 |                  0.070 |                                3.27 |                                0.686 |               0.210 |
| 0.1 (Broad) |                 0.434 |                  0.088 |                                4.25 |                                0.862 |               0.203 |
| 0.2 (Broad) |                 0.534 |                  0.110 |                                5.23 |                                1.078 |               0.206 |
| 0.3 (Broad) |                 0.634 |                  0.165 |                                6.21 |                                 1.62 |               0.261 |
| 0  (Narrow) |                 0.334 |                  0.085 |                                3.27 |                                0.833 |               0.255 |
*** Part Ib
#+ATTR_LATEX: :environment longtable :align |p{2cm}|p{3cm}|p{3.5cm}|p{3.5cm}|p{3cm}|
| Incline ($^\circ$) | $m_\text{block}$ (kg) | $m_\text{hanger}$ (kg) | $F_n = (m_\text{block})(9.8$ m/s^2) | $F_t = \mu_k F_n$ \quad $\mu_k = 0.227$ |
|--------------------+-----------------------+------------------------+-------------------------------------+-----------------------------------------|
|                 20 |                 0.334 |                  0.157 |                                3.27 |                                   0.742 |
|                 30 |                 0.334 |                  0.220 |                                3.27 |                                   0.742 |
|                 40 |                 0.334 |                  0.285 |                                3.27 |                                   0.742 |
** Part II
*** Experimental Results
#+begin_src julia :eval no-export :results graphics :file ./P2_Experimental.png :exports results
  using Plots, DataFrames, GLM

  m = 0.85
  g = 9.8
  cf = [  2,   4,   6,   8,   10]
  vt = [1.3, 5.7, 9.1, 5.8, 10.1]
  data = DataFrame(x=(cf * m * g), y=(vt.^2))
  fit = lm(@formula(y ~ x), data)

  scatter(data.x, data.y,
          label="",
          xlabel="mg",
          ylabel="vt²")
  plot!(data.x, data.y, label="")
  plot!(data.x, predict(fit, data), label="Fit", linestyle=:dash)

  savefig("./P2_Experimental.png")
#+end_src

#+RESULTS:
[[file:./P2_Experimental.png]]

*** Theoretical Results

#+begin_src julia :eval no-export :results graphics :file ./P2_Theoretical.png :exports results
  using Plots

  m = 0.85
  g = 9.8
  C = 0.75
  ρ = 1.25
  A = 3.63
  cf = [2, 4, 6, 8, 10]
  vt2(n) = (n * m * g * 2) / (C * ρ * A)

  scatter(cf * m * g, vt2.(cf),
          legend=false,
          xlabel="mg",
          ylabel="vt²")
  plot!(cf * m * g, vt2.(cf))

  savefig("./P2_Theoretical.png")
#+end_src

#+RESULTS:
[[file:./P2_Theoretical.png]]

*** Percent Error
$$\begin{align}
% \text{ Error} &= \frac{}{}
\end{align}$$
* Questions
** 1. Did there seem to be a difference in the required force between the narrow and broad side?
Yes, there was a significant difference in the required force between the narrow and broad side of the block.
** 2. What should the graph of the velocity vs. time look like when the filter reaches terminal velocity?
The graph of the velocity vs. time should plateau when the filter reaches terminal velocity.
** 3. Why is it unwise to suddenly apply the brakes of a car on a wet or icy pavement? (Hint: What type of friction is acting between your tires and the roadway if your tires are sliding along the road? What if your tires are rolling along the road?) Briefly explain.
It is unwise to suddenly apply the brakes of a car on a wet or icy pavement because there is less friction, making it more difficult to brake.
** 4. Explain why it is so important that the string that connects the block to the hanging masses must be parallel to the surface of the plane? Would the frictional force change if the string were not parallel? Explain why or why not.
It is important that the string that connects the block to the hanging masses be parallel to the surface of the plane because if were not, then the problem would be more complicated.
Yes, the frictional force would change if the string were not parallel because the direction would change.
** 5. Give at least one everyday example where a reduction in the force of friction would be undesirable.
One everyday example where a reduction in friction force would be undesirable would be when walking down a staircase.
** 6. How would the results of your experiment change if the fluid through which the objects were dropped had been more viscous? How about if the object had the same dimensions but was made heavier?
The results of my experiment would change if the fluid through which the objects were dropped had been more viscous by decreasing the terminal velocity of the objects.

If the object had the same dimensions but was made heavier, the terminal velocity of the objects would increase.
* Conclusion
In the first part of this lab, we focused on how friction affects the motion of an object.
We experimented with this concept by sliding a block across the surface of a plane.
We then focused the effect of drag on the motion of an object in free fall by dropping coffee filters under a motion sensor.

In both cases, our expectations generally matched our results.
We expected that the friction force would increase along with the surface area of the block in contact with the plane, as it did.
We also expected the terminal velocity of the coffee filters to increase along with the mass and once again, the results generally matched our expectations.
