#+title:Energy in the Human Body
#+author: Eric Nguyen
#+options: toc:nil
#+latex_header: \usepackage[margin=1in]{geometry}
* Introduction
The purpose of this lab is to show through real life observation the relevance of the concept of energy in the human body and how energy changes in the human body.
* Procedure
** Apparatus
Stopwatch, meterstick
** Part I
1. Use the stopwatch to measure the initial heart rate for one minute.
2. Use the stopwatch to measure the time it takes to climb the stairs.
3. Use the stopwatch to measure the final heart rate for 20 seconds.
4. Use the meterstick to measure the vertical displacement.
* Precautions
- Stop the stopwatch precisely.
- Use the appropriate units.
* Error
The primary source of error for my lab report is the inprecise measurement of the height of the stairs and mass of my body.
Using the meterstick, I made a rough estimate of the height of one flight of stairs, then multiplied that measurement by the number of floors I climbed.
My reasoning behind this decision is that it would be incredibly tedious to precisely measure the height of the stairs.
As for my mass, I simply use the last measurement I could remember.
* Data
$$\begin{align}
HR_\text{initial} &= 67 \text{ bpm} \\
HR_\text{final}  &= 171 \text{ bpm} \\
\Delta h &= 12 \text{ m} \times 5 \text{ floors} = 60 \text{ m} \\
\Delta t &= 63 \text{ s} \\
REE &= 5 \text{ kJ/min} = \frac{250}{3} \text{ W} \\
TEE &= 48 \text{ kJ/min} = 800 \text{ W} \\
E_\text{human} &= (TEE - REE) \cdot \Delta t \\
&= \left(800 \text{ W} - \frac{250}{3} \text{ W}\right) \cdot 63 \text{ s} \\
&= 45150 \text{ J} \\
\Delta U_g &= mg \Delta h \\
&= (59 \text{ kg}) (9.8 \text{ m/s}^2) (60 \text{ m}) \\
&= 34692 \text{ J} \\
E_\text{human} &> \Delta U_g \\
\% \text{ Difference} &= \frac{45150 \text{ J} - 34692 \text{ J}}{\frac{45150 \text{ J} + 34692 \text{ J}}{2}} \times 100\% \\
&\approx 26.2 \%
\end{align}$$
* Questions
** How do you convert the number of beats measured in 20 seconds to beats per minute?
Use unit conversion as usual.
In this case, we simply convert seconds to minutes as we already include beats in our units.

$$\begin{align}
HR_\text{final}  &= \left(\frac{57 \text{ beats}}{20 \text{ s}}\right) \times \left(\frac{60 \text{ s}}{1 \text{ min}}\right) = 171 \text{ bpm}
\end{align}$$
** Why must we subtract the $REE$ value from the $TEE$ value to find the human energy expended for the trip up the stairs?
We must subtract the $REE$ value from the $TEE$ value because $REE$ is the initial state and $TEE$ is the final state.
If we were to omit this calculation, then the initial state would be zero which would not be true.
** Theoretically speaking, if we assume the energy stored in the body $U_\text{stored}$ became kinetic $K$ and gravitational potential energy $U_\text{g}$ during the trip. Write an energy conservation relation between point 1 before the trip, point 2 during the trip and point 3 after the trip in terms of $U_\text{stored}$, $K$, and $U_\text{g}$. Be sure to add the subscripts to clearly indicate points 1, 2, and 3.
$$\begin{align}
U_\text{stored} = {U_g}_2 + K = {U_g}_3
\end{align}$$
** Indicate how each of the energy types in the above equation $U_\text{stored}$, $K$, and $U_\text{g}$ changes from one point to the next during the 3 phases of the trip. (e.g. decreases, no change, or use symbols < > =).
*** Phase 1
| $U_\text{stored}$ | no change |
| $U_g$         | no change |
| $K$               | no change |
*** Phase 2
| $U_\text{stored}$ | decreases |
| $U_g$         | increases |
| $K$               | increases |
*** Phase 3
| $U_\text{stored}$ | no change |
| $U_g$         | no change |
| $K$               | no change |
** When energy is given as a rate it is called *power* and is usually given in terms of Watts which are Joules per second; however, power could also be given in kcal per hour, horsepower, or several other units. How much horsepower were you generating when you were climbing the stairs? (You will need to look up the conversion between watts and horsepower).
$$\begin{align}
\left(800 \text{ W} - \frac{250}{3} \text{ W}\right) \times \frac{1 \text{ hp}}{746 \text{ W}} &\approx 0.96 \text{ hp}
\end{align}$$
** A simple energy conservation argument can be made that if all the calories we eat are metabolized, there is no excess energy to be stored as fat and we do not gain weight. However, if we consume fewer calories than our body requires, then we will lose weight as our body uses those fat reserves. Based on the values you obtained in this lab, how many calores a day should you eat to avoid losing or gaining any weight if you climb the stairs for 15 minutes every day and then rest the remainder of the day?
$$\begin{align}
E_\text{human} &= 45150 \text{ J} \times \frac{1 \text{ cal}}{4.184 \text{ J}} \approx 10791.11 \text{ cal}
\end{align}$$
** If a system is /isolated/, the total energy within that system is constant. Consider the case of a hot cup of coffee: when it cools down, the energy goes into the surrounding air, causing the air to warm. If the coffee and the air are placed in an isolated chamber, the total energy of the system (coffee + air) is constant because the energy lost by the coffee is exactly equal to the energy gained by the air. \newline\newline The only way for the total energy of a system to change is when it is not isolated, meaning that /external/ forces or sources of energy interact with the system. If we want to define a system where the total energy is conserved, which one of the choices below would be the best system to consider?
#+begin_quote
A. The person \\
B. The person and the Earth \\
C. The Earth
#+end_quote
B. The person and the Earth
** Based on your answer to Question 7, why doesn't the total energy of the Earth-person system change when the person goes up to a new height, gaining potential energy?
The total energy of the Earth-person system doesn't change when the person goes up to a new height because the Earth loses energy as it pulls the person down with gravity while the person gains potential energy as their height increases. 
* Conclusion
In this lab, we achieved a greater understanding of the concept of energy and the conservation of energy by observing energy expenditure in the human body.
Specifically, we learned about how different types of energy change in a moving system and how total energy is conserved throughout the process unless external sources of energy interact with the system.
As a result of this lab, we potentially can apply this knowledge in better managing personal caloric consumption and energy usage.
