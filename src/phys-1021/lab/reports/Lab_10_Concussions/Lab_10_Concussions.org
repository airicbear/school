#+title: Concussions
#+author: Eric Nguyen, Gabriela Chassedin, Nathan William Perry
#+options: toc:nil
#+latex_header: \usepackage[margin=1in]{geometry}
* Introduction
The purpose of this lab is to gain a better understanding of the change of momentum in physical collisions through data analysis.
* Procedure
** Apparatus
Computer with PASCO interface and Capstone software,
carts with force sensors and extra weights,
Vernier dual-range force sensor,
photogate
** Part I
1. Attach the force sensor to the end the track.
    Attach the photogate above the track a few centimeters from the force sensor.
2. Attach a small flag to the cart that will pass through the photogate.
3. Configure the appropriate measurements in Capstone.
4. Add a graph display of force over time with two digital displays overhead indicating speed and time in gate.
5. Elevate the other end of the track for the force of gravity to accelerate the cart.
6. Make sure the data is being recorded continuously, then click the Record button and release the cart from the end of the track, taking note of the position at which it was released.
7. Repeat this data collection twice for analysis.
8. Repeat this data collection three times using weights.
9. Record the maximum force for each collision.
10. Highlight the area at which the collision occurs, then use the integrate button to determine the impulse of the collision.
** Part II
1. Repeat the procedure in Part I using a helmet on the force sensor.
* Precautions
- Do multiple trials to ensure quality of data.
- Ensure the force does not damage equipment.
- Remember to convert units, especially for the flag.
* Error
In this lab, we found the trials to generally have the same results as evident in our experiments from Part I.
So we, out of laziness, decided that doing more trials in Part II would not make a significant difference.
This allows for errors as we are unable to identify whether or not the single trials we did could have been of poor quality.
* Data
For the data portion of this lab, I exclude repeated trials as they appear redundant.
** Part I
#+caption: Collision without helmet or mass
[[file:collision_no_helm_no_mass_3.png]]

#+caption: Collision without helmet but with mass
[[file:collision_no_helm_with_mass_3.png]]

\newpage
** Part II
#+caption: Collision with helmet but without mass
[[file:collision_with_helm_no_mass_1.png]]

\newpage

#+caption: Collision with helmet and mass
[[file:collision_with_helm_with_mass_1.png]]
* Questions
** Will the velocity of the cart change when we put weights on it? Why or why not?
Yes.
The velocity of the cart will change when we put weights on it as if we assume momentum $p$ is constant, then we can rearrange the equation for momentum to show that a change in mass indeed causes a change in velocity $v = \frac{p}{m}$.
** Compare the shape of the force vs. time curve with the helmet to that without a helmet. Describe your observations.
The shape of the force vs. time curve between the graphs with and without helmets look generally similar, with nearly identical position in time, force, and impulse.
** How does the time scale of the impact differ between the two cases?
The time scale of the impact differ with the helmet slightly increasing the time scale.
** Compare the impulse between the cases with and without a helmet. Was there a significant difference in the impulse with and without a helmet? Was there a significant difference in the maximum force experienced with and without a helmet? Was there a significant difference in the maximum force experienced with and without a helmet? It has been shown that higher accelerations of the head cause concussions, does your data support the common assertion that helmets help prevent concussions? Support your answer.
In all cases, the differences seem to be insignificant.

Our data does not support the common assertion that helmets help prevent concussions as there was no significant difference between the accelerations and impulses of the cases with and without a helmet.
** The impulse is also equal to the change in momentum. How did adding mass affect the momentum of the cart? Did this cause a noticeable difference in the impulse data?
Adding mass increased the momentum of the cart.
As a result, we indeed noticed a considerable difference in the impulse data.
** Figure 1 below is published data from a study predicting the likelihood of concussion based on the acceleration contribute to concussions (in other words the collision also causes the head to turn). The graph above accounts for both types of acceleration showing the likelihood of a concussion given to a set of values of linear and rotational acceleration. Refer to the data you collected to find the highest value of linear acceleration you recorded. Using this value refer to Figure 1 to determine the rotational acceleration that would have to occur for a 1% chance of concussion in your experiment.
$$\begin{align}
a &= \frac{0.23 \text{ m/s}}{0.04 \text{ s}} = 5.75 \text{ m/s}^2 \\
\alpha &= \frac{5.75 \text{ m/s}^2}{0.3 \text{ m}} \approx 19.17 \text{ rad/s}^2
\end{align}$$
** Figure 2 is a graph of the empirical cumulative distribution function (CDF) vs the linear acceleration for two different data sets (HITS and NFL). The CDF is the fraction of observations that are below the specified value on the x axis. For example, about 90 percent of the sub-concussive impacts in the HITS data occured at accelerations below 50 g. \newline\newline According to the NFL data set, what was the highest acceleration experienced that did not cause a concussion? What was the minimum acceleration that did cause a concussion?
According to the NFL data set, the highest acceleration experienced that did not cause a concussion was approximately 100 g.
The minimum acceleration that did cause a concussion was approximately 30 g.
* Conclusion
In this lab, we used concussions as an example to demonstrate the concept of momentum and how it can be applied to a real issue.
We discovered that while helmets do not play a significant role in preventing concussions, we found that the impulse of the collision does.
Specifically, an increased mass or increased velocity will increase the chances of concussion to occur.
This can also be described in terms of force and time, where increased force and decreased time would also increase the chances of concussion.
