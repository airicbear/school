#+title: Conservation of Energy with Rotation
#+author: Eric Nguyen, Kai Clark, Conley Easter
#+options: toc:nil
#+latex_header: \usepackage[margin=1in]{geometry}
* Introduction
In this lab, we simulate conservation of energy when rotational and translational kinetic energy are involved.
* Procedure
** Apparatus
Computer with PASCO interface and PASCO Capstone software,
rotary motion sensor,
ring stands,
pulley,
string,
masses,
solid disk,
meter stick
** Part I
1. Using the string, hang the ring stand from the solid disk.
2. Rotating the solid disk, wind up the string until the top of the hanging mass is up to the height of the disk.
3. In Capstone, make a table and graph display of the angular velocity of the sensor.
4. Hit the Record button and release the hanging mass so that it causes the disk to rotate.
5. Repeat data collection twice.
6. For each trial, take note of the maximum angular velocity of the disk.
** Part II
1. Calculate $U_0$, $KE_{\text{R}_f}$ and $KE_{\text{T}_f}$.
* Precautions
* Error
* Data
* Questions
* Conclusion
