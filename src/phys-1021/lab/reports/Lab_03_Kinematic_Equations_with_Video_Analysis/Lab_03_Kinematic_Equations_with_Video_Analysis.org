#+title: Lab Report 3: Kinematic Equations with Video Analysis
#+author: Eric Nguyen, Gabriela Chassedin, Nathan William Perry
#+options: toc:nil num:nil
#+latex_header:\usepackage[margin=1in]{geometry}

* Introduction
In this lab, our objective is to gain an understanding of projectile motion.
More specifically, we observe the physical quantities associated with a projectile in motion and how the force of gravity affects such quantities.

* Procedure
Our procedure for this lab is simple.
All we do is analyze video footage of a tennis ball that is tossed into the air using specialized software.
The apparatus for all three parts of the lab is as follows: a computer with PASCO Capstone software, a tennis ball, a camera, and a meterstick.
** Part I
In Part I, we setup an environment in which the video will be taken.
This is a necessary step to ensure that the video has all of the references needed for the software to make the correct measurements.
After the environment is setup, we then proceed to take the video.

For this part, we had it setup so that both a meterstick and the entire path of the tennis ball from start to finish would be apparent in the video.
Once we had it setup, we used a smartphone camera to take the video.
Satisfied with the quality of the video, we then uploaded it to the computer software for analysis.
** Part II
In Part II, we analyze the video footage, continuing the procedure from Part I.
The Capstone software allows us to do this easily by providing a graphical interface in which we simply click or drag elements with little typing required.
These are the sequence of steps we followed to analyze the video:

1. Drag a movie display onto the workspace.
2. Within the movie display,
   1. Click on ``Video Analysis mode'' in the toolbar.
   2. Drag the $x\text{-}y$ coordinates tool onto the initial position of the tennis ball to define the origin of the graph.
   3. Drag the distance scale tool and match its ends with the meterstick's.
   4. Use the ``Next Frame'' button to find the point in the video just when the ball is released.
   5. Repeatedly click on the position of the ball as the video automatically steps forward one frame until just before the ball hits the ground.
** Part III
In Part III, we continue the analysis from Part II.

1. Make graphs for both components of the position, velocity, and acceleration vectors.
2. Fit a trend to each graph, using the points in each to determine whether to use a linear fit or nonlinear fit.
* Precautions
There were very few precautions to take in this lab.
The precautions we did take include making sure the tennis ball was in the video from when it left the hand and its entire path until it the ground.
This precautionary step allowed us to gather the data correctly.
* Error
The main source of error we encountered in this lab was incorrect user input when defining the position of the tennis ball.
We also found that the tools used in the software were initially confusing in regards to their usage.

One error not pertaining to the software would be not taking into account the air resistance acting on the tennis ball.
* Data Calculations Fitting
- $x\text{-position}$ kinematic equation: $\Delta x = v_\text{f} t - \frac{1}{2} at^2$
- $y\text{-position}$ kinematic equation: $v_\text{f} - v_0 = at$.
[[./Lab_03_Kinematic_Equations_with_Video_Analysis.png]]
* Questions
** 1. What is the time value when the ball in your video is at its maximum height?
The time value when the ball in my video is at its maximum height is approximately half a second.
** 2. What is the time value when the ball in your video has zero $y\text{-velocity}$?
Because this the $y\text{-velocity}$ is zero when the ball is at its maximum height, the time value will be the same as the one answered from Question 1.
** 3. Compare your fit function to the kinematic equations and write the matching kinematic equation next to your graph in your report.
My fit function for the $x\text{-component}$ of the tennis ball's position seems to match with the kinematic equation, $\Delta x = v_\text{f} t - \frac{1}{2} at^2$, whereas my fit function for the $y\text{-component}$ of the tennis ball's position matches with the kinematic equation, $v_\text{f} - v_0 = at$.
** 4. For the $y\text{-velocity}$ vs time graph, how do you find the $y\text{-acceleration}$ from the fit? What is the acceleration expected to be for such an object in free fall?
For the $y\text{-velocity}$ vs time graph, I find the $y\text{-acceleration}$ by looking at the slope of the fit---in this case, -10.7.
I know that the slope, -10.7 m/s^2 is the acceleration because I know that acceleration is the change in velocity and the slope represents that.

The expected acceleration for such an object in free fall would be 9.8 m/s^2 assuming that the object is within the atmosphere of Earth.
** 5. What is the $x\text{-acceleration}$ according to your graph and fit of the $x\text{-velocity}$ data?
The $x\text{-acceleration}$ according to my graph and the fit of the $x\text{-velocity}$ data would be the rate of change in the $x\text{-velocity}$.
In this case, the fit is a quadratic equation, so the $x\text{-acceleration}$ will not be constant.
Using calculus, we find that the $x\text{-acceleration}$ would be $a_x(t) = -1.804x + 0.921$.
** Discussion D
The fit results from my $y\text{-velocity}$ generally agree with how the data changes over time in my $y\text{-acceleration}$ graph after the initial force, with most data hovering around -10 m/s^2.
** Discussion E
The Capstone software can calculate velocity and acceleration data given only the position because given the position data of an object, you can observe its rate of change over time.
With the rate of change of the position, you have the velocity of the object.
From there, you can observe the rate of change in the velocity over time, giving you the object's acceleration.
* Conclusion
In this lab, we used the Capstone software to analyze the motion of a projectile using video footage of its position over time.

Before our analysis, we mainly expected two things: that our $x\text{-acceleration}$ would be 0 m/s^2 and our $y\text{-acceleration}$ would be 9.8 m/s^2.
After our analysis, we found that neither of our components of acceleration exactly matched these two expected quantities, however were close.
Our results most likely did not match these quantities accurately as expected due to user error when clicking on the tennis ball's position in the Capstone software. 
Additionally, there may have been air resistance that we did not account for.
