* PHYS 1021
** Instructor Information
*** MWF Lecture Instructor
Michael Opferman
*** Email
michael.opferman@temple.edu

tug08341@temple.edu
*** Office
SERC 480
*** Office Hours
    | Monday, Wednesday | 1:30-2:45 (Walk-in)   |
    | Tuesday, Thursday | 2:30-3:30 (Walk-in)   |
    | Friday            | 10:30-11:30 (Walk-in) |

    I am also available at other times by appointment
** Dr. Xu (Recitation)
*** Name
Xiaojun Xu
*** Role
Instructor for Friday @ 9:00 in Wachman 408
*** Email
xiaojun@temple.edu
*** Phone
508-353-6888
*** Office
SERC 453
*** Office hours
Friday 2:00-4:00pm
** Details
*** Course Materials
Textbook: College Physics: A Strategic Approach, Vol 1, 4th 
edition by Knight, Jones, and Field. Publisher: Pearson 
Prentice Hall.
Information about the various ways that you can purchase the 
textbook can be found on Canvas in the “Getting Started” 
Module 

Homework: This course will use Modified Mastering Physics, 
which is linked directly into your Canvas account. You must 
purchase access either through a textbook bundle or by itself. 
Information about how to set up can be found on Canvas in 
the “Getting Started” Module 
*** Grading
- 70% Exams
  - 35% Final Exam
  - 35% 3 Midterm Exams (5%, 15%, 15%)
- 15% Lab
- 5% Homework (mostly on MMP)
- 10% In-Class work (lecture + recitation)
** Work flow
1. Look over the material at home before class
2. Go over the material in class
3. DIY in class
4. DIY in recitation and lab
5. DIY at home
6. Exam
   
