#+TITLE: MATH 1042 Syllabus

* Course

Mathematics 1042.005.

* Course Title

Calculus II.

* How this course will be taught

Virtual. This course will be taught
synchronously throughout the semester during its regularly schedule time
using Zoom. The link to the Zoom meetings is available on Canvas.

In-person activities and instruction for the fall 2020 semester will end
Nov. 20, at the start of the fall break. The remaining week of classes,
study period and finals will be conducted remotely.

* Time

MWF 2:40 - 3:50 PM.

* Place

All lectures and office hours will be held online via Zoom.

* Instructor

Dumitru Dan Rusu.

* Instructor Office

Zoom Meeting ID 964 456 7400.

* Instructor Email

dumitru.rusu@temple.edu

* Instructor Phone

N/A please use email.

* Course Web Page

https://math.temple.edu/ugrad/coordinated/1042/

* Office Hours

MWF 3:50 - 5:00 PM. Office hours may not be used to
cover material missed due to unjustified absences.

* Prerequisites

Math 1041 (Calculus I) with a grade of C or better or
transfer credits for Math 1041.

* Textbook

Calculus: Early Transcendentals by James Stewart; 8th
Edition, Cengage Learning. IMPORTANT: together with the textbook you
need to have access to WebAssign because part of your homework will be
assigned there. IF YOU TOOK MATH 1041 IN OR AFTER THE FALL 2015, YOU
SHOULD ALREADY HAVE THIS BOOK AND THE WebAssign ACCESS. If it is not the
case, you can purchase a standalone WebAssign Instant Access (it is
called WebAssign Instant Access for Calculus, Multi-Term Courses, 1st
Edition) and use the electronic version of the textbook (eBook). Or you
can purchase the Bundle that contains both the paper textbook (loose
leaf version) and the WebAssign Printed Access Card for Stewart's
Calculus: Early Transcendentals; 8th Edition; Multi-Term. The publisher
(Cengage) created a special site for Temple students where you can buy
either of these two versions for a discounted price ($119.99 for Cangage
Unlimited 4 month access, $125 for the standalone access and about $144
for the Bundle):
http://services.cengagebrain.com/course/site.html?id=4465142. You can
also buy the Bundle at the Main Campus Temple Bookstore
(http://www.temple.edu/bookstore), but it will cost $172. Please DO NOT
buy any EARLIER editions of this book. Please also DO NOT buy any used
books since they will not let you access WebAssign (the WebAssign codes
ARE NOT transferable).

* Technology specifications for this course

A working computer with a
reliable internet connection, a Webcam, and audio capability.
Recommended Internet Speed: 8mbps download & 5mbps upload. You can test
your connection at https://www.speedtest.net. Please note: Hard-wired
connections are more consistent than Wi-Fi for Zoom sessions. A scanning
app such as AdobeScan or CamScanner is required as is access to Zoom and
Canvas (the Canvas app is also recommended).\\
Limited resources are available for students who do not have the
technology they need for class. Students with educational technology
needs, including no computer or camera or insufficient Wifi-access,
should submit a request outlining their needs using the Student
Emergency Aid Fund form. The University will endeavor to meet needs,
such as with a long-term loan of a laptop or Mifi device, a refurbished
computer, or subsidized internet access.

* Course Goals

To teach techniques of integration and applications of
definite integrals and infinite series.

* Topics Covered

The definite integral and the Fundamental Theorem of
Calculus, applications of the definite integral, techniques of
integration, improper integrals, sequences and series, including power
and Taylor series.

* Course Grading

Your course grade will be computed according to the
following scheme: Review on Limits (WA assignment) - 1%, Homework
assigned on WA - 5%, Quiz Average - 14%, Exam 1 - 19%, Exam 2 - 19%,
Exam 3 - 19%, Final Exam - 23%.

* Exam Dates

We will have three common midterms: Test 1: Friday,
September 25, from 5:20 PM to 6:20 PM; Test 2: Friday, October 23rd,
from 5:20 PM to 6:20 PM, Test 3: Friday, November 20th, from 5:20 PM to
6:20 PM, and the comprehensive common Final Exam: Thursday, December
10th, from 3:30 to 5:30 PM.

* Remote proctoring statement

Zoom, Proctorio or a similar proctoring
tool /may/ be used to proctor exams and quizzes in this course. These
tools verify your identity and record online actions and surroundings.
It is your responsibility to have the necessary government or school
issued ID, a laptop or desktop computer with a reliable internet
connection, the Google Chrome and Proctorio extension, a webcam/built-in
camera and microphone, and system requirements for using Proctorio,
Zoom, or a similar proctoring tool. Before the exam begins, the proctor
may require a scan of the room in which you are taking the exam.

* Attendance Policy

Attendance is required. Students who miss 6-11 MWF
classes without an excuse will have their grades lowered by one notch
(e.g., from B to B-). Those who miss 12-17 MWF classes without an excuse
will have their grades lowered by two notches, etc.

* Attendance Protocol and Your Health

If you feel unwell, you should
not come to campus, and you will not be penalized for your absence.
Instructors are required to ensure that attendance is recorded for each
in-person or synchronous class session. The primary reason for
documentation of attendance is to facilitate contact tracing, so that if
a student or instructor with whom you have had close contact tests
positive for COVID-19, the university can contact you. Recording of
attendance will also provide an opportunity for outreach from student
services and/or academic support units to support students should they
become ill. Faculty and students agree to act in good faith and work
with mutual flexibility. The expectation is that students will be honest
in representing class attendance.

* Expectations for Class Conduct

In order to maintain a safe and
focused learning environment, we must all comply with the four public
health pillars: wearing face coverings, maintaining physical distancing,
washing our hands and monitoring our health. It is also important to
foster a respectful and productive learning environment that includes
all students in our diverse community of learners. Our differences, some
of which are outlined in the University's nondiscrimination statement,
will add richness to this learning experience. Therefore, all opinions
and experiences, no matter how different or controversial they may be
perceived, must be respected in the tolerant spirit of academic
discourse.\\
Treat your classmates and instructor with respect in all communication,
class activities, and meetings. You are encouraged to comment, question,
or critique an idea but you are not to attack an individual. Please
consider that sarcasm, humor and slang can be misconstrued in online
interactions and generate unintended disruptions. Profanity should be
avoided as should the use of all capital letters when composing
responses in discussion threads, which can be construed as "shouting"
online. Remember to be careful with your own and others' privacy. In
general, have your behavior mirror how you would like to be treated by
others.

* Statement on recording and distribution of recordings of class sessions

Any recordings permitted in this class can only be used for
the student's personal educational use. Students are not permitted to
copy, publish, or redistribute audio or video recordings of any portion
of the class session to individuals who are not students in the course
or academic program without the express permission of the faculty member
and of any students who are recorded. Distribution without permission
may be a violation of educational privacy law known as FERPA as well as
certain copyright laws. Any recordings made by the instructor or
university of this course are the property of Temple University.

*Calculator Policy:* NO CALCULATORS may be used during the exams and
most of the quizzes.

*Homework:* Homework will be regularly assigned from the textbook and
from Additional Homework Problems document and will be the basis for the
weekly quizzes. A part of that assigned homework you will do using the
WA (WebAssign). It will be graded by the computer. Your result for the
WA part of the homework will be counted as 5% of your Course Average.

*Quizzes:* There will be a 20-40 minutes quiz every week (when there is
no midterm) at the end of class. Each quiz will consist of 4-5 problems
similar to the ones assigned from the textbook. All steps necessary to
justify an answer must be shown. A missed quiz will count as a zero.
There will be no make-up quizzes. The lowest two scores for the quizzes
will be dropped.

*Review Assignment On Limits:* You will have a review assignments on
Limits on WebAssign: It will be given right before we start Chapter 11
on infinite series. The grade of this Review assignment on Limits will
be counted as 1% of your Course Average.

*Make Up Policy:* There will be NO MAKE UP exams except in the case of a
DOCUMENTED EMERGENCY, like an illness or an accident. The documents
confirming the emergency (from a hospital or police) must be provided.
If you miss an exam, you must contact your instructor AND Professor
Boris Datskovsky (Director of Advising and Coordinated Courses) by email
right away, ideally before the exam (the email address of Professor
Datskovsky is: bdats@temple.edu). Requests for makeups will not be
honored if they come more than 48 hours after the time of the exam.

*Canvas:* This is a registered Canvas course. Please go there regularly
to see important announcements and keep track of your current grades.

*Common Final Exam:* Final exam will be held on Thursday, December 10,
from 3:30 PM to 5:30 PM. Please note that if you miss the final exam and
do not make alternative arrangements before the grades are turned in,
your grade for the course will be F.

*Letter Grades:* 0-49 F, 50-54 D-, 55-64 D, 65-69 D+, 70-72 C-, 73-76 C,
77-79 C+, 80-82 B-, 83-86 B, 87-89 B+, 90-92 A-, 93-100 A.

*Academic Support:* The Student Success Center (SSC) and the Math TA and
CA Consulting Center (MCC) both provide excellent support services for
this course throughout the semester. The MCC will provide both in person
and online tutoring. The schedule will be posted in your course Canvas
page after the first week of class. The Student Success Center services
include the Writing Center, the PASS program, Academic Coaching, STEM
tutoring, the Conversation Partners program, and more. In Fall 2020, all
Student Success Center services will be available for students, and all
will be offered exclusively online.

*Exam Security Policy:* We have a zero tolerance policy towards
cheating. Students caught cheating on a problem in a test (receiving
outside help, using unauthorized resources or devices such as
calculators, online resources, etc.) will receive a score of 0 for the
entire test. This is consistent with the Temple University Academic
Honor Code (see
https://secretary.temple.edu/sites/secretary/files/policies/03.70.12.pdf
) that states: "Every member of the university community is responsible
for upholding the highest standards of honesty at all times. Students,
as members of the community, are responsible for adhering to the
principles of academic honesty and integrity".

*Technology for Assessments:* During the quizzes and exams, you will
need to log in to both Zoom and Canvas on your computer where you will
be taking the exam and to Zoom with your smartphone. The computer camera
must be pointing at your face, while your smartphone will be pointing at
your work area, that is the paper on which you are writing. You will
need to have a system to suspend the phone securely above your work
area. If you cannot make a system that does that, you can buy a simple
clip-on holder on Amazon:
https://www.amazon.com/Cell-Phone-Clip-Stand-Holder/dp/B079QY6RFQ/ I
stress that you are not required to buy this device. If you have a way
to hold your phone steady above your work area, that is fine. Also, you
may be able to find a cheaper phone holder online. If you are not logged
in with both devices, your exam will not be accepted.

Any student who has a need for accommodations based on the impact of a
documented disability or medical condition should contact Disability
Resources and Services (DRS) in 100 Ritter Annex
([[mailto:drs@temple.edu][drs@temple.edu]]; 215-204-1280) to request
accommodations and learn more about the resources available to you. If
you have a DRS accommodation letter to share with me, or you would like
to discuss your accommodations, please contact me as soon as practical.
I will work with you and with DRS to coordinate reasonable
accommodations for all students with documented disabilities. All
discussions related to your accommodations will be confidential.

Freedom to teach and freedom to learn are inseparable facets of academic
freedom. The University has adopted a policy on Student and Faculty
Academic Rights and Responsibilities (Policy # 03.70.02) which can be
accessed [[http://policies.temple.edu/][here]] (opens in new
tab/window).

Students will be charged for a course unless dropped by the Drop/Add
deadline date. Check the
[[http://www.temple.edu/registrar/documents/calendars][University calendar]] (opens in new tab/window) for exact dates.

During the Drop/Add period, students may drop a course with no record of
the class appearing on their transcript. Students are not financially
responsible for any courses dropped during this period. In the following
weeks prior to or on the withdrawal date students may withdraw from a
course with the grade of "W" appearing on their transcript. After the
withdrawal date students may not withdraw from courses. Check the
[[http://www.temple.edu/registrar/documents/calendars][University Calendar]] (opens in new tab/window) for exact dates.
[[http://bulletin.temple.edu/undergraduate/academic-policies/withdrawal-policies/][See
the full policy by clicking here]] (opens in new tab/window).

The grade "I" (an "incomplete") is only given if students cannot
complete the course work due to circumstances beyond their control. It
is necessary for the student to have completed the majority of the
course work with a passing average and to sign an incomplete contract
which clearly states what is left for the student to do and the deadline
by which the work must be completed. The incomplete contract must also
include a default grade that will be used in case the "I" grade is not
resolved by the agreed deadline.
[[http://bulletin.temple.edu/undergraduate/academic-policies/incomplete-coursework/][See the full policy by clicking here]] (opens in new tab/window).
