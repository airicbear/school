#+TITLE: Calculus 2 Cheatsheet

* Midterm 2

** 7.1 Integration by Parts

Find best \(u\) and \(dv\).
From those, find \(du = u \;dx\) and \(v = \int dv\).
Solve new expression, \(uv - \int v \;du\).

** 7.2 Trigonometric Integrals

First, know the integrals for \(\tan{x}\) and \(\sec{x}\).

\begin{align*}
\int \tan{x} \;dx &= \ln{|\sec{x}|} + C \\
\int \sec{x} \;dx &= \ln{|\sec{x} + \tan{x}|} + C
\end{align*}

Know the following half angle identities.

\begin{align*}
\sin^2{x} &= \frac{1 - \cos{2x}}{2} \\
\cos^2{x} &= \frac{1 + \cos{2x}}{2} \\
\end{align*}

Know the following double angle identities.

\begin{align*}
\sin{(2x)} &= 2 \sin{x} \cos{x} \\
\cos{(2x)} &= \cos^2{x} - \sin^2{x} \\
&= 2 \cos^2{x} - 1 \\
&= 1 - 2 \sin^2{x}
\end{align*}

Here is the strategy for \(\int \sin^m{x} \cos^n{x} \;dx\)

1. Cosine odd? Save \(\cos{x}\). Use \(\cos^2{x} = 1 - \sin^2{x}\). Substitute \(u = \sin{x}\).

2. Sine odd? Save \(\sin{x}\). Use \(\sin^2{x} = 1 - \cos^2{x}\). Substitute \(u = \cos{x}\).

3. Both even? Use half-angle identities.

Here is the strategy for \(\int \tan^m{x} \sec^n{x} \;dx\) problems.

1. Secant power is even? Save \(\sec^2{x}\). Use \(\sec^2{x} = 1 + \tan^2{x}\). Substitute \(u = \tan{x}\).

2. Tangent is odd? Save \(\sec{x} \tan{x}\). Use \(\tan^2{x} = \sec^2{x} - 1\). Substitute \(u = \sec{x}\).

Some other identities you may want to know

\begin{align*}
\sin{A} \cos{B} &= \frac{1}{2} [\sin{(A - B)} + \sin{(A + B)}] \\
\sin{A} \sin{B} &= \frac{1}{2} [\cos{(A - B)} - \cos{(A + B)}] \\
\cos{A} \cos{B} &= \frac{1}{2} [\cos{(A - B)} + \cos{(A + B)}]
\end{align*}

*** Types of problems to expect

\begin{align*}
\int \cos^3{x} \;dx \\
\int \sin^5{x} \cos^2{x} \;dx \\
\int_0^{\pi} \sin^2{x} \;dx \\
\int \sin^4{x} \;dx \\
\int \tan^6{x} \sec^4{x} \;dx \\
\int \tan^5{\theta} \sec^7{\theta} \;d\theta \\
\int \tan^3{x} \;dx \\
\int \sec^3{x} \;dx \\
\int \sin{4x} \cos{5x} \;dx
\end{align*}

** 7.3 Trigonometric Substitution

Know the corresponding substitutions.

\begin{align*}
\sqrt{a^2 - x^2} &\implies x = a \sin{\theta} \\
\sqrt{a^2 + x^2} &\implies x = a \tan{\theta} \\
\sqrt{x^2 - a^2} &\implies x = a \sec{\theta}
\end{align*}

*** Types of problems to expect

\begin{align*}
\int \frac{\sqrt{9 - x^2}}{x^2} \;dx \\
\frac{x^2}{a^2} + \frac{y^2}{b^2} &= 1 \\
\int \frac{1}{x^2 \sqrt{x^2 + 4}} \;dx \\
\int \frac{x}{\sqrt{x^2 + 4}} \;dx \\
\int \frac{dx}{\sqrt{x^2 - a^2}} \\
\int_0^{3 \sqrt{3} / 2} \frac{x^3}{(4x^2 + 9)^{3/2}} \;dx \\
\int \frac{x}{\sqrt{3 - 2x - x^2}} \;dx
\end{align*}

** 7.4 Integration of Rational Functions by Partial Fractions

A rational function \(\frac{P(x)}{Q(x)}\) is improper if \(\deg{(P)} \geq \deg{(Q)}\).
If it is improper, use long division or algebra to make it proper.

Then, follow the partial fractions method.

\[\frac{P(x)}{Q(x)} = \frac{A}{Q_1(x)} + \frac{B}{Q_2(x)} + \cdots\]

Use the following partial fractions method for irreducible \(Q(x)\).

\[\frac{P(x)}{Q(x)} &= \frac{Ax + B}{Q_1{(x)}} + \ldots\]

** 7.8 Improper Integrals

There are two types of improper integrals.

1. Unbounded region.
2. Infinite discontinuity.

Given a \(p\)-integral of the form \(\int_1^{\infty} \frac{1}{x^p} \;dx\), the integral is divergent if \(p \leq 1\), otherwise it is convergent.

Indeterminate form is represented by \(\frac{0}{0}\), \(\infty \cdot 0\), \((-1)^{\infty}\), \(0^0\).
Use L'Hopital's Rule to make it not indeterminate.

Comparison Theorem. \(f(x) \geq g(x) \geq 0\).

1. \(\int_a^{\infty} f(x) \;dx\) convergent? \(\int_a^{\infty} g(x) \;dx\) convergent.
1. \(\int_a^{\infty} g(x) \;dx\) divergent? \(\int_a^{\infty} f(x) \;dx\) divergent.
