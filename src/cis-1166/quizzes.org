#+title: Quizzes
#+latex_header: \usetikzlibrary{graphs,graphdrawing,graphs.standard,quotes}
#+latex_header: \usegdlibrary{trees,examples}

* Quiz 6

** Problem 1

   Consider three functions
   \(f:A \to A\);
   \(g:A \to A\);
   \(h:A \to A\);
   where \(A = \{1, 2, 3, 4\}\).
   Using the definitions of \(f\), \(g\) and \(h\) given below,
   find \(f \circ g \circ h(A)\) (4 points)

   #+begin_quote
     f(1) = 2; f(2) = 1; f(3) = 1; f(4) = 4
     g(1) = 2; g(2) = 4; g(3) = 1; g(4) = 3
     h(1) = 1; h(2) = 3; h(3) = 1; h(4) = 3
   #+end_quote
   
** Problem 2

   \(a\) and \(b\) are integers such that \(a \equiv 4 \;(\bmod\; 7)\) and \(b \equiv 6 \;(\bmod\; 7)\).
   Find the integer \(c\) with \(0 \leq c \leq 6\) such that

   - \(c \equiv 3a \;(\bmod\; 7)\)

     \[\begin{align}
     &\equiv 12 \;(\bmod\; 7) \\
     &\equiv 5
     \end{align}\]

   - \(c \equiv 2a + 4b \;(\bmod\; 7)\)

     \[\begin{align}
     &\equiv 9 \;(\bmod 7\;) \\
     &\equiv 4
     \end{align}\]

   - \(c \equiv a^2 - b^2 \;(\bmod\; 7)\)

     \[\begin{align}
     &\equiv 1
     \end{align}\]

** Problem 3

   - Prove or disprove:
     For all integers a, b, c, d, if a | b and c | d, then (a + c) | (b + d).

     /*Disprove*/

   - Compute: -43 div 9 = -5

   - Compute: -43 mod 9 = 2

* DONE Quiz 6.1, 6.2
  CLOSED: [2020-03-27 Fri 11:22]

** FAIL Question 1

   Suppose that a "word" is any string of seven letters of the alphabet,
   with repeated letters allowed and case doesn't matter.

   (a) How many words have the letter T as their fourth letter?

   - 26^6

   (b) How many words begin with an A or a B?

   - +26^6+26^6+ ❌

   - 2*26^6 ✓

   (c) How many words begin have no vowels?

   - +26^7-5^7+ ❌

   - 21^7 ✓

** Question 2

   Consider all bit strings of length 12.
   How many begin with 11 and end with 10?

   - 2^8

** Question 3

   Explain how the Pigeonhole Principle can be used to show that
   among any 11 integers, at least two must have the same last digit.

   My answer:

   #+begin_quote
   The domain of the digits is of size 10 with the elements being 0,1,2,3,4,5,6,7,8,9.

   By the Pigeonhole Principle, there are only 10 digits available, but here we are given 11 integers which is more than the number of digits we have. This means that there must be at least two integers who share the same digit.
   #+end_quote

** FAIL Question 4

   Using the English alphabet and allowing repeated letters, find the
   number of words of length 8 that begin and end with the same letter.

   - +8*(26^6)+ ❌

   - 26*26^6 ✓

   - 26^7 ✓

* DONE Quiz 6.3, 6.4
  CLOSED: [2020-03-27 Fri 11:38]

** Question 1

   Nine people (Ann, Ben, Cal, Dot, Ed, Fran, Gail, Hal, and Ida) are in a room.

   In how many ways can this be done if Hal or Ida (but not both) are in the picture?

   - [X] 2*5*P(7,4)

** Question 2

   How many ways are there to select 6 students from a class of 25 to serve on a committee?

   - [X] C(25,6)

** Question 3

   Find the coefficient of \(x^9\) in the expansion of \((2 + x^3)^{10}\)

   - [X] C(10,3)*2^7

* DONE Quiz 9.1, 9.3, & 9.4
  CLOSED: [2020-04-05 Sun 16:51]

** FAIL 1.

Determine whether the binary relation given  is:  (1) reflexive, (2) symmetric, (3) antisymmetric, (4) transitive.
The relation R on {w,x,y,z} where R = {(w,w),(w,x),(x,w),(x,x),(x,z),(y,y),(z,y),(z,z)}.                 {R9.11}
Answer Y for "yes" meaning that the relation has the given property of N for "no" the relation does not have the given property:

1. reflexive: Y
2. symmetric: N
3. antisymmetric: +Y+ ❌ (N ✓)
4. transitive: N

** 2.

Suppose R and S are relations on {a, b, c, d}, where R = {(a,b),(a,d),(b,c),(c,c),(d,a)} and S = {(a,c),(b,d),(d,a)}.  Find the composition \(R \circ S\) of the relations.      {R9.30}

- [X] {(a,c),(b,a),(d,b),(d,d)}

** 3.

Given the matrix of a relation, find the matrix of the transitive closure of the relation:

\[\begin{bmatrix}
1&0&0\\
0&1&1\\
1&0&1
\end{bmatrix}\]

a->a
b->b
b->c
c->a
c->c
*b->a*

\[\begin{bmatrix}
1&0&0\\
1&1&1\\
1&0&1
\end{bmatrix}\]

* DONE Quiz 11.1, 11.2, 11.3
  CLOSED: [2020-04-19 Sun 21:13]

** Question 1

   A tree must be a simple graph with a cycle but cannot be connected.

   - [ ] True

   - [X] False

** Question 2

   If \(T\) is a rooted binary tree of height 5,
   then \(T\) has at most 25 leaves.

   - [ ] True

   - [X] False

** Question 3

   If \(T\) is a tree with 17 vertices, then there is
   a simple path in \(T\) of length 17.

   - [ ] True

   - [X] False

     If \(T\) is a tree with 17 vertices, then it has
     16 edges, so the maximum path length is 16.

** Question 4

   Every tree is bipartite.

   - [X] True

   - [ ] False

** FAIL Question 5

   Consider the binary search tree that would be
   generated if the words SHE, SELLS, SEA, SHELLS,
   BY, THE, SEA, SHORE are inserted into the tree
   in the given order (draw the tree to answer the
   following questions):
   
   a) How many comparisons with words in the tree
   are needed to determine if the word SHARK is in
   the tree?

   - +4+ ❌ (2 ✓)
     (For some reason, I thought the second letter in "SHARK" was "A".)

   b) How many comparisons with words in the tree
   are needed to determine if the word SEAWEED is
   in the tree?

   - 4

   \[\tikz \graph [binary tree layout] {
     SHE -- {
       SELLS -- {
         SEA -- {
           BY,
           / [as=SEA] -- SEAWEED [second,draw]
         },
         SHARK [draw]
       },
       SHELLS -- THE [second] -- SHORE
     }
   };\]
