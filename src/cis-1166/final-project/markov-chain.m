%% Markov Chain Simulation
%
% Author:  Eric Nguyen
% Date:    2020-04-24
% Section: 007
%
% Written in MATLAB 2019b.
% You can run this code using the MATLAB desktop
% application or you can visit https://matlab.mathworks.com/
% for an online version of MATLAB.
%
% Alternatively, you can use the GPL licensed
% GNU/Octave, available at https://www.gnu.org/software/octave/
%

%% Program Description
%
% Simulate a Markov chain given:
% 1. A matrix of transition probabilities (of size N*N)
% 2. A initial distribution (of size N)
%
% which outputs the sequence X_0, X_1, ...
%
% Note:
% User input for the matrix preferably should
% be in the format "[a b c; d e f; g h i]"
%               or "[a,b,c; d,e,f; g,h,i]"
% for a matrix
%                         [a b c
%                          d e f
%                          g h i]
%
% where columns are delimited by spaces and
% rows are delimited by semicolons.
%
% The input format for vectors is just a
% one-dimensional matrix.
%

%% Simulation of Markov chain
% Take user input
T = input('Enter the matrix of transition probabilities: ');
Q = input('Enter the initial distribution: ');
N = input('Enter the number of steps to take: ');

% We will just have the state space to be
% 1 to the length of Q since it isn't specified
S = 1:length(Q);

% Preallocate the sequence X
X = zeros(1,N);

% For each step k, select a random state for X_k
for k=1:N
    % Choose a random decimal number between 0 and 1
    r = rand();
    
    % Cut 1 into n states of size qi
    % i.e., ..[q0]..[q1]..[qn]..[1]
    qs = cumsum(Q);
    
    % Whereever R lands, get the i of the qi that follows it
    % e.g., ..[q0]..[r][q1]..[qn]..[1] = q1
    I = find(r <= qs, 1, 'first');
    
    % Set the state to S_I
    X(k) = S(I);
    
    % Multiply the probability distribution by
    % the matrix of transition probabilities
    Q = Q * T;
end

% Finally, output the sample path of the Markov chain
disp(X);
