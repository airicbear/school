Welcome, I'm Eric Nguyen.
This website is for my school related material.
I'm currently under CST, majoring in Data Science ([[https://bulletin.temple.edu/undergraduate/science-technology/computer-information-science/data-science-computation-modeling-bs/][Link to Data Science Bulletin]]).
I went with the MATH 1022 (Precalculus) route ([[https://cst.temple.edu/students/advising-sheets-and-flowcharts/year-2019-2020/data-science-computation-modeling][Link to Data Science course flowchart]])
since I suck at math (and school in general).
I'm mostly just making this website for myself, but if you happen to find it useful,
feel free to take a look!

If you need help with any of these courses or just want someone to study with,
feel free to reach out to me!
Here are links to [[https://keybase.io/airicbear][my Keybase]] and [[https://twitter.com/airicbear][my Twitter]].
My Discord tag is =drenching_rain#1423=.
If you prefer to use more obscure mediums of communication
such as IRC, Matrix, Mastodon, etc., I'm open to that!
Just tell me in class or message me on Keybase first and I would not mind
trying out whatever communication software you prefer, as long as it seems
trustworthy to me.
